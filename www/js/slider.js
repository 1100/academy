$(document).ready(function($){
    initSlider();
});

function initSlider() {
    initSliderCycle();
    $('.slide-content').hover(function () {
        clearInterval(window.slider_cycle_interval);
        $('.slide-content.active').removeClass('active');
        $('.slide-image.active').removeClass('active');

        $(this).addClass('active');
        $('.slide-image').eq($(this).index()).addClass('active');
    }, function () {
        initSliderCycle();
    });
}

function initSliderCycle() {
    clearInterval(window.slider_cycle_interval);
    window.slider_cycle_interval = setInterval( function() {
        let active_slide_index = ($('.slide-content.active').index() + 1) % $('.slide-content').length;
        $('.slide-content.active').removeClass('active');
        $('.slide-image.active').removeClass('active');
        $('.slide-content').eq(active_slide_index).addClass('active');
        $('.slide-image').eq(active_slide_index).addClass('active');
    }, 3000);
}