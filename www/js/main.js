// remove '#_=_' hash after facebook
if (window.location.hash && window.location.hash === '#_=_') {
    if (window.history && history.pushState) {
        window.history.pushState("", document.title, window.location.pathname);
    } else {
        // prevent scrolling by storing the page's current scroll offset
        var scroll = {
            top: document.body.scrollTop,
            left: document.body.scrollLeft
        };
        window.location.hash = '';
        // restore the scroll offset, should be flicker free
        document.body.scrollTop = scroll.top;
        document.body.scrollLeft = scroll.left;
    }
}

$(function($) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var phone_input = $('[data-type="phone"]');
    phone_input.attr('placeholder', '+3_ (___) ___-__-__');
    phone_input.mask('+39 (999) 999-99-99');

    if($("#map").length) {
        var coordinate = new google.maps.LatLng(50.4897184, 30.4751307);
        var mapOptions = {
            zoom: 17,
            center: coordinate,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            streetViewControl: false,
            mapTypeControl: false
        };

        map = new google.maps.Map(document.getElementById("map"),mapOptions);
        new google.maps.Marker({
            position: new google.maps.LatLng(50.4897184, 30.4751307),
            map: map
        });
    }
    $(window).resize(function(){
        if(map!=null) {
            map.setCenter(coordinate);
        }
    });

    $("[data-hide]").on("click", function(){
        $("#" + $(this).attr("data-hide")).slideUp();
    });

    $('[data-href]').click(function (e) {
        window.location.href = this.dataset.href;
    });

    jQuery("a.popup-video").YouTubePopUp();
});

/* TESTS */

window.timer_interval = 0;
window.timeLeft = null;

function startTest(testId) {
    var questionsLeft = document.getElementById('questionsLeft');
    var testBody = document.getElementById('testBody');
    var testBegin = document.getElementById('testBegin');
    var videoWrapper = document.getElementById('videoWrapper');
    $.ajax({
        type: 'GET',
        url: '/cabinet/test/' + testId + '/start/',
        success: function (data) {
            document.testId = testId;
            testBody.innerHTML = data.question_form;
            questionsLeft.innerHTML = 'Осталось вопросов: ' + data.questions_left;
            protectForm();
            setSecondsLeft(data.time_left);
            window.timer_interval = intervalTrigger();

            testBegin.style.display = 'none';
            testBody.style.display = 'block';
            videoWrapper.style.display = 'none';
        }
    });
}

function answerOrNext() {
    var is_next = Boolean(document.forms.answer_form.elements.submit.value === 'next_question');
    if (is_next) {
        getNextQuestion();
    } else {
        checkAnswer();
    }
    return false;
}

function checkAnswer() {
    var answer_form = document.forms.answer_form;
    var formData = new FormData(answer_form);
    var question_id = formData.get('question_id');
    var answer_id = formData.get('answer_id');

    $.ajax({
        type: 'GET',
        url: '/cabinet/test/' + document.testId + '/question/' + question_id + '/answer/' + answer_id,
        success: function (data) {
            if (answer_id == data.correct_answer_id) {
                document.getElementById('answer_label_' + answer_id).classList.add('correctAnswer');
            } else {
                document.getElementById('answer_label_' + answer_id).classList.add('wrongAnswer');
            }
            questionsLeft.innerHTML = 'Осталось вопросов: ' + data.questions_left;
            document.forms.answer_form.elements.submit.value = 'next_question';
            document.forms.answer_form.elements.submit.innerHTML = (data.questions_left > 0) ? "Следующий вопрос" : "Завершить";
            var answers = document.querySelectorAll('[name="answer_id"]');
            for (var i = 0; i < answers.length; i++) {
                answers[i].disabled = true;
            }
            document.forms.answer_form.onSubmit = getNextQuestion;
        }
    });
    return false;
}

function getNextQuestion() {
    var questionsLeft = document.getElementById('questionsLeft');
    var timeLeft = document.getElementById('timeLeft');
    var testBody = document.getElementById('testBody');
    var oneTestHeader = document.getElementById('oneTestHeader');

    $.ajax({
        type: 'GET',
        url: '/cabinet/test/' + document.testId + '/question/',
        success: function (data) {
            if(data.end_test) {
                location.reload();
            } else {
                testBody.innerHTML = data.question_form;
                protectForm();
                if (data.time_left === 0) {
                    window.clearInterval(window.timer_interval);
                } else {
                    setSecondsLeft(data.time_left);
                }
            }
        }
    });

    return false;
}

function intervalTrigger() {
    return window.setInterval( function() {
        var seconds_left = getSecondsLeft();
        if (window.timeLeft === null) {
            window.timeLeft = document.getElementById('timeLeft');
        }
        window.timeLeft.innerHTML = secondsToHHMMSS(seconds_left);
        setSecondsLeft(seconds_left-1);
        if (seconds_left <= 0 ) {
            checkAnswer();
            getNextQuestion();
            window.clearInterval(window.timer_interval);
        }
    }, 1000);
}

function setSecondsLeft(seconds_left)
{
    localStorage.setItem('seconds_left', seconds_left);
}

function getSecondsLeft()
{
    return localStorage.getItem('seconds_left');
}

function secondsToHHMMSS(seconds) {
    var hours = (Math.floor(seconds / 3600));
    var minutes = Math.floor((seconds - (hours * 3600)) / 60);
    seconds = seconds - (hours * 3600) - (minutes * 60);
    if (hours < 10) {
        if (hours == 0) {
            hours = "";
        } else {
            hours = "0" + hours + ":";
        }
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    return hours + minutes + ':' + seconds;
}

function protectForm() {
    var protected_block = $('#protected_block');

    protected_block.show();

    $(window).blur(function () {
        protected_block.hide();
    });
    $(window).focus(function () {
        protected_block.show();
    });

    $(window).keydown(function (e) {
        if (e.keyCode == 42 || e.keyCode == 44) {
            protected_block.hide();
        }
    });

    $(window).keyup(function (e) {
        if (e.keyCode == 42 || e.keyCode == 44) {
            protected_block.show();
        }
    });

    $(window).on('copy', function (e) {
        var clipboard = e.clipboardData;
        if (clipboard) {
            clipboard.setData('Text', '');
        }
        return false;
    });
}