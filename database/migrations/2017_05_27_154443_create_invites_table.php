<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('Пригласить друга');
            $table->integer('points')->unsigned()->default(0);
            $table->integer('invites_count')->unsigned()->default(0);
            $table->string('faculty');

            $table->foreign('faculty')->references('slug')->on('faculties')->onDelete('cascade');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('invite_key')->nullable(); //TODO: set not nullable
            $table->string('invited_key')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('invite_key');
            $table->dropColumn('invited_key');
        });

        Schema::dropIfExists('invites');
    }
}
