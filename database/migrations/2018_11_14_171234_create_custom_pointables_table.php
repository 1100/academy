<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomPointablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_pointables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('points')->unsigned()->default(0);
            $table->string('faculty');

            $table->foreign('faculty')->references('slug')->on('faculties')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_pointables');
    }
}
