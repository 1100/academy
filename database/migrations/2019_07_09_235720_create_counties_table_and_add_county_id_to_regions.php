<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountiesTableAndAddCountyIdToRegions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        DB::table('countries')->insert([
            ['name' => 'Украина'],
            ['name' => 'Республика Беларусь'],
        ]);

        Schema::table('regions', function (Blueprint $table) {
            $table->integer('country_id')->unsigned()->default('1');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
        });

        DB::table('regions')->insert([
            [
                'name' => 'Брестская',
                'country_id' => 2,
            ],
            [
                'name' => 'Витебская',
                'country_id' => 2,
            ],
            [
                'name' => 'Гомельская',
                'country_id' => 2,
            ],
            [
                'name' => 'Гродненская',
                'country_id' => 2,
            ],
            [
                'name' => 'Минская',
                'country_id' => 2,
            ],
            [
                'name' => 'Могилёвская',
                'country_id' => 2,
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('regions', function(Blueprint $table) {
            $table->dropForeign(['country_id']);
            $table->dropColumn('country_id');
        });
        Schema::dropIfExists('countries');
    }
}
