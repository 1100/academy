<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateRolesAndPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_permissions', function (Blueprint $table) {
            $table->string('slug')->primary();
        });

        Schema::create('admin_roles', function (Blueprint $table) {
            $table->string('slug')->primary();
            $table->string('name');
        });
        DB::table('admin_roles')->insert([
            [
                'slug' => 'admin',
                'name' => 'Администратор',
            ],
            [
                'slug' => 'manager',
                'name' => 'Менеджер',
            ],
            [
                'slug' => 'curator',
                'name' => 'Куратор',
            ],
        ]);

        Schema::create('admin_role_admin_permission', function (Blueprint $table) {
            $table->string('admin_role');
            $table->string('admin_permission');
            $table->unique(['admin_role', 'admin_permission']);

            $table->foreign('admin_role')->references('slug')->on('admin_roles')->onDelete('cascade');
            $table->foreign('admin_permission')->references('slug')->on('admin_permissions')->onDelete('cascade');
        });

        Schema::create('admin_admin_role', function (Blueprint $table) {
            $table->integer('admin_id')->unsigned();
            $table->string('admin_role');
            $table->unique(['admin_id', 'admin_role']);

            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade');
            $table->foreign('admin_role')->references('slug')->on('admin_roles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_admin_role');
        Schema::dropIfExists('admin_role_admin_permission');
        Schema::dropIfExists('admin_permissions');
        Schema::dropIfExists('admin_roles');
    }
}
