<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        DB::table('regions')->insert([
            ['name' => 'Винницкая'],
            ['name' => 'Волынская'],
            ['name' => 'Днепропетровская'],
            ['name' => 'Донецкая'],
            ['name' => 'Житомирская'],
            ['name' => 'Закарпатская'],
            ['name' => 'Запорожская'],
            ['name' => 'Ивано-Франковская'],
            ['name' => 'Киевская'],
            ['name' => 'Кировоградская'],
            ['name' => 'Луганская'],
            ['name' => 'Львовская'],
            ['name' => 'Николаевская'],
            ['name' => 'Одесская'],
            ['name' => 'Полтавская'],
            ['name' => 'Ровенская'],
            ['name' => 'Сумская'],
            ['name' => 'Тернопольская'],
            ['name' => 'Харьковская'],
            ['name' => 'Херсонская'],
            ['name' => 'Хмельницкая'],
            ['name' => 'Черкасская'],
            ['name' => 'Черниговская'],
            ['name' => 'Черновицкая'],
            ['name' => 'АР Крым'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regions');
    }
}
