<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('levels', function (Blueprint $table) {
            $table->string('slug')->primary();
            $table->string('name');
            $table->unsignedTinyInteger('number');
        });

        DB::table('levels')->insert([
            [
                'slug' => 'start',
                'name' => 'Start',
                'number' => 1,
            ],
            [
                'slug' => 'basic',
                'name' => 'Basic',
                'number' => 2,
            ],
            [
                'slug' => 'advanced',
                'name' => 'Advanced',
                'number' => 3,
            ],
        ]);

        Schema::table('users', function(Blueprint $table) {
            $table->string('level')->default('guest')->after('activation_status');
            $table->foreign('level')->references('slug')->on('levels');
        });

        Schema::table('tests', function(Blueprint $table) {
            $table->string('level')->default('basic')->after('faculty');
            $table->foreign('level')->references('slug')->on('levels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dropForeign(['level']);
            $table->dropColumn('level');
        });

        Schema::table('tests', function(Blueprint $table) {
            $table->dropForeign(['level']);
            $table->dropColumn('level');
        });

        Schema::dropIfExists('levels');
    }
}
