<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCombinedUniqueKeyToAnswersSessions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('answers_sessions', function (Blueprint $table) {
            $table->index(['question_session_id', 'answer_id'], 'answer_sessions_combined_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('answers_sessions', function (Blueprint $table) {
            $table->dropIndex('answer_sessions_combined_unique');
        });
    }
}
