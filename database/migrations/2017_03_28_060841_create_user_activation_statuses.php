<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserActivationStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_activation_statuses', function (Blueprint $table) {
            $table->string('slug')->primary();
            $table->string('name');
        });

        DB::table('user_activation_statuses')->insert([
            [
                'slug' => 'registered',
                'name' => 'Зарегистрирован'
            ],
            [
                'slug' => 'activated',
                'name' => 'Активирован'
            ]
        ]);

        Schema::table('users', function(Blueprint $table) {
            $table->string('activation_status')->default('registered');
            $table->foreign('activation_status')->references('slug')->on('user_activation_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dropForeign(['activation_status']);
            $table->dropColumn('activation_status');
        });
        Schema::dropIfExists('user_activation_statuses');
    }
}
