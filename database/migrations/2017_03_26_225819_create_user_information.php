<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateUserInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information_types', function (Blueprint $table) {
            $table->string('slug')->primary();
            $table->string('name')->unique();
            $table->string('validator');
        });

        Schema::create('information_fields', function (Blueprint $table) {
            $table->string('slug')->primary();
            $table->string('name')->unique();
            $table->string('description')->nullable();
            $table->boolean('login')->default(false);
            $table->boolean('hidden')->default(false);
            $table->string('default_value')->nullable();
            $table->string('type');

            $table->foreign('type')->references('slug')->on('information_types');
        });

        Schema::create('user_information', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('information_field');
            $table->string('value')->nullable();

            $table->unique(['user_id', 'information_field']);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('information_field')->references('slug')->on('information_fields')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_information');
        Schema::dropIfExists('information_fields');
        Schema::dropIfExists('information_types');
    }
}
