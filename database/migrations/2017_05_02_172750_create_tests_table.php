<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('faculty');
            $table->integer('question_count')->unsigned()->default(10);
            $table->integer('points')->unsigned()->default(0);
            $table->timestamp('start_date')->useCurrent();
            $table->timestamp('end_date')->nullable();
            $table->integer('duration')->unsigned()->default(10);

            $table->foreign('faculty')->references('slug')->on('faculties')->onDelete('cascade');
        });

        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('test_id')->unsigned();

            $table->foreign('test_id')->references('id')->on('tests')->onDelete('cascade');
        });

        Schema::create('answers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('question_id')->unsigned();
            $table->boolean('is_correct')->default(false);

            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
        });

        Schema::create('tests_sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('test_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->dateTime('start_date');
            $table->dateTime('end_date');

            $table->foreign('test_id')->references('id')->on('tests')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('questions_sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_id')->unsigned();
            $table->integer('test_session_id')->unsigned();

            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
            $table->foreign('test_session_id')->references('id')->on('tests_sessions')->onDelete('cascade');
        });

        Schema::create('answers_sessions', function (Blueprint $table) {
            $table->integer('question_session_id')->unsigned();
            $table->integer('answer_id')->unsigned();

            $table->foreign('question_session_id')->references('id')->on('questions_sessions')->onDelete('cascade');
            $table->foreign('answer_id')->references('id')->on('answers')->onDelete('cascade');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->integer('rating')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('rating');
        });

        Schema::dropIfExists('answers_sessions');
        Schema::dropIfExists('questions_sessions');
        Schema::dropIfExists('tests_sessions');
        Schema::dropIfExists('answers');
        Schema::dropIfExists('questions');
        Schema::dropIfExists('tests');
    }
}
