<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('points', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('pointable_id')->unsigned();
            $table->string('pointable_type');
            $table->float('percent');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('tests_sessions', function (Blueprint $table) {
            $table->integer('points_id')->unsigned();

            $table->foreign('points_id')->references('id')->on('points')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tests_sessions', function(Blueprint $table) {
            $table->dropForeign('tests_sessions_points_id_foreign');
            $table->dropColumn('points_id');
        });

        Schema::dropIfExists('points');
    }
}
