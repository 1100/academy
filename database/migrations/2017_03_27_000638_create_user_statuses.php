<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_statuses', function (Blueprint $table) {
            $table->string('slug')->primary();
            $table->string('name');
        });

        DB::table('user_statuses')->insert([
            [
                'slug' => 'not_verified',
                'name' => 'Не проверен'
            ],
            [
                'slug' => 'approved',
                'name' => 'Одобрен'
            ],
            [
                'slug' => 'rejected',
                'name' => 'Отклонен'
            ],
        ]);

        Schema::table('users', function(Blueprint $table) {
            $table->string('status')->default('not_verified');
            $table->foreign('status')->references('slug')->on('user_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dropForeign(['status']);
            $table->dropColumn('status');
        });
        Schema::dropIfExists('user_statuses');
    }
}
