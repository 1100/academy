<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DesignFacultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faculties')->insert([
            [
                'slug' => 'design',
                'name' => 'Дизайнеры и архитекторы',
                'description' => 'Программа мотивации и ценные призы для дизайнеров',
                'color' => 'green',
                'open_rating' => true,
                'open_registration' => false,
            ]
        ]);

        $this->call(InformationFieldsForDesignFacultySeeder::class);
    }
}
