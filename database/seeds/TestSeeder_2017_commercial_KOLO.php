<?php

use Illuminate\Database\Seeder;
use App\Test;

class TestSeeder_2017_commercial_KOLO extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $test_array = [
            'name' => 'Новинки KOLO 2017',
            'faculty' => 'commercial',
            'points' => 100,
            'question_count' => 10,
            'duration' => 10,
            'start_date' => '2017-08-01 00:00:00',
            'end_date' => '2017-08-30 00:00:00',
            'questions' => [
                [
                    'name' => 'На сегодняшний день в предложении KOLO представлены 12 унитазов без ободка. Какая модель унитаза Rimfree появилась в 2017 году? ',
                    'answers' => [
                        [
                            'name' => 'Life!',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Rekord',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Coctail',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ego',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой размер самого маленького умывальника серии Life!?',
                    'answers' => [
                        [
                            'name' => '40 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '35 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '45 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '43 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой размер самого большого умывальника серии Life!?',
                    'answers' => [
                        [
                            'name' => '130 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '125 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '120 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '100 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой размер асимметричного умывальника Life!?',
                    'answers' => [
                        [
                            'name' => '70 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '80 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '65 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '100 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая особенность умывальника Life! 40 см?',
                    'answers' => [
                        [
                            'name' => 'Левая/правая полочка с отверстием для смесителя',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Круглая форма',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Монтируется на пьедестал',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Встраиваемый под столешницу',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие умывальники Life! изготавливаются с двумя отверстиями под смеситель?',
                    'answers' => [
                        [
                            'name' => '100 и 130 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '90 и 110 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '90 и 120 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '100 и 120 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите варианты монтажа умывальников Life! 60 и 80 см.',
                    'answers' => [
                        [
                            'name' => 'На полупьедестал, шкафчик или столешницу при монтаже в стену',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Только на полупьедестал',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только на шкафчик',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только на столешницу',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие унитазы представлены в серии Life?',
                    'answers' => [
                        [
                            'name' => 'Подвесной с ободком и подвесной Rimfree',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Напольный, подвесной с ободком и подвесной Rimfree',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Напольный с ободком, напольный Rimfree, подвесной с ободком и подвесной Rimfree',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Приставной к стене Rimfree и подвесной Rimfree',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какими сиденьями комплектуются унитазы серии Life!?',
                    'answers' => [
                        [
                            'name' => 'Дюропластовое сиденье с металлическими петлями и дюропластовое сиденье soft-close',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Дюропластовое сиденье soft-close',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дюропластовое сиденье soft-close Click2Clean',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сиденье из АВС-пластика и дюропластовое сиденье soft-close',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите цвета мебели серии Life!',
                    'answers' => [
                        [
                            'name' => 'Белый глянец и серый глянец',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Белый матовый и черный матовый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белый глянец и светлый дуб',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белый глянец, серебряный графит, черный матовый',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Для какого умывальника изготавливается шкафчик с дверцей?',
                    'answers' => [
                        [
                            'name' => '40 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '60 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '70 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '80 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите дополнительное преимущество для хранения в шкафчиках Life!?',
                    'answers' => [
                        [
                            'name' => 'Органайзер с антискользящим ковриком для хранения косметики внутри ящика',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Встроенное зеркало',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Фен в комплекте',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Откидывающееся сиденье',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите особенность высокого подвесного шкафчика Life!',
                    'answers' => [
                        [
                            'name' => 'Зеркало на внутренней стороне двери',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Розетка с USB-портом',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Чехол для хранения косметики на внутренней стороне двери',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Внутренняя подсветка',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите цвет боковых сторон зеркал серии Life!',
                    'answers' => [
                        [
                            'name' => 'Алюминий сатин',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Хром глянец',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Хром матовый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белый глянец',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'К какой группе ванн относятся ванны Split?',
                    'answers' => [
                        [
                            'name' => 'Асимметричные ванны',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Прямоугольные ванны',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Угловые ванны',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Овальные ванны',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите размеры ванн Split',
                    'answers' => [
                        [
                            'name' => '150х80 см, 160х90 см, 170х90 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '150х70 см, 160х80 см, 170х80 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '150х70 см, 160х70 см, 170х70 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '160х75 см, 170х75см, 180х75 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая глубина ванн Split?',
                    'answers' => [
                        [
                            'name' => '42 cм',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '44 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '39 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '40 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какими ножками комплектуются ванны Split?',
                    'answers' => [
                        [
                            'name' => 'SN0',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Не комплектуется',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'SN7',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'SN8',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите конструктивную особенность ванн Split',
                    'answers' => [
                        [
                            'name' => 'Центральный слив',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Подлокотники',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Встроенный подголовник',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Усиленные борта',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Из скольких секций состоят стеклянные ширмы Split?',
                    'answers' => [
                        [
                            'name' => '3 секции',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '1 секция',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '2 секции',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '4 секции',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие прямоугольные ванны могут комплектоваться ширмами Split?',
                    'answers' => [
                        [
                            'name' => 'Все модели прямоугольных ванн KOLO',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Только Modo, Comfort Plus',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только Clarissa, Comfort',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только Saga, Sensa',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите главную особенность подвесного унитаза Modo',
                    'answers' => [
                        [
                            'name' => 'Чаша без ободка Rimfree',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Удлиненная чаша 70 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Укороченная чаша 51 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Чаша круглой формы',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите монтажный элемент Geberit в акционном предложении для подвесного унитаза Modo.',
                    'answers' => [
                        [
                            'name' => 'Монтажный элемент Geberit Sigma и Duofix Delta',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Только монтажный элемент Geberit Sigma ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только монтажный элемент Geberit Omega',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только монтажный элемент Geberit Duofix Delta',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите опции сидений, которые используются для комплектации подвесных унитазов Modo.',
                    'answers' => [
                        [
                            'name' => 'Дюропластовое сиденье и супер тонкое сиденье Slim',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Цветные дюропластовые сиденья на выбор покупателя',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сиденья из дюропласта и АВС-пластика',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сиденья из МДФ и натурального дерева',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите главные преимущества унитазов без ободка Rimfree.',
                    'answers' => [
                        [
                            'name' => 'Простой уход, экономичность, охрана окружающей среды, привлекательный внешний вид',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Функциональность, экономичность, ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Удобный, красивый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Компактный, экономный',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что означает простой уход для унитаза без ободка?',
                    'answers' => [
                        [
                            'name' => 'Просто протереть салфеткой ',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Ополоснуть водой',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Очистить чистящим средством',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Побрызгать освежителем для туалетов',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что означает экономичность для унитаза без ободка?',
                    'answers' => [
                        [
                            'name' => 'Слив 4 и 2 литра позволяет потреблять на 60% меньше воды',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Цена ниже, чем унитаза с ободком',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сиденье с антибактериальным покрытием препятствует размножению микробов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Простой монтаж',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое преимущество унитазов без ободка положительно влияет на охрану окружающей среды?',
                    'answers' => [
                        [
                            'name' => 'При уходе за унитазом без ободка не нужно использовать агрессивную бытовую химию',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'При уходе за унитазом без ободка не нужен освежитель воздуха',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'При уходе за унитазом без ободка не нужно использовать воду',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Унитаз без ободка имеет систему самоочистки',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите керамические новинки серии TWINS',
                    'answers' => [
                        [
                            'name' => 'Мебельные умывальники с тонким бортом 50, 60 и 80 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Подвесное биде',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Встраиваемые мебельные умывальники под столешницу 50 и 60 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Подвесной унитаз Rimfree',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой формы раковина у умывальников TWINS с тонким бортом?',
                    'answers' => [
                        [
                            'name' => 'Прямоугольная',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Овальная',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Круглая ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Квадратная',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите новый размер мебельного умывальника TWINS',
                    'answers' => [
                        [
                            'name' => '80 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '70 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '90 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '120 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая высота борта у новых мебельных умывальников TWINS?',
                    'answers' => [
                        [
                            'name' => '3,5 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '5 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '1 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '6 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите мебельные новинки серии TWINS',
                    'answers' => [
                        [
                            'name' => 'Шкафчик под умывальник 80 см, зеркало 80 см, зеркальный шкафчик 80 см существующего дизайна',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Низкий боковой шкафчик',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Шкафчик с зеркалом и полочками',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Шкафчики под умывальники нового дизайна',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой высоты ножки используются для комплектации шкафчиков с умывальниками с тонким бортом?',
                    'answers' => [
                        [
                            'name' => '25 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '20 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '15 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '10 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
            ],
        ];

        Test::createFromArray($test_array);
    }
}
