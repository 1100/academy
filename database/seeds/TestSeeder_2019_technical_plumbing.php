<?php

use Illuminate\Database\Seeder;
use App\Test;

class TestSeeder_2019_technical_plumbing extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $test_array = [
            'name' => 'Сантехнические системы',
            'faculty' => 'technical',
            'points' => 300,
            'question_count' => 30,
            'duration' => 30,
            'start_date' => '2019-08-15 00:00:00',
            'end_date' => '2019-11-30 23:59:59',
            'questions' => [
                [
                    'name' => 'Какое основное отличие бачков Geberit Delta от Geberit Sigma?',
                    'answers' => [
                        [
                            'name' => 'Бюджетное решение с меньшим ассортиментом клавиш',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Больше габариты и толщина бачка ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Меньше гарантийный срок службы',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Более сложный монтаж',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая толщина бачка скрытого монтажа серии Delta UP100?',
                    'answers' => [
                        [
                            'name' => '12 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '8 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '15 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '22 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое максимальное рабочее давление впускного клапана в бачке Geberit',
                    'answers' => [
                        [
                            'name' => '10 атм.',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '6 атм.',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '3 атм.',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '0 атм.',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'С каким сантехническим прибором используют бачок скрытого монтажа?',
                    'answers' => [
                        [
                            'name' => 'Напольный унитаз',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Писсуар',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Биде',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Подвесной унитаз',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Каким давлением испытываются все бачки Geberit при производстве?',
                    'answers' => [
                        [
                            'name' => '10 атм.',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '0,1 атм.',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '1 атм.',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '25 атм.',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'С какого материала произведен бачок скрытого монтажа Geberit?',
                    'answers' => [
                        [
                            'name' => 'PE - полиэтилен',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'PP - полипропилен',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'PBT - полибутилен',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'SB - стирол-бутадиеновый сополимер',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое минимальное рабочее давление воды для бачка скрытого монтажа Geberit Sigma?',
                    'answers' => [
                        [
                            'name' => '0,1 атм.',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '0 атм.',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '1 атм.',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '3 атм.',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Из какого материала выполнена мембрана клапана слива бачка скрытого монтажа?',
                    'answers' => [
                        [
                            'name' => 'Силиконовая резина',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Резина EPDM',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Каучук',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Полиэтилен',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие возможны настройки клапана смыва в бачках Geberit?',
                    'answers' => [
                        [
                            'name' => 'Большой смыв 4.5/6/7.5',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Большой смыв 6/9',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Большой смыв 6/7.5',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Большой смыв 4/6',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой смыв применен в бачках Geberit?',
                    'answers' => [
                        [
                            'name' => 'Механический двойной смыв',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Механический смыв',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Пневматический смыв',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Комбинированный смыв',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие кнопки смыва подходят к инсталляции Duofix Sigma для подвесного унитаза ?',
                    'answers' => [
                        [
                            'name' => 'Все кнопки Sigma',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Все кнопки Sigma и Delta',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Все кнопки Sigma и Omega',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Все кнопки Sigma, Omega и Delta',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Монтажный блок Geberit Duofix для унитаза предназначен для установки с прибором:',
                    'answers' => [
                        [
                            'name' => 'Подвесной унитаз',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Напольный унитаз',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Компакт KOLO',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Чаша Генуя',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Клавиша Sigma 01 имеет две кнопки смыва. Это сделано для:',
                    'answers' => [
                        [
                            'name' => 'Двойной смыв',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Дизайна',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Надежности',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Лучше видно',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое защитное покрытие имеет рама инсталляции Geberit Duofix?',
                    'answers' => [
                        [
                            'name' => 'Порошковая краска',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Лак',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Оцинковка',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Полиэтиленовое покрытие',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая гарантия на инсталляции Geberit Duofix?',
                    'answers' => [
                        [
                            'name' => '10 лет',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '5 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '2 года',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '50 лет',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой диаметр имеет фановый отвод в комплекте с инсталляцией для унитаза Duofix?',
                    'answers' => [
                        [
                            'name' => '90 мм',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '110 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '160 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '125 мм',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как подсоединить фановый отвод 90 мм Duofix для унитаза  к стандартной канализации 110 мм?',
                    'answers' => [
                        [
                            'name' => 'Использовать адаптер из комплекта поставки',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Купить дополнительно переход 90х110',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Фановый отвод в комплекте с инсталляцией для унитаза Duofix имеет диаметр 110мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Использовать любое доступное решение',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой инструмент необходим для монтажа клавиши Sigma?',
                    'answers' => [
                        [
                            'name' => 'Для монтажа не нужны инструменты',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Монтажный набор, который идет в комплекте',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Монтажный набор, который покупается отдельно',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только шестигранник',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой принцип работы системы удаления запаха Duofresh?',
                    'answers' => [
                        [
                            'name' => 'Удаление запаха непосредственно из чаши унитаза через фильтр установленный за кнопкой смыва',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Удаление запаха непосредственно из чаши унитаза через фильтр устанновленный за стеной',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Удаление запаха принудительно через вентиляцию помещения',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Удаление запаха через вентиляцию помещения',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие варианты высоты Duofix Omega?',
                    'answers' => [
                        [
                            'name' => '82/98/112',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '82/98/114',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '98/101/114',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '101/112/114',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая клавиша имеет подсветку и управляется бесконтактно?',
                    'answers' => [
                        [
                            'name' => 'Sigma80',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Sigma70',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Sigma60',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Sigma40',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Для чего нужна белая трубка сбоку в инсталляции Duofix?',
                    'answers' => [
                        [
                            'name' => 'Для подключения AquaClean',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Для подключения гигиенического душа',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Для подключения подвода воды',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Для подвода электричества',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что значит отметка 1 м на раме Duofix?',
                    'answers' => [
                        [
                            'name' => 'Отметка высота установки в 1м до чистого пола',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Высота установки унитаза',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Допустимая регулировка высоты',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Высота установки ножек',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Где расположен фильтр в системе Duofresh?',
                    'answers' => [
                        [
                            'name' => 'В откидной клавише смыва',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'В откидной дверцей в стене ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Фильтр интегрирован в трубу подвода воздуха',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'С этой системе нет фильтра, используются освежающие кубики',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что нужно дополнительно заказать для бесконтактных клавиш смыва унитаза Sigma10, Sigma80?',
                    'answers' => [
                        [
                            'name' => 'Блок питания',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Электро привод',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Шнур соединения',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Блок для батареи',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Где можно приобрести необходимые зап.части?',
                    'answers' => [
                        [
                            'name' => 'Обратится в сервис-центр',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Обратится в точку продажи',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Обратится к сантехнику ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Отремонтировать согласно инструкции',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая высота Duofix для писсуара от чистого пола?',
                    'answers' => [
                        [
                            'name' => 'Регулируется от 112 до 130 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Максимум 112 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '112 см и регулируемые ножки 20 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '130 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Можно ли использовать писсуар с подключением воды с резьбой 1/2" и Duofix для писсуара?',
                    'answers' => [
                        [
                            'name' => 'Да, можно с дополнительным специальным переходником (покупается отдельно) или Duofix для писсуара с подключением воды с резьбой 1/2"',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Да, можно с дополнительным специальным переходником',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет, нельзя',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нужно использовать специальный монтажный элемент Duofix с подключением воды с резьбой 1/2"',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая высота стандартного водяного затвора сифонов Geberit?',
                    'answers' => [
                        [
                            'name' => '5 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '2 см ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '10 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '15 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой присоединительный диаметр к канализации имеет трап в стену  с гидрозатвором 50 мм?',
                    'answers' => [
                        [
                            'name' => '50 мм',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '32 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '63 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '75 мм',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая минимальная высота трапа CleanLine?',
                    'answers' => [
                        [
                            'name' => '65 мм',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '30 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '10 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '15 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой рекомендуемый уклон плитки при монтаже трапа в стену?',
                    'answers' => [
                        [
                            'name' => '1-2%',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '0,5%',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '3%',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '1°',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Накладка трапа CleanLine с возможностью укорачивания может быть длиной:',
                    'answers' => [
                        [
                            'name' => '30 - 160 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '20 - 90 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '30 - 150 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '20 -130 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая производительность трапа CleanLine, монтажной высоты 90 мм?',
                    'answers' => [
                        [
                            'name' => '0,8 л/с',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '50 л/мин',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '2 м^3/ч',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '10 л/мин',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая ширина монтажного элемента Duofix для душевой системы, с водоотводом в стене?',
                    'answers' => [
                        [
                            'name' => '50 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '60 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '30 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '112 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Для чего необходим водяной затвор в сифоне?',
                    'answers' => [
                        [
                            'name' => 'Для предотвращения попадания загрязненного воздуха',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Улучшения гидравлики',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сброса давления',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сбора мусора',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Монтажный блок из пенопласта в трапе CleanLine предназначен для',
                    'answers' => [
                        [
                            'name' => 'Звукоизоляции',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Большей высоты',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Улучшения производительности',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Для гидроизоляции',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая возможная толщина плитки пола при использовании CleanLine20 и CleanLine60?',
                    'answers' => [
                        [
                            'name' => '8-40 мм',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '5-26 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5-36 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '8-36 мм',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая возможная толщина плитки пола при использовании трапа в стену?',
                    'answers' => [
                        [
                            'name' => '2-26 мм',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '2-36 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5-40 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '10-40 мм',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие варианты монтажа трапа CleanLine?',
                    'answers' => [
                        [
                            'name' => 'Монтаж к стену и по середине комнаты',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Монтаж к стене',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Монтаж по средине комнаты',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Угловой монтаж',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что нужно докупить для стандартного монтажа точечного трапа в пол Geberit?',
                    'answers' => [
                        [
                            'name' => 'Ничего, все в комплекте',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Гидроизоляцию',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Монтажный набор',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Декоративную накладку',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие варианты высоты монтажа точечного трапов Geberit для душа без поддона?',
                    'answers' => [
                        [
                            'name' => 'Высота пола 65-90 и 90-220 мм',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Высота пола 65-120 и 120-200 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Высота пола 65-200 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Регулируется по высоте пола',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'С какой стороны подключается канализация для трапа в стену Geberit?',
                    'answers' => [
                        [
                            'name' => 'Слева или справа',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Только слева',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только справа',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'С любой стороны',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие существуют монтажные элементы Duofix для трапа в стену?',
                    'answers' => [
                        [
                            'name' => 'Высотой 50 и 130 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Высотой 50 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Высотой 130 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Высотой 50 и 112 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой вариант монтажа смесителей есть в ассортименте Geberit?',
                    'answers' => [
                        [
                            'name' => 'Монтаж на раковину и на стену',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Монтаж только на раковину',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Монтаж только на стену',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Универсальный монтаж',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой вариант питания бесконтактных смесителей Geberit для монтажа на раковине ',
                    'answers' => [
                        [
                            'name' => '220В, батарея 6В и от генератора',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '220В и батарея 6В',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'только 220В',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'только батарея 6В',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая гарантия на электронные системы Geberit ',
                    'answers' => [
                        [
                            'name' => '2 года',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '5 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '10 лет ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'пожизненно',
                            'is_correct' => false,
                        ],
                    ],
                ],
            ],
        ];

        Test::createFromArray($test_array);

    }
}
