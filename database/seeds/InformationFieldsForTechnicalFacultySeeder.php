<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InformationFieldsForTechnicalFacultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('faculty_information_field')->insert([
                [
                    'faculty' => 'technical',
                    'information_field' => 'phone',
                    'required' => true,
                    'display' => true,
                    'rating_display' => false,
                    'position' => '0',
                ],
                [
                    'faculty' => 'technical',
                    'information_field' => 'first_name',
                    'required' => true,
                    'display' => true,
                    'rating_display' => true,
                    'position' => '2',
                ],
                [
                    'faculty' => 'technical',
                    'information_field' => 'last_name',
                    'required' => true,
                    'display' => true,
                    'rating_display' => true,
                    'position' => '1',
                ],
                [
                    'faculty' => 'technical',
                    'information_field' => 'father_name',
                    'required' => false,
                    'display' => false,
                    'rating_display' => false,
                    'position' => '3',
                ],
                [
                    'faculty' => 'technical',
                    'information_field' => 'channel',
                    'required' => false,
                    'display' => false,
                    'rating_display' => false,
                    'position' => '10',
                ],
                [
                    'faculty' => 'technical',
                    'information_field' => 'city',
                    'required' => false,
                    'display' => false,
                    'rating_display' => false,
                    'position' => '4',
                ],
                [
                    'faculty' => 'technical',
                    'information_field' => 'city_residence',
                    'required' => false,
                    'display' => false,
                    'rating_display' => false,
                    'position' => '5',
                ],
                [
                    'faculty' => 'technical',
                    'information_field' => 'college',
                    'required' => false,
                    'display' => false,
                    'rating_display' => false,
                    'position' => '6',
                ],
                [
                    'faculty' => 'technical',
                    'information_field' => 'college_curator',
                    'required' => false,
                    'display' => false,
                    'rating_display' => false,
                    'position' => '7',
                ],
                [
                    'faculty' => 'technical',
                    'information_field' => 'education',
                    'required' => false,
                    'display' => false,
                    'rating_display' => false,
                    'position' => '8',
                ],
                [
                    'faculty' => 'technical',
                    'information_field' => 'experience',
                    'required' => false,
                    'display' => false,
                    'rating_display' => false,
                    'position' => '9',
                ],
            ]
        );
    }
}
