<?php

use Illuminate\Database\Seeder;
use App\Test;
use Carbon\Carbon;

class TestSeeder_2016_1 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $test_array = [
            'name' => 'Керамика и мебель. KOLO.',
            'faculty' => 'commercial',
            'points' => 100,
            'question_count' => 10,
            'duration' => 10,
            'start_date' => '2017-08-01 00:00:00',
            'end_date' => '2017-08-30 00:00:00',
            'questions' => [
                [
                    'name' => 'В каком году был создан завод по производству санитарной керамики в г.Kоло?',
                    'answers' => [
                        [
                            'name' => '1909',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '1956',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '1993',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '1962',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Чем отличается санитарный фарфор от санитарного фаянса?',
                    'answers' => [
                        [
                            'name' => 'Коэффициентом твердости',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Коэффициентом сопротивления',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Коэффициентом электропроводности',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Коэффициентом водопоглощения',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой срок гарантии на керамику KOLO?',
                    'answers' => [
                        [
                            'name' => '3 года',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '10 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '7 лет',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой известный дизайнер разрабатывает дизайн для ТМ KOLO?',
                    'answers' => [
                        [
                            'name' => 'Филипп Старк',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Джон Гальяно',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Жан-Франко Ферре',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Антонио Читтерио',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Как называется система быстрого снятия сидений для унитазов  KOLO?',
                    'answers' => [
                        [
                            'name' => 'Quick Release',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Quick Click',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Push2Clean',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Click2Clean',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Сколько унитазов Rimfree в ассортименте KOLO?',
                    'answers' => [
                        [
                            'name' => '7',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '9',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '15',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '12',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Как называется специальное покрытие для керамики и стекла KOLO?',
                    'answers' => [
                        [
                            'name' => 'CleanOn',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Easy Clean',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'KeraTect',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Reflex',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Назовите цвета ручек для мебели MODO?',
                    'answers' => [
                        [
                            'name' => 'Белый, хром, черный',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дуб, хром, белый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Хром, платина, серебро',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Хром, венге, дуб',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Как называется устройство, которе обспечивает равномерную подачу воды в унитазах Rimfree?',
                    'answers' => [
                        [
                            'name' => 'Насос равномерной подачи воды',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Водонакопитель',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Фильтр равномерной подачи воды',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Водораспределитель',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Что в дословном переводе означает Rimfree?',
                    'answers' => [
                        [
                            'name' => 'Без бактерий',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Свободный слив',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Легкое очищение',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Без ободка',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Как называется линейка продуктов KOLO для людей с ограниченными физическими возможностями?',
                    'answers' => [
                        [
                            'name' => 'Easy way',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Simple life',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'LifeWell',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Lehnen ',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Как называется самый популярный писсуар ТМ KOLO?',
                    'answers' => [
                        [
                            'name' => 'Снайпер',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Майкл',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Оникс',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Феликс',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какая серия KOLO имеет наибольший ассортимент?',
                    'answers' => [
                        [
                            'name' => 'Ego',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Coctail',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Idol',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Nova Pro',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какой срок гарантии на мебель KOLO?',
                    'answers' => [
                        [
                            'name' => '3 года',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'пожизненная',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '2 года',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Сколько линеек продуктов насчитывает серия Nova Pro?',
                    'answers' => [
                        [
                            'name' => '2 линейки продуктов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '3 линейки продуктов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '7 линеек продуктов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 линеек продуктов',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В какой серии санитарной керамики есть бачки округлой формы и прямоугольные?',
                    'answers' => [
                        [
                            'name' => 'Freja',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Traffic',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ego',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'NOVA Pro',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В какой серии санитарной керамики есть подвесные унитазы с круглой и прямоугольной формой чаши?',
                    'answers' => [
                        [
                            'name' => 'EGO',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'MODO',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'TWIX',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'NOVA Pro',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Что придает дюропластовому сиденью антибактериальные свойства?',
                    'answers' => [
                        [
                            'name' => 'Интегрированное хлорирование ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Обработка средствами на спиртовой основе',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Покрытие Reflex',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ионы серебра',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какая из серий керамики KOLO доступна с покрытием Reflex?',
                    'answers' => [
                        [
                            'name' => 'Nova Pro',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Runa',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Freja',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Traffic',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В какой серии керамики KOLO есть унитазы с косым выпуском?',
                    'answers' => [
                        [
                            'name' => 'Style',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Traffic',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Nova Pro',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Modo',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какой цвет  не используется для изготовления мебели KOLO?',
                    'answers' => [
                        [
                            'name' => 'Белый глянец',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Черный матовый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Серый ясень',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Канадский клен',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Арматура от какого производителя установлена в бачках Freja?',
                    'answers' => [
                        [
                            'name' => 'Siamp',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Oliveira',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'AlcaPlast',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Geberit',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Что такое система Push-to-Open ? ',
                    'answers' => [
                        [
                            'name' => 'Система плавного опускания сиденья для унитаза',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Система быстрого монтажа',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Система плавного открытия сиденья для унитаза',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Система плавного открытия дверей в мебели',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В каком напольном унитазе KOLO есть скрытые крепления?',
                    'answers' => [
                        [
                            'name' => 'Idol',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Modo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Nova Pro',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Style',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Как называется детская серия керамики KOLO?',
                    'answers' => [
                        [
                            'name' => 'Бемби',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Baby',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Nova Pro Pico',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Nova Pro Junior',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В какой серии KOLO есть двойные умывальники?',
                    'answers' => [
                        [
                            'name' => 'Ego',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Twins',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Style',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Traffic',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какие унитазы комплектуются сиденьями Slim?',
                    'answers' => [
                        [
                            'name' => 'Traffic',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ego',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Idol',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Modo',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Назовите новинки 2016 года в серии Nova Pro.',
                    'answers' => [
                        [
                            'name' => 'Подвесной унитаз Rimfree прямоугольный',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Мойка керамическая 70 см, угловые умывальники 35 и 50 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Умывальник 45 см прямоугольный, биде подвесные овальное и прямоугольное',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Угловые умывальники 35 и 50 см, биде подвесные овальное и прямоугольное',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В каких цветах изготавливается сиденье для детских унитазов KOLO?',
                    'answers' => [
                        [
                            'name' => 'Белый и голубой',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Голубой и розовый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белый и черный',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белый и красный',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В какой серии есть напольный приставной унитаз?',
                    'answers' => [
                        [
                            'name' => 'Style',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Modo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Twins',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ego',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какая гарантия на петли для мебельных шкафчиков KOLO?',
                    'answers' => [
                        [
                            'name' => '2 года',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '10 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Пожизненная',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Назовите размеры мебельных умывальников Runa.',
                    'answers' => [
                        [
                            'name' => '45,55,65',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '50,55,60',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '50,60,65',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '50,60,70',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Назовите размеры мебельных умывальников Freja.',
                    'answers' => [
                        [
                            'name' => '40,50,60',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '50,60,70',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '45,55,70',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '45,55,65',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В какой серии есть боковой высокий угловой шкафчик?',
                    'answers' => [
                        [
                            'name' => 'Traffic',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Nova Pro',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Rekord',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Modo',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В каких сериях есть напольные унитазы Rimfree?',
                    'answers' => [
                        [
                            'name' => 'Ego, Modo, Coctail',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Twins, Rekord, Freja',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Supero, Solo, Runa',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Traffic, Style, Nova Pro',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В какой серии  санитарной керамики KOLO нет унитазов и биде?',
                    'answers' => [
                        [
                            'name' => 'Ego',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Traffic',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Solo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Twins',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В какой серии есть умывальники, встраиваемые на столешницу?',
                    'answers' => [
                        [
                            'name' => 'Traffic и Style',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Twins и Modo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Nova Pro и Rekord',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ego и Cocktail',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Сколько лет гарантии на доступность запасных частей у Kolo?',
                    'answers' => [
                        [
                            'name' => '15',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '20',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '8',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '10',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В какой серии сиденье для унитаза не комплектуется металлическим креплением?',
                    'answers' => [
                        [
                            'name' => 'Все сидения Коlо комплектуются металическими креплениями, кроме Idol',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Все сидения Коlо комплектуются металическими креплениями, кроме Freja',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Все сидения Коlо комплектуются металическими креплениями, кроме Solo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Все сидения Коlо комплектуются металическими креплениями',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Что является дополнительной защитой для мебели Kolo?',
                    'answers' => [
                        [
                            'name' => 'Двойная кромка',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Водоодталкивающее восковое покрытие',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Обработка поверхности плиты водоодталкивающим спреем',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Защитное лакированное покрытие',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какая упаковка у компактов Kolo?',
                    'answers' => [
                        [
                            'name' => 'Коробки из 3-х слойного гофрокартона',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Стретч-пленка',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Деревянные паллеты',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Коробки из 5-ти слойного гофрокартона',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В каких сериях санитарной керамики KOLO есть умывальники, встраиваемые в столешницу?',
                    'answers' => [
                        [
                            'name' => 'Ego и Traffic',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Modo и Solo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Twins и Runa',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Cocktail  и Nova Pro',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В каких сериях санитарной керамики KOLO есть умывальники, встраиваемые под столешницу?',
                    'answers' => [
                        [
                            'name' => 'Ego, Cocktail, Freja',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Traffic, Modo, Twins',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Rekord, Idol, Solo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Style, Nova Pro',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В какой серии есть подвесной унитаз Rimfree для людей с ограниченными возможностями?',
                    'answers' => [
                        [
                            'name' => 'Ego',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Style',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Traffic',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Nova Pro',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В каких цветах изготавливается мебель Nova Pro?',
                    'answers' => [
                        [
                            'name' => 'Белый глянец и венге',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Светлый дуб и белый ясень',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Серебряный графит и черный матовый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белый глянец и серый ясень',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В какую серию входит боковой шкафчик карго?',
                    'answers' => [
                        [
                            'name' => 'Runa',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Nova Pro',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Rekord',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Twins',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Как работает система Click2Clean?',
                    'answers' => [
                        [
                            'name' => 'Прокрутить',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Потянуть от себя',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Потянуть на себя',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нажать',
                            'is_correct' => true,
                        ],
                    ],
                ],
            ],
        ];

        Test::createFromArray($test_array);
    }
}
