<?php

use Illuminate\Database\Seeder;

class EmploymentTypeInformationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createInformationType();
        $this->createInformationField();
    }

    private function createInformationType()
    {
        DB::table('information_types')->insert([
            [
                'slug' => 'employment_type',
                'name' => 'Тип занятости',
                'validator' => 'exists:employment_types,id',
            ],
        ]);
    }

    private function createInformationField()
    {
        DB::table('information_fields')->insert([
            [
                'slug' => 'employment_type_select',
                'name' => 'Тип занятости',
                'description' => '',
                'login' => false,
                'hidden' => false,
                'default_value' => null,
                'type' => 'employment_type'
            ],
        ]);
    }
}
