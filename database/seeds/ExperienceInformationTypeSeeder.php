<?php

use Illuminate\Database\Seeder;

class ExperienceInformationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createInformationType();
        $this->createInformationField();
    }

    private function createInformationType()
    {
        DB::table('information_types')->insert([
            [
                'slug' => 'experience',
                'name' => 'Стаж работы',
                'validator' => 'exists:experiences,id',
            ],
        ]);
    }

    private function createInformationField()
    {
        DB::table('information_fields')->insert([
            [
                'slug' => 'experience_select',
                'name' => 'Стаж работы',
                'description' => '',
                'login' => false,
                'hidden' => false,
                'default_value' => null,
                'type' => 'experience'
            ],
        ]);
    }
}
