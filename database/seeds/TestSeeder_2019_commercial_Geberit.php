<?php

use Illuminate\Database\Seeder;
use App\Test;

class TestSeeder_2019_commercial_Geberit extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $test_array = [
            'name' => 'Базовый тест: модуль Geberit',
            'faculty' => 'commercial',
            'points' => 450,
            'question_count' => 30,
            'duration' => 30,
            'start_date' => '2019-05-15 00:00:00',
            'end_date' => '2019-11-30 23:59:59',
            'questions' => [
                [
                    'name' => 'Основное отличие бачков Geberit Delta от Geberit Sigma?',
                    'answers' => [
                        [
                            'name' => 'Бюджетное решение с меньшим ассортиментом клавиш',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Больше габариты и толщина бачка ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Меньше гарантийный срок службы',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Другая монтажная высота',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Когда нужно начинать монтировать клавишу Sigma 60 (2016)?',
                    'answers' => [
                        [
                            'name' => 'После завершающего этапа монтажа инсталляции и плитки',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'На первом этапе монтажа инсталляции',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'На этапе монтажа плитки',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'С ее монтажа нужно начать все работы',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Из какого материала выполнена клавиша смыва Sigma 30 Старт-Стоп?',
                    'answers' => [
                        [
                            'name' => 'Из литого цинкового сплава',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Из пластика',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Из нержавеющей стали',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Из стекла',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Из какого материала выполнена клавиша смыва Sigma 30 двойной смыв?',
                    'answers' => [
                        [
                            'name' => 'Из пластика',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Из нержавеющей стали',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Из стекла',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Из литого цинкового сплава',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Из какого материала выполнена клавиша смыва Sigma 20 двойной смыв?',
                    'answers' => [
                        [
                            'name' => 'Из пластика или нержавеющей стали',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Из нержавеющей стали',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Из пластика',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Из пластика или литого цинкового сплава',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие возможны настройки клапана смыва в бачках Geberit?',
                    'answers' => [
                        [
                            'name' => 'Большой смыв 4.5/6/7.5',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Малый смыв 2/4',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Большой смыв 4/6/9',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Смыв 6/3',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Из какого материалы выполнены бачки Geberit?',
                    'answers' => [
                        [
                            'name' => 'PE',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'PP',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'PVC',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'PB',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Каким методом проверяют бачки Geberit на герметичность?',
                    'answers' => [
                        [
                            'name' => 'Давлением 10 атм',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Давлением 0.10 атм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Лазерными датчиками',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Давлением 15 атм',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой стандартный смыв применен в бачках Geberit?',
                    'answers' => [
                        [
                            'name' => 'Механический двойной смыв',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Механический смыв',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Пневматический двойной смыв',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Комбинированный смыв',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой материал для производства бачков скрытого монтажа самый надежный и долговечный?',
                    'answers' => [
                        [
                            'name' => 'PE',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'PP',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'PVC',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'PB',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Сколько лет Geberit производит бачки скрытого монтажа?',
                    'answers' => [
                        [
                            'name' => 'Более 50 лет',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Более 40 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Более 100 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Более 60 лет',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что значат буквы в артикулярных номерах клавиш Omega?',
                    'answers' => [
                        [
                            'name' => 'Сочетание цвета клавиши смыва',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Форму клавиши смыва',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Линейку клавиши смыва',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Порядковый номер',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Клавиша смыва Type 70 это',
                    'answers' => [
                        [
                            'name' => 'Дистанционный смыв унитаза с гидросервоприводом',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Смыв с гидросервоприводом',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Пневматический двойной смыв унитаза',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дистанционная пневматический смыв унитаза',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие клавиши подходят для набора 3 в 1 без клавиши смыва арт.458.126.00.1?',
                    'answers' => [
                        [
                            'name' => 'Все клавиши Delta',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Delta 51 и Delta 50',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Delta 21 и Delta 20',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Все подходящие по размеру',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как работает система удаления запаха Duofresh?',
                    'answers' => [
                        [
                            'name' => 'Удаление запаха непосредственно из чаши унитаза через фильтр в кнопке смыва',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Удаление запаха непосредственно из чаши унитаза через вентиляцию помещения',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Удаление запаха непосредственно из чаши унитаза вентилятором установленным в унитазе',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Удаление запаха  через вентиляцию помещения',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие возможны варианты высоты монтажного элемента Duofix Omega?',
                    'answers' => [
                        [
                            'name' => '82/98/112',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '82/98/114',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '98/101/114',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '101/112/114',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая толщина монтажного элемента Duofix Omega',
                    'answers' => [
                        [
                            'name' => '12 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '12.5 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '14 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '16 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'На сколько клавиша смыва Omega 20 меньше чем клавиша Sigma 20',
                    'answers' => [
                        [
                            'name' => '0.25',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '0.32',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Omega 20 и Sigma 20 одинаковые по размеру',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '0.2',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое важное отличие в комплекте Duofix Delta 458.126.00.1?',
                    'answers' => [
                        [
                            'name' => 'Не комплектуется клавишей смыва',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Не комплектуется звукоизолирующей прокладкой',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Не комплектуется крепежом к стене',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Комплектуется клавишей смыва Delta 20',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'В какой клавише смыва есть подсветка и и функция бесконтактного управления?',
                    'answers' => [
                        [
                            'name' => 'Sigma 80',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Sigma 70',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Sigma 60',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Sigma 40',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Для чего используется белая трубка сбоку в инсталляции Duofix?',
                    'answers' => [
                        [
                            'name' => 'Для подключения AquaClean',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Для подключения гигиенического душа',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Для подключения подвода воды',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Для подвода электричества',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что значит отметка 1 м на раме Duofix?',
                    'answers' => [
                        [
                            'name' => 'Отметка высоты установки в 1м до чистого пола',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Высота установки унитаза',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Допустимая регулировка высоты',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Высота установки ножек',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие особенности клавиши Sigma 70',
                    'answers' => [
                        [
                            'name' => 'Цельная поверхность клавиши и легкое нажатие',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Простота монтажа и большой выбор цветов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Бесконтактный смыв и современный дизайн',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Применение новой технологии обработки стекла',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Клавиша смыва Type 10 это',
                    'answers' => [
                        [
                            'name' => 'Дистанционная клавиша смыва унитаза с пневмоприводом',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Клавиша смыва с пневмоприводом',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Пневмо клавиша смыва писсуара',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дистанционная пневмо клавиша смыва писсуара',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'При покупке бесконтактных клавиш смыва Sigma 10, Sigma 80, какой элемент нужно заказать дополнительно?',
                    'answers' => [
                        [
                            'name' => 'Блок питания',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Электро привод',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Шнур соединения',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Блок для батареи',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Из какого материалы выполнен бачок Geberit Delta?',
                    'answers' => [
                        [
                            'name' => 'PE',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'PP',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'PVC',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'PB',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие отличия между клавишами Sigma 60 и Omega 60?',
                    'answers' => [
                        [
                            'name' => 'Размер клавиш',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Размер и материал клавиш',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Размер и цвет клавиш',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Материал клавиш',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие отличия между клавишами Sigma 50 и Delta 50',
                    'answers' => [
                        [
                            'name' => 'Материал клавиш',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Размер клавиш',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Размер и функциональность клавиш',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Функциональность клавиш',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие отличия между клавишами Omega 20 и Delta 21',
                    'answers' => [
                        [
                            'name' => 'Размер клавиш',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Материал клавиш',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Материал и функциональность',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Функциональность клавиш',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какого цвета нет у Monolith',
                    'answers' => [
                        [
                            'name' => 'Серый',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Белый ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Песочный ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Черный',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая высота Monolith для унитаза',
                    'answers' => [
                        [
                            'name' => '101 и 114 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '101 и 112см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '98 и 112 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '82 и 98 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как производится смыв у Monolith?',
                    'answers' => [
                        [
                            'name' => 'Клавиши смыв справа на верхней панели',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Клавиши смыв слева на верхней панели',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Клавиши смыва фронтально',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дистанционный смыв',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое отличие между Monolith и Monolith Plus для унитаза?',
                    'answers' => [
                        [
                            'name' => 'Удаление запахов',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Цвет стекла',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Простата монтажа',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Возможность мотажа биде',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Комфортная подсветка',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Цвет стекла',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Простата монтажа',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Возможность монтажа биде',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое отличие между Monolith и  Monolith Plus для унитаза?',
                    'answers' => [
                        [
                            'name' => 'Сенсорные клавиши смыва',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Цвет стекла',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Простата монтажа',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Возможность монтажа биде',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что нужно купить дополнительно для монтажа Monolith?',
                    'answers' => [
                        [
                            'name' => 'Ничего все в комплекте',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Ничего, кроме крепления к стене',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ничего, кроме звукоизолирующей прокладки',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только кнопку смыва',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой высоты Monolith для биде?',
                    'answers' => [
                        [
                            'name' => '101 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '114 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '101 и 114 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '112 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Для чего используют бачок скрытого монтажа?',
                    'answers' => [
                        [
                            'name' => 'Для монтажа  с напольным унитазом',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Для монтажа с консольным унитазом',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Для монтажа с подвесным унитазом',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Для монтажа с напольным писсуаром',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая гарантия на Geberit Duofix для общественных сан.узлов?',
                    'answers' => [
                        [
                            'name' => '10 лет',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '7 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '15 лет',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что рекомендует компания Geberit использовать для монтажа писсуара',
                    'answers' => [
                        [
                            'name' => 'Инсталляцию Geberit Duofix для писсуара',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Инсталляцию Geberit Delta для писсуара',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Инсталляцию Geberit Sigma для писсуара',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Инсталляцию Geberit Omega для писсуара',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой принцип работы клавиши Sigma 80?',
                    'answers' => [
                        [
                            'name' => 'Беcконтактный двойной смыв',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Пневматический двойной смыв',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Механический смыв старт-стоп',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Комбинированный двойной смыв',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что нужно делать в случаи появления неисправности?',
                    'answers' => [
                        [
                            'name' => 'Обратится в сервис-центр',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Обратится в точку продажи',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Обратится к сантехнику ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Исправить неполадку самостоятельно согласно инструкции',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Входит ли в комплект угловой кран подвода воды к бачку?',
                    'answers' => [
                        [
                            'name' => 'Да входит 1/2"',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Да входит 3/8"',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Да входит, с адаптером',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Покупается дополнительно',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой материал поверхности клавиши смыва Sigma21 Geberit применен первый раз?',
                    'answers' => [
                        [
                            'name' => 'Натуральный камень',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Сверхтвердый пластик',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нано-материал',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Натуральная ценная порода дерева',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие материалы поверхности доступны в  клавише смыва Sigma21?',
                    'answers' => [
                        [
                            'name' => 'Стекло, пластик',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие материалы поверхности доступны в клавише смыва Sigma21?',
                    'answers' => [
                        [
                            'name' => 'Стекло, нерж.сталь',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Стекло, камень',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Камень, нерж.сталь',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие геометрические формы применены в кнопках клавиши смыва Sigma21?',
                    'answers' => [
                        [
                            'name' => 'Овал',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Круг',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Квадрат',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Треугольник',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Есть ли у клавиши смыва Sigma21 индивидуальный вариант отделки?',
                    'answers' => [
                        [
                            'name' => 'Да, есть возможность индивидуальной отделки',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Нет возможности индивидуальной отделки',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только индивидуальные варианты отделки из каталога',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Да, с возможностью индивидуальной отделки только кнопок смыва',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая главная проблема традиционных линейных трапов для душа без поддона?',
                    'answers' => [
                        [
                            'name' => 'Невидимое загрязнение под декоративной накладкой',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Большая длина декоративной накладки',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Недостаточная длина декоративной накладки',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Не современный дизайн декоративной накладки',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое отличие трапа CleanLine от традиционных трапов для душа без поддона?',
                    'answers' => [
                        [
                            'name' => 'Интегрированная в трап гидроизоляция',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Использование технологий, которые не требуют гидроизоляции',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Наличие гидроизоляции в комплекте ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Подробная инструкция по выбору и применению гидроизоляции',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Продуманная конструкция, которую легко чистить',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Большой выбор декоративных накладок',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Применение самоочищающейся накладки',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Специальный раствор для чистки в комплекте',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Простой монтаж',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Применение специального монтажного комплекта',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Не нужен инструмент при монтаже',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Применение прочных крепежных ножек',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как очистить трапа CleanLine ',
                    'answers' => [
                        [
                            'name' => 'Протереть с наружи и очистить внутренний гребень',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Снять накладку и очистить под ней',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Применить специальное средство для чистки',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Очистить пазы снаружи сильным напором проточной воды',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'С какого материала изготовлена декоративная часть трапа CleanLine?',
                    'answers' => [
                        [
                            'name' => 'Нержавеющая сталь',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Цинковое литье',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Высокопрочный пластик',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Многокомпонентный',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая возможная длина декоративной части трапа CleanLine?',
                    'answers' => [
                        [
                            'name' => '30-90 и 30-130 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '30-70 и 30-140 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '30-100 и 30-130 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '30-110 и 30-140 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой материал сифона CleanLine',
                    'answers' => [
                        [
                            'name' => 'РЕ',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Сталь',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'PVC',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Керамика',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое отличие между CleanLine 20 и CleanLine 60?',
                    'answers' => [
                        [
                            'name' => 'Дизайн (форма) декоративной части',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Длина декоративной части',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сочетания цветов декоративной части',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Материал декоративной части',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая возможная толщина плитки пола при использовании CleanLine20 и CleanLine60?',
                    'answers' => [
                        [
                            'name' => '8-40 мм',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '5-26 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5-36 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '8-36 мм',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая возможная толщина плитки пола при использовании трапа в стену?',
                    'answers' => [
                        [
                            'name' => '2-26 мм',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '2-36 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5-40 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '10-40 мм',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие варианты монтажа трапа CleanLine?',
                    'answers' => [
                        [
                            'name' => 'Универсальный монтаж в пол',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Монтаж у стены',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Монтаж по средине комнаты',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Угловой монтаж',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой размер декоративной части точечного трапа в пол Geberit?',
                    'answers' => [
                        [
                            'name' => '8 х 8 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '10 х 10 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '12 х 12 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '14 х 14 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что нужно дополнительно купить для стандартного монтажа точечного трапа в пол Geberit?',
                    'answers' => [
                        [
                            'name' => 'Ничего , все в комплекте',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Гидроизоляцию',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Монтажный набор',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Декоративную часть',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая должна быть высота пола для применения трапов Geberit для душа без поддона?',
                    'answers' => [
                        [
                            'name' => 'высота пола 65-90 и 90-200 мм',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'высота пола 60-90 и 90-200 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'высота пола 65-200 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'регулируется по высоте высота пола',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие возможны варианты производительности трапов Geberit для душа без поддона?',
                    'answers' => [
                        [
                            'name' => '0,4 л/с и 0,8 л/с',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '0,4 л/с и 1,0 л/с',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'выше 0,8 л/с',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'выше 0,4 л/с',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая минимальная высота пола для монтажа трапов Geberit для душа без поддона?',
                    'answers' => [
                        [
                            'name' => '65 мм',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '60 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '90 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '100 мм',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как очистить трап в стену Geberit?',
                    'answers' => [
                        [
                            'name' => 'Снять декоративную накладку и очистить гребень для волос',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Протереть с наружи',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Применить специальное средство',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Не нужно обслуживать',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое основное отличие трапа в стену от других трапов для душа без поддона Geberit?',
                    'answers' => [
                        [
                            'name' => 'Монтаж в стене',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Меньше место для монтажа',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Меньше монтажная высота пола',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Больше производительность',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой уклон пола должен быть для хорошей работы трапов Geberit для душа без поддона?',
                    'answers' => [
                        [
                            'name' => '1-2%',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'до 1 %',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'свыше 2%',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'не требует уклон',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'С какой стороны подключается канализация для трапа в стену Geberit?',
                    'answers' => [
                        [
                            'name' => 'Слева или справа',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Только слева ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только справа',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'С любой стороны',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая возможная высота монтажных элементов Duofix для трапа в стену?',
                    'answers' => [
                        [
                            'name' => 'Высотой 50 и 130 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Высотой 50 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Высотой 130 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Высотой 50 и 112 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что нужно дополнительно купить к Duofix для писсуара для монтажа к стене?',
                    'answers' => [
                        [
                            'name' => 'Крепление к стене',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Ничего, все в комплекте',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сифон',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Звукоизолирующую прокладку',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что нужно дополнительно купить к Duofix для писсуара для монтажа в пустотелую стену?',
                    'answers' => [
                        [
                            'name' => 'Ничего, все в комплекте',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Крепление к стене',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сифон',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Звукоизолирующую прокладку',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая высота Duofix для писсуара от чистого пола?',
                    'answers' => [
                        [
                            'name' => 'Регулируется от 112 до 130 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Максимум 112 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '112 см и регулируемые ножки 20 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '130 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая батарея используется для питания инфракрасный смыва Геберит?',
                    'answers' => [
                        [
                            'name' => '2 шт по 1.5В тип АА',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'тип Крона 9В',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'только от сети 220В',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'специальная батарея  Геберит',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой принцип работы бесконтактной встроенной системы смыва для писсуара?',
                    'answers' => [
                        [
                            'name' => 'Инфракрасный принцип смыва',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Радарный смыв',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Емкостной принцип',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Индукционный принцип',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой принцип работы ручной встроенной системы смыва для писсуара?',
                    'answers' => [
                        [
                            'name' => 'Пневматический принцип',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Механический принцип',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Индукционный принцип',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Принцип Гидролифта',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие возможны варианты использования встроенных систем смыва для писсуара?',
                    'answers' => [
                        [
                            'name' => 'С Duofix или с блоком управления (без инсталляции)',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Только с Duofix',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только без Duofix',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только с блоком управления (без инсталляции)',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой размер декоративной накладки клавиши встроенного смыва писсуара Geberit?',
                    'answers' => [
                        [
                            'name' => '13 х 13 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '15 х 15 см ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '13 х 15 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Большой ассортимент размеров',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Где установлено управление скрытого смыва писсуара Geberit?',
                    'answers' => [
                        [
                            'name' => 'В инсталляции, за керамикой писсуара',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'В инсталляции над керамикой писсуара',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Интегрировано в керамике писсуара',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'В стене рядом с писсуаром',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой смыв имеют клавиши смыва унитаза Sigma 80 и Sigma 10?',
                    'answers' => [
                        [
                            'name' => 'Двойной бесконтактный смыв',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Большой бесконтактный смыв',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Большой механический и малый бесконтактный смыв',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Большой бесконтактный и малый механический смыв',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Из какого материала выполнены клавиши type70',
                    'answers' => [
                        [
                            'name' => 'Стекло и нерж.сталь',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Стекло и пластик',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только стекло',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только пластик',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой вариант монтажа смесителей есть в ассортименте Geberit?',
                    'answers' => [
                        [
                            'name' => 'Монтаж на раковину и на стену',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Монтаж только на раковину',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Монтаж только на стену',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Универсальный монтаж',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой вариант питания бесконтактных смесителей Geberit для монтажа на раковине ',
                    'answers' => [
                        [
                            'name' => '220В, батарея 6В и от генератора',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '220В и батарея 6В',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только 220В',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только батарея 6В',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой вариант питания бесконтактных смесителей Geberit для монтажа на стену?',
                    'answers' => [
                        [
                            'name' => '220В, батарея 6В и от генератора',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '220В и батарея 6В',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только 220В',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только батарея 6В',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие варианты цвета смесителей Geberit для раковин?',
                    'answers' => [
                        [
                            'name' => 'Хром глянцевый',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Хром глянцевый и хром матовый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Хром глянцевый и сатин',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Хром глянцевый с вставкой хром матовый',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Из какого материала выполнены смесители Geberit для монтажа на раковине?',
                    'answers' => [
                        [
                            'name' => 'Хромированная латунь',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Хромированная нержавеющая сталь',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нержавеющая сталь',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Высоколегированная сталь',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое важное отличие слива-перелива с кнопкой для ванны?',
                    'answers' => [
                        [
                            'name' => 'Открытие и закрытие нажатием кнопки на переливе',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Открытие нажатием кнопки на переливе, принудительное закрытие ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Включение и выключение подачи воды нажатием кнопки',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Включение функции гидромассажа нажатием кнопки',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Входит ли сифон для писсуара в комплектацию Duofix для писсуара?',
                    'answers' => [
                        [
                            'name' => 'Да, входит',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Нет, не входит',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Покупается дополнительно на выбор с горизонтальным или вертикальным выпуском',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'В нем нет необходимости',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое стандартное подключение писсуара предусмотрено в Duofix для писсуара?',
                    'answers' => [
                        [
                            'name' => 'Соединение керамики писсуара с подводом воды в Duofix трубкой Ф32 мм с резиновым уплотнением',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Соединение керамики писсуара с подводом воды в Duofix с резьбой 1/2"',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Подключение выбрать исходя их подключения в писсуаре',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Универсальное подключение',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Можно ли использовать писсуар с подключением воды с резьбой 1/2" и Duofix для писсуара?',
                    'answers' => [
                        [
                            'name' => 'Да, можно с дополнительным специальным переходником (покупается отдельно)',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Да, можно ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет, нельзя',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нужно использовать специальный монтажный элемент Duofix с подключением воды с резьбой 1/2"',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Можно ли смонтировать встроенное управление смывом после монтажа Duofix для писсуара?',
                    'answers' => [
                        [
                            'name' => 'Да, можно после монтажа Duofix',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Нет нельзя, только в процессе монтажа Duofix',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет нельзя, только перед монтажом Duofix',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Они не совместимы',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой вариант питания скрытого управления смывом Geberit для писсуара?',
                    'answers' => [
                        [
                            'name' => '220В',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '220В и батарея 9В',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '220В и батарея 2 х 1.5В',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только батарея',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой вариант писсуаров Geberit расходует минимальное количество воды?',
                    'answers' => [
                        [
                            'name' => 'Работающий без воды',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'С ИК смывом 0.5 л',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'С интегрированным смывом 0.5 л',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'С любым вариантом смыва',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая инновация применена в керамике писсуаров Geberit?',
                    'answers' => [
                        [
                            'name' => 'Керамика без ободка',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Новое покрытие керамики',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Новый крепеж керамики',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Новый материал керамики',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая гарантия на электронные системы Geberit?',
                    'answers' => [
                        [
                            'name' => '2 года',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '5 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '10 лет ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Пожизненно',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Основное отличие поверхности Setaplano от конкурентов?',
                    'answers' => [
                        [
                            'name' => 'Материал поверхности и комплексный подход к решению ',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Больше типоразмеров поверхности',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только квадратная форма поверхности и производительность сифона',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только прямоугольная форма поверхности и производительность сифона',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какого цвета поверхность Setaplano?',
                    'answers' => [
                        [
                            'name' => 'Только белая',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Разных цветов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Цвет на заказ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Три основных цвета',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как правильно подобрать поверхность Setaplano',
                    'answers' => [
                        [
                            'name' => 'Три артикула используя таблицу в каталоге',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Один артикул из каталога',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Один артикул и дополнительные артикула по требованию клиента',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Два артикула из таблицы в каталоге',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Предусмотрена ли гидроизоляция в поверхности Setaplano?',
                    'answers' => [
                        [
                            'name' => 'Да , интегрирована в поверхность Setaplano',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Нет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет, поставляется отдельно',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Не требуется',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'В какую сторону можно отводить воду сифоном поверхности Setaplano?',
                    'answers' => [
                        [
                            'name' => 'В любую, на 360 градусов',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Влево или вправо',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только назад',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только вперед',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие варианты монтажа поверхности Setaplano?',
                    'answers' => [
                        [
                            'name' => 'В уровень с полом',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'В уровень с полом и ниже уровня пола',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'В уровень с полом и выше уровня пола',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Любой вариант монтажа',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Чем достигается высокий уровень гигиены поверхности Setaplano?',
                    'answers' => [
                        [
                            'name' => 'Гладкая поверхность, без углов, нет мест для сбора грязи',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Сифон по центру, с решеткой для сбора грязи',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'С помощь решетка для сбора грязи',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сифон сбоку с решеткой для сбора грязи',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'С каких элементов состоит комплект Setaplano?',
                    'answers' => [
                        [
                            'name' => 'Поверхность, монтажная рама, монтажный комплект',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Поверхность, ножки, сифон',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Поверхность, сифон, доп. материалы ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Поверхность, монтажная рама, сифон',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как называются серии инновационных инфракрасных смесителей Geberit?',
                    'answers' => [
                        [
                            'name' => 'Piave и Brenta',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Тип 185 и Тип 186',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Тип 60',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'HyTronic 185',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как монтируется настенный инфракрасные смеситель Geberit Piave и Brenta?',
                    'answers' => [
                        [
                            'name' => 'Только на монтажный элемент Duofix',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Прямо на стену ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Используется любой монтажный комплект',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Используя монтажную коробку HansaVarox',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие варианты питания инфракрасных смеситель Geberit Piave и Brenta?',
                    'answers' => [
                        [
                            'name' => 'Батарея, сеть 220В, генератор',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Батарея, сеть 220В, аккумулятор',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Батарея, сеть 220В',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сеть 220В, аккумулятор',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Чем достигается элегантный дизайн инфракрасных смесителей Geberit Piave и Brenta?',
                    'answers' => [
                        [
                            'name' => 'Функциональные части вынесены в отдельный блок',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Новые технологии проектирования и производства',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Новые материалы смесителей',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Составные части из Нано-деталей',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как регулируется температура воды инфракрасных смеситель Geberit Piave и Brenta?',
                    'answers' => [
                        [
                            'name' => 'С помощью винта в функциональном блоке',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Снаружи на смесителе',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Внутри смесителя (скрытая регулировка)',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Не регулируется',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие варианты монтажных элементов Duofix доступны для инфракрасных смеситель Geberit Piave и Brenta?',
                    'answers' => [
                        [
                            'name' => 'Duofix и Duofix со скрытым сифоном для настенного смесителя или смесителя на раковину',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Duofix для смесителя',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Стандартный Duofix с опцией для использования со смесителем',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет необходимости использовать Duofix',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой длинны излив настенных инфракрасных смеситель Geberit Piave и Brenta?',
                    'answers' => [
                        [
                            'name' => '17 или 22 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '18 или 22 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '22 см ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '18 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Куда подводится питание сети 220В для инфракрасных смеситель Geberit Piave и Brenta? ',
                    'answers' => [
                        [
                            'name' => 'К функциональному блоку под раковиной',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'К изливу смесителя',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Куда подводится питание сети 220В для инфракрасных смеситель Geberit Piave и Brenta?',
                    'answers' => [
                        [
                            'name' => 'К Duofix сверху',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет возможности питания от сети',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие допополнительные функции в унитазе-биде Geberit AquaClean Mera Comfort?',
                    'answers' => [
                        [
                            'name' => 'Поднятие крышки, подогрев сиденья и Комфортная подсветка',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Поднятие крышка, Comfortная подсветка и быстрый нагрев воды',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'подогрев сиденья, Comfortная подсветка и смыв с пульта ДУ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Быстрый нагрев воды, Поднятие крышки и смыв с пульта ДУ',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой смыв применен у модели унитаза-биде Geberit AquaClean Mera?',
                    'answers' => [
                        [
                            'name' => 'Турбо смыв',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Супер смыв',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Смерч технология',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Торнадо смыв',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как изменилось время работы функции биде унитаза-биде Geberit AquaClean Mera?',
                    'answers' => [
                        [
                            'name' => 'Увеличено время работы до 50 сек',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Увеличено время работы до 60 сек',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Изменено время работы до 30 сек',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дополнительная функция регулировки времени работы',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Чем достигается максимальный гигиена использования дамского душа модели унитаза-биде Geberit AquaClean Mera?',
                    'answers' => [
                        [
                            'name' => 'Отдельная форсунка, открывается только при использовании',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Применение новой технологии подачи воды',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Применение специальных моющих средств',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Специальная программа чистки',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой датчик активирует функцию биде в Geberit AquaClean Mera?',
                    'answers' => [
                        [
                            'name' => 'Датчик веса',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'IR Сенсор ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Кнопка вкл.',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Кнопка выкл.',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие доступны варианты цвета унитаза-биде Geberit AquaClean Меrа?',
                    'answers' => [
                        [
                            'name' => 'Белый с хромированной или белой панелью',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Белый с хромированной панелью',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белый с нержавеющей или белой панелью',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белый с нержавеющей панелью',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Требуется ли уклон для CleanLine под плитку?',
                    'answers' => [
                        [
                            'name' => 'Нет, не требуется',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Да, в случае монтажа в центре комнаты',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Да, в случае монтажа у стены',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Требуется ли уклон для  CleanLine под плитку?',
                    'answers' => [
                        [
                            'name' => 'Да, согласно инструкции',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Из какого материала сделан CleanLine под плитку?',
                    'answers' => [
                        [
                            'name' => 'Пластик',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нержавеющая сталь',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Литой сплав',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Натуральный камень',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Каким образом осуществляется доступ к канализации у CleanLine под плитку?',
                    'answers' => [
                        [
                            'name' => 'Через снятую декоративную крышку напрямую к канализации',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'После демонтажа сифона',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Через ревизию снизу поверхности',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Через ревизию в стояке канализации',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая длина CleanLine под плитку?',
                    'answers' => [
                        [
                            'name' => '18 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '30 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '90 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая длина нCleanLine под плитку?',
                    'answers' => [
                        [
                            'name' => '130 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Чем отличается монтаж CleanLine под плитку?',
                    'answers' => [
                        [
                            'name' => 'Изменили конструкцию сифона',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Изменили монтажную высоту',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет необходимости монтировать гидроизоляцию',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ничего нового, монтаж проводится так же, как и в предыдущих моделях',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие модели есть у Geberit AquaClean Tuma?',
                    'answers' => [
                        [
                            'name' => 'Geberit AquaClean Tuma Classic и Comfort',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Geberit AquaClean Tuma Classic',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Geberit AquaClean Tuma Comfort',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Geberit AquaClean Tuma Plus',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие варианты решений есть у Geberit AquaClean Tuma?',
                    'answers' => [
                        [
                            'name' => 'Крышка-биде с наружным подключением',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Крышка-биде с скрытым подключением',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Полное решение унитаз-биде',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Полное решение унитаз-биде и решение крышка-биде',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая технология смыва применена в полном решении унитаза-биде Geberit AquaClean Tuma?',
                    'answers' => [
                        [
                            'name' => 'Безободковая чаша унитаза с технологией смыва TurboFlush',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Безободковая чаша унитаза с технологией смыва Rimfree',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Безободковая чаша унитаза с технологией смыва TurboFlush в модели Comfort и Rimfree в модели Classic',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая технология смыва применена в полном решении унитаза-биде  Geberit AquaClean Tuma?',
                    'answers' => [
                        [
                            'name' => 'Классическая технология смыва и чаша унитаза',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Чем отличаются модели Comfort от Classic Geberit AquaClean Tuma?',
                    'answers' => [
                        [
                            'name' => 'У Geberit AquaClean Tuma Comfort доступно полное решение, а в Classic только крышка-биде',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'У Geberit AquaClean Tuma Comfort доступны дополнительные функции отличные от Classic',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'У Geberit AquaClean Tuma Classic дополнительные функции отличные от Comfort',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'У Geberit AquaClean Tuma Classic доступно полное решение, а в Comfort только крышка-биде',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие дополнительные функции доступны в модели Geberit AquaClean Tuma Comfort в отличии от Classic?',
                    'answers' => [
                        [
                            'name' => 'Технология омывания WhirlSpray',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Проточный нагрев воды',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Плавное опускание крышки SoftClose',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Удаление запаха',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Дамский душ',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Проточный нагрев воды',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Технология смыва TurboFlush',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Плавное опускание крышки SoftClose',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Плавное опускание крышки SoftClose',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сидение с подогревом',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Технология смыва TurboFlush',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Технология омывания WhirlSpray',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Фен с теплым воздухом',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Функция декальцинации',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Технология омывания WhirlSpray',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Бесконтактная автоматика крышки унитаза',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Бесконтактная автоматика крышки унитаза',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '6 ступеней регулировки напора',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Пульт дистанционного управления',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Функция декальцинации',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Есть ли функция дамского душа в Geberit AquaClean Tuma?',
                    'answers' => [
                        [
                            'name' => 'Нет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Да, в модели Geberit AquaClean Tuma Classic',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Да, в модели Geberit AquaClean Tuma Comfort',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Да, во всех моделях Geberit AquaClean Tuma',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая система очистки установлена в Geberit AquaClean Tuma?',
                    'answers' => [
                        [
                            'name' => 'Программа чистки чаши унитаза',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Чистка форсунок специальной жидкостью',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Программа чистки сидения',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Программа удаления накипи',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Чистятся ли форсунки биде в Geberit AquaClean Tuma?',
                    'answers' => [
                        [
                            'name' => 'Да, после каждого применения чистой водой',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Да, после каждого применения специальной жидкостью',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Да, до и после каждого применения специальной жидкостью',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Да, до и после каждого применения чистой водой',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Можно ли установить клавишу Sigma на бачок или инсталляцию Delta?',
                    'answers' => [
                        [
                            'name' => 'Можно',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нельзя',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Можно, если установить специальную рамку ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Можно, но только механические клавиши ',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как устанавливается клавиша на инсталляциях Omega?',
                    'answers' => [
                        [
                            'name' => 'Вертикально, как на Sigma и Delta',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Горизонтально',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Вертикально или горизонтально',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Вертикально на 112см, и вертикально или горизонтально на элементах высотой 82 и 98 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В каком году была основана наша компания Geberit?',
                    'answers' => [
                        [
                            'name' => '1854',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '1864',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '1874',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '1905',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Смывной бачок Geberit выдувают из единого куска пластика, в чем его преимущество? ',
                    'answers' => [
                        [
                            'name' => 'Позволяет проверить герметичность сразу на конвейере',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Прочнее',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Долговечнее, используется премиум материал  ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Все вышеперечисленное верно ',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Минимальная длина декоративной панели – дренажного канала  CleanLine?',
                    'answers' => [
                        [
                            'name' => '50 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '40 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '30 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '20 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Из какого материала выполнена поверхность Setaplano ?',
                    'answers' => [
                        [
                            'name' => 'Акрил',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Варикор',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Минеральный камень',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сталь',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Чем знаменателен 1905 год?',
                    'answers' => [
                        [
                            'name' => 'Основание Геберит',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Изготовлен первый унитаз ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Выпущен первый смывной бачок из дерева ',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Выпущен первый пластиковый бачок ',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как можно узнать артикул инсталляции если она уже установлена? ',
                    'answers' => [
                        [
                            'name' => 'По форме определить в интернете',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'По клавише определить',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Найти артикул на задней внутренней поверхности смывного бачка ',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Найти артикул на задней поверхности клавиши смыва',
                            'is_correct' => false,
                        ],
                    ],
                ],
            ],
        ];

        Test::createFromArray($test_array);
    }
}
