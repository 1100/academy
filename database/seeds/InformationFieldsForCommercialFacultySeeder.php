<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InformationFieldsForCommercialFacultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faculty_information_field')->insert([
            [
                'faculty' => 'commercial',
                'information_field' => 'facebook',
                'required' => true,
                'display' => true,
                'rating_display' => false,
                'position' => 7,
            ],
            [
                'faculty' => 'commercial',
                'information_field' => 'email',
                'required' => true,
                'display' => true,
                'rating_display' => false,
                'position' => 5,
            ],
            [
                'faculty' => 'commercial',
                'information_field' => 'phone',
                'required' => true,
                'display' => true,
                'rating_display' => false,
                'position' => 6,
            ],
            [
                'faculty' => 'commercial',
                'information_field' => 'first_name',
                'required' => true,
                'display' => true,
                'rating_display' => true,
                'position' => 0,
            ],
            [
                'faculty' => 'commercial',
                'information_field' => 'last_name',
                'required' => true,
                'display' => true,
                'rating_display' => true,
                'position' => 1,
            ],
            [
                'faculty' => 'commercial',
                'information_field' => 'city',
                'required' => true,
                'display' => false,
                'rating_display' => false,
                'position' => 2,
            ],
            [
                'faculty' => 'commercial',
                'information_field' => 'region',
                'required' => true,
                'display' => false,
                'rating_display' => false,
                'position' => 3,
            ],
            [
                'faculty' => 'commercial',
                'information_field' => 'shop_name',
                'required' => true,
                'display' => false,
                'rating_display' => false,
                'position' => 4,
            ],
        ]);
    }
}
