<?php

use Illuminate\Database\Seeder;
use App\Test;

class TestSeeder_2019_commercial_video_nova_pro extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $test_array = [
            'name' => 'Видео-обзор и распаковка подвесного унитаза KOLO Nova Pro Rimfree (арт.M39018000)',
            'description' => 'Все вопросы этой викторины относятся к продукции Geberit и подвесному унитазу KOLO Nova Pro Rimfree (арт.M39018000)',
            'faculty' => 'commercial',
            'points' => 100,
            'question_count' => 10,
            'duration' => 10,
            'start_date' => '2019-11-20 00:00:00',
            'end_date' => '2019-11-30 23:59:59',
            'video_identifier' => '3bXBkIsyFww',
            'questions' => [
                [
                    'name' => 'Какая официальная гарантия на подвесные унитазы KOLO?',
                    'answers' => [
                        [
                            'name' => '7 лет',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '10 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '2 года',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая гарантия на сиденья для подвесных унитазов KOLO?',
                    'answers' => [
                        [
                            'name' => '2 года',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '7 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '10 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 лет',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой минимальный эффективный объем воды необходим для большого смыва в подвесном унитазе KOLO Nova Pro Rimfree?',
                    'answers' => [
                        [
                            'name' => '4 л',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '6 л',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '7 л',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '7.5 л',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой минимальный эффективный объем воды необходим для малого смыва в подвесном унитазе KOLO Nova Pro Rimfree?',
                    'answers' => [
                        [
                            'name' => '2 л',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '1 л',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '2.5 л',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '4 л',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Крепления к сиденью унитаза KOLO Nova Pro Rimfree:',
                    'answers' => [
                        [
                            'name' => 'Металлические хромированные',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Металлический матовые',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Пластиковые, белые',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Пластиковые, хромированные',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Из какого материала изготовлено сиденье, которое входит в комплект KOLO Nova Pro Rimfree',
                    'answers' => [
                        [
                            'name' => 'Дюропласт',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Полипропилен',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Полиэтилен',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Полистирол',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Набор креплений для навешивания унитаза на инсталляцию:',
                    'answers' => [
                        [
                            'name' => 'Входит в комплект к инсталляции',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Входит в комплект к подвесному унитазу KOLO Nova Pro Rimfree',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нужно покупать отдельно',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Входит в комплект и к унитазу, и к инсталляции',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой срок наличия запасных частей гарантирует KOLO',
                    'answers' => [
                        [
                            'name' => '10 лет',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '25 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '7 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '50 лет',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что является дополнительным удобством при монтаже сиденья с крышкой унитаза?',
                    'answers' => [
                        [
                            'name' => 'Все ответы верны',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Монтаж без инструментов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Детальная инструкция по монтажу',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Наличие плашки-шаблона для монтажа',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие преимущества унитазов без ободка Rimfree?',
                    'answers' => [
                        [
                            'name' => 'Все ответы верны',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Экономичный смыв',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет места для скопления грязи и микробов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Не нужны агрессивные моющие средства для очистки',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Система Soft Close обеспечивает',
                    'answers' => [
                        [
                            'name' => 'Плавное закрывание крышки и сиденья унитаза',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Плавное закрывание крышки унитаза',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Плавное закрывание сиденья унитаза',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Плавное закрывание и открывание крышки и сиденья унитаза',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что входит в комплект KOLO Nova Pro Rimfree?',
                    'answers' => [
                        [
                            'name' => 'Подвесной унитаз, сиденье с крышкой, комплект креплений, инструкция по монтажу сиденья',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Подвесной унитаз, комплект для монтажа унитаза на инсталляцию, инструкция по монтажу унитаза на инсталляцию, сиденье с крышкой',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Подвесной унитаз, комплект креплений, сиденье с крышкой, подвод воды к унитазу, инструкция по монтажу сиденья унитаза',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Инструкция по навешиванию унитаза, комплект креплений, сиденье с крышкой, подвесной унитаз',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что не входит в комплект KOLO Nova Pro Rimfree:',
                    'answers' => [
                        [
                            'name' => 'Комплект для монтажа унитаза на инсталляцию',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Комплект креплений для сиденья к унитазу',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Инструкция по монтажу сиденья',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Крепления с системой SoftClose',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как называется система плавного опускания крышки и сиденья KOLO?',
                    'answers' => [
                        [
                            'name' => 'Soft Close',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Soft Touch',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Quick Release',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Click2Clean',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что входит в комплект KOLO Nova Pro Rimfree?',
                    'answers' => [
                        [
                            'name' => 'Все ответы верны',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Инструкция по монтажу сиденья',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Комплект креплений сиденья к унитазу',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Крепления с системой SoftClose',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как называется безободковая чаша унитаза KOLO?',
                    'answers' => [
                        [
                            'name' => 'Rimfree',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'CleanRim',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'RimOff',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Rimless',
                            'is_correct' => false,
                        ],
                    ],
                ],
            ],
        ];

        Test::createFromArray($test_array);
    }
}
