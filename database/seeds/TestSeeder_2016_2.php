<?php

use Illuminate\Database\Seeder;
use App\Test;

class TestSeeder_2016_2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $test_array = [
            'name' => 'Ванны и кабины. KOLO.',
            'faculty' => 'commercial',
            'points' => 100,
            'question_count' => 10,
            'duration' => 10,
            'start_date' => '2017-08-01 00:00:00',
            'end_date' => '2017-08-30 00:00:00',
            'questions' => [
                [
                    'name' => 'В каком польском городе расположено производство ванн KOLO?',
                    'answers' => [
                        [
                            'name' => 'Коло',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белосток',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дымер',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Озоркув',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какая из указанных ванн, является асимметричной?',
                    'answers' => [
                        [
                            'name' => 'MODO',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Saga',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Matis',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Mystery',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'С помощью чего можно реставрировать небольшие повреждения на ваннах?',
                    'answers' => [
                        [
                            'name' => 'Силиконовый герметик',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Паста на основе Shellac',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Эпоксидная смола',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Полировочная паста',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какие ванны KOLO имеют усиленные борты?',
                    'answers' => [
                        [
                            'name' => 'Modo и Magnum',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Relax и  Inspiration',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Mirra и Mystery',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Clarissa и Comfort Plus',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Как называются угловые ванны KOLO?',
                    'answers' => [
                        [
                            'name' => 'Mirra, Modo, Opal Plus',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Saga, Sensa, Mystery',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Spring, Comfort, Comfort Plus',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Magnum, Relax, Inspiration',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Что такое Antislide? ',
                    'answers' => [
                        [
                            'name' => 'Система крепления подголовника',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Система креплений для декоративных панелей',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Система магнитных креплений дверей в душкабинах',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Антискользящее покрытие, которое сводит к минимуму риск во время купания',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В каких продуктах Kolo используют Antislide?',
                    'answers' => [
                        [
                            'name' => 'Ванны Mirra и все поддоны Kolo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ванны Modo и кабинки Suppero',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ширма для ванной Niven',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ванны Modo и поддоны Pacyfik',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какие, из указанных ванн, асимметричные?',
                    'answers' => [
                        [
                            'name' => 'Magnum, Diuna',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Opal Plus, Saga',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Sensa, Modo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Spring, Mystery',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Каким набором ножек комплектуются угловые ванны KOLO?',
                    'answers' => [
                        [
                            'name' => 'SN0',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'SN4',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'SN7',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'SN8',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В каких размерах выпускается ванна Mystery?',
                    'answers' => [
                        [
                            'name' => '150х70, 160х70, 170х70 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '120х70, 140х70, 160х70 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '150х100, 170х110 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '140х90 и 150х95 см',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какой объем асимметричной ванны Mirra 170х110 см?',
                    'answers' => [
                        [
                            'name' => '178 л',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '210 л',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '300 л',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '264 л',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какую ванну можно комплектовать ширмой Niven?',
                    'answers' => [
                        [
                            'name' => 'Saga',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Sensa',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Comfort',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Mirra',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В каких размерах выпускается ванна Saga?',
                    'answers' => [
                        [
                            'name' => '150х75, 160х75, 170х75',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '140х70, 150х70, 170х75',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '150х70, 160х70, 170х75',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '150х75, 160х75, 170х80',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В каких размерах выпускается прямоугольная ванна Comfort Plus?',
                    'answers' => [
                        [
                            'name' => '150х70, 160х70, 170х70, 180х70',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '150х70, 160х70, 170х75, 180х80, 190х90 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '150х75, 160х75, 170х75, 180х80, 190х90 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '150х75, 160х80, 170х75, 180х80, 190х90 см',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Сиденья какой ширины доступны для ванн KOLO?',
                    'answers' => [
                        [
                            'name' => '70, 80, 85 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '75, 85, 95 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '75, 80, 85 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '75, 80, 90 см',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какую ванну KOLO можно комплектовать сиденьем?',
                    'answers' => [
                        [
                            'name' => 'Modo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Mirra',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Magnum',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Comfort Plus',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какие ванны комплектуются комплектом ножек SN0?',
                    'answers' => [
                        [
                            'name' => 'Diuna, Comfort, Relax',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Magnum, Spring, Promis',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Mystery, Modo, Comfort Plus',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Opal Plus, Mirra, Sensa, Saga',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какие модели кабин доступны только с покрытием Reflex KOLO?',
                    'answers' => [
                        [
                            'name' => 'Geo 6',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Supero',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Rekord',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Next, Ultra',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какой толщины стекло, используется для изготовления двери кабинки Next?',
                    'answers' => [
                        [
                            'name' => '3 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '4 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '6 мм',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какой толщины стекло, используется для изготовления двери кабинки Ultra?',
                    'answers' => [
                        [
                            'name' => '3 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '4 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '6 мм',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какой толщины стекло, используется для изготовления двери кабинки Geo 6?',
                    'answers' => [
                        [
                            'name' => '3 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '4 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '6 мм',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какой толщины стекло, используется для изготовления двери кабинки Rekord?',
                    'answers' => [
                        [
                            'name' => '6 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '3 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '4 мм',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какой толщины стекло, используется для изготовления двери кабинки Supero?',
                    'answers' => [
                        [
                            'name' => '6 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '3 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '4 мм',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Что такое Push2Clean?',
                    'answers' => [
                        [
                            'name' => 'Система бесконтактного открытия дверей в душевых кабинках',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Система быстрого монтажа душевого поддона',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Система вентиляции ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Cистема магнитных креплений дверей в душевых кабинках',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В каких кабинках применяется технология Push2Clean?',
                    'answers' => [
                        [
                            'name' => 'Next',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Supero',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Geo 6',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ultra',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Что такое  ScreenGuard?',
                    'answers' => [
                        [
                            'name' => 'Защитный экран, который предохраняет мыло от брызг',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Закаленное стекло',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Непотеющее стекло',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Cтекло покрытое специальной пленкой, которое в случае повреждения не рассыпается на осколки',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Каких цветов изготавливают петли для душевых кабинок KOLO?',
                    'answers' => [
                        [
                            'name' => 'Белый глянец и матовый хром',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Черный матовый и хром',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Стальной и бронзовый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Хром и серебряный блеск.',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какая кабина комплектуется вешалками для полотенец?',
                    'answers' => [
                        [
                            'name' => 'Supero',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Geo 6',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Next',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ultra',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В каких душевых поддонах используют Antislide?',
                    'answers' => [
                        [
                            'name' => 'Standard Plus',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'First',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Simplo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Pasyfik',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Как называется самый глубокий душевой поддон Kolo?',
                    'answers' => [
                        [
                            'name' => 'Pasyfik',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Simplo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'First',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Deep',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какая самая большая глубина душевого поддона Kolo?',
                    'answers' => [
                        [
                            'name' => '9 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '6 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '15 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '21 см',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Из какого материала изготовлены панели Kolo?',
                    'answers' => [
                        [
                            'name' => 'Из акрила',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Из полипропилена',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Из полиэтилена',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Из полистирола',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Фронтальных прямоугольных панелей какого размера нет в асортиментной линейке  Kolo?',
                    'answers' => [
                        [
                            'name' => '150 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '140 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '180 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '190 см',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Что не является преимуществом акрилововых ванн?',
                    'answers' => [
                        [
                            'name' => 'Акриловые ванны легкие, но в то же время очень прочные',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Благодаря своей гладкой поверхности, акрил легко поддерживать в чистоте',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Поверхность акрила теплая, что дает приятное ощущение при контакте с кожей',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Акриловые ванны имеют низкий коефициент водопоглащения',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Что не относится к преимуществам прямоугольных панелей UNI4 TM Kolo?',
                    'answers' => [
                        [
                            'name' => 'Цвет панели полностью соответствует цвету акриловых ванн Kolo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Эргономичный дизайн благодаря углублению для ног в нижней части панели',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Широкий размерный ряд как фронтальных, так и боковых панелей',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Приозводятся из лучшего в мире санитарного акрила - Lucite',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какой срок гарантии на акриловые ванны Kolo?',
                    'answers' => [
                        [
                            'name' => '10 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '2 года',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '7 лет',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какой срок гарантии на душевые кабины Kolo?',
                    'answers' => [
                        [
                            'name' => '10 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '7 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '2 года',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Каким набором ножек комплектуется ванна Saga?',
                    'answers' => [
                        [
                            'name' => 'SN6',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'SN7',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'SN8',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'SN0',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Каким набором ножек комплектуется ванна Modo?',
                    'answers' => [
                        [
                            'name' => 'SN4',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'SN7',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'SN0',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'SN8',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какого размера боковых панелей нет в асортиментной линейке  Kolo?',
                    'answers' => [
                        [
                            'name' => '75 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '80 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '90 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '85 см',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В какой серии ванн Kolo представлен размер 140х90 см?',
                    'answers' => [
                        [
                            'name' => 'Sensa',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Comfort',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Spring',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Mistery',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В какой серии ванн Kolo представлен ра размер 120х70 см?',
                    'answers' => [
                        [
                            'name' => 'Sensa',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Comfort Plus',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Mistery',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Diuna',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Чем армируют акриловую ванну для придания ей жесткости?',
                    'answers' => [
                        [
                            'name' => 'Минеральная вата',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Стружка',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Поролон',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Стекловолокно',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Что не относится к преимуществам душевых кабин Kolo?',
                    'answers' => [
                        [
                            'name' => 'Закаленное стекло гарантирует безопасность и длительность использования',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Магнитный уплотнитель, который обеспечивает герметичность кабины',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Высокое качество и стильный дизайн',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Двери кабины изготовлены из высококачественного пластика',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какими поддонами рекомендуется комплектовать душевые кабинки Next?',
                    'answers' => [
                        [
                            'name' => 'First',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Deep',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Standard Plus',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Pasyfik',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'Какой цвет профиля в кабинках Ultra?',
                    'answers' => [
                        [
                            'name' => 'Белый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Хром',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Хром матовый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Серебряный блеск',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В какой серии ванн Kolo представлен размер 190x90 см?',
                    'answers' => [
                        [
                            'name' => 'Opal Plus',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Modo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Mirra',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Clarissa',
                            'is_correct' => true,
                        ],
                    ],
                ],[
                    'name' => 'В какой серии ванн Kolo представлен размер 170x80 см?',
                    'answers' => [
                        [
                            'name' => 'Sensa',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Comfort',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Comfort Plus',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Mirra',
                            'is_correct' => true,
                        ],
                    ],
                ],
            ],
        ];

        Test::createFromArray($test_array);
    }
}
