<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TechnicalFacultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faculties')->insert([
            [
                'slug' => 'technical',
                'name' => 'Монтажники',
                'description' => 'Образовательный проект для сантехников и инсталляторов',
                'color' => 'purple',
                'open_rating' => false,
            ],
        ]);
        $this->call(InformationFieldsForTechnicalFacultySeeder::class);
    }
}
