<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DemoTest::class);
        $this->call(TestSeeder_2019_commercial_KOLO::class);
        $this->call(TestSeeder_2019_commercial_Geberit::class);
        $this->call(TestSeeder_2019_commercial_new_products::class);
        $this->call(TestSeeder_2019_technical_plumbing::class);
        $this->call(TestSeeder_2019_technical_piping::class);
        $this->call(TestSeeder_2019_commercial_video_nova_pro::class);
        $this->call(TestSeeder_2019_commercial_video_delta::class);
        $this->call(TestSeeder_2019_commercial_video_sigma::class);
        $this->call(TestSeeder_2019_technical_video_sigma::class);
        $this->call(TestSeeder_2019_technical_video_cleanline::class);
    }
}
