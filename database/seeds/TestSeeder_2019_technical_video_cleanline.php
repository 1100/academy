<?php

use Illuminate\Database\Seeder;
use App\Test;

class TestSeeder_2019_technical_video_cleanline extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $test_array = [
            'name' => 'Душевые каналы Geberit CleanLine',
            'faculty' => 'technical',
            'description' => 'Перед стартом теста посмотрите 3 видеоролика, которые помогут вам при прохождении этого теста',
            'points' => 150,
            'question_count' => 15,
            'duration' => 20,
            'start_date' => '2019-12-23 00:00:00',
            'end_date' => '2020-01-31 23:59:59',
            'video_identifier' => 'H0yhLbybIcE,aMrV2NFVotE,BBhGLJcDnIo',
            'questions' => [
                [
                    'name' => 'Как монтируется корпус душевого канала?',
                    'answers' => [
                        [
                            'name' => 'На черновой пол дюбелями из комплекта ',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'На черновой пол и заливается цементом',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'На теплоизоляцию дюбелями из комплекта ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'На теплоизоляцию и заливается цементом',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая цель использования душевого канала CleanLine?',
                    'answers' => [
                        [
                            'name' => 'Водоотведение воды от одной душевой системы в зоне душа',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Отведение воды из сан.узла',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Отведение воды от ванной с установкой на пол',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Разделение "мокрой" и "сухой" зон в сан. узле',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Для создания водоотведения душевым каналом CleanLine нужно заказать:',
                    'answers' => [
                        [
                            'name' => 'Корпус душевого канала + декоративную часть душевого канала CleanLine',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Корпус душевого канала + монтажный комплект',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Декоративную часть душевого канала CleanLine + монтажный комплект',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Корпус душевого канала + декоративную часть душевого канала CleanLine + монтажный комплект',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Нужно ли устанавливать демферную ленту?',
                    'answers' => [
                        [
                            'name' => 'Да, обязательно',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'На усмотрение заказчика',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'На усмотрение монтажника',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет, не нужно',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой нужно предусмотреть уклон пола душевой зоны к трапу?',
                    'answers' => [
                        [
                            'name' => '1-2%',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '3-5%',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '2 мм на 1 м',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Уклон не нужен',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'На каком уровне устанавливается верхняя часть корпуса трапа?',
                    'answers' => [
                        [
                            'name' => 'По уровню отметки чистового пола с учетом уклона',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'По уровню отметки чистового пола',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'По уровню горизонтально',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Не имеет значения',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'До какого уровня нужно залить цементную стяжку?',
                    'answers' => [
                        [
                            'name' => 'До уровня гидроизоляции верхней части корпуса трапа',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'До верхнего уровня верхней части корпуса трапа',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'До уровня демферной ленты',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'До уровня пола в помещении',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назначение фартука голубого цвета с надписью Geberit под крышкой верхней части корпуса трапа:',
                    'answers' => [
                        [
                            'name' => 'Гидроизоляция',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Звукоизоляция',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Противопожарная защита',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Декоративная',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Нужна ли дополнительная гидроизоляция в месте душевой зоны при установке трапа?',
                    'answers' => [
                        [
                            'name' => 'Да, согласно инструкции',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'На усмотрение заказчика',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'На усмотрение монтажника',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет, не нужно',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Где можно установить душевой канал CleanLine?',
                    'answers' => [
                        [
                            'name' => 'Три варианта: два варианта монтажа у стены и в центре душевой зоны',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Два варианта монтажа у стены',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Два варианта монтажа в центре душевой зоны ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Два варианта: монтаж у стены и в центре душевой зоны',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая длина декоративной части душевого канала CleanLine?',
                    'answers' => [
                        [
                            'name' => '30-90 см, 30-130 см и 30-160 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '30-90 см и 30-130 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '30-130 см и 30-160 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '30-90 мм, 30-130 мм и 30-160 мм',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Можно ли изменить длину декоративной части душевого канала CleanLine?',
                    'answers' => [
                        [
                            'name' => 'Да, можно обрезать мелкой ножовкой',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Да, можно обрезать "болгаркой"',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Да, обрезать кратно 10 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет, длину изменить нельзя',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как обработать места среза декоративной части?',
                    'answers' => [
                        [
                            'name' => 'Обработать напильником и установить заглушки из комплекта',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Установить заглушки из комплекта',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Обработать напильником',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Обрезать нельзя',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как крепится декоративная часть душевого канала CleanLine?',
                    'answers' => [
                        [
                            'name' => 'При помощи плиточного клея',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'При помощи цементного раствора',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'На фиксаторы на обратной стороне декоративной части',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Прижимается винтом на корпус трапа',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Для чего используется синее кольцо на декоративной части душевого канала CleanLine?',
                    'answers' => [
                        [
                            'name' => 'Для правильного распределения плиточного клея в области патрубка',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Для гидроизоляции',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Для замера глубины входа в патрубок',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Не нужно при монтаже',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Нужен ли уклон вдоль декоративной части душевого канала CleanLine?',
                    'answers' => [
                        [
                            'name' => 'Да, нужен до 1%',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Да, нужен до 1-2%',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'На усмотрение монтажника',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет, не нужен',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'На каком уровне относительно чистового пола монтируется декоративная часть душевого канала CleanLine?',
                    'answers' => [
                        [
                            'name' => 'Ниже уровня плитки на 1-2 мм',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Ниже уровня плитки на 1 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Выше уровня плитки на 1 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'На уровне плитки',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая минимальная длина декоративной части душевого канала CleanLine?',
                    'answers' => [
                        [
                            'name' => 'Можно обрезать до длины 30 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '90 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '80 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '70 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
            ],
        ];

        Test::createFromArray($test_array);

    }
}
