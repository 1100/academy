<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RegionInformationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->renameOldRegionField();
        $this->createInformationType();
        $this->createInformationField();
        $this->addInformationFieldToCommercialFaculty();
        $this->convertOldFieldDataToNewFieldData();
        $this->removeOldRegionInformationField();
    }

    private function renameOldRegionField()
    {
        DB::table('information_fields')->where('slug', 'region')->update(['name' => 'Область (старое)']);
    }

    private function createInformationType()
    {
        DB::table('information_types')->insert([
            [
                'slug' => 'region',
                'name' => 'Область',
                'validator' => 'exists:regions,id',
            ],
        ]);
    }

    private function createInformationField()
    {
        DB::table('information_fields')->insert([
            [
                'slug' => 'region_select',
                'name' => 'Область',
                'description' => '',
                'login' => false,
                'hidden' => false,
                'default_value' => null,
                'type' => 'region'
            ],
        ]);
    }

    private function addInformationFieldToCommercialFaculty()
    {
        DB::table('faculty_information_field')->insert([
            [
                'faculty' => 'commercial',
                'information_field' => 'region_select',
                'required' => false,
                'display' => true,
                'rating_display' => false,
                'position' => 3,
            ],
        ]);
    }

    private function convertOldFieldDataToNewFieldData()
    {
        $region_variants = [
            '0' => [
                'тест',
                'NULL',
                null,
                'Al Manamah',
                'Gray Mountain',
                'Test',
                'Гомельская',
                'Минская',
                'Могилевская',
            ],
            '1' => [
                'Винницкая',
            ],
            '2' => [
                'Волинська',
                'Волынская',
            ],
            '3' => [
                'Днепропетровская',
            ],
            '4' => [
                'Донецкая',
            ],
            '5' => [
                'Житомирская',
            ],
            '6' => [
                'Закарпатская',
            ],
            '7' => [
                'Запорожская',
                'Запоррожская',
                'Zaporozhye',
            ],
            '8' => [
                'Ивано-Франковская',
            ],
            '9' => [
                'Киев',
                'Киевская',
                'Киивська',
                'Київська',
                'Kiev',
            ],
            '10' => [
                'Кировоградская',
            ],
            '11' => [
                'Луганская',
            ],
            '12' => [
                'Львівська',
                'Львовская',
            ],
            '13' => [
                'Николаевская',
            ],
            '14' => [
                'Одесскач',
                'Одесская',
            ],
            '15' => [
                'Полтавcкая',
                'Полтавская',
            ],
            '16' => [
                'Рівненська',
                'Ровенская',
                'Ровенская область',
            ],
            '17' => [
                'Сумская',
            ],
            '18' => [
                'Тернопільська',
                'Тернопольская',
            ],
            '19' => [
                'Харківська область',
                'Харьков',
                'Харьковская',
                'Xарьковская',
            ],
            '20' => [
                'Херсонская',
            ],
            '21' => [
                'Хмельницкая',
            ],
            '22' => [
                'Чeркасская',
                'Черкасская',
            ],
            '23' => [
                'Черниговская',
            ],
            '24' => [
                'Чернівецька',
                'Черновицкая',
                'Черновицкая область',
                'Chernovickaya oblatj',
            ],
            '25' => [
                'АР Крым',
            ],
        ];
        foreach ($region_variants as $region_id => $variants) {
            $update_data = [
                'information_field' => 'region_select',
                'value' => ($region_id == 0) ? null : $region_id,
            ];

            DB::table('user_information')
                ->where('information_field', '=', 'region')
                ->whereIn('value', $variants)
                ->update($update_data);
        }
    }

    private function removeOldRegionInformationField()
    {
        DB::table('user_information')->where('information_field', 'region')->delete();
        DB::table('faculty_information_field')->where('information_field', 'region')->delete();
        DB::table('information_fields')->where('slug', 'region')->delete();
    }
}
