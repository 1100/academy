<?php

use Illuminate\Database\Seeder;
use App\Test;

class DemoTest extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $test_array = [
            'name' => 'Пробный Тест',
            'faculty' => 'commercial',
            'points' => 0,
            'question_count' => '5',
            'duration' => 5,
            'start_date' => '2019-05-15 00:00:00',
            'end_date' => '2019-11-30 23:59:59',
            'questions' => [
                [
                    'name' => 'Основное отличие бачков Geberit Delta от Geberit Sigma?',
                    'answers' => [
                        [
                            'name' => 'Бюджетное решение с меньшим ассортиментом клавиш',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Больше габариты и толщина бачка ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Меньше гарантийный срок службы',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Другая монтажная высота',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие возможны настройки клапана смыва в бачках Geberit?',
                    'answers' => [
                        [
                            'name' => 'Большой смыв 4.5/6/7.5',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Малый смыв 2/4',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Большой смыв 4/6/9',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Смыв 6/3',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой материал для производства бачков скрытого монтажа самый надежный и долговечный?',
                    'answers' => [
                        [
                            'name' => 'PE',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'PP',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'PVC',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'PB',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как работает система удаления запаха Duofresh?',
                    'answers' => [
                        [
                            'name' => 'Удаление запаха непосредственно из чаши унитаза через фильтр в кнопке смыва',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Удаление запаха непосредственно из чаши унитаза через вентиляцию помещения',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Удаление запаха непосредственно из чаши унитаза вентилятором установленным в унитазе',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Удаление запаха  через вентиляцию помещения',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая гарантия на электронные системы Geberit?',
                    'answers' => [
                        [
                            'name' => '2 года',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '5 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '10 лет ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Пожизненно',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'В каком году был создан завод по производству санитарной керамики в г.Kоло?',
                    'answers' => [
                        [
                            'name' => '1909',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '1956',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '1993',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '1962',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая серия KOLO имеет наибольший ассортимент?',
                    'answers' => [
                        [
                            'name' => 'Traffic',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Style',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Idol',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Nova Pro',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Как называется детская серия керамики KOLO?',
                    'answers' => [
                        [
                            'name' => 'Бемби',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Baby',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Nova Pro Pico',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Nova Pro Junior',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В какую серию Kolo входит боковой шкафчик карго?',
                    'answers' => [
                        [
                            'name' => 'Rekord',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Nova Pro',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Traffic',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Twins',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Сиденья какой ширины доступны для ванн KOLO  Comfort Plus?',
                    'answers' => [
                        [
                            'name' => '70, 80, 85 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '75, 85, 95 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '75, 80, 85 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '75, 80, 90 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
            ],
        ];

        Test::createFromArray($test_array);
    }
}
