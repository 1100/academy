<?php

use Illuminate\Database\Seeder;
use App\Test;
use Carbon\Carbon;

class TestSeeder_2016_3 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $test_array = [
            'name' => 'Geberit. Инсталляции.Клавиши. Monolith.',
            'faculty' => 'commercial',
            'points' => 100,
            'question_count' => 10,
            'duration' => 10,
            'start_date' => '2017-08-01 00:00:00',
            'end_date' => '2017-08-30 00:00:00',
            'questions' => [
                [
                    'name' => 'Основное отличие бачков Geberit Delta от Geberit Sigma?',
                    'answers' => [
                        [
                            'name' => 'Бюджетное решение с меньшим ассортиментом клавиш',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Больше габариты и толщина бачка ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Меньше гарантийный срок службы',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Более сложный монтаж',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие кнопки смыва подходят к инсталляции Duofix Sigma с системой удаления запаха для подвесного унитаза ?',
                    'answers' => [
                        [
                            'name' => 'Все кнопки Sigma',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Все кнопки Sigma, Omega и Delta',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Все кнопки Sigma, кроме Sigma 40',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Все кнопки Sigma и Omega',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие произошли изменения в 2016 году в клавише Sigma 60?',
                    'answers' => [
                        [
                            'name' => 'Новые цвета стекла и улучшенная технология монтажа',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Цвет стекла и цвет декоративной рамки',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Увеличение скорости монтажа',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Возможность установки на всех инсталляциях Geberit',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая клавиша смыва заменит клавишу Tango?',
                    'answers' => [
                        [
                            'name' => 'Sigma 30 смыв Старт-Стоп',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Sigma 30 Двойной смыв',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Balero',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Останется в ассортименте',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая клавиша смыва заменит клавишу Balero?',
                    'answers' => [
                        [
                            'name' => 'Sigma 30 двойной смыв',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Sigma 30 смыв Старт-Стоп',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Tango',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Останется в ассортименте',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое изменение произойдет с Sigma 20?',
                    'answers' => [
                        [
                            'name' => 'Изменится дизайн',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Будет снята с производства',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Уменьшиться размер клавиши',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Возможность установки заподлицо',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие возможны настройки клапана смыва в бачках Geberit?',
                    'answers' => [
                        [
                            'name' => 'Большой смыв 4.5/6/7.5',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Малый смыв 2/4',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Большой смыв 4/6/9',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Смыв 6/3',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Из какого материалы выполнены бачки Geberit?',
                    'answers' => [
                        [
                            'name' => 'PE',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'PP',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'PVC',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'PB',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Каким методом проверяют бачки Geberit на герметичность?',
                    'answers' => [
                        [
                            'name' => 'Давлением 10 атм',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Давлением 0.10 атм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Лазерными датчиками',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Давлением 15 атм',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой стандартный смыв применен в бачках Geberit?',
                    'answers' => [
                        [
                            'name' => 'Механический двойной смыв',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Механический смыв',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Пневматический двойной смыв',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Комбинированный смыв',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой материал для производства бачков скрытого монтажа самый надежный и долговечный?',
                    'answers' => [
                        [
                            'name' => 'PE',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'PP',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'PVC',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'PB',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Сколько лет Geberit производит бачки скрытого монтажа?',
                    'answers' => [
                        [
                            'name' => 'Более 50 лет',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Более 40 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Более 100 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Более 60 лет',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что значат буквы в артикулярных номерах клавиш Omega?',
                    'answers' => [
                        [
                            'name' => 'Сочетание цвета клавиши смыва',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Форму клавиши смыва',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Линейку клавиши смыва',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Порядковый номер',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой параметр изменился в бачкахмонтажных элементах Geberit Sigma 8 см?',
                    'answers' => [
                        [
                            'name' => 'Ширина монтажного элемента',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Толщина монтажного элемента',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Скорость монтажа',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Звукоизоляция в комплекте',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Клавиша смыва Type 70 это',
                    'answers' => [
                        [
                            'name' => 'Дистанционный смыв унитаза с гидросервоприводом',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Смыв с гидросервоприводом',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Пневматический двойной смыв унитаза',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дистанционная пневматический смыв унитаза',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как называется новая клавиша в линии Delta?',
                    'answers' => [
                        [
                            'name' => 'Delta 51',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Delta 50',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Delta 15',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Delta 20',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как работает система удаления запаха Duofresh?',
                    'answers' => [
                        [
                            'name' => 'Удаление запаха непосредственно из чаши унитаза через угольный фильтр',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Удаление запаха непосредственно из чаши унитаза через керамический фильтр',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Удаление запаха непосредственно из чаши унитаза через бумажный фильтр',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Удаление запаха непосредственно через вентиляцию помещения',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой материал клавиш смыва для Duofresh?',
                    'answers' => [
                        [
                            'name' => 'Пластик /Стекло',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Пластик /Метал',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Метал /Стекло',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Безопасное Стекло',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие возможны варианты высоты монтажного элемента Duofix Omega?',
                    'answers' => [
                        [
                            'name' => '82/98/112',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '82/98/114',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '98/101/114',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '101/112/114',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая толщина монтажного элемента Duofix Omega',
                    'answers' => [
                        [
                            'name' => '12 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '12.5 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '14 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '16 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'На сколько клавиша смыва Omega 20 меньше чем клавиша Sigma 20',
                    'answers' => [
                        [
                            'name' => '0.25',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '0.32',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Omega 20 и Sigma 20 одинаковые по размеру',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '0.2',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое важное отличие в новом комплекте Duofix Delta 458.126.00.1?',
                    'answers' => [
                        [
                            'name' => 'Не комплектуется клавишей смыва',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Не комплектуется звукоизолирующей прокладкой',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Не комплектуется крепежом к стене',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Комплектуется клавишей смыва Delta 20',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'В какой клавише смыва есть подсветка и и функция бесконтактного управления?',
                    'answers' => [
                        [
                            'name' => 'Sigma 80',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Sigma 70',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Sigma 60',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Sigma 40',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Для чего используется белая трубка сбоку в инсталляции Duofix?',
                    'answers' => [
                        [
                            'name' => 'Для подключения AquaClean',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Для подключения гигиенического душа',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Для подключения подвода воды',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Для подвода электричества',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что значит отметка 1 м на раме Duofix?',
                    'answers' => [
                        [
                            'name' => 'Отметка высоты установки в 1м до чистого пола',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Высота установки унитаза',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Допустимая регулировка высоты',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Высота установки ножек',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие особенности клавиши Sigma 70',
                    'answers' => [
                        [
                            'name' => 'Цельная поверхность клавиши и легкое нажатие',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Простота монтажа и большой выбор цветов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Бесконтактный смыв и современный дизайн',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Применение новой технологии обработки стекла',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Клавиша смыва Type 10 это',
                    'answers' => [
                        [
                            'name' => 'Дистанционная клавиша смыва унитаза с пневмоприводом',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Клавиша смыва с пневмоприводом',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Пневмо клавиша смыва писсуара',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дистанционная пневмо клавиша смыва писсуара',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Где расположен фильтр в системе Duofresh?',
                    'answers' => [
                        [
                            'name' => 'В откидной клавише смыва ',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'В откидной дверце в стене ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Фильтр интегрирован в трубу подвода воздуха',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'В этой системе нет фильтра, используются освежающие кубики',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'При покупке бесконтактных клавиш смыва Sigma 10, Sigma 80, какой элемент нужно заказать дополнительно?',
                    'answers' => [
                        [
                            'name' => 'Блок питания',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Электро привод',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Шнур соединения',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Блок для батареи',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие сантехнические модули Monolith изменили конструкцию в 2016 году?',
                    'answers' => [
                        [
                            'name' => 'Все Monolith для унитаза и Monolith для биде',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Все сантехнические модули Monolith Plus для унитаза',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Все сантехнические модули Monolith для умывальника и биде',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Все сантехнические модули Monolith для биде',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Из какого материалы выполнен бачок Geberit Delta?',
                    'answers' => [
                        [
                            'name' => 'PE',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'PP',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'PVC',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'PB',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие отличия между клавишами Sigma 60 и Omega 60?',
                    'answers' => [
                        [
                            'name' => 'Размер клавиш',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Размер и материал клавиш',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Размер и цвет клавиш',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Материал клавиш',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие отличия между клавишами Sigma 50 и Delta 50',
                    'answers' => [
                        [
                            'name' => 'Материал клавиш',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Размер клавиш',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Размер и функциональность клавиш',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Функциональность клавиш',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие отличия между клавишами Omega 20 и Delta 21',
                    'answers' => [
                        [
                            'name' => 'Размер клавиш',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Материал клавиш',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Материал и функциональность',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Функциональность клавиш',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какого цвета нет у Monolith',
                    'answers' => [
                        [
                            'name' => 'Серый',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Белый ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Песочный ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Черный',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая высота Monolith для унитаза',
                    'answers' => [
                        [
                            'name' => '101 и 114 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '101 и 112см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '98 и 112 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '82 и 98 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой вариант выдвижных ящиков может быть у сантехнического модуля Monolith для умывальника',
                    'answers' => [
                        [
                            'name' => 'Без ящиков, ящик слева, ящик справа или ящики обоих сторон',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Без ящиков, ящик слева или справа',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Без ящиков или ящики с обоих сторон',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Без ящиков',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как производится смыв у Monolith?',
                    'answers' => [
                        [
                            'name' => 'Клавиши смыв справа на верхней панели',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Клавиши смыв слева на верхней панели',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Клавиши смыва фронтально',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дистанционный смыв',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое отличие между Monolith и Monolith Plus для унитаза?',
                    'answers' => [
                        [
                            'name' => 'Удаление запахов',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Цвет стекла',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Простата монтажа',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Возможность мотажа биде',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое отличие между Monolith и Monolith Plus для унитаза?',
                    'answers' => [
                        [
                            'name' => 'Комфортная подсветка',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Цвет стекла',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Простата монтажа',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Возможность монтажа биде',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое отличие между Monolith и  Monolith Plus для унитаза?',
                    'answers' => [
                        [
                            'name' => 'Сенсорные клавиши смыва',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Цвет стекла',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Простата монтажа',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Возможность монтажа биде',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что нужно купить дополнительно для монтажа Monolith?',
                    'answers' => [
                        [
                            'name' => 'Ничего все в комплекте',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Ничего, кроме крепления к стене',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ничего, кроме звукоизолирующей прокладки',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только кнопку смыва',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое изменение в Monolith для унитаза произошло в 2016?',
                    'answers' => [
                        [
                            'name' => 'Сокращение времени монтажа',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Регулировка высоты',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Клавиша смыва',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Цвет подсветки',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Для чего используют бачок скрытого монтажа?',
                    'answers' => [
                        [
                            'name' => 'Для монтажа  с напольным унитазом',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Для монтажа с консольным унитазом',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Для монтажа с подвесным унитазом',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Для монтажа с напольным писсуаром',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая гарантия на Geberit Duofix для общественных сан.узлов?',
                    'answers' => [
                        [
                            'name' => '10 лет',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '7 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '15 лет',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что рекомендует компания Geberit использовать для монтажа писсуара',
                    'answers' => [
                        [
                            'name' => 'Инсталляцию Geberit Duofix для писсуара',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Инсталляцию Geberit Delta для писсуара',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Инсталляцию Geberit Sigma для писсуара',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Инсталляцию Geberit Omega для писсуара',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Где расположены компоненты системы удаления запахов DuoFresh',
                    'answers' => [
                        [
                            'name' => 'В кнопке Sigma 40',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'В стене',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'В инсталляции Duofix',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'В трубе соединения',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой принцип работы клавиши Sigma 80?',
                    'answers' => [
                        [
                            'name' => 'Беcконтактный двойной смыв',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Пневматический двойной смыв',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Механический смыв старт-стоп',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Комбинированный двойной смыв',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что нужно делать в случаи появления неисправности?',
                    'answers' => [
                        [
                            'name' => 'Обратится в сервис-центр',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Обратится в точку продажи',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Обратится к сантехнику ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Исправить неполадку самостоятельно согласно инструкции',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Входит ли в комплект угловой кран подвода воды к бачку?',
                    'answers' => [
                        [
                            'name' => 'Да входит 1/2"',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Да входит 3/8"',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Да входит, с адаптером',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Покупается дополнительно',
                            'is_correct' => false,
                        ],
                    ],
                ],
            ],
        ];

        Test::createFromArray($test_array);
    }
}
