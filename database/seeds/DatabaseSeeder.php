<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(InformationFieldsSeeder::class);
        $this->call(TechnicalFacultySeeder::class);
        $this->call(CommercialFacultySeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(TestSeeder::class);
        $this->call(OldUsers::class);
    }
}
