<?php

use Illuminate\Database\Seeder;
use App\Test;

class TestSeeder_2017_commercial_Geberit extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $test_array = [
            'name' => 'Новинки Geberit 2017',
            'faculty' => 'commercial',
            'points' => 100,
            'question_count' => 10,
            'duration' => 10,
            'start_date' => '2017-08-01 00:00:00',
            'end_date' => '2017-08-30 00:00:00',
            'questions' => [
                [
                    'name' => 'Основное отличие поверхности Setaplano от конкурентов?',
                    'answers' => [
                        [
                            'name' => 'Материал поверхности и комплексный подход к решению ',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Больше типоразмеров поверхности',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только квадратная форма поверхности и производительность сифона',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только прямоугольная форма поверхности и производительность сифона',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какого цвета поверхность Setaplano?',
                    'answers' => [
                        [
                            'name' => 'Только белая',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Разных цветов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Цвет на заказ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Три основных цвета',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как правильно подобрать поверхность Setaplano',
                    'answers' => [
                        [
                            'name' => 'Три артикула используя таблицу в каталоге',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Один артикул из каталога',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Один артикул и дополнительные артикула по требованию клиента',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Два артикула из таблицы в каталоге',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Сколько типоразмеров поверхности Setaplano?',
                    'answers' => [
                        [
                            'name' => '11',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '12',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '14',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '16',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Предусмотрена ли звукоизоляция в поверхности Setaplano?',
                    'answers' => [
                        [
                            'name' => 'Да, предустановлена на монтажной раме',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Нет ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет, поставляется отдельно',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Не требуется',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Предусмотрена ли гидроизоляция в поверхности Setaplano?',
                    'answers' => [
                        [
                            'name' => 'Да , интегрирована в поверхность Setaplano',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Нет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет, поставляется отдельно',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Не требуется',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'В какую сторону можно отводить воду сифоном поверхности Setaplano?',
                    'answers' => [
                        [
                            'name' => 'В любую, на 360 градусов',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Влево или вправо',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только назад',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только вперед',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие варианты монтажа поверхности Setaplano?',
                    'answers' => [
                        [
                            'name' => 'В уровень с полом',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'В уровень с полом и ниже уровня пола',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'В уровень с полом и выше уровня пола',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Любой вариант монтажа',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Чем достигается высокий уровень гигиены поверхности Setaplano?',
                    'answers' => [
                        [
                            'name' => 'Гладкая поверхность, без углов, нет мест для сбора грязи',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Сифон по центру, с решеткой для сбора грязи',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'С помощь решетка для сбора грязи',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сифон сбоку с решеткой для сбора грязи',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Где расположен сифон на поверхности Setaplano?',
                    'answers' => [
                        [
                            'name' => 'Сбоку у стены',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'По центру поверхности',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'В углу поверхности',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Скрытое решение',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как монтируется сифона поверхности Setaplano?',
                    'answers' => [
                        [
                            'name' => 'Специальное место фиксации в монтажной раме',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Сбоку, в удобном для монтажника месте на раме',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Не черновой пол в удобном месте',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Свободное размещение',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Каким образом осуществляется доступ к канализации поверхности Setaplano?',
                    'answers' => [
                        [
                            'name' => 'Через снятую крышку сифона напрямую к канализации',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'После демонтажа сифона',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Через ревизию снизу поверхности',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Через ревизию в стояке канализации',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'С каких элементов состоит комплект Setaplano?',
                    'answers' => [
                        [
                            'name' => 'Поверхность, монтажная рама, монтажный комплект',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Поверхность, ножки, сифон',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Поверхность, сифон, доп. материалы ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Поверхность, монтажная рама, сифон',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что входит в состав монтажного комплекта поверхности Setaplano?',
                    'answers' => [
                        [
                            'name' => 'Сифон, декоративная крышка, гребень для волос, ножки, крепления',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Сифон, гребень для волос, ножки',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сифон, ножки',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Декоративная крышка, гребень для волос, ножки , крепления',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая толщина прочного поверхностного слоя поверхности Setaplano?',
                    'answers' => [
                        [
                            'name' => 'Поверхность Setaplano, это литой однородный материал ',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '30 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '10 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '20 мм',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как называются серии новых инфракрасных смесителей Geberit?',
                    'answers' => [
                        [
                            'name' => 'Piave и Brenta',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Тип 185 и Тип 186',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Тип 60',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'HyTronic 185',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой формы новые инфракрасные смеситель Geberit?',
                    'answers' => [
                        [
                            'name' => 'Прямоугольной и округлой',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Округлой',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Прямоугольной',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дизайнерская',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как монтируется новый настенный инфракрасные смеситель Geberit?',
                    'answers' => [
                        [
                            'name' => 'Только на монтажный элемент Duofix',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Прямо на стену ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Используется любой монтажный комплект',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Используя монтажную коробку HansaVarox',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие варианты питания новых инфракрасных смеситель Geberit',
                    'answers' => [
                        [
                            'name' => 'Батарея, сеть 220В, генератор',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Батарея, сеть 220В, аккумулятор',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Батарея, сеть 220В',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сеть 220В, аккумулятор',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Чем достигается элегантный дизайн новых инфракрасных смесителей Geberit?',
                    'answers' => [
                        [
                            'name' => 'Функциональные части вынесены в отдельный блок',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Новые технологии проектирования и производства',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Новые материалы смесителей',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Составные части из Нано-деталей',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как регулируется температура воды новых инфракрасных смеситель Geberit?',
                    'answers' => [
                        [
                            'name' => 'С помощью винта в функциональном блоке',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Снаружи на смесителе',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Внутри смесителя (скрытая регулировка)',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Не регулируется',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие варианты монтажных элементов Duofix доступны для новых инфракрасных смеситель Geberit?',
                    'answers' => [
                        [
                            'name' => 'Duofix и Duofix со скрытым сифоном для настенного смесителя или смесителя на раковину',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Duofix для смесителя',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Стандартный Duofix с опцией для использования со смесителем',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет необходимости использовать Duofix',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие новые инфракрасные смесители Geberit можно использовать без монтажных элементов Duofix?',
                    'answers' => [
                        [
                            'name' => 'Смесители для монтажа на раковину',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Любой из новых смесителей не требует монтажа на Duofix',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только настенные смесители',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только с питанием от батареи.',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Можно ли использовать новые инфракрасные смесители Geberit для предварительно смешанной воды?',
                    'answers' => [
                        [
                            'name' => 'Да, можно',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Нет, нельзя',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только смесители на раковину',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только смесители на стену',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой длинны излив новых настенных инфракрасных смеситель Geberit?',
                    'answers' => [
                        [
                            'name' => '17 или 22 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '18 или 22 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '22 см ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '18 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая функциональная часть новых инфракрасных смеситель Geberit находится в изливе?',
                    'answers' => [
                        [
                            'name' => 'IR сенсор',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Клапан',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Блок смешения',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Обратный клапан',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Куда подводится питание сети 220В для новых инфракрасных смеситель Geberit ',
                    'answers' => [
                        [
                            'name' => 'К функциональному блоку под раковиной',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'К изливу смесителя',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'К Duofix сверху',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет возможности питания от сети',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие новые модели унитазов-биде появилась в ассортименте Geberit?',
                    'answers' => [
                        [
                            'name' => 'Mera Classic и Mera Comfort',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Sela Comfort',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Sela Plus',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'АС 8000+Comfort',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое отличие у новых моделей унитаза-биде Geberit AquaClean?',
                    'answers' => [
                        [
                            'name' => 'Mera Comfort имеет 3 дополнительные функции в сравнении с Mera Classic',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Дополнительная возможность выбора цвета у Mera Comfort',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Mera Comfort имеет 2 дополнительные функции в сравнении с Mera Classic',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Mera Comfort оснащен наружным бачком',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие допополнительные функции в унитазе-биде Geberit AquaClean Mera Comfort?',
                    'answers' => [
                        [
                            'name' => 'Поднятие крышки, подогрев сиденья и Comfortная подсветка',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Поднятие крышка, Comfortная подсветка и быстрый нагрев воды',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'подогрев сиденья, Comfortная подсветка и смыв с пульта ДУ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Быстрый нагрев воды, Поднятие крышки и смыв с пульта ДУ',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой смыв применен у модели унитаза-биде Geberit AquaClean Mera?',
                    'answers' => [
                        [
                            'name' => 'Турбо смыв',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Супер смыв',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Смерч технология',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Торнадо смыв',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что нового в форсунке биде унитаза-биде Geberit AquaClean Mera?',
                    'answers' => [
                        [
                            'name' => 'Новая технология струи обогащенная воздухом',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Технология подмешивания кислорода',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Высшая степень гигиеничности',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Скрытое расположение',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как изменилось время работы функции биде унитаза-биде Geberit AquaClean Mera?',
                    'answers' => [
                        [
                            'name' => 'Увеличено время работы до 50 сек',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Увеличено время работы до 60 сек',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Изменено время работы до 30 сек',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дополнительная функция регулировки времени работы',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие произошли изменения в системе удаления запахов унитаза-биде Geberit AquaClean Mera?',
                    'answers' => [
                        [
                            'name' => 'Применен новый керамический фильтр',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Применен новый угольный фильтр',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Увеличена производительность системы',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Простота применения',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Чем достигается максимальный гигиена использования дамского душа модели унитаза-биде Geberit AquaClean Mera?',
                    'answers' => [
                        [
                            'name' => 'Отдельная форсунка, открывается только при использовании',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Применение новой технологии подачи воды',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Применение специальных моющих средств',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Специальная программа чистки',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Срок службы фильтра системы удаления запахов унитаза-биде Geberit AquaClean Mera?',
                    'answers' => [
                        [
                            'name' => 'Примерно 5 лет',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Примерно 3 года',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Индикация использования через 1-2 года',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Индикация использования через 2-3 года',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие изменения в дизайне унитаза-биде Geberit AquaClean Mera?',
                    'answers' => [
                        [
                            'name' => 'Декоративная вставка двух цветов',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Округлая форма унитаза',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Прямоугольная форма унитаза',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Новый цвет крышки',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой датчик активирует функцию биде в Geberit AquaClean Mera?',
                    'answers' => [
                        [
                            'name' => 'Датчик веса',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'IR Сенсор ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Кнопка вкл.',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Кнопка выкл.',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как монтируется унитаз-биде Geberit AquaClean Mera?',
                    'answers' => [
                        [
                            'name' => 'На монтажный элемент Duofix',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'С помощью монтажного набора',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'С помощью монтажного комплекта',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Напольный монтаж',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие доступны варианты цвета унитаза-биде Geberit AquaClean Меrа?',
                    'answers' => [
                        [
                            'name' => 'Белый с хромированной или белой панелью',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Белый с хромированной панелью',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белый с нержавеющей или белой панелью',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белый с нержавеющей панелью',
                            'is_correct' => false,
                        ],
                    ],
                ],
            ],
        ];

        Test::createFromArray($test_array);
    }
}
