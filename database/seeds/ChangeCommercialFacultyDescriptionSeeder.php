<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ChangeCommercialFacultyDescriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faculties')->where('slug', 'commercial')->update(['description' => 'Обучение, программа мотивации и ценные призы для продавцов']);
    }
}
