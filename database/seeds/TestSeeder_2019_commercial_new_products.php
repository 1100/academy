<?php

use Illuminate\Database\Seeder;
use App\Test;

class TestSeeder_2019_commercial_new_products extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $test_array = [
            'name' => 'Новинки 2019',
            'description' => 'Тест по новинкам 2019 года брендов KOLO (GEO, MODO Pure) и Geberit (Acanto, VariForm, DuoFresh, CleanLine, SELA, клавиши смыва).',
            'faculty' => 'commercial',
            'points' => 600,
            'question_count' => 30,
            'duration' => 30,
            'start_date' => '2019-10-07 00:00:00',
            'end_date' => '2019-11-30 23:59:59',
            'questions' => [
                [
                    'name' => 'Как называется новая душевая кабинка KOLO?',
                    'answers' => [
                        [
                            'name' => 'Fresh Soft',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Atol Plus',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Standard',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'GEO',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая толщина стекла используется для кабинки GEO?',
                    'answers' => [
                        [
                            'name' => '4,5 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5,5 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '6 мм',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Сколько продуктов в портфолио кабинки GEO?',
                    'answers' => [
                        [
                            'name' => '10',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '14',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '18',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '16',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Сколько артикулов нужно использовать для заказа кабины GEO?',
                    'answers' => [
                        [
                            'name' => 'Два артикула (часть А и часть В)',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Четыре артикула',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Три артикула',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Один артикул',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое стандартное покрытие для стеклянных элементов кабинки GEO?',
                    'answers' => [
                        [
                            'name' => 'Без дополнительного покрытия',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'KeraTect',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Светоотражающая плёнка',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Reflex',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой цвет профиля в кабинах GEO?',
                    'answers' => [
                        [
                            'name' => 'Белый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Матовый хром',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Чёрный',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Серебряный блеск',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие ручки используются в кабинах GEO?',
                    'answers' => [
                        [
                            'name' => 'Круглые',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Квадратные',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Релинг L=20 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Интегрированные в профиль кабины',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какого цвета стеклянные части кабины GEO?',
                    'answers' => [
                        [
                            'name' => 'Передние стенки - прозрачное стекло, задние стенки - матовое стекло',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Матовое стекло',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Узорчатое стекло',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Прозрачное стекло',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие размеры кабин GEO?',
                    'answers' => [
                        [
                            'name' => '80х90 см, 90х90 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '90х90 см, 100х100 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '90х80 см, 90х90 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '80х80 см, 90х90 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Как можно охарактеризовать двери Pivot по принципу открытия?',
                    'answers' => [
                        [
                            'name' => 'Складные двери',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Раздвижные двери',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Роллетные двери',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Поворотные двери',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой угол открытия дверей Pivot?',
                    'answers' => [
                        [
                            'name' => '75 градусов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '80 градусов ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '100 градусов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '90 градусов',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Как можно охарактеризовать двери Bifold по принципу открытия?',
                    'answers' => [
                        [
                            'name' => 'Раздвижные двери',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Поворотные двери',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Роллетные двери',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Складные двери',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие размеры дверей Pivot и Bifold в серии GEO?',
                    'answers' => [
                        [
                            'name' => '90 и 100 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '80 и 100 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '85 и 95 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '80 и 90 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие размеры раздвижных дверей GEO?',
                    'answers' => [
                        [
                            'name' => '90, 105, 115, 135 и 155 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '95, 115, 125, 145 и 165 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '100, 115, 130 и 150 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '100, 110, 120, 140 и 160 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие размеры боковых стенок GEO?',
                    'answers' => [
                        [
                            'name' => '90 и 100 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '80 и 100 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '85 и 95 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '80 и 90 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Из скольких элементов-секций состоит ширма для ванны GEO?',
                    'answers' => [
                        [
                            'name' => 'Двухэлементная',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Трёхэлементная',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Четырёхэлементная',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Одноэлементная',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой размер ширмы GEO для ванны?',
                    'answers' => [
                        [
                            'name' => '140х90 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '120х80 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '160х90 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '150х80 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие новинки представлены в серии MODO?',
                    'answers' => [
                        [
                            'name' => 'Умывальник 120 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Напольное биде',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Унитаз напольный Rimfree',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Подвесной унитаз Rimfree и подвесное биде со скрытыми креплениями',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Как называются новые изделия серии MODO?',
                    'answers' => [
                        [
                            'name' => 'MODO Plus',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'MODO Pro',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'MODO Soft',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'MODO Pure',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое главное отличие нового подвесного унитаза MODO от старой модели?',
                    'answers' => [
                        [
                            'name' => 'Размеры',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Форма ободка чаши',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Слив',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Скрытые крепления',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Как называется новая коллекция керамики и мебели Geberit?',
                    'answers' => [
                        [
                            'name' => 'Fidelio',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Status',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Fresh',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Acanto',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие умывальники Acanto поставляются с комплектом быстрого монтажа Geberit EFF1?',
                    'answers' => [
                        [
                            'name' => '65 и 75 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '45 и 75 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '60 и 65 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '90 и 120 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие умывальники Acanto имеют боковые полочки для хранения туалетных принадлежностей?',
                    'answers' => [
                        [
                            'name' => '45 и 65 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '60 и 65 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '90 и 120 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '75 и 90 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой размер асимметричного умывальника Acanto?',
                    'answers' => [
                        [
                            'name' => '75 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '120 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '65 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '90 cм',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие умывальники Acanto имеют встроенную керамическую систему перелива?',
                    'answers' => [
                        [
                            'name' => '90 и 120 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '60 и 65 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '45 и 60 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '60 и 75 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие размеры имеют рукомойники Acanto?',
                    'answers' => [
                        [
                            'name' => '38 и 42 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '42 и 48 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '32 и 35 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '40 и 45 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите размеры встраиваемых умывальников Acanto',
                    'answers' => [
                        [
                            'name' => '55, 65 и 80 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '50, 70 и 80 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '55, 85 и 95 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '60, 75 и 90 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Чем отличаются встраиваемые умывальники Acanto от других умывальников серии?',
                    'answers' => [
                        [
                            'name' => 'Наличием перелива',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Формой',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Размерами',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Тонким бортом',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой аксессуар поставляется в комплекте с умывальниками Acanto со встроенной системой перелива?',
                    'answers' => [
                        [
                            'name' => 'Сифон',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Комплект креплений',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Декоративная мыльница',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Крышка для сливного отверстия',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая глубина (расстояние от переднего края умывальника до стены) укороченных умывальников Acanto?',
                    'answers' => [
                        [
                            'name' => '48 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '45 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '46 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '42 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите основные преимущества напольных унитазов Acanto',
                    'answers' => [
                        [
                            'name' => 'Глазурованное смывное кольцо, воронкообразная чаша, дюропластовое сиденье',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Современный дизайн, качественные комплектующие ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Прямоугольная форма, горизонтальный выпуск',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Приставные к стене, Rimfree, мультивыпуск, скрытые крепления',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая глубина (расстояние от переднего края унитаза до стены) подвесных и напольных унитазов Acanto?',
                    'answers' => [
                        [
                            'name' => '56 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '53 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '48 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '51 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая высота напольных унитазов и биде Acanto?',
                    'answers' => [
                        [
                            'name' => '45 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '51 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '48 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '42 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Из каких материалов изготавливаются сиденья для унитазов Acanto?',
                    'answers' => [
                        [
                            'name' => 'АВС-пластик ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'МДФ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дюропласт без антибактериального покрытия',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дюропласт с антибактериальным покрытием',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите виды сидений для унитазов Acanto',
                    'answers' => [
                        [
                            'name' => 'АВС-пластик с металлическими креплениями, дюропластовое с микролифтом',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сиденье из МДФ с металлическими креплениями, дюропластовое с металлическими креплениями',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дюропластовое с микролифтом',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дюропластовое с металлическими креплениями, дюропластовое с микролифтом, дюропластовое с микролифтом и функцией быстрого снятия',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Из каких материалов изготавливаются тумбочки и шкафчики Acanto?',
                    'answers' => [
                        [
                            'name' => 'Влагостойкое ДСП',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Влагостойкое МДФ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'ДСП и МДФ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Влагостойкие плиты высокой плотности ХДФ, фасады облицованы стеклянными панелями',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите цвета мебели Acanto',
                    'answers' => [
                        [
                            'name' => 'Белый, серый, натуральный дуб',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белый, светлый дуб, тёмный дуб',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белый, коричневый, вишня',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белый, лава, чёрный, песочно-серый',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите виды тумбочек под умывальники Acanto',
                    'answers' => [
                        [
                            'name' => 'С одной дверцей, с одним выдвижным ящиком, с двумя выдвижными ящиками',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'С одной дверцей, с двумя дверцами, с двумя выдвижными ящиками',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'С одним выдвижным ящиком и одним внутренним ящиком, с двумя выдвижными ящиками и двумя внутренними ящиками',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'С одной дверцей, с одним выдвижным ящиком и одним внутренним ящиком, с двумя выдвижными ящиками и двумя внутренними ящиками',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите главные преимущества конструкции тумбочек под умывальники Acanto',
                    'answers' => [
                        [
                            'name' => 'Механизм плавного закрывания, простой монтаж',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Современный дизайн, оптимизированное пространство для хранения',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Механизм плавного закрывания, универсальность использования',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Механизм плавного закрывания, 4D регулирование фронтальных панелей, механизм предотвращения выпадения ящиков',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие дополнительные аксессуары можно заказать для тумбочек под умывальники Acanto?',
                    'answers' => [
                        [
                            'name' => 'Органайзеры для внутренних выдвижных ящиков, сифон',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сифон',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ножки',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Органайзеры для внутренних выдвижных ящиков, ножки',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие аксессуары поставляются в комплекте с тумбочками для умывальников Acanto?',
                    'answers' => [
                        [
                            'name' => 'Крепёж',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Декоративные заглушки',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сифон',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Бутылочный сифон, крепёж',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите типы высоких боковых шкафчиков Acanto',
                    'answers' => [
                        [
                            'name' => 'Высокий шкафчик с одной дверцей, высокий шкафчик с двумя дверцами',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Высокий ящик с одной дверцей, высокий ящик с одной дверцей и двумя выдвижными ящиками',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Высокий шкафчик с двумя дверцами',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Высокий шкафчик с двумя дверями, шкафчик высокий плоский с одной дверцей, шкафчик-карго',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая главная особенность высокого бокового шкафчика с двумя дверцами Acanto?',
                    'answers' => [
                        [
                            'name' => 'Дверцы с механизмом плавного закрывания',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Возможность установки дверей на правую или левую сторону',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Зеркало на внутренней стороне верхней дверцы',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Внутренний запирающийся ящик и металлические полки на внутренней стороне верхней дверцы',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите особенности конструкции низкого бокового шкафчика Acanto',
                    'answers' => [
                        [
                            'name' => '1 дверца, 2 полки',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '2 выдвигающихся ящика',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '1 выдвигающийся ящик',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '1 выдвигающийся ящик, 1 внутренний ящик, стеклянная верхняя поверхность ',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите размеры зеркальных шкафчиков Acanto',
                    'answers' => [
                        [
                            'name' => '65, 90 и 120 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '75, 90 и 120 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '70, 100 и 130 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '60, 75 и 90 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Входят ли в стандартный комплект поставки зеркального шкафчика Acanto внутреннее и увеличительное зеркало?',
                    'answers' => [
                        [
                            'name' => 'Входит только внутреннее зеркало',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Не входят, это опционные продукты',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Входит только увеличительное зеркало',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Входят',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие преимущества универсальной полочки Acanto?',
                    'answers' => [
                        [
                            'name' => 'Лёгкий монтаж',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Крючки для полотенец',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Подсветка',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дополнительный объем для хранения, 3 крючка для полотенец в комплекте',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое назначение универсальной магнитной доски для ванной комнаты Acanto?',
                    'answers' => [
                        [
                            'name' => 'Для дизайна',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Для заметок',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дополнительная защита от влаги',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Для хранения туалетных принадлежностей в удобном месте',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Что входит в комплект универсальной магнитной доски для ванной комнаты Acanto?',
                    'answers' => [
                        [
                            'name' => 'Крепёж',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Набор магнитов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Декоративные рамки',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Три металлических бокса для хранения, двусторонний скотч для крепления доски к стене',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Как называется новая коллекция встраиваемых умывальников Geberit?',
                    'answers' => [
                        [
                            'name' => 'Varicor',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Various',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Velvet',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'VariForm',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Сколько моделей умывальников входит в новую серию VariForm?',
                    'answers' => [
                        [
                            'name' => '35',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '84',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '63',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '26',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Умывальники каких геометрических форм входят в серию VariForm?',
                    'answers' => [
                        [
                            'name' => 'Круг, овал, прямоугольник',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Круг, овал, квадрат, прямоугольник',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Круг, треугольник, прямоугольник',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Круг, овал, эллипс, прямоугольник',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Из какого материала изготавливаются умывальники VariForm?',
                    'answers' => [
                        [
                            'name' => 'Сантехнический фаянс',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Бисквитный фарфор',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Композитный материал Varicor',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сантехнический фарфор',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'При разработке конструкции встраиваемых умывальников VariForm были учтены актуальные дизайнерские тенденции. Назовите главную из них.',
                    'answers' => [
                        [
                            'name' => 'Скрытый перелив',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дополнительное пространство для размещения туалетных принадлежностей',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ассиметричная форма',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Тонкий борт ',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите варианты установки смесителя для встраиваемых умывальников VariForm',
                    'answers' => [
                        [
                            'name' => 'Только на умывальник',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только на столешницу',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только монтаж в стену',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'На умывальник, на столешницу, монтаж в стену',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите способы монтажа встраиваемых умывальников VariForm',
                    'answers' => [
                        [
                            'name' => 'Только на столешницу',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'На столешницу и в столешницу',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'На столешницу и под столешницу',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'На столешницу, в столешницу, под столешницу',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Основное отличие новой системы удаления запаха DuoFresh для бачков Geberit?',
                    'answers' => [
                        [
                            'name' => 'Использование для широкого ассортимента клавиш Sigma',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Использование для широкого ассортимента клавиш Delta',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Использование для широкого ассортимента клавиш Omega',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Использование для широкого ассортимента клавиш Delta и Sigma',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Основное отличие новой системы удаления запаха DuoFresh для бачков Geberit?',
                    'answers' => [
                        [
                            'name' => 'Светодиодная подсветка для легкой ориентации',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Сенсорное управление',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Подсветка кнопки смыва при задействовании',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Подсветка кнопок смыва',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Основное отличие новой системы удаления запаха DuoFresh для бачков Geberit?',
                    'answers' => [
                        [
                            'name' => 'Две модели с автоматической и ручной активацией',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Модель с автоматической активацией',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Модель с ручной активацией',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Программируемая активация',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Основное отличие новой системы удаления запаха DuoFresh для бачков Geberit?',
                    'answers' => [
                        [
                            'name' => 'Использование нового керамического фильтра',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Использование нового угольного фильтра',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Фильтр не изменился',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Новый принцип фильтрования',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'С какими бачками можно использовать системы удаления запаха DuoFresh для бачков Geberit?',
                    'answers' => [
                        [
                            'name' => 'С бачками Sigma 12 и 8 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'С бачками Sigma 8 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'С бачками Sigma 12 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Со всеми бачками Geberit',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как заменить фильтр в системе удаления запаха DuoFresh?',
                    'answers' => [
                        [
                            'name' => 'Сдвинуть клавишу смыва вправо и заменить фильтр',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Сдвинуть клавишу смыва влево и заменить фильтр',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Откинуть клавишу как в предыдущей модели и заменить фильтр',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Опустить клавишу вниз и заменить фильтр',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Из какого материалы выполнены клавиши для новой системы удаления запаха DuoFresh?',
                    'answers' => [
                        [
                            'name' => 'Могут быть использованы различные клавиши из различных материалов Sigma 01, 10, 20, 21,30,50',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Только специальные клавиши для системы DuoFresh: стекло и пластик',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Могут быть использованы различные пластиковые клавиши Sigma',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Могут быть использованы различные стеклянные клавиши Sigma',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как монтируется клавиша смыва с новой системой удаления запаха DuoFresh?',
                    'answers' => [
                        [
                            'name' => 'Устанавливается традиционно на сдвижную рамку из комплекта системы',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Устанавливается традиционно на рамку без использования инструмента',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Устанавливается традиционно, прикручиваясь винтами на рамку ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'На уже предустановленной рамке, как и ранее',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой вариант цвета сдвижной рамки в новой системе удаления запаха DuoFresh?',
                    'answers' => [
                        [
                            'name' => 'Хром и антрацит (серый)',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Хром глянцевый ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Хром матовый ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белый и черный',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Можно ли использовать дезинфицирующие картриджи в новой системе удаления запаха DuoFresh?',
                    'answers' => [
                        [
                            'name' => 'Да можно, они в форме цилиндриков',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Да можно, как и ранее, они в форме кубиков',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет такой функции, можно добавить опционально',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет, нельзя ',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как использовать только функцию дезинфицирующих картриджей?',
                    'answers' => [
                        [
                            'name' => 'Для этого есть отдельный продукт, рамка-вставка для картриджей',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Отключить функцию фильтрации в приложении',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Убрать фильтр из кнопки',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Отключить питание системы',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Когда появилась новая система удаления запаха DuoFresh?',
                    'answers' => [
                        [
                            'name' => 'С 1.07.2019',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'С 1.04.2019',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'С 1.01.2020',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'С 1.01.2019',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой цвет Клавиш смыва появился в 2019?',
                    'answers' => [
                        [
                            'name' => 'Черный матовый',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Черный глянцевый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Темно-серый матовый ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Темно-серый глянцевый',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой цвет Клавиш смыва появился в 2019?',
                    'answers' => [
                        [
                            'name' => 'Белый матовый',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Белый глянцевый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белый альпин',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белый евро',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой цвет Клавиш смыва появился в 2019?',
                    'answers' => [
                        [
                            'name' => 'Новый хром матовый, не оставляющий отпечатков',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Новый хром глянцевый, не оставляющий отпечатков',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Новый белый глянцевый, не оставляющий отпечатков',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Новый черный глянцевый, не оставляющий отпечатков',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'В каких клавишах доступен новый хромированный цвет, не оставляющий отпечатков?',
                    'answers' => [
                        [
                            'name' => 'Sigma 20 и Sigma 30',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Sigma 10 и Sigma 21',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Sigma 21 и Sigma 50',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Sigma 50',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'В каких клавишах доступен новый черный цвет?',
                    'answers' => [
                        [
                            'name' => 'Sigma 10, Sigma 20, Sigma30 и Sigma30 старт-стоп',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Sigma 20 и Sigma30',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Sigma 10 и Sigma 20',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Sigma 20 и Sigma30, Sigma30 старт-стоп',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'В каких клавишах доступен новый белый цвет?',
                    'answers' => [
                        [
                            'name' => 'Sigma 10, Sigma 20, Sigma30 и Sigma30 старт-стоп',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Sigma 20 и Sigma30',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Sigma 10 и Sigma 20',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Sigma 20 и Sigma30, Sigma30 старт-стоп',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Есть ли металлические клавиши смыва в новом белом цвете?',
                    'answers' => [
                        [
                            'name' => 'Да, Sigma 10 и Sigma30 старт-стоп',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Да, Sigma 10 и Sigma30',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Да, Sigma 20 и Sigma30',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Есть ли металлические клавиши смыва в новом черном цвете?',
                    'answers' => [
                        [
                            'name' => 'Да, Sigma 10 и Sigma30 старт-стоп',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Да, Sigma 10 и Sigma30',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Да, Sigma 20 и Sigma30',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Есть ли пластиковые клавиши смыва в новом черном цвете?',
                    'answers' => [
                        [
                            'name' => 'Да, Sigma 20 и Sigma30',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Да, Sigma 10 и Sigma30 старт-стоп',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Да, Sigma 20 и Sigma30 старт-стоп',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Есть ли пластиковые клавиши смыва в новом белом цвете?',
                    'answers' => [
                        [
                            'name' => 'Да, Sigma 20 и Sigma30',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Да, Sigma 10 и Sigma30 старт-стоп',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Да, Sigma 20 и Sigma30 старт-стоп',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что нового в клавише Sigma50?',
                    'answers' => [
                        [
                            'name' => 'Новый дизайн кнопок смыва',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Новый цвет кнопок смыва',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Новый дизайн рамки',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ничего не изменилось',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что нового в клавише Sigma50?',
                    'answers' => [
                        [
                            'name' => 'Новый материал отделки - натуральный камень Мустанг',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Новый материал отделки - натуральный камень Гранит',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Новый материал отделки - натуральный камень Мрамор',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Новый материал отделки - натуральная кожа',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что нового в клавише Sigma50?',
                    'answers' => [
                        [
                            'name' => 'Новый цвет отделки - стекло песок',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Новый цвет отделки - стекло салатовый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Новый цвет отделки - стекло белый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Новый цвет отделки - стекло синий',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Есть ли рамка у новой клавиши Sigma50?',
                    'answers' => [
                        [
                            'name' => 'Нет, дизайн без рамки',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Да, хромированная толщиной 2 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Да, только для исполнения с натуратьным камнем',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Да, только для исполнения со стеклом',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Есть ли в новой клавише  Sigma50 опция отделки материалом заказчика?',
                    'answers' => [
                        [
                            'name' => 'Да, есть клавиша смыва под индивидуальную отделку',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Нет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Варианты отделки только из ассортимента каталога',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Все клавиши могут быть использованы для индивидуальной отделки',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как изменился артикулярный номер новой клавиши смыва Sigma50?',
                    'answers' => [
                        [
                            'name' => 'Последняя цифра с 5 поменялась на 2',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Последняя цифра с 5 поменялась на 6',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Последняя цифра с 5 поменялась на 1',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ничего не изменилось',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Можно ли установить новую клавишу смыва Sigma50 на новую система удаления запаха DuoFresh?',
                    'answers' => [
                        [
                            'name' => 'Да, можно',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Нет, нельзя ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Да, можно. Только в исполнении индивидуальной отделки',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Да, можно. Только в исполнении с рамкой',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'На какую клавишу смыва можно поменять старые клавиши смыва Twinline?',
                    'answers' => [
                        [
                            'name' => 'На новую клавишу Twinline30',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'На новую клавишу Twinline20',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'На новую клавишу Twinline21',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ничего не поменялось',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'На какие клавиши смыва похожа (адаптирован дизайн) новая клавиша смыва Twinline?',
                    'answers' => [
                        [
                            'name' => 'Адаптирован к Sigma30 / Omega30',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Адаптирован к Sigma20 / Omega20',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Адаптирован к Delta21',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Схожа с старой клавишей смыва Twinline',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что нового в линейном трапе CleanLine20?',
                    'answers' => [
                        [
                            'name' => 'Новая длина 30-160 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Новая длина 20-160 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Новая длина 20-150 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Новая длина 30-150 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что нового в линейном трапе CleanLine20?',
                    'answers' => [
                        [
                            'name' => 'Новая длина ',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Новый цвет ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Новый материал',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ничего не изменилось',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что изменилось в монтаже нового линейного трапа CleanLine20?',
                    'answers' => [
                        [
                            'name' => 'Ничего не изменилось',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Изменилась конструкция монтажного комплекта',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Изменился диаметр подключения',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Изменилась высота монтажа',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что нового в чаше унитаза-биде AquaClean Sela?',
                    'answers' => [
                        [
                            'name' => 'Чаша без ободка с технологией TurboFlush',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Чаша без ободка с технологией Rimfree',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Чаша без ободка с технологией Tornado',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ничего не изменилось',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что нового в названии унитаза-биде AquaClean Sela?',
                    'answers' => [
                        [
                            'name' => 'Название не изменилось',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Добавилось слово унитаз-биде',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Добавилось слово AquaClean',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Добавилось слово Sela',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая новая функция в унитаза-биде AquaClean Sela?',
                    'answers' => [
                        [
                            'name' => 'Отдельная форсунка для Lady в защищенном исходном положении',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Форсунка может быть установлена на пять различных положений',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Анальный душ с технологией WhirlSpray и регулируемой интенсивностью распыления',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Регулируемая колебательная струя',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая новая функция в унитаза-биде AquaClean Sela?',
                    'answers' => [
                        [
                            'name' => 'Мгновенный водонагреватель для немедленной теплой воды',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Форсунка может быть установлена на пять различных положений',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Анальный душ с технологией WhirlSpray и регулируемой интенсивностью распыления',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Регулируемая колебательная струя',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая новая функция в унитаза-биде AquaClean Sela?',
                    'answers' => [
                        [
                            'name' => 'Подсветка',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Форсунка может быть установлена на пять различных положений',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Анальный душ с технологией WhirlSpray и регулируемой интенсивностью распыления',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сиденье для унитаза и крышка для унитаза из дюропласта с SoftClosing и Quick Release',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая новая функция в унитаза-биде AquaClean Sela?',
                    'answers' => [
                        [
                            'name' => 'Программа удаления накипи',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Скрытые соединения (дополнительные внешние соединения через керамическое отверстие снизу)',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Анальный душ с технологией WhirlSpray и регулируемой интенсивностью распыления',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Регулируемая колебательная струя',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая новая функция в унитаза-биде AquaClean Sela?',
                    'answers' => [
                        [
                            'name' => 'Функции и настройки с помощью пульта дистанционного управления (без панели управления)',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Форсунка может быть установлена на пять различных положений',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Скрытые соединения (дополнительные внешние соединения через керамическое отверстие снизу)',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сиденье для унитаза и крышка для унитаза из дюропласта с SoftClosing и Quick Release',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое покрытие чаши унитаза-биде AquaClean Sela?',
                    'answers' => [
                        [
                            'name' => 'Покрытие KeraTect',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Покрытие Reflex',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Покрытие Reflex Kera',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Покрытие Reflex Kera Tect',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Для какой цели у нового унитаза-биде AquaClean Sela есть подсветка?',
                    'answers' => [
                        [
                            'name' => 'Комфортно ориентироваться ночью',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Показывает этап работы системы',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Показывает состояние системы',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет такой функции',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какими функциями можно управлять с боковой панели управления на новом унитазе-биде AquaClean Sela?',
                    'answers' => [
                        [
                            'name' => 'Боковой панели управления нет на новом унитазе-биде AquaClean Sela',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Можно управлять функцией биде',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Можно управлять функцией сушка феном и биде',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Можно управлять всеми функциями унитаза-биде',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как можно управлять новом унитазом-биде AquaClean Sela?',
                    'answers' => [
                        [
                            'name' => 'При помощи пульта ДУ из комплекта, со смартфона или с дополнительной стационарной панели на стене',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'При помощи пульта ДУ из комплекта, со смартфона или боковой панели на унитазе-биде',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'При помощи пульта ДУ из комплекта, со смартфона',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Со смартфона или с дополнительной стационарной панели на стене',
                            'is_correct' => false,
                        ],
                    ],
                ],
            ],
        ];

        Test::createFromArray($test_array);
    }
}
