<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InformationFieldsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(InformationTypesSeeder::class);

        DB::table('information_fields')->insert([
            [
                'slug' => 'email',
                'name' => 'Email',
                'description' => 'Email адрес',
                'login' => true,
                'hidden' => false,
                'default_value' => null,
                'type' => 'email'
            ],
            [
                'slug' => 'phone',
                'name' => 'Телефон',
                'description' => 'Номер телефона',
                'login' => true,
                'hidden' => false,
                'default_value' => null,
                'type' => 'phone'
            ],
            [
                'slug' => 'facebook',
                'name' => 'Facebook',
                'description' => 'Акаунт Facebook',
                'login' => true,
                'hidden' => false,
                'default_value' => null,
                'type' => 'facebook'
            ],
            [
                'slug' => 'first_name',
                'name' => 'Имя',
                'description' => '',
                'login' => false,
                'hidden' => false,
                'default_value' => null,
                'type' => 'string'
            ],
            [
                'slug' => 'last_name',
                'name' => 'Фамилия',
                'description' => '',
                'login' => false,
                'hidden' => false,
                'default_value' => null,
                'type' => 'string'
            ],
            [
                'slug' => 'father_name',
                'name' => 'Отчество',
                'description' => '',
                'login' => false,
                'hidden' => false,
                'default_value' => null,
                'type' => 'string'
            ],
            [
                'slug' => 'city',
                'name' => 'Город',
                'description' => '',
                'login' => false,
                'hidden' => false,
                'default_value' => null,
                'type' => 'string'
            ],
            [
                'slug' => 'region',
                'name' => 'Область',
                'description' => '',
                'login' => false,
                'hidden' => false,
                'default_value' => null,
                'type' => 'string'
            ],
            [
                'slug' => 'city_residence',
                'name' => 'Город проживания',
                'description' => '',
                'login' => false,
                'hidden' => false,
                'default_value' => null,
                'type' => 'string'
            ],
            [
                'slug' => 'channel',
                'name' => 'Канал',
                'description' => '',
                'login' => false,
                'hidden' => true,
                'default_value' => 'Сторонний',
                'type' => 'string'
            ],
            [
                'slug' => 'college',
                'name' => 'ПТУ',
                'description' => 'Название ПТУ',
                'login' => false,
                'hidden' => false,
                'default_value' => null,
                'type' => 'string'
            ],
            [
                'slug' => 'college_curator',
                'name' => 'Куратор группы',
                'description' => 'ПТУ куратор группы',
                'login' => false,
                'hidden' => false,
                'default_value' => null,
                'type' => 'string'
            ],
            [
                'slug' => 'education',
                'name' => 'Образование',
                'description' => '',
                'login' => false,
                'hidden' => false,
                'default_value' => null,
                'type' => 'string'
            ],
            [
                'slug' => 'experience',
                'name' => 'Стаж роботы',
                'description' => '',
                'login' => false,
                'hidden' => false,
                'default_value' => null,
                'type' => 'string'
            ],
            [
                'slug' => 'shop_name',
                'name' => 'Магазин',
                'description' => '',
                'login' => false,
                'hidden' => false,
                'default_value' => null,
                'type' => 'string'
            ]
        ]);
    }
}
