<?php

use Illuminate\Database\Seeder;
use App\AdminPermission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles_permissions = [
            'admin' => [
                'admins',
                'user-information',
                'news',
                'support-messages',
                'users',
                'rating',
                'test-session',
                'faculty-user-information',
                'faculty-settings',
                'faculty-display-settings',
                'tasks',
                'subscribers',
            ],
            'manager' => [
                'news',
                'faculty-user-information',
                'faculty-settings',
                'tasks',
            ],
            'curator' => [
                'support-messages',
                'users',
                'rating',
                'faculty-display-settings',
            ]
        ];

        AdminPermission::createAndAttachFromArray($roles_permissions);
    }
}
