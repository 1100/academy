<?php

use Illuminate\Database\Seeder;
use App\Test;

class TestSeeder_2019_technical_piping extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $test_array = [
            'name' => 'Трубопроводные системы',
            'faculty' => 'technical',
            'points' => 200,
            'question_count' => 20,
            'duration' => 20,
            'start_date' => '2019-08-15 00:00:00',
            'end_date' => '2019-11-30 23:59:59',
            'questions' => [
                [
                    'name' => 'Диаметр условного прохода трубы 1/2" соответствует диаметру трубы Mapress: ',
                    'answers' => [
                        [
                            'name' => '18 мм',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '15 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '22 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '25 мм',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Из какого металла рекомендуется использовать трубы для питьевой воды?',
                    'answers' => [
                        [
                            'name' => 'Нержавеющая сталь ',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Оцинкованная сталь',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сплав CuNiFe',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Алюминий',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что означает цвет индикатора опрессовки на фитингах Geberit Mapress?',
                    'answers' => [
                        [
                            'name' => 'Материал фитинга',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Место установка пресс-инструмента',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Диаметр фитинга',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Защищает фитинг от коррозии',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что означает синий цвет индикатора на фитинге?',
                    'answers' => [
                        [
                            'name' => 'Нержавеющая сталь ',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Медь',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Оцинкованная сталь',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сплав CuNiFe',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что означает красный цвет индикатора на фитинге?',
                    'answers' => [
                        [
                            'name' => 'Оцинкованная сталь',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Нержавеющая сталь ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Медь',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сплав CuNiFe',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Для чего предназначена полиэтиленовая заглушка на фитингах Geberit Mapress?',
                    'answers' => [
                        [
                            'name' => 'Предотвращения попадания грязи',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Для привлекательного внешнего вида',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'От порезов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Для герметизации',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Сколько раз можно отпрессовать фитинг Geberit Mapress?',
                    'answers' => [
                        [
                            'name' => 'Один раз',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '2 раза',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '3 раза',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Много раз',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Трубопроводная система Mapress из углеродистой стали с фитингами с черным уплотнительным кольцом (CIIR) рассчитана на рабочую температуру',
                    'answers' => [
                        [
                            'name' => '120°С',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '95°С',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '180°С',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '220°С',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое максимальное рабочее давление системы Geberit Mapress из нержавеющей стали для горячего водоснабжения?',
                    'answers' => [
                        [
                            'name' => '16 атм.',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '6 атм.',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '10 атм.',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '100 атм.',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Для опрессовки каких диаметров фитингов Mapress используется обжимное кольцо с адаптером?',
                    'answers' => [
                        [
                            'name' => '35 мм и выше',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '54 мм и выше',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '76 мм и выше',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только для 108 мм',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Для чего используется шаблон с синим маркером?',
                    'answers' => [
                        [
                            'name' => 'Отмечать глубину захода трубы в фитинг',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Отмерять диаметр фитинга',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Отмерять диаметр трубы',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Отмерять глубину зачистки трубы ',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой инструмент используется при монтаже трубопроводной системы Mapress?',
                    'answers' => [
                        [
                            'name' => 'Инструмент Geberit и пресс-губки Geberit для инструмента',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Любой пресс- инструмент',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Любой проверенный компанией Geberit пресс-инструмент',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Пресс-губки Geberit для инструмента ',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Чем отличается выполнение пресс соединения d 108 мм от остальных диаметров?',
                    'answers' => [
                        [
                            'name' => 'Соединение выполняется с применением 2х адаптеров ',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Соединение выполняется с применением 2х обжимных колец',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Более трудоемкий процесс',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ничем не отличается',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой совместимости электроинструмент можно использовать для опрессовки фитингов Mapress d76-108 мм?',
                    'answers' => [
                        [
                            'name' => 'Использовать [2XL] или [3] совместимости',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Любой пресс-инструмент Geberit',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только [1] совместимости',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только [2] совместимости',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как выполнить переход с трубы системы Mapress на резьбу?',
                    'answers' => [
                        [
                            'name' => 'Соединить с пресс-фитингом адаптером с резьбой',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Нарезать резьбу',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Приварить резьбу',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Невозможен переход на резьбу',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Возможно ли сгибать трубы системы Mapress?',
                    'answers' => [
                        [
                            'name' => 'Да, можно при выполнении требований производителя',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Можно гнуть',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нельзя гнуть',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Можно, только в заводских условиях на спецоборудовании',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что указывает на отпрессованное соединение?',
                    'answers' => [
                        [
                            'name' => 'Удаленный индикатор опрессовки на фитинге и отсутствие течи под давлением воды в системе',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Визуально видно, что фитинг отпрессован',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Есть индикатор отпрессованного состояния',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Вставленный до отметки фитинг и отсутствия цветного индикатора',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой минимальный диаметр труб и фитингов системы Mapress?',
                    'answers' => [
                        [
                            'name' => '15 мм',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '16 мм ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '18 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '20 мм',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая максимальная рабочая температура системы ПНД (Полиэтилен низкого давления)?',
                    'answers' => [
                        [
                            'name' => '80°С',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '20°С',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '60°С',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '100°С',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Каким из способов можно соединять Трубы из ПНД (Полиэтилен низкого давления)?',
                    'answers' => [
                        [
                            'name' => 'Торцевая сварка встык',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Пайка с флюсом',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Склеивать клеем',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Радиальное  прессование',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой оптимальный уклон горизонтального участка канализации?',
                    'answers' => [
                        [
                            'name' => '0.03',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '2°',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '0.15',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '3°С',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'При сварке встык трубы ПНД (Полиэтилен низкого давления) необходимо подготовить торцы трубы:',
                    'answers' => [
                        [
                            'name' => 'Снять фаску торцевателем',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Обезжирить края трубы средством',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Протереть спиртом',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Почистить тряпкой',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Максимальная длина горизонтального участка канализации без дополнительной вентиляции (от стояка) не должна превышать ',
                    'answers' => [
                        [
                            'name' => '4м',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '6м',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '8м',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '10м',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Максимальная длина горизонтального вентилируемого участка канализации (с дополнительной вентиляцией) может быть',
                    'answers' => [
                        [
                            'name' => '10м',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '4м',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '6м',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '20м',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Трубы Geberit HDPE (Полиэтилен низкого давления) можно использовать для напорной канализации с рабочим давлением',
                    'answers' => [
                        [
                            'name' => '1,5 атм.',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '3 атм.',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '6 атм.',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '10 атм.',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Компенсаторная муфта используется для ',
                    'answers' => [
                        [
                            'name' => 'компенсации температурных расширений',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'соединения труб',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'ремонта труб',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'обслуживания и чистки канализации',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Нужно ли крепить трубы ПНД при укладке в землю?',
                    'answers' => [
                        [
                            'name' => 'Да, нужны анкерные опоры',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Нет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'На усмотрения заказчика',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нужно обмотать изоляцией',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'На что указывают метки вдоль труб и фитингов?',
                    'answers' => [
                        [
                            'name' => 'Угол 90° между метками, для удобства монтажа',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Указывают на скручивание трубы',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Указывают как надевать муфту',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Указывают место крепления',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Зачем ребра на фитингах Silent?',
                    'answers' => [
                        [
                            'name' => 'Для лучшего поглощения шума',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Для удобства монтажа',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Для внешнего отличия с другими системами',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Производственная необходимость',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Можно ли соединять методом сварки в стык трубы ПНД и Silent db20?',
                    'answers' => [
                        [
                            'name' => 'Да, они совместимы',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Нет, они не совместимы',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'С применением специального средства для обработки торцов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Можно, но нужно специальное оборудование',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Можно ли соединять методом сварки в стык трубы ПНД и Silent РР?',
                    'answers' => [
                        [
                            'name' => 'Нет, они не совместимы',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Да, они совместимы',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'С применением специального средства для обработки торцов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Можно, но нужно специальное оборудование',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Можно ли обрезать фитинги ПНД?',
                    'answers' => [
                        [
                            'name' => 'Да, на размер указанный в каталоге',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Да, до минимального размера по требованию заказчика',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нет, не предусмотрено технологией',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Фитинги имеют оптимальный размер',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что нужно учесть при монтаже компенсаторной муфты?',
                    'answers' => [
                        [
                            'name' => 'Температуру окружающей среды',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Размер муфты',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Диаметр трубы',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Частоту установки муфт',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Как правильно крепить систему труб из ПНД?',
                    'answers' => [
                        [
                            'name' => 'Применяя анкерные и скользящие опоры согласно технологии производителя',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Применяя скользящие опоры согласно технологии производителя',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Применяя опоры согласно технологии производителя',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Как позволяет конкретная ситуация при монтаже',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Для чего используют эл.сварные муфты?',
                    'answers' => [
                        [
                            'name' => 'Для соединения труб и фитингов ПНД в труднодоступных местах',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Для соединения металлопластиковых труб ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Для крепления труб',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Для перехода на другой материал труб',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Можно ли перейти с трубы ПНД Geberit на раструбную систему труб?',
                    'answers' => [
                        [
                            'name' => 'Да, с использование переходов из ассортимента',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Нет, нельзя',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Можно с использованием специальных инструментов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только с трубопроводными системами некоторых производителей',
                            'is_correct' => false,
                        ],
                    ],
                ],
            ],
        ];

        Test::createFromArray($test_array);
    }
}
