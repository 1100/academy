<?php

use Illuminate\Database\Seeder;
use App\Test;

class TestSeeder_2019_commercial_KOLO extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $test_array = [
            'name' => 'Базовый тест: модуль KOLO',
            'faculty' => 'commercial',
            'points' => 300,
            'question_count' => 20,
            'duration' => 20,
            'start_date' => '2019-05-15 00:00:00',
            'end_date' => '2019-11-30 23:59:59',
            'questions' => [
                [
                    'name' => 'В каком году был создан завод по производству санитарной керамики в г.Kоло?',
                    'answers' => [
                        [
                            'name' => '1909',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '1956',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '1993',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '1962',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Чем отличается санитарный фарфор от санитарного фаянса?',
                    'answers' => [
                        [
                            'name' => 'Коэффициентом твердости',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Коэффициентом сопротивления',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Коэффициентом электропроводности',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Коэффициентом водопоглощения',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой срок гарантии на керамику KOLO?',
                    'answers' => [
                        [
                            'name' => '3 года',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '10 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '7 лет',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Как называется система быстрого снятия сидений для унитазов  KOLO?',
                    'answers' => [
                        [
                            'name' => 'Quick Release',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Quick Click',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Push2Clean',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Click2Clean',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Как называется специальное покрытие для керамики и стекла KOLO?',
                    'answers' => [
                        [
                            'name' => 'CleanOn',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Easy Clean',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'KeraTect',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Reflex',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите цвета ручек для мебели MODO?',
                    'answers' => [
                        [
                            'name' => 'Белый, хром, черный',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дуб, хром, белый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Хром, платина, серебро',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Хром, венге, дуб',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Как называется устройство, которое обспечивает равномерную подачу воды в унитазах Rimfree?',
                    'answers' => [
                        [
                            'name' => 'Насос равномерной подачи воды',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Водонакопитель',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Фильтр равномерной подачи воды',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Водораспределитель',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Что в дословном переводе означает Rimfree?',
                    'answers' => [
                        [
                            'name' => 'Без бактерий',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Свободный слив',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Легкое очищение',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Без ободка',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Как называется линейка продуктов KOLO для людей с ограниченными физическими возможностями?',
                    'answers' => [
                        [
                            'name' => 'Easy way',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Simple life',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'LifeWell',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Lehnen ',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Как называется самый популярный писсуар ТМ KOLO?',
                    'answers' => [
                        [
                            'name' => 'Снайпер',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Майкл',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Оникс',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Феликс',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая серия KOLO имеет наибольший ассортимент?',
                    'answers' => [
                        [
                            'name' => 'Traffic',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Style',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Idol',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Nova Pro',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой срок гарантии на мебель KOLO?',
                    'answers' => [
                        [
                            'name' => '3 года',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'пожизненная',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '2 года',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Сколько линеек продуктов насчитывает серия Nova Pro?',
                    'answers' => [
                        [
                            'name' => '2 линейки продуктов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '3 линейки продуктов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '7 линеек продуктов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '4 линейки продуктов',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В какой серии санитарной керамики есть бачки округлой формы и прямоугольные?',
                    'answers' => [
                        [
                            'name' => 'Freja',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Traffic',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Style',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'NOVA Pro',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В какой серии санитарной керамики есть подвесные унитазы с круглой и прямоугольной формой чаши?',
                    'answers' => [
                        [
                            'name' => 'Rekord',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'MODO',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Twins',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'NOVA Pro',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Что придает дюропластовому сиденью антибактериальные свойства?',
                    'answers' => [
                        [
                            'name' => 'Интегрированное хлорирование ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Обработка средствами на спиртовой основе',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Покрытие Reflex',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ионы серебра',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая из серий керамики KOLO доступна с покрытием Reflex?',
                    'answers' => [
                        [
                            'name' => 'Nova Pro',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Runa',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Freja',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Traffic',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В какой из этих серий керамики KOLO есть унитазы с косым выпуском?',
                    'answers' => [
                        [
                            'name' => 'Style',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Traffic',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Nova Pro',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Modo',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой цвет  не используется для изготовления мебели KOLO?',
                    'answers' => [
                        [
                            'name' => 'Белый глянец',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Черный матовый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Серый ясень',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Канадский клен',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Что такое система Push-to-Open ? ',
                    'answers' => [
                        [
                            'name' => 'Система плавного опускания сиденья для унитаза',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Система быстрого монтажа',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Система плавного открытия сиденья для унитаза',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Система плавного открытия дверей в мебели',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В каких сериях KOLO есть напольные унитазы Rimfree со скрытыми креплениями?',
                    'answers' => [
                        [
                            'name' => 'Idol, Solo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Modo, Rekord',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Primo, Freja',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Traffic, Nova Pro',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Как называется детская серия керамики KOLO?',
                    'answers' => [
                        [
                            'name' => 'Бемби',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Baby',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Nova Pro Pico',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Nova Pro Junior',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В какой серии KOLO есть двойные умывальники?',
                    'answers' => [
                        [
                            'name' => 'Rekord',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Twins',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Style',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Traffic',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие унитазы комплектуются сиденьями Slim?',
                    'answers' => [
                        [
                            'name' => 'Traffic',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Style',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Idol',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Modo',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой формы биде в серии Nova Pro?',
                    'answers' => [
                        [
                            'name' => 'Биде овальное',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Биде прямоугольное',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Биде круглое и прямоугольное',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Биде овальное и прямоугольное',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В каких цветах изготавливается сиденье для детских унитазов KOLO?',
                    'answers' => [
                        [
                            'name' => 'Белый и голубой',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Голубой и розовый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белый и черный',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белый и красный',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите размеры мебельных умывальников Runa.',
                    'answers' => [
                        [
                            'name' => '45,55,65',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '50,55,60',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '50,60,65',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '50,60,70',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите размеры мебельных умывальников Freja.',
                    'answers' => [
                        [
                            'name' => '40,50,60',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '50,60,70',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '45,55,70',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '45,55,65',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В какой серии Kolo есть боковой высокий угловой шкафчик?',
                    'answers' => [
                        [
                            'name' => 'Traffic',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Nova Pro',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Rekord',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Modo',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В какой серии  санитарной керамики KOLO нет унитазов и биде?',
                    'answers' => [
                        [
                            'name' => 'Modo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Traffic',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Solo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Twins',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Сколько лет гарантии на доступность запасных частей у Kolo?',
                    'answers' => [
                        [
                            'name' => '15',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '20',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '8',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '10',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Что является дополнительной защитой для мебели Kolo?',
                    'answers' => [
                        [
                            'name' => 'Двойная кромка',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Водоодталкивающее восковое покрытие',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Обработка поверхности плиты водоодталкивающим спреем',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Покрытие водоотталкивающим лаком с УФ фильтром',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая упаковка у компактов Kolo?',
                    'answers' => [
                        [
                            'name' => 'Коробки из 3-х слойного гофрокартона',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Стретч-пленка',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Деревянные паллеты',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Коробки из 5-ти слойного гофрокартона',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В какой серии есть подвесной унитаз Rimfree для людей с ограниченными возможностями?',
                    'answers' => [
                        [
                            'name' => 'Modo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Style',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Traffic',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Nova Pro',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В каких цветах изготавливается мебель Nova Pro?',
                    'answers' => [
                        [
                            'name' => 'Белый глянец и венге',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Светлый дуб и белый ясень',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белый матовый и черныйглянец',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белый глянец и серый ясень',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В какую серию Kolo входит боковой шкафчик карго?',
                    'answers' => [
                        [
                            'name' => 'Rekord',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Nova Pro',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Traffic',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Twins',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Как работает система Click2Clean?',
                    'answers' => [
                        [
                            'name' => 'Прокрутить',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Потянуть от себя',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Потянуть на себя',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Нажать',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'К какой группе ванн относятся ванны Split?',
                    'answers' => [
                        [
                            'name' => 'Асимметричные ванны',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Прямоугольные ванны',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Угловые ванны',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Овальные ванны',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите размеры ванн Split',
                    'answers' => [
                        [
                            'name' => '150х80 см, 160х90 см, 170х90 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '150х70 см, 160х80 см, 170х80 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '150х70 см, 160х70 см, 170х70 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '160х75 см, 170х75см, 180х75 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите конструктивную особенность ванн Split',
                    'answers' => [
                        [
                            'name' => 'Центральный слив',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Подлокотники',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Встроенный подголовник',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Усиленные борта',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Из скольких секций состоят стеклянные ширмы Split?',
                    'answers' => [
                        [
                            'name' => '3 секции',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '1 секция',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '2 секции',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '4 секции',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие прямоугольные ванны могут комплектоваться ширмами Split?',
                    'answers' => [
                        [
                            'name' => 'Все модели прямоугольных ванн KOLO',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Только Modo, Comfort Plus',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только Clarissa, Comfort',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Только Saga, Sensa',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите главную особенность подвесного унитаза Modo',
                    'answers' => [
                        [
                            'name' => 'Чаша без ободка Rimfree',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Удлиненная чаша 70 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Укороченная чаша 51 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Чаша круглой формы',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая длина подвесного унитаза серии Nova Pro БЕЗ ПРЕГРАД?',
                    'answers' => [
                        [
                            'name' => '70 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '80 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '70 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '75 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите опции сидений, которые используются для комплектации подвесных унитазов Modo.',
                    'answers' => [
                        [
                            'name' => 'Дюропластовое сиденье и супер тонкое сиденье Slim',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Цветные дюропластовые сиденья на выбор покупателя',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сиденья из дюропласта и АВС-пластика',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сиденья из МДФ и натурального дерева',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите главные преимущества унитазов без ободка Rimfree.',
                    'answers' => [
                        [
                            'name' => 'Простой уход, экономичность, охрана окружающей среды, привлекательный внешний вид',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Функциональность, экономичность, ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Удобный, красивый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Компактный, экономный',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что означает простой уход для унитаза без ободка?',
                    'answers' => [
                        [
                            'name' => 'Просто протереть салфеткой ',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Ополоснуть водой',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Очистить чистящим средством',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Побрызгать освежителем для туалетов',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Что означает экономичность для унитаза без ободка?',
                    'answers' => [
                        [
                            'name' => 'Слив 4 и 2 литра позволяет потреблять на 60% меньше воды',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Цена ниже, чем унитаза с ободком',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Сиденье с антибактериальным покрытием препятствует размножению микробов',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Простой монтаж',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какое преимущество унитазов без ободка положительно влияет на охрану окружающей среды?',
                    'answers' => [
                        [
                            'name' => 'При уходе за унитазом без ободка не нужно использовать агрессивную бытовую химию',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'При уходе за унитазом без ободка не нужен освежитель воздуха',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'При уходе за унитазом без ободка не нужно использовать воду',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Унитаз без ободка имеет систему самоочистки',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой формы раковина у умывальников TWINS с тонким бортом?',
                    'answers' => [
                        [
                            'name' => 'Прямоугольная',
                            'is_correct' => true,
                        ],
                        [
                            'name' => 'Овальная',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Круглая ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Квадратная',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Назовите размеры мебельных умывальников TWINS с тонким бортом',
                    'answers' => [
                        [
                            'name' => '50, 60 и 80 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '50, 60 и 75 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '45, 55и 65 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '55, 65 и 75 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая высота борта у  мебельных умывальников TWINS с тонким бортом?',
                    'answers' => [
                        [
                            'name' => '3,5 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '5 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '1 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '6 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой высоты ножки используются для комплектации шкафчиков Twins с умывальниками с тонким бортом?',
                    'answers' => [
                        [
                            'name' => '25 см',
                            'is_correct' => true,
                        ],
                        [
                            'name' => '20 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '15 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '10 см',
                            'is_correct' => false,
                        ],
                    ],
                ],
                [
                    'name' => 'В каком польском городе расположено производство ванн KOLO?',
                    'answers' => [
                        [
                            'name' => 'Коло',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Белосток',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Дымер',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Озоркув',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая из указанных ванн, является асимметричной?',
                    'answers' => [
                        [
                            'name' => 'MODO',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Saga',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Relax ',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Mystery',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'С помощью чего можно реставрировать небольшие повреждения на ваннах?',
                    'answers' => [
                        [
                            'name' => 'Силиконовый герметик',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Паста на основе Shellac',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Эпоксидная смола',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Полировочная паста',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие ванны KOLO имеют усиленные борты?',
                    'answers' => [
                        [
                            'name' => 'Modo и Magnum',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Relax и  Inspiration',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Mirra и Mystery',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Clarissa и Comfort Plus',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Как называются угловые ванны KOLO?',
                    'answers' => [
                        [
                            'name' => 'Mirra, Modo, Opal Plus',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Saga, Sensa, Mystery',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Spring, Comfort, Comfort Plus',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Magnum, Relax, Inspiration',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Что такое Antislide? ',
                    'answers' => [
                        [
                            'name' => 'Система крепления подголовника',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Система креплений для декоративных панелей',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Система магнитных креплений дверей в душкабинах',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Антискользящее покрытие, которое сводит к минимуму риск во время купания',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В каких продуктах Kolo используют Antislide?',
                    'answers' => [
                        [
                            'name' => 'Ванны Mirra и все поддоны Kolo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ванны Modo и кабинки Suppero',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ширма для ванной Niven',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Ванны Modo и поддоны Pacyfik',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какие из указанных ванн асимметричные?',
                    'answers' => [
                        [
                            'name' => 'Magnum, Diuna',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Opal Plus, Saga',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Sensa, Modo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Spring, Mystery',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В каких размерах выпускается ванна Mystery?',
                    'answers' => [
                        [
                            'name' => '150х70, 160х70, 170х70 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '120х70, 140х70, 160х70 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '150х100, 170х110 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '140х90 и 150х95 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В каких размерах выпускается ванна Saga?',
                    'answers' => [
                        [
                            'name' => '150х75, 160х75, 170х75',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '140х70, 150х70, 170х70',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '150х70, 160х70, 170х75',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '150х75, 160х75, 170х80',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В каких размерах выпускается прямоугольная ванна Comfort Plus?',
                    'answers' => [
                        [
                            'name' => '150х70, 160х70, 170х70, 180х70',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '150х70, 160х70, 170х75, 180х80, 190х90 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '150х75, 160х75, 170х75, 180х80, 190х90 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '150х75, 160х80, 170х75, 180х80, 190х90 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Сиденья какой ширины доступны для ванн KOLO  Comfort Plus?',
                    'answers' => [
                        [
                            'name' => '70, 80, 85 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '75, 85, 95 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '75, 80, 85 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '75, 80, 90 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Как называется овальная ванна KOLO?',
                    'answers' => [
                        [
                            'name' => 'Modo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Mirra',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Split',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Clarissa',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой толщины стекло, используется для изготовления дверей кабинки Next?',
                    'answers' => [
                        [
                            'name' => '3 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '4 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 мм',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '6 мм',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Каких цветов изготавливают петли для душевых кабинок KOLO?',
                    'answers' => [
                        [
                            'name' => 'Белый глянец и матовый хром',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Черный матовый и хром',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Стальной и бронзовый',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Хром и серебряный блеск.',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В каких душевых поддонах используют Antislide?',
                    'answers' => [
                        [
                            'name' => 'Standard Plus',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'First',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Simplo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Pacyfik',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Как называется самый глубокий душевой поддон Kolo?',
                    'answers' => [
                        [
                            'name' => 'Pacyfik',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Simplo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'First',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Deep',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какая самая большая глубина душевого поддона Kolo?',
                    'answers' => [
                        [
                            'name' => '9 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '6 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '15 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '21 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Фронтальных прямоугольных панелей какого размера нет в асортиментной линейке  Kolo?',
                    'answers' => [
                        [
                            'name' => '150 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '140 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '180 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '190 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Из какого материала изготавливаются панели UNI4 TM Kolo?',
                    'answers' => [
                        [
                            'name' => 'Санитарный акрил',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Полипропилен',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Поливинилхлорид',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Полистирол',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой срок гарантии на акриловые ванны Kolo?',
                    'answers' => [
                        [
                            'name' => '10 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '2 года',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '7 лет',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какой срок гарантии на душевые кабины Kolo?',
                    'answers' => [
                        [
                            'name' => '10 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '7 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '5 лет',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '2 года',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какого размера боковых панелей нет в асортиментной линейке  Kolo?',
                    'answers' => [
                        [
                            'name' => '75 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '80 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '90 см',
                            'is_correct' => false,
                        ],
                        [
                            'name' => '85 см',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В какой серии ванн Kolo представлен размер 140х90 см?',
                    'answers' => [
                        [
                            'name' => 'Sensa',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Comfort',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Spring',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Mystery',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В какой серии ванн Kolo представлен размер 120х70 см?',
                    'answers' => [
                        [
                            'name' => 'Sensa',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Comfort Plus',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Mystery',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Diuna',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Чем армируют акриловую ванну для придания ей жесткости?',
                    'answers' => [
                        [
                            'name' => 'Минеральная вата',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Древесная стружка',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Модифицированный поролон',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Стекловолокно',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'Какими поддонами рекомендуется комплектовать душевые кабинки Next?',
                    'answers' => [
                        [
                            'name' => 'First',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Deep',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Standard Plus',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Paсyfik',
                            'is_correct' => true,
                        ],
                    ],
                ],
                [
                    'name' => 'В какой серии ванн Kolo представлен размер 190x90 см?',
                    'answers' => [
                        [
                            'name' => 'Opal Plus',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Modo',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Mirra',
                            'is_correct' => false,
                        ],
                        [
                            'name' => 'Clarissa',
                            'is_correct' => true,
                        ],
                    ],
                ],
            ],
        ];

        Test::createFromArray($test_array);

    }
}
