<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommercialFacultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faculties')->insert([
            [
                'slug' => 'commercial',
                'name' => 'Продавцы',
                'description' => 'Программа мотивации и ценные призы для лучших продавцов',
                'color' => 'blue',
                'open_rating' => true,
            ]
        ]);

        $this->call(InformationFieldsForCommercialFacultySeeder::class);
    }
}
