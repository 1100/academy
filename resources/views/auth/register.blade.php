@extends('layouts.default')

@section('content')
    @include ('faculty.hero')
    @if (!Auth::user())
        @include ('faculty.registration_form')
    @endif
    @if ($faculty->events_enabled)
        <div class="container pb-5">
            <h2 class="content-title underlined-title">Мероприятия факультета</h2>
            <div>
                <div class="col col-12 col-lg-8">
                    <p class="row">
                        Компания Геберит регулярно проводит беспланые обучающие мероприятия для проектантов и сантехников.
                    </p>
                    @include ('faculty.events.events')
                </div>
            </div>
        </div>
    @endif
@endsection
