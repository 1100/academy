@extends('layouts.default')

@section('content')
    <div class="container py-5">
        <h1 class="content-title underlined-title">Вход</h1>
        <form method="POST" class="col col-12 col-md-6">
            {{ csrf_field() }}
            <div class="form-group">
                <label>
                    Логин (Ваш e-mail или телефон)
                    <input name="login" type="text" class="form-control{{ $errors->has('login') ? ' is-invalid' : '' }}" value="{{ old('login') }}" required>
                    @if ($errors->has('login'))
                        <span class="invalid-feedback">
                            {{ $errors->first('login') }}
                        </span>
                    @endif
                </label>
            </div>

            <div class="form-group">
                <label>
                    Пароль
                    <input name="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                            {{ $errors->first('password') }}
                        </span>
                    @endif
                </label>
            </div>

            <div class="form-group form-check">
                <input name="remember" type="checkbox" id="rememberMeCheckbox" class="form-check-input" {{ old('remember') ? 'checked' : '' }}>
                <label class="form-check-label mr-3" for="rememberMeCheckbox" style="width:auto;">
                    Запомнить меня
                </label>
                <a href="{{ route('password.request') }}">
                    Забыли пароль?
                </a>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Войти</button>
            </div>

        </form>
        <div class="col col-12 col-md-6">
            @if ($fields->find('facebook'))
                <p>Для участников с привязанным facebook-аккаунтом:</p>
                @include ('user_information.input.'.$fields->find('facebook')->type)
                @php $fields->forget('facebook') @endphp
            @endif
        </div>
    </div>

@endsection
