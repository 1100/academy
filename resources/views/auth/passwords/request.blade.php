@extends('layouts.default')

@section('content')
    <div class="container py-5">
        <h1 class="content-title underlined-title">Восстановление пароля</h1>
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }} <br>
            Если Вы не получили email-сообщение со ссылкой для сброса пароля, обратитесь
            <a href="{{ route('contacts') }}">поддержку</a>.
        </div>
        @endif
        <form method="POST" class="col col-12 col-md-6" action="{{ route('password.send') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <label>
                    E-Mail или Телефон
                    <input name="login" type="text" class="form-control{{ $errors->has('login') ? ' is-invalid' : '' }}" value="{{ (old('login')) ? old('login') : ((!is_null($email)) ? $email : '') }}" required>
                    @if ($errors->has('login'))
                        <span class="invalid-feedback">
                            {{ $errors->first('login') }}
                        </span>
                    @endif
                </label>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Восстановить пароль</button>
            </div>

        </form>
    </div>
@endsection
