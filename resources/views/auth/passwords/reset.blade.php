@extends('layouts.default')

@section('content')
    <div class="container py-5">
        <h1 class="content-title underlined-title">Восстановление пароля</h1>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }} <br>
                Если Вы не получили email-сообщение со ссылкой для сброса пароля, обратитесь
                <a href="{{ route('contacts') }}">поддержку</a>.
            </div>
        @endif
        <form method="POST" class="col col-12 col-md-6" action="{{ route('password.request') }}">
            {{ csrf_field() }}
            <input type="hidden" name="token" value="{{ $token }}">
            @isset( $phone )
                <input type="hidden" name="phone" value="{{ $phone }}">
            @endisset

            <div class="form-group">
                <label>
                    E-Mail
                    <input name="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ $email or old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            {{ $errors->first('email') }}
                        </span>
                    @endif
                </label>
            </div>

            <div class="form-group">
                <label>
                    Пароль
                    <input id="password" name="password" type="password" class="form-control" placeholder="••••••••" required autocomplete="new-password">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                {{ $errors->first('password') }}
            </span>
                    @endif
                </label>
            </div>
            <div class="form-group">
                <label>
                    Повторите пароль
                    <input id="password_confirmation" name="password_confirmation" type="password" class="form-control" placeholder="••••••••" required autocomplete="new-password">
                    @if ($errors->has('password_confirmation'))
                        <span class="invalid-feedback">
                {{ $errors->first('password_confirmation') }}
            </span>
                    @endif
                </label>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Восстановить пароль</button>
            </div>

        </form>
    </div>
@endsection
