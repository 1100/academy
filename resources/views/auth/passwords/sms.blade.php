@extends('layouts.default')

@section('content')
    <div class="container py-5">
        <h1 class="content-title underlined-title">Восстановление пароля через СМС</h1>

        <p>
            Если Вы не получили СМС-сообщение с кодом для сброса пароля, обратитесь в <a href="{{ route('contacts') }}">поддержку</a>.
        </p>

        <form method="POST" class="col col-12 col-md-6" action="{{ route('password.reset_by_sms') }}">
            {{ csrf_field() }}
            <input type="hidden" name="token" value="{{ $token }}">
            <input type="hidden" name="phone" value="{{ $phone }}">

            <div class="form-group">
                <label>
                    Код из СМС
                    <input name="code" type="text" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" value="{{ $code or old('code') }}" required autofocus>
                    @if ($errors->has('code'))
                        <span class="invalid-feedback">
                            {{ $errors->first('code') }}
                        </span>
                    @endif
                </label>
            </div>

            <div class="form-group">
                <label>
                    Пароль
                    <input id="password" name="password" type="password" class="form-control" placeholder="••••••••" required autocomplete="new-password">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                {{ $errors->first('password') }}
            </span>
                    @endif
                </label>
            </div>
            <div class="form-group">
                <label>
                    Повторите пароль
                    <input id="password_confirmation" name="password_confirmation" type="password" class="form-control" placeholder="••••••••" required autocomplete="new-password">
                    @if ($errors->has('password_confirmation'))
                        <span class="invalid-feedback">
                {{ $errors->first('password_confirmation') }}
            </span>
                    @endif
                </label>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Восстановить пароль</button>
            </div>

        </form>
    </div>
@endsection
