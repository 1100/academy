@extends('layouts.default')
@section('content')
    <div class="container py-5">
        <h1 class="content-title underlined-title">Новости Академии</h1>
        <div class="row mx-0">
            <div class="col col-12 col-md-8">
                @foreach($posts as $post)
                <div class="row pb-5">
                    <div class="col col-12 col-sm-5 post-image" @if($post->image)style="background-image: url('{{ Storage::url($post->image) }}')"@endif>
                    </div>
                    <div class="col col-12 col-sm-7 post-content pt-3 pt-sm-0">
                        <h1 class="post-title">
                            <a href="{{ route('post', ['post_id' => $post->id]) }}">
                                {{ $post->title }}
                            </a>
                        </h1>
                        <p>
                            {{ $post->created_at->format('d.m.Y') }}
                        </p>
                        <a href="{{ route('post', ['post_id' => $post->id]) }}" class="btn btn-primary">
                            <i class="icon icon-arrow"></i>
                            Подробнее
                        </a>
                    </div>
                </div>
                @endforeach
                <nav aria-label="Страницы новостей" class="row justify-content-center">
                    {{ $posts->links() }}
                </nav>
            </div>
            <div class="col col-12 col-md-4" style="flex: 0 0 230px;">
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FGeberit.UKR&tabs=timeline&width=230&height=600&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1211366195628815" width="230" height="600" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
            </div>
        </div>
    </div>
@endsection