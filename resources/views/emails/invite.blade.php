<img src="{{ url('/') }}/images/header_logo.png" alt="Geberit Academy Logo" height="50px">
<font face="Arial, sans-serif" size="2" color="#000">
    <h1 style="margin-top:25px;"><font size="+1" color="#3a75c4">Приглашаем Вас в Академию сантехнических наук Geberit!</font></h1>
    <p>Один из Ваших друзей поделился с Вами возможностью выиграть поездку в Германию с экскурсией на завод Geberit и развлекательной программой.</p>
    <p>Для того, чтобы принять участие в конкурсе, подайте заявку на регистрацию по этой ссылке:</p>
    <p><a href="{{ route('invite', ['faculty' => $faculty, 'invited_key' => $invite_key]) }}">{{ route('invite', ['faculty' => $faculty, 'invited_key' => $invite_key]) }}</a></p>
    <p style="font-style:italic;">С пожеланиями успехов и вдохновения в обучении, <br> команда Академии сантехнических наук.</p>
    <p><a href="{{ url('/') }}">{{ url('/') }}</a><br>
        info@geberit-academy.com.ua</p>
</font>