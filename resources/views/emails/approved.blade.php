<img src="{{ url('/') }}/images/header_logo.png" alt="Geberit Academy Logo" height="50px">
<font face="Arial, sans-serif" size="2" color="#000">
    <h1 style="margin-top:25px;"><font size="+1" color="#3a75c4">Добро пожаловать в Академию сантехнических наук Geberit!</font></h1>
    <p>Ваш аккаунт прошёл проверку организаторами Академии и был одобрен. Для Вас открыт доступ ко всем возможностям Академии, в том числе материалам и тестированию.</p>
    <p>Вы можете войти в свой кабинет по ссылке:</p>
    <p><a href="{{ route('login') }}">{{ route('login') }}</a></p>
    <p style="font-style:italic;">С пожеланиями успехов и вдохновения в обучении, <br> команда Академии сантехнических наук.</p>
    <p><a href="{{ url('/') }}">{{ url('/') }}</a><br>
        info@geberit-academy.com.ua</p>
</font>