<img src="{{ url('/') }}/images/header_logo.png" alt="Geberit Academy Logo" height="50px">
<font face="Arial, sans-serif" size="2" color="#000">
    <h1 style="margin-top:25px;"><font size="+1" color="#3a75c4">Приглашаем Вас в обновленную Академию сантехнических наук Geberit!</font></h1>
    <p>Для того, чтобы принять участие в конкурсе, сбросьте пароль для своего аккаунта по этой ссылке:</p>
    <p><a href="{{ route('password.reset.from_email',['email'=>$user_email]) }}">Сбросить пароль</a></p>
    <p style="font-style:italic;">С пожеланиями успехов и вдохновения в обучении, <br> команда Академии сантехнических наук.</p>
    <p><a href="{{ url('/') }}">{{ url('/') }}</a><br>
        info@geberit-academy.com.ua</p>
</font>