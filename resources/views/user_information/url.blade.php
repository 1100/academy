<div class="form-group">
    {{ $field->name }}:
    <span class="font-weight-bold">
        <a href="{{ $field->value }}" target="_blank">
            {{ $field->value }}
        </a>
    </span>
</div>