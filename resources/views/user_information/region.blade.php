<div class="form-group">
    {{ $field->name }}:
    <span class="font-weight-bold">{{ $field->value }}</span>
</div>