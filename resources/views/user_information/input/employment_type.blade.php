<div class="form-group">
    <label>
        {{ $field->name }}
        <select id="{{ $field->slug }}" class="form-control{{ $errors->has($field->slug) ? ' is-invalid' : '' }}" name="{{ $field->slug }}">
            <option value="" selected disabled>Выберите тип из списка...</option>
            @foreach(\App\EmploymentType::all() as $employment_type)
                <option value="{{ $employment_type->id }}" {{ ($employment_type->id == $field->value or $employment_type->id == old($field->slug)) ? 'selected' : '' }}>
                    {{ $employment_type->name }}
                </option>
            @endforeach
        </select>
        @if ($errors->has($field->slug))
            <span class="invalid-feedback">
                {{ $errors->first($field->slug) }}
            </span>
        @endif
    </label>
</div>