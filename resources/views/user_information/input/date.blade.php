<div class="form-group">
    <label>
        {{ $field->name }}
        <input id="{{ $field->slug }}" name="{{ $field->slug }}" type="date" class="form-control{{ $errors->has($field->slug) ? ' is-invalid' : '' }}" placeholder="Введите {{ mb_strtolower($field->name) }}" value="{{ $field->value or old($field->slug) }}">
        @if ($errors->has($field->slug))
            <span class="invalid-feedback">
                {{ $errors->first($field->slug) }}
            </span>
        @endif
    </label>
</div>