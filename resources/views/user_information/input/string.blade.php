<div class="form-group">
    <label>
        {{ $field->name }}
        <input id="{{ $field->slug }}" name="{{ $field->slug }}" type="text" class="form-control{{ $errors->has($field->slug) ? ' is-invalid' : '' }}" placeholder="{{ $field->name }}" value="{{ $field->value or old($field->slug) }}">
        @if ($errors->has($field->slug))
            <span class="invalid-feedback">
                {{ $errors->first($field->slug) }}
            </span>
        @endif
    </label>
</div>