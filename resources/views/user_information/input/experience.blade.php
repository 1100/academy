<div class="form-group">
    <label>
        {{ $field->name }}
        <select id="{{ $field->slug }}" class="form-control{{ $errors->has($field->slug) ? ' is-invalid' : '' }}" name="{{ $field->slug }}">
            <option value="" selected disabled>Выберите стаж из списка...</option>
            @foreach(\App\Experience::all() as $experience)
                <option value="{{ $experience->id }}" {{ ($experience->id == $field->value or $experience->id == old($field->slug)) ? 'selected' : '' }}>
                    {{ $experience->name }}
                </option>
            @endforeach
        </select>
        @if ($errors->has($field->slug))
            <span class="invalid-feedback">
                {{ $errors->first($field->slug) }}
            </span>
        @endif
    </label>
</div>