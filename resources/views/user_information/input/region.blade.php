<div class="form-group">
    <label>
        Страна
        <select id="country_select" class="form-control{{ $errors->has($field->slug) ? ' is-invalid' : '' }}" name="country">
            <option value="" selected disabled>Выберите страну из списка...</option>
            @foreach(\App\Country::all() as $country)
                @php
                    $selected_country_id = false;
                    if ($field->value) {
                        $selected_country_id = \App\Region::find($field->value)
                            ? \App\Region::find($field->value)->country_id
                            : false;
                    }
                    if (old($field->slug)) {
                        $selected_country_id = \App\Region::find(old($field->slug))
                            ? \App\Region::find(old($field->slug))->country_id
                            : false;
                    }
                @endphp
                <option value="{{ $country->id }}" {{ $country->id == $selected_country_id ? 'selected' : '' }}>
                    {{ $country->name }}
                </option>
            @endforeach
        </select>
    </label>
</div>
<div class="form-group">
    <label>
        {{ $field->name }}
        <select id="{{ $field->slug }}" class="form-control{{ $errors->has($field->slug) ? ' is-invalid' : '' }}" name="{{ $field->slug }}">
            <option value="" selected disabled>Выберите область из списка...</option>
            @foreach(\App\Region::all() as $region)
                <option data-country-id="{{ $region->country_id }}" value="{{ $region->id }}" {{ ($region->id == $field->value or $region->id == old($field->slug)) ? 'selected' : '' }}>
                    {{ $region->name }}
                </option>
            @endforeach
        </select>
        @if ($errors->has($field->slug))
            <span class="invalid-feedback">
                {{ $errors->first($field->slug) }}
            </span>
        @endif
    </label>
</div>

<script>
    document.body.onload = function() {
        let country_select = document.getElementById('country_select');
        let region_select = document.getElementById('region_select');
        let phone_input = $('[data-type="phone"]');

        if (!country_select.value) {
            region_select.disabled = true;
            if (phone_input) {
                phone_input.prop('disabled', true);
            }
        }

        country_select.onchange = function() {
            region_select.value = '';
            region_select.disabled = false;
            let regions = document.querySelectorAll('option[data-country-id]');
            regions.forEach(function(region) {
                if (region.getAttribute('data-country-id') === country_select.value) {
                    region.style.display = 'block';
                    region.disabled = false;
                } else {
                    region.style.display = 'none';
                    region.disabled = true;
                }
            });

            if (phone_input) {
                phone_input.prop('disabled', false);
                if (country_select.value == 2) { // BY
                    phone_input.attr('placeholder', '+37 (5__) ___-__-__');
                    phone_input.mask('+37 (599) 999-99-99');
                } else { // UA
                    phone_input.attr('placeholder', '+38 (0__) ___-__-__');
                    phone_input.mask('+38 (099) 999-99-99');
                }
            }
        };
    };

</script>