<div class="form-group">
    <label>
        Пароль
        <input id="password" name="password" type="password" class="form-control" placeholder="••••••••" required autocomplete="new-password">
        @if ($errors->has('password'))
            <span class="invalid-feedback">
                {{ $errors->first('password') }}
            </span>
        @endif
    </label>
</div>
<div class="form-group">
    <label>
        Повторите пароль
        <input id="password_confirmation" name="password_confirmation" type="password" class="form-control" placeholder="••••••••" required autocomplete="new-password">
        @if ($errors->has('password_confirmation'))
            <span class="invalid-feedback">
                {{ $errors->first('password_confirmation') }}
            </span>
        @endif
    </label>
</div>