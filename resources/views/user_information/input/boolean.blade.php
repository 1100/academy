<div class="form-group">
    <label>
        {{ $field->name }}
        <input id="{{ $field->slug }}" name="{{ $field->slug }}" type="checkbox" class="form-check-input{{ $errors->has($field->slug) ? ' is-invalid' : '' }}" @if( (isset($field->value) && !empty($field->value)) || (isset($field->value) && $field->value == '1')) checked @endif value="1">
        @if ($errors->has($field->slug))
            <span class="invalid-feedback">
                {{ $errors->first($field->slug) }}
            </span>
        @endif
    </label>
</div>