@if (Auth::user())
    @if (!Auth::user()->getInformation('facebook')) {{-- Привязать фейсбук --}}
    <div class="form-group">
        <label>
            Ваш Facebook:
            нет
            <a href="{{ url('/login/facebook') }}" class="btn btn-secondary">
                <i class="fab fa-facebook-f"></i>
                Привязать Facebook
            </a>
        </label>
    </div>
    @else {{-- Отвязать фейсбук --}}
    <div class="form-group">
        <label>
            Ваш Facebook:
            <a href="https://fb.com/app_scoped_user_id/{{ Auth::user()->getInformation('facebook') }}" target="_blank">{{  Auth::user()->getInformation('facebook') }}</a>
            <a href="{{ url('/cabinet/delete_facebook') }}" class="btn btn-secondary">
                Отвязать Facebook
            </a>
        </label>
    </div>
    @endif
@else
    <div class="form-group">
        <a href="{{ route('login.facebook', ['faculty' => (isset($faculty->slug))?$faculty->slug:null]) }}" class="btn btn-secondary">
            <i class="fab fa-facebook-f"></i>
            Войти через Facebook
        </a>
    </div>
@endif