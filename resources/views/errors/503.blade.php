<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="keywords" content="Geberit, Geberit Academy, Академия Сантехнических Наук" />
    <meta name="description" content="Академия Сантехнических Наук" />
    <title>Академия Сантехнических Наук Geberit</title>
    <link rel="shortcut icon" href="favicon.ico" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/granim/1.0.6/granim.min.js"></script>
</head>
<body>
<canvas id="canvas-background"></canvas>
<div class="container">
    <div class="header">
        <img src="/images/header_logo.svg" class="logo_left"/>
        <img src="/images/header_logo_right.png" class="logo_right"/>
    </div>
    <img src="/images/under_construction.png" class="icon"/>
    <h3>Проводяться технічні роботи.</h3>
    <h3>Роботу сайту буде відновлено найближчим часом.</h3>
</div>
<style>
    html,
    body {
        width: 100%;
        height: 100%;
        margin: 0;
        position: relative;
    }
    #canvas-background {
        width: 100%;
        height: 100%;
        position: fixed;
        top: 0;
        left: 0;
        z-index: -1;
    }
    .container {
        width: 90%;
        max-width: 800px;
        background-color: #fff;
        padding: 1em;
        margin: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        margin-right: -50%;
        transform: translate(-50%, -50%);
        text-align: center;
    }
    .icon {
        height: 250px;
        width: auto;
    }
    .header {
        height: 60px;
    }
    .logo_left {
        float: left;
    }
    .logo_right {
        float: right;
    }
    @media screen and (max-width:480px) {
        .header {
            text-align: left;
            height: auto;
        }
        .logo_left,
        .logo_right {
            float: inherit;
            display: inline-block;
            padding: 5px 0;
        }
    }
</style>
<script type="text/javascript">
    var granimInstance = new Granim({
        element: '#canvas-background',
        name: 'background-animation',
        direction: 'left-right',
        opacity: [1, 1],
        isPausedWhenNotInView: true,
        states : {
            "default-state": {
                gradients: [
                    ['#0B7CC1', '#000000'],
                    ['#000000', '#4C145E']
                ]
            }
        }
    });
</script>
</body>
</html>
