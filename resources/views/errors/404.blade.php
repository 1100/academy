@extends('layouts.default')

@section('content')
    <div class="container">
        <h1>Ошибка 404. Страница не найдена</h1>
        Запрашиваемой Вами страницы не существует.
    </div>
@endsection