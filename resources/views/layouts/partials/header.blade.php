<header class="border-bottom">
    <div class="container">
        <a href="/">
            <img src="/images/header_logo.svg" alt="Академия Сантехнических Наук">
        </a>
        <div class="float-right hamburger d-sm-block d-md-none" data-toggle="collapse" data-target="#navbarMenu"
             aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>
        <img class="float-right d-none d-md-block" src="/images/header_logo_right.png" alt="Академия Сантехнических Наук">
    </div>
</header>

<nav class="navbar navbar-light navbar-expand-md border-bottom">
    <div class="container">

        <div class="collapse navbar-collapse" id="navbarMenu">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item {!! classActivePath('/', false, 'active') !!}">
                    <a class="nav-link" href="/">Главная</a>
                </li>
                <li class="nav-item dropdown {!! classActivePath('register', true, 'active') !!}">
                    <a class="nav-link dropdown-toggle" href="#" id="facultiesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Факультеты
                    </a>
                    <div class="dropdown-menu" aria-labelledby="facultiesDropdown">
                        @foreach($faculties as $faculty)
                            <a class="dropdown-item {!! classActivePath('register.'.$faculty->slug, true, 'active') !!}" href="{{ route('register', $faculty->slug) }}">{{ $faculty->name }}</a>
                        @endforeach
                    </div>
                </li>
                <li class="nav-item {!! classActivePath('cabinet', true, 'active') !!}">
                    <a class="nav-link" href="{{ route('cabinet.dashboard') }}">Кабинет</a>
                </li>
                @if (Auth::user())
                <li class="nav-item {!! classActivePath('news', true, 'active') !!}">
                    <a class="nav-link" href="{{ route('news') }}">Новости</a>
                </li>
                @endif
                <li class="nav-item {!! classActivePath('contacts', true, 'active') !!}">
                    <a class="nav-link" href="{{ route('contacts') }}">Контакты</a>
                </li>
                @if (Auth::guard('admin')->user())
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('admin.faculties.show') }}">Админ-панель</a>
                    </li>
                @endif
            </ul>
            <ul class="navbar-nav">
                @if (Auth::user())
                <li class="nav-item">
                    <a href="#" class="btn btn-secondary" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Выход
                    </a>
                    <form id="logout-form" class="d-none" action="{{ url('/logout') }}" method="POST">
                        {{ csrf_field() }}
                    </form>
                </li>
                @else
                <li class="nav-item">
                    <a href="{{ route('login') }}" class="btn btn-secondary">
                        Вход в кабинет
                    </a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>