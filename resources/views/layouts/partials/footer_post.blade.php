<li class="icon_small_arrow right_white">
    <a href="{{ route('post', ['post_id' => $post->id]) }}">
        {{ $post->title }}
    </a>
    <abbr title="{{ $post->created_at->format('d.m.y') }}" class="timeago">{{ $post->created_at->format('d.m.y') }}</abbr>
</li>