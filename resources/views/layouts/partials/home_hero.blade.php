<div class="container pb-5">
    <div class="row">
        @foreach($faculties as $faculty)
            <div class="faculty-block col col-12 col-md-4">
                <h1 class="faculty-title">
                    <a href="{{ route('register', $faculty->slug) }}">
                        {{ $faculty->name }}
                    </a>
                </h1>
                <p class="faculty-description">{{ $faculty->description }}</p>
                @if($faculty->open_registration)
                <a href="{{ route('register', $faculty->slug) }}" class="btn btn-secondary faculty-button">
                    <i class="icon icon-arrow"></i>
                    Регистрация
                </a>
                @else
                    <a href="#" class="btn btn-secondary faculty-button disabled">
                        Регистрация закрыта
                    </a>
                @endif
            </div>
        @endforeach
    </div>
</div>