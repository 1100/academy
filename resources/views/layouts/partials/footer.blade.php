<footer class="border-top py-4">
    <div class="container">
        <div class="float-left text-muted">
            <p>© 2019 ТОВ "Геберит Трейдинг"</p>
            <p>04073, Украина, Киев, просп. Степана Бандеры, 9<br>
                info@geberit-academy.com.ua</p>
        </div>
        <img class="float-right" src="/images/header_logo_right.png" alt="Академия Сантехнических Наук">
    </div>
</footer>