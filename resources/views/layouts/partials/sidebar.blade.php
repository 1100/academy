<div class="page_right">
    <div class="sidebar_box first">
        <h3 class="box_header">
            Мы в Facebook
        </h3>
        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FGeberit.UKR%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="340" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" class="margin_top_10"></iframe>
    </div>
    <div class="sidebar_box">
        <h3 class="box_header">
            Свяжитесь с нами
        </h3>
        <p class="info">
            Вы можете связаться с нами любым удобным для Вас способом:
        </p>
        <ul class="contact_data">
            <li class="clearfix">
                <span class="social_icon mail"></span>
                <p class="value">
                    по email: <a href="mailto:info@geberit-academy.com.ua">info@geberit-academy.com.ua</a>
                </p>
            </li>
            <li class="clearfix">
                <span class="social_icon icon_form"></span>
                <p class="value">
                    или <a href="#" title="Обратная связь">заполнить форму обратной связи</a>
                </p>
            </li>
        </ul>
    </div>
</div>