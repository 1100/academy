<li class="post">
    <ul class="comment_box clearfix">
        <li class="date">
            <div class="value">{{ $post->created_at->format('d.m.y') }}</div>
            <div class="arrow_date"></div>
        </li>
        <li class="comments_number {{ $post->faculty ? 'faculty-'.$post->faculty()->color : '' }}">
        </li>
    </ul>
    <div class="post_content">
        @if($post->image)
            <a class="post_image" href="{{ route('post', ['post_id' => $post->id]) }}" title="{{ $post->title }}">
                <img src="{{ Storage::url($post->image) }}" alt="{{ $post->title }}" />
            </a>
        @endif
        <h2>
            <a href="{{ route('post', ['post_id' => $post->id]) }}" title="{{ $post->title }}">
                {{ $post->title }}
            </a>
        </h2>
        <p>
            {{ str_limit(strip_tags($post->content), 250) }}
        </p>
        @if (mb_strlen(strip_tags($post->content)) > 250)
        <a title="Читать полностью" href="{{ route('post', ['post_id' => $post->id]) }}" class="more">
            Читать полностью &rarr;
        </a>
        @endif
    </div>
</li>