<div class="media mb-2">
    <div class="avatar_wrapper mr-2" style="background-image: url('{{ $user->avatarUrl() }}')"></div>
    <div class="media-body align-self-center">
        <div><b>{{ $user->getDisplayName() }}</b></div>
        <div>
            <span class="level-badge level-badge-{{ $user->level()->slug }}">{{ $user->level()->name }}</span>
        </div>
    </div>
</div>
<div class="nav flex-column nav-pills">
    <a class="nav-link{!! classActivePath('cabinet') !!}" href="{{ route('cabinet.dashboard') }}">
        Обзор
    </a>
    <a class="nav-link{!! classActivePath('cabinet.materials') !!}" href="{{ route('cabinet.materials') }}">
        Материалы
    </a>
    <a class="nav-link{!! classActivePath('cabinet.tests') !!}" href="{{ route('cabinet.tests') }}">
        Тесты
    </a>
    <a class="nav-link{!! classActivePath('cabinet.events') !!}" href="{{ route('cabinet.events.show') }}">
        Мероприятия
    </a>
    <a class="nav-link{!! classActivePath('cabinet.information') !!}" href="{{ route('cabinet.information.show') }}">
        Моя информация
    </a>
    <a class="nav-link{!! classActivePath('cabinet.conditions') !!}" href="{{ route('cabinet.conditions') }}">
        Условия
    </a>
    <a class="nav-link{!! classActivePath('cabinet.help') !!}" href="{{ route('cabinet.help') }}">
        Помощь
    </a>
</div>