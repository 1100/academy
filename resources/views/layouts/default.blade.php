<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <title>Академия Сантехнических Наук</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="keywords" content="Geberit, Geberit Academy, Академия Сантехнических Наук" />
    <meta name="description" content="Академия Сантехнических Наук" />
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/YouTubePopUp.css') }}">
    <link rel="stylesheet" href="{{ asset('css/flipdown.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" />
</head>
<body>
    <div class="site_container">
        @include('layouts.partials.header')

        @yield('hero')

        @yield('content')

        @yield('sidebar')

        @include('layouts.partials.footer')
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key= AIzaSyA2W_dEztMkbz-FHIIyb-V13ETccsfy3mA"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-50881261-25"></script>
    <script src="{{ asset('js/YouTubePopUp.jquery.js') }}"></script>
    <script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ asset('js/flipdown.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};

        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-50881261-25');
    </script>
</body>
</html>
