<div class="text-center text-larger">До окончания тестирования осталось:</div>
<div id="flipdown" class="flipdown"></div>
<script>
    document.addEventListener('DOMContentLoaded', () => {
        var flipdown = new FlipDown( {{ \Carbon\Carbon::parse($faculty->countdown_date)->timestamp }} )
            .start();
    });
</script>