@extends('layouts.default')

@section('content')
    <div class="container py-5">
        <div class="row">
            <div class="col col-12 col-md-8 order-2 order-md-1">
                <h1 class="content-title underlined-title">Личный кабинет</h1>

                @if($faculty->countdown_enabled)
                    @include('cabinet.countdown')
                @endif
                @include('cabinet.dashboard-alerts')

                @include('cabinet.dashboard-info')

                @if ($user->status === 'approved')
                    @if ($faculty->isEnabledInvite())
                        <h2 class="content-subtitle my-3">Приглашение друзей</h2>
                        <form method="POST" action="{{ route('cabinet.send_invite') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>
                                    Email
                                    <input id="email" name="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"  value="{{ old('email') }}">
                                    <small class="form-text text-muted">На этот email будет отправлена ссылка-приглашение.</small>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            {{ $errors->first('email') }}
                                        </span>
                                    @endif
                                </label>
                            </div>
                            <button type="submit" class="btn btn-primary">Пригласить</button>
                        </form>
                    @endif
                @endif
            </div>
            <div class="col col-12 col-md-4 order-1 order-md-2">
                @include('layouts.partials.sidebar_cabinet')
            </div>
        </div>
    </div>
@endsection