@extends('layouts.default')

@section('content')
    <div class="container py-5">
        <div class="row">
            <div class="col col-12 col-md-8 order-2 order-md-1">
                <h1 class="content-title underlined-title">Условия</h1>
                <div class="text-body">
                    @if($faculty->slug === 'commercial')
                        @include('cabinet.conditions-commercial')
                    @elseif($faculty->slug === 'technical')
                        @include('cabinet.conditions-technical')
                    @else
                        @include('cabinet.conditions-default')
                    @endif
                </div>
            </div>
            <div class="col col-12 col-md-4 order-1 order-md-2">
                @include('layouts.partials.sidebar_cabinet')
            </div>
        </div>
    </div>
@endsection