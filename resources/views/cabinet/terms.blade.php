@extends('layouts.with_sidebar')

@section('content')
    <div class="page relative noborder">
        <div class="slider_content_box small_hero clearfix">
            <div class="slider_content" style="display: block;">
                <h1 class="title">
                    Личный кабинет
                </h1>
            </div>
        </div>
        <div class="page_layout page_margin_top clearfix">
            <div class="page_left">
                <h3 class="box_header">
                    Условия
                </h3>
                <div class="columns clearfix">
                    <p>
                        <a href="{{ url('/') }}/terms/{{ $faculty_slug }}.pdf" target="_blank" class="more blue">Скачать PDF</a>
                    </p>
                    <div style="position:relative; width:100%; height:0px; padding-bottom:80%;">
                        <iframe style="position:absolute; left:0; top:0; width:100%; height:100%"
                                src="{{ url('/') }}/terms/ViewerJS/#../{{ $faculty_slug }}.pdf" allowfullscreen webkitallowfullscreen>
                        </iframe>
                    </div>

                </div>
            </div>
@endsection
@section('sidebar')
    @include('layouts.partials.sidebar_cabinet')
        </div>
    </div>
@endsection