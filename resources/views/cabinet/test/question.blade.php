<form id="protected_block" name="answer_form" onSubmit="return answerOrNext();">
    <p class="question">{!! $question->name !!}</p>
    <input type="hidden" name="question_id" value="{{ $question->id }}"/>
    @foreach($question->answers()->get()->shuffle() as $answer)
    <div class="answer">
        <input id="answer_{{ $answer->id }}" type="radio" name="answer_id" value="{{ $answer->id }}" required/>
        <label id="answer_label_{{ $answer->id }}" for="answer_{{ $answer->id }}">{{ $answer->name }}</label>
        <div class="clearfix"></div>
    </div>
    @endforeach
    <button type="submit" name="submit" class="btn btn-primary submit_answer" value="check_answer">Ответить</button>
</form>