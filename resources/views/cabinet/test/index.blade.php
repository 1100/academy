@extends('layouts.default')

@section('content')
    <div class="container py-5">
        <div class="row">
            <div class="col col-12 col-md-8 order-2 order-md-1">
                <h1 class="content-title underlined-title">Тесты</h1>
                @if ($user->status !== 'approved')
                    @include('cabinet.dashboard-not-approved-alerts')
                @else
                    @if($user->faculty()->first()->tests_enabled)
                        @foreach($tests as $i => $test)
                            @if ($test->userHasEnoughLevel())
                            <div class="test_block {{ ($test->getStatus() === 'open' && $test->getPassingStatus($user->id) === 'not_passing') || $test->getPassingStatus($user->id) === 'passing' ? 'active' : '' }}" data-href="{{ route('cabinet.test.show', ['test_id' => $test->id]) }}">
                                <div class="test_status">
                                    @if($test->getPassingStatus($user->id) == 'completed')
                                        @if($test->userPassed())
                                            <i class="far fa-fw fa-check-square" aria-hidden="true"></i>
                                        @else
                                            <i class="far fa-fw fa-minus-square" aria-hidden="true"></i>
                                        @endif
                                    @elseif($test->getPassingStatus($user->id) == 'passing')
                                        <span class="fa-stack" aria-hidden="true">
                                            <i class="far fa-fw fa-square fa-stack-1x" aria-hidden="true"></i>
                                            <i class="fas fa-fw fa-exclamation fa-stack-0-5x" aria-hidden="true"></i>
                                        </span>
                                    @else
                                        <i class="far fa-fw fa-square"></i>
                                    @endif
                                </div>
                                <div class="test_content">
                                    <h2 class="h5 test_title">
                                        Тема {{ $i+1 }}. {{ $test->name }}
                                        <span class="level-badge level-badge-{{ $test->level()->slug }}">{{ $test->level()->name }}</span>
                                    </h2>
                                    <div class="test_description">{{ $test->description }}</div>
                                    <div class="test_meta">
                                        <i class="far fa-calendar" aria-hidden="true"></i>
                                        {{ date('d.m', strtotime($test->start_date)) }}
                                        -
                                        {{ date('d.m', strtotime($test->end_date)) }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endforeach
                    @else
                        <div class="alert alert-light">
                            На данный момент тестирование не доступно.
                        </div>
                    @endif
                @endif
            </div>
            <div class="col col-12 col-md-4 order-1 order-md-2">
                @include('layouts.partials.sidebar_cabinet')
            </div>
        </div>
    </div>
@endsection