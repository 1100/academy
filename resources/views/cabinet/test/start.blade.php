@extends('layouts.default')

@section('content')
    <div class="container py-5">
        <div class="row">
            <div class="col col-12 col-md-8 order-2 order-md-1">
                <h1 class="content-title underlined-title">Тестирование по теме: {{ $test->name }}</h1>
                <div class="text-body">
                    <div class="test_meta">
                        <i class="far fa-calendar" aria-hidden="true"></i>
                        {{ date('d.m', strtotime($test->start_date)) }}
                        -
                        {{ date('d.m', strtotime($test->end_date)) }}
                    </div>
                    <div id="oneTestHeader">
                        <span id="timeLeft"></span>
                        <h3 id="questionsLeft"></h3>
                    </div>
                    @if(!empty($test->video_identifiers) && ($test->getPassingStatus() !== 'passing'))
                        @foreach($test->video_identifiers as $video_identifier)
                            <div id="videoWrapper" style="position: relative; padding-bottom: 56.25%; height:0;">
                                <iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://www.youtube-nocookie.com/embed/{{$video_identifier}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        @endforeach
                    @endif
                    <div id="testBegin">
                        <p class="text-larger mt-3">{{ $test->description }}</p>
                        <h2 class="h4">Внимание!</h2>
                        <p>
                            После начала тестирования у Вас не будет возможности пересдать тест или сделать паузу.<br><br>
                            Для успешной квалификации на уровень
                            <span class="level-badge level-badge-{{ $test->level()->slug }}">{{ $test->level()->name }}</span>
                            необходимо ответить правильно минимум на <b>{{ $test->minimum_correct_answers }}</b> вопросов из <b>{{ $test->questions_count() }}</b>.<br><br>
                            На прохождение теста отводится <b>{{ $test->duration }}</b> минут.<br><br>

                            Для начала тестированя нажмите на кнопку "Начать тест".
                        </p>
                        <p><a href="{{ route('cabinet.tests') }}">Вернуться к списку тестов</a></p>
                        <button id="startTest" class="btn btn-primary" onclick="startTest({{ $test->id }});">{{ ($test->getPassingStatus() === 'passing') ? 'Продолжить тест' : 'Начать тест' }}</button>
                    </div>
                    <div id="testBody">
                    </div>

                    <script>
                        var subjects = [];
                        var question_counts = [];
                        var durations = [];
                        subjects[{{ $test->id }}] = '{{ $test->name }}';
                        question_counts[{{ $test->id }}] = '{{ $test->questions_count() }}';
                        durations[{{ $test->id }}] = '{{ $test->duration }}';
                    </script>


                </div>
            </div>
            <div class="col col-12 col-md-4 order-1 order-md-2">
                @include('layouts.partials.sidebar_cabinet')
            </div>
        </div>
    </div>
@endsection