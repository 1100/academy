@extends('layouts.default')

@section('content')
    <div class="container py-5">
        <div class="row">
            <div class="col col-12 col-md-8 order-2 order-md-1">
                <h1 class="content-title underlined-title">Тестирование по теме: {{ $test->name }}</h1>
                <div class="text-body">
                    <div class="test_meta">
                        <i class="far fa-calendar" aria-hidden="true"></i>
                        {{ date('d.m', strtotime($test->start_date)) }}
                        -
                        {{ date('d.m', strtotime($test->end_date)) }}
                    </div>
                    <div id="oneTestHeader">
                        <span id="timeLeft"></span>
                        <h3 id="questionsLeft"></h3>
                    </div>
                    <p>{{ $test->description }}</p>
                    @if($test->getStatus() == 'not_open')
                        Тест начнётся {{ date('d.m', strtotime($test->start_date)) }}
                    @else
                        Тест закончился {{ date('d.m', strtotime($test->end_date)) }}
                    @endif
                    <p><a href="{{ route('cabinet.tests') }}">Вернуться к списку тестов</a></p>
                </div>
            </div>
            <div class="col col-12 col-md-4 order-1 order-md-2">
                @include('layouts.partials.sidebar_cabinet')
            </div>
        </div>
    </div>
@endsection