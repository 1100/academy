@extends('layouts.default')

@section('content')
    <div class="container py-5">
        <div class="row">
            <div class="col col-12 col-md-8 order-2 order-md-1">

                <h1 class="content-title underlined-title">Материалы</h1>
                @if ($user->status !== 'approved')
                    @include('cabinet.dashboard-not-approved-alerts')
                @else
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="presentations-tab" data-toggle="tab" href="#presentations" role="tab"
                               aria-controls="presentations" aria-selected="true">Презентации</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="videos-tab" data-toggle="tab" href="#videos" role="tab"
                               aria-controls="videos" aria-selected="false">Видео</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="products-tab" data-toggle="tab" href="#products" role="tab"
                               aria-controls="products" aria-selected="false">Брошюры и каталоги</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="information-tab" data-toggle="tab" href="#information" role="tab"
                               aria-controls="information" aria-selected="false">Информация</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="presentations" role="tabpanel" aria-labelledby="presentations-tab">
                            @include('cabinet.materials.commercial.presentations')
                        </div>
                        <div class="tab-pane fade" id="videos" role="tabpanel" aria-labelledby="videos-tab">
                            @include('cabinet.materials.commercial.videos')
                        </div>
                        <div class="tab-pane fade" id="products" role="tabpanel" aria-labelledby="products-tab">
                            @include('cabinet.materials.commercial.products')
                        </div>
                        <div class="tab-pane fade" id="information" role="tabpanel" aria-labelledby="information-tab">
                            @include('cabinet.materials.commercial.information')
                        </div>
                    </div>
                @endif
            </div>
            <div class="col col-12 col-md-4 order-1 order-md-2">
                @include('layouts.partials.sidebar_cabinet')
            </div>
        </div>
    </div>
@endsection