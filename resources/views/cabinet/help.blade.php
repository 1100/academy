@extends('layouts.default')

@section('content')
    <div class="container py-5">
        <div class="row">
            <div class="col col-12 col-md-8 order-2 order-md-1">
                <h1 class="content-title underlined-title">Помощь</h1>
                <p>Возникли вопросы? Мы всегда рады ответить. Давайте оставаться на связи. Отправьте Ваше сообщение через контактную форму — и мы обязательно ответим!</p>
                <form method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>
                            Ваше сообщение
                            <textarea class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" name="message"></textarea>
                            @if ($errors->has('message'))
                                <span class="invalid-feedback">
                                    Ошибка! {{ $errors->first('message') }}
                                </span>
                            @endif
                        </label>
                    </div>

                    <button type="submit" class="btn btn-primary">Отправить</button>

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                </form>
            </div>
            <div class="col col-12 col-md-4 order-1 order-md-2">
                @include('layouts.partials.sidebar_cabinet')
            </div>
        </div>
    </div>
@endsection