<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/information/images/Geberit_Company_Presentation_ UA 2019.jpg') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Информация о компании</h2>
        <p>Презентация компании Geberit 2019</p>

        <a href="{{ Storage::url('materials/information/Geberit_Company_Presentation_ UA 2019.ppsx') }}"
           class="btn btn-primary" target="_blank">
            Скачать PPSX
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/information/images/preview_geberit.jpg') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit.ua</h2>
        <p>Центр загрузки файлов</p>

        <a href="http://www.geberit.ua/ru_ua/target_groups/installer/service/download_center_4/download_center_3.jsp"
           class="btn btn-primary" target="_blank">
            Перейти
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/information/images/Podskazka_prodavtsu_2017.png') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Памятка Geberit</h2>
        <p>Подсказка продавцу продукции Geberit</p>

        <a href="{{ Storage::url('materials/information/Podskazka_prodavtsu_2017.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/information/images/preview_kolo.jpg') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Kolo.ua</h2>
        <p>Каталоги</p>

        <a href="https://www.kolo.ua/design/catalogues/" class="btn btn-primary" target="_blank">
            Перейти
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/information/images/preview_kolo.jpg') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Памятка Kolo</h2>
        <p>Памятка продавца продукции Kolo</p>

        <a href="{{ Storage::url('materials/information/Pamyatka_KOLO_UA.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>


