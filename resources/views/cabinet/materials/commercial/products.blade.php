<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/products/images/2019_KOLO.jpg') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>KOLO. Прайс-лист 2019</h2>
        <p>Действителен с 01.04.2019 г.</p>

        <a href="{{ Storage::url('materials/products/KOLO Pricelist 2019 web small.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/products/images/2019_Bathroom_Magazine.JPG') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Каталог ванных комнат Geberit 2019/2020</h2>

        <a href="{{ Storage::url('materials/products/2019_Bathroom_Magazine.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/products/images/2019_Cataloge_Ceramic.JPG') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Керамика и мебель Geberit. Каталог 2019/2020</h2>

        <a href="{{ Storage::url('materials/products/2019_Cataloge_Ceramic.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/products/images/2019_Cataloge_Sanitary.JPG') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Сантехнические системы Geberit. Каталог 2019/2020</h2>

        <a href="{{ Storage::url('materials/products/2019_Cataloge_Sanitary.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/products/images/geberit_price_ceramic_2019.JPG') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Керамика и мебель Geberit. Прайс-лист 2019</h2>
        <p>Действителен с 01.04.2019 г.</p>

        <a href="{{ Storage::url('materials/products/geberit_price_ceramic Final_web_12.04.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/products/images/geberit_price_santechnik_2019.JPG') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Сантехнические системы Geberit. Прайс-лист 2019</h2>
        <p>Действителен с 01.04.2019 г.</p>

        <a href="{{ Storage::url('materials/products/geberit_price_2019 Final_web_12.04.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/products/images/2019_Leaflet_instalation.JPG') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Надежные инсталляции и клавиши смыва Geberit</h2>
        <p>Более 50 лет безупречной репутации</p>

        <a href="{{ Storage::url('materials/products/2019_Leaflet_instalation.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/products/images/2019_Brochure_Actuatorplates.jpg') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Клавиши смыва Geberit</h2>
        <p>Выбор имеет значение</p>

        <a href="{{ Storage::url('materials/products/2019_Brochure_Actuatorplates.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/products/images/2019_Brochure_Shower.JPG') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Решения для душевой зоны Geberit</h2>
        <p>Чистый слив</p>

        <a href="{{ Storage::url('materials/products/2019_Brochure_Shower.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/products/images/2019_Brochure_Mapress_Mepla_Volex.JPG') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Трубопроводные системы Geberit</h2>
        <p>Все работает совершенно</p>

        <a href="{{ Storage::url('materials/products/2019_Brochure_Mapress_Mepla_Volex.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/products/images/2019_Brochure_Waste&Drainage.JPG') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Канализационные системы Geberit</h2>
        <p>Безопасный выбор для водоотвода</p>

        <a href="{{ Storage::url('materials/products/2019_Brochure_Waste&Drainage.PDF') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/products/images/2019_Cataloge_Pipes.JPG') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Трубопроводные системы Geberit. Каталог 2019/2020</h2>

        <a href="{{ Storage::url('materials/products/2019_Cataloge_Pipes.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/products/images/2019_Cataloge_Volex.JPG') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Трубопроводные системы Geberit Volex Geberit Silent-PP. Каталог 2019/2020</h2>

        <a href="{{ Storage::url('materials/products/2019_Cataloge_Volex.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/products/images/Beskontaktnye_smesiteli_Geberit.jpg') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Бесконтактные смесители Geberit</h2>
        <p>Тонкий и умный</p>

        <a href="{{ Storage::url('materials/products/Beskontaktnye_smesiteli_Geberit.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/products/images/2016_Brochure_Monolith_OUT.jpg') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Сантехнический модуль Geberit Monolith</h2>
        <p>В главной роли</p>

        <a href="{{ Storage::url('materials/products/2016_Brochure_Monolith_OUT.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/products/images/Sovershen Komfort Final-2.jpg') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Унитаз-биде Geberit AquaClean</h2>
        <p>Совершенный комфорт</p>

        <a href="{{ Storage::url('materials/products/Sovershen Komfort Final-2.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/products/images/2016_Broshure_Delta.jpg') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Встраиваемые системы Geberit Delta</h2>
        <p>Надежно встроено</p>

        <a href="{{ Storage::url('materials/products/2016_Broshure_Delta.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>


