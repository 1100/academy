<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/uHyT5YnKJE4/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Innovation – One step ahead. Traditionally</h2>
        <p>1:57</p>

        <a href="https://www.youtube.com/watch?v=uHyT5YnKJE4" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/OBLU3g163X0/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit AquaClean Sela functionality</h2>
        <p>2:06</p>

        <a href="https://www.youtube.com/watch?v=OBLU3g163X0" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/wh4_3yHv25U/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit AquaClean Sela installation</h2>
        <p>8:09</p>

        <a href="https://www.youtube.com/watch?v=wh4_3yHv25U" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/-QF6NAbI0oA/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit AquaClean Sela set up</h2>
        <p>3:56</p>

        <a href="https://www.youtube.com/watch?v=-QF6NAbI0oA" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/TGBu3QFBtf8/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit DuoFresh Module functionality</h2>
        <p>1:56</p>

        <a href="https://www.youtube.com/watch?v=TGBu3QFBtf8" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/TssQArBDFZY/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit DuoFresh Module installation</h2>
        <p>6:17</p>

        <a href="https://www.youtube.com/watch?v=TssQArBDFZY" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/At4kRkkNJ-c/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit AquaClean Tuma Comfort functionality</h2>
        <p>2:29</p>

        <a href="https://www.youtube.com/watch?v=At4kRkkNJ-c" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/c4cXt34IvNY/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit AquaClean Tuma installation</h2>
        <p>6:06</p>

        <a href="https://www.youtube.com/watch?v=c4cXt34IvNY" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/X-_uEldA55w/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit AquaClean Tuma Comfort service</h2>
        <p>2:08</p>

        <a href="https://www.youtube.com/watch?v=X-_uEldA55w" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/rJyqw9qP8tc/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>Shower surface Setaplano installation</h2>
        <p>8:29</p>

        <a href="https://www.youtube.com/watch?v=rJyqw9qP8tc" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/c-Frqd3rb18/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>Monolith</h2>
        <p>4:56</p>

        <a href="https://www.youtube.com/watch?v=c-Frqd3rb18" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/QKCOrZGveas/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>AquaClean Mera Comfort and Classic installation</h2>
        <p>7:28</p>

        <a href="https://www.youtube.com/watch?v=QKCOrZGveas" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/a3rGr8Bs3PI/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>AquaClean Mera Comfort set up</h2>
        <p>4:35</p>

        <a href="https://www.youtube.com/watch?v=a3rGr8Bs3PI" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/4AwK6K2hd5A/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>AquaClean Mera Classic set up</h2>
        <p>4:25</p>

        <a href="https://www.youtube.com/watch?v=4AwK6K2hd5A" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/Dz6k6VDFaVI/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>AquaClean Mera Comfort standard clean</h2>
        <p>2:38</p>

        <a href="https://www.youtube.com/watch?v=Dz6k6VDFaVI" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/ri7vmhKRAfM/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>AquaClean Mera Classic standard clean</h2>
        <p>2:38</p>

        <a href="https://www.youtube.com/watch?v=ri7vmhKRAfM" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/uKY1xdRGXKQ/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>AquaClean Mera Classic simple clean</h2>
        <p>2:29</p>

        <a href="https://www.youtube.com/watch?v=uKY1xdRGXKQ" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/vSKmnnQBOoI/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>AquaClean Mera Comfort simple clean</h2>
        <p>2:29</p>

        <a href="https://www.youtube.com/watch?v=vSKmnnQBOoI" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/O-hOrsRNzfU/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>AquaClean Mera Classic functionality</h2>
        <p>2:35</p>

        <a href="https://www.youtube.com/watch?v=O-hOrsRNzfU" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/STkyKn1n914/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>AquaClean Mera Comfort functionality</h2>
        <p>2:37</p>

        <a href="https://www.youtube.com/watch?v=STkyKn1n914" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/Xc7VLKzlWXQ/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>AquaClean Mera Classic service</h2>
        <p>2:00</p>

        <a href="https://www.youtube.com/watch?v=Xc7VLKzlWXQ" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/TT3raQl8XCE/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>AquaClean Mera Comfort service</h2>
        <p>2:00</p>

        <a href="https://www.youtube.com/watch?v=TT3raQl8XCE" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/y-8jl1MYicc/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>CleanLine</h2>
        <p>1:06</p>

        <a href="https://www.youtube.com/watch?v=y-8jl1MYicc" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/xs_l2RVbBdw/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>Ceramics Production</h2>
        <p>8:17</p>

        <a href="https://www.youtube.com/watch?v=xs_l2RVbBdw" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/G2TDU-5anZc/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>Urinal System</h2>
        <p>3:14</p>

        <a href="https://www.youtube.com/watch?v=G2TDU-5anZc" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/0SSOkIeCA58/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>Bath production</h2>
        <p>4:47</p>

        <a href="https://www.youtube.com/watch?v=0SSOkIeCA58" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/xPCabG-KxL4/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>Setaplano feature B2B nightmare master</h2>
        <p>0:55</p>

        <a href="https://www.youtube.com/watch?v=xPCabG-KxL4" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/RWcFU957tSE/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>Setaplano feature B2C silk master</h2>
        <p>0:54</p>

        <a href="https://www.youtube.com/watch?v=RWcFU957tSE" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/wY7lMR3p8kA/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>Rimfree Kolo</h2>
        <p>1:44</p>

        <a href="https://www.youtube.com/watch?v=wY7lMR3p8kA" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>

<div class="material">
    <div class="material-image">
        <img src="https://i.ytimg.com/vi/6DjddNhVbVc/hqdefault.jpg" alt=""/>
    </div>
    <div class="material-content">
        <h2>PushControl</h2>
        <p>1:28</p>

        <a href="https://www.youtube.com/watch?v=6DjddNhVbVc" class="popup-video btn btn-primary">
            Смотреть
        </a>
    </div>
</div>