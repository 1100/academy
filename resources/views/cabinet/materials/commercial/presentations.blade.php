<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/2019 ACANTO Geberit.jpg') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Коллекция керамики и мебели Acanto. Новинка 2019</h2>

        <a href="{{ Storage::url('materials/presentations/2019 ACANTO Geberit.ppsx') }}" class="btn btn-primary"
           target="_blank">
            Скачать PPSX
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/2019 VARIFORM Geberit.jpg') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Умывальники Variform. Новинка 2019</h2>

        <a href="{{ Storage::url('materials/presentations/2019 VARIFORM Geberit.ppsx') }}" class="btn btn-primary"
           target="_blank">
            Скачать PPSX
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/2019 АquaClean Sela.jpg') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit AquaClean Sela. Новинка 2019</h2>

        <a href="{{ Storage::url('materials/presentations/2019 АquaClean Sela.ppsx') }}" class="btn btn-primary"
           target="_blank">
            Скачать PPSX
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/2019 Новые клавиши смыва Geberit.jpg') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Клавиши смыва. Новинки 2019</h2>

        <a href="{{ Storage::url('materials/presentations/2019 Новые клавиши смыва Geberit.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/2019 Система удаления запахов DuoFresh Geberit.jpg') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Новая система удаления запахов DuoFresh. Новинка 2019</h2>

        <a href="{{ Storage::url('materials/presentations/2019 Система удаления запахов DuoFresh Geberit.ppsx') }}" class="btn btn-primary"
           target="_blank">
            Скачать PPSX
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/2019 Shower GEO KOLO.jpg') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>KOLO Душевая кабина Geo. Новинка 2019</h2>

        <a href="{{ Storage::url('materials/presentations/2019 Shower GEO KOLO.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/2019 Трапы для пола Geberit.jpg') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Varino Трапы для пола</h2>

        <a href="{{ Storage::url('materials/presentations/2019 Трапы для пола Geberit.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/preview_geberit.jpg') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit</h2>
        <p>Обзор основных продуктов</p>

        <a href="{{ Storage::url('materials/presentations/Main_prod_GEBERIT.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/preview_kolo.jpg') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>KOLO</h2>
        <p>Обзор основных продуктов</p>

        <a href="{{ Storage::url('materials/presentations/2018_KOLO_Обзорная презентация _24 04 2018.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit AquaClean Tuma. Новинка 2018.png') }}"
             alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit AquaClean Tuma. Новинка 2018</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit AquaClean Tuma. Новинка 2018.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit Cleanline под плитку. Новинка 2018.png') }}"
             alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Cleanline под плитку. Новинка 2018</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit Cleanline под плитку. Новинка 2018.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit Sigma21. Новинка 2018.png') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Sigma21. Новинка 2018</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit Sigma21. Новинка 2018.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit Новый клапан наполнения. Новинка 2018.png') }}"
             alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Новый клапан наполнения. Новинка 2018</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit Новый клапан наполнения. Новинка 2018.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit Системы инсталляций для WC. Duofix.png') }}"
             alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Системы инсталляций для WC. Duofix</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit Системы инсталляций для WC. Duofix.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit Основы скрытых бачков.png') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Основы скрытых бачков</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit Основы скрытых бачков.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit Сантехнический модуль Monolith.png') }}"
             alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Сантехнический модуль Monolith</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit Сантехнический модуль Monolith.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit Трапы для душа. Без поддона.png') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Трапы для душа. Без поддона</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit Трапы для душа. Без поддона.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit CleanLine. Душевой канал.png') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit CleanLine. Душевой канал</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit CleanLine. Душевой канал.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit Трап в пол для душа. Отведение воды из душа.png') }}"
             alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Трап в пол для душа. Отведение воды из душа</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit Трап в пол для душа. Отведение воды из душа.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit Трап в стену для душа. Инновационное решение.png') }}"
             alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Трап в стену для душа. Инновационное решение</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit Трап в стену для душа. Инновационное решение.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit Setaplano. Поверхность для душевых зон.png') }}"
             alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Setaplano. Поверхность для душевых зон</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit Setaplano. Поверхность для душевых зон.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit AquaClean Mera. Classic_Comfort.png') }}"
             alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit AquaClean Mera. Classic & Comfort</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit AquaClean Mera. Classic_Comfort.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit AquaClean. Обзор продуктов.png') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit AquaClean. Обзор продуктов</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit AquaClean. Обзор продуктов.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit Автоматический смыв унитаза. Sigma80 & Sigma10.png') }}"
             alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Автоматический смыв унитаза. Sigma80 & Sigma10</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit Автоматический смыв унитаза. Sigma80 & Sigma10.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit Системы смыва писсуара. Basic.png') }}"
             alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Системы смыва писсуара. Basic</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit Системы смыва писсуара. Basic.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit Системы смыва писсуара. Автоматический, пневматический, скрытый.png') }}"
             alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Системы смыва писсуара. Автоматический, пневматический, скрытый</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit Системы смыва писсуара. Автоматический, пневматический, скрытый.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit Система смесителей. Новые продукты 2017.png') }}"
             alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Система смесителей. Новые продукты 2017</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit Система смесителей. Новые продукты 2017.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit Писсуары.png') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Писсуары</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit Писсуары.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit Дистанционный смыв унитаза. Type 01_10.png') }}"
             alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Дистанционный смыв унитаза. Type 01/10</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit Дистанционный смыв унитаза. Type 01_10.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit Обзор систем смыва и смесителей.png') }}"
             alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Обзор систем смыва и смесителей</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit Обзор систем смыва и смесителей.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Geberit Дистанционный смыв type 70. Для гидравлического сервопривода.png') }}"
             alt=""/>
    </div>
    <div class="material-content">
        <h2>Geberit Дистанционный смыв type 70. Для гидравлического сервопривода</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Geberit Дистанционный смыв type 70. Для гидравлического сервопривода.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Механизмы Geberit. Basic клапан наполнения и смыва.png') }}"
             alt=""/>
    </div>
    <div class="material-content">
        <h2>Механизмы Geberit. Basic клапан наполнения и смыва</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Механизмы Geberit. Basic клапан наполнения и смыва.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Механизмы Geberit. Premium клапан наполнения и смыва.png') }}"
             alt=""/>
    </div>
    <div class="material-content">
        <h2>Механизмы Geberit. Premium клапан наполнения и смыва</h2>
        <p></p>
        <a href="{{ Storage::url('materials/presentations/Механизмы Geberit. Premium клапан наполнения и смыва.pdf') }}"
           class="btn btn-primary" target="_blank">
            скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/preview_3.jpg') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Sigma 8 cm</h2>
        <p>Новые Sigma 8 cm. Информация о продукции</p>

        <a href="{{ Storage::url('materials/presentations/New_Sigma_8_cm.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/5_etapov.png') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>5 этапов</h2>
        <p>Оксана Марусич</p>

        <a href="{{ Storage::url('materials/presentations/5_etapov.pdf') }}" class="btn btn-primary" target="_blank">
            Скачать PDF
        </a>
    </div>
</div>
<div class="material">
    <div class="material-image" href="#" title="">
        <img src="{{ Storage::url('materials/presentations/images/Prodazhi_na_rezultat.png') }}" alt=""/>
    </div>
    <div class="material-content">
        <h2>Продажи на результат</h2>
        <p>Оксана Марусич</p>

        <a href="{{ Storage::url('materials/presentations/Prodazhi_na_rezultat.pdf') }}" class="btn btn-primary"
           target="_blank">
            Скачать PDF
        </a>
    </div>
</div>


