@if ($user->status === 'not_verified')
    <div class="alert alert-warning">
        <span>
            <b>Приветствуем в Академии сантехнических наук Geberit!</b><br>
                Благодарим за Вашу заявку на регистрацию в факультете "{{ $user->faculty()->first()->description }}".<br><br>
            Заявка успешно принята и ожидает проверки. После её подтверждения Вы сможете получить доступ ко всем материалам и тестированию.<br><br>
            Убедитесь что Вы заполнили всю возможную информацию в разделе <a href="{{ route('cabinet.information.show') }}">"Моя информация"</a>.<br>
            Если у Вас есть вопросы – свяжитесь с нами через раздел <a href="{{ route('cabinet.help') }}">"Помощь"</a>.
        </span>
    </div>
    @elseif ($user->status === 'rejected')
    <div class="alert alert-danger">
        <span>
            Ваша заявка на участие была отклонена.<br>
            Если у Вас есть вопросы – свяжитесь с нами через раздел <a href="{{ route('cabinet.help') }}">"Помощь"</a>.
        </span>
    </div>
@endif