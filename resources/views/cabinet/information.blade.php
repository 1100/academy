@extends('layouts.default')

@section('content')
    <div class="container py-5">
        <div class="row">
            <div class="col col-12 col-md-8 order-2 order-md-1">
                <h1 class="content-title underlined-title">Моя информация</h1>

                @if($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Ошибка</strong>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Закрыть">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                <h2 class="content-subtitle my-3">Основная информация</h2>
                <div class="form">
                    @foreach($login_fields as $field)
                        @php ($field->value = $user->getInformation($field->slug))
                        @if($field->value && $field->slug != 'facebook')
                            @include('user_information.'.$field->type)
                        @else
                            @include('user_information.'.$field->type)
                        @endif
                    @endforeach
                </div>

                <h2 class="content-subtitle my-3">Фотография</h2>
                <form class="form change_avatar" method="POST" enctype="multipart/form-data" action="{{ route('cabinet.information.change_avatar') }}">
                    {{ csrf_field() }}
                    <div class="avatar_wrapper" style="background-image: url('{{ $user->avatarUrl() }}')"></div>
                    <label class="file_wrapper">
                        <button type="submit" class="btn btn-primary">Загрузить фотографию</button>
                        <input type="file" id="avatar" name="avatar" placeholder="Изображение">
                    </label>
                    <div class="save_wrapper">
                        <button type="submit" class="btn btn-primary" disabled="true" id="save_avatar_btn">Сохранить
                        </button>
                    </div>
                </form>
                <script>
                    function handleImagesSelect(evt) {
                        var files = evt.target.files;

                        for (var i = 0, f; f = files[i]; i++) {
                            var reader = new FileReader();
                            reader.onload = (function (image) {
                                return function (e) {
                                    var avatar_wrapper = document.createElement('div');
                                    avatar_wrapper.className = 'avatar_wrapper';
                                    avatar_wrapper.innerHTML = '<div class="avatar_wrapper" style="background-image: url(' + e.target.result + ');"></div> ';
                                    var old_avatar_wrapper = document.querySelectorAll('.change_avatar .avatar_wrapper')[0];
                                    old_avatar_wrapper.parentNode.replaceChild(avatar_wrapper, old_avatar_wrapper);
                                    document.getElementById('save_avatar_btn').disabled = false;
                                };
                            })(f);
                            reader.readAsDataURL(f);
                        }
                    }

                    document.getElementById('avatar').addEventListener('change', handleImagesSelect, false);
                </script>

                <h2 class="content-subtitle my-3">Дополнительная информация</h2>
                <form class="form" method="POST" action="{{ route('cabinet.information.change_additional') }}">
                    {{ csrf_field() }}
                    @foreach($additional_fields as $field)
                        @php ($field->value = $user->getInformation($field->slug))
                        @include('user_information.input.'.$field->type)
                    @endforeach
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </form>

                <h2 class="content-subtitle my-3">Смена пароля</h2>
                <form class="form" method="POST" action="{{ route('cabinet.information.change_password') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>
                            Новый пароль
                            <input id="password" name="password" type="password" class="form-control" placeholder="••••••••" required autocomplete="new-password">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('password') }}
                                </span>
                            @endif
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            Повторите пароль
                            <input id="password_confirmation" name="password_confirmation" type="password" class="form-control" placeholder="••••••••" required autocomplete="new-password">
                            @if ($errors->has('password_confirmation'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('password_confirmation') }}
                                </span>
                            @endif
                        </label>
                    </div>
                    <button type="submit" class="btn btn-primary">Отправить</button>
                </form>
            </div>
            <div class="col col-12 col-md-4 order-1 order-md-2">
                @include('layouts.partials.sidebar_cabinet')
            </div>
        </div>
    </div>
@endsection