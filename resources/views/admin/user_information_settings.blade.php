@extends('admin.layouts.default')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="admin_table">
                    <caption><h4>Настройки дополнительной информации участников</h4></caption>
                    <thead>
                        <tr>
                            <th>Название</th>
                            <th>Описание</th>
                            <th>Тип</th>
                            <th>Логин</th>
                            <th>Скрытый</th>
                            <th>По умолчанию</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($information_fields as $field)
                            <tr>
                                <td>{{ $field->name }}</td>
                                <td>{{ $field->description }}</td>
                                <td>{{ $field->information_type->name }}</td>
                                <td>
                                    @if($field->login)
                                        <i class="fa fa-fw fa-check text-success"></i>
                                    @else
                                        <i class="fa fa-fw fa-times text-danger"></i>
                                    @endif
                                </td>
                                <td>
                                    @if($field->hidden)
                                        <i class="fa fa-fw fa-check text-success"></i>
                                    @else
                                        <i class="fa fa-fw fa-times text-danger"></i>
                                    @endif
                                </td>
                                <td>{{ $field->default_value }}</td>
                                <td>
                                    @can('permission', 'edit-user-information')
                                        <a class="text-info" href="{{ route('admin.show_user_information', ['user_information_slug' => $field->slug]) }}">
                                            <button>
                                                <i class="fa fa-pencil"></i> редактировать
                                            </button>
                                        </a>
                                    @endcan
                                    @can('permission', 'delete-user-information')
                                        <form method="post" class="form-inline" action="{{ route('admin.delete_user_information', ['user_information_slug' => $field->slug]) }}">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="text-danger">
                                                <i class="fa fa-times"></i> удалить
                                            </button>
                                        </form>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @can('permission', 'add-user-information')
                <h4>Добавление нового поля</h4>
                <form role="form" method="POST" action="{{ url('admin/add_user_information') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('field_name') ? ' has-error' : '' }}">
                        <label for="field_name" class="control-label">Название:</label>
                        <input id="field_name" type="text" class="form-control" name="field_name" value="{{ old('field_name') }}" required autofocus>
                        @if ($errors->has('field_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('field_name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('field_description') ? ' has-error' : '' }}">
                        <label for="field_description" class="control-label">Описание:</label>
                        <input id="field_description" type="text" class="form-control" name="field_description" value="{{ old('field_description') }}">
                        @if ($errors->has('field_description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('field_description') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('field_default_value') ? ' has-error' : '' }}">
                        <label for="field_default_value" class="control-label">Значение по умолчанию:</label>
                        <input id="field_default_value" type="text" class="form-control" name="field_default_value" value="{{ old('field_default_value') }}">
                        @if ($errors->has('field_default_value'))
                            <span class="help-block">
                                <strong>{{ $errors->first('field_default_value') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                        <label for="type" class="control-label">Тип:</label>
                        <select id="type" class="form-control custom-select" name="type" value="{{ old('type') }}" required>
                        @foreach ($information_types as $type)
                            <option value="{{ $type->slug }}">{{ $type->name }}</option>
                        @endforeach
                        </select>
                        @if ($errors->has('type'))
                            <span class="help-block">
                                <strong>{{ $errors->first('type') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('field_login') ? ' has-error' : '' }}">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="field_login" value="1">
                                Использовать как логин
                            </label>
                        </div>

                        @if ($errors->has('field_login'))
                            <span class="help-block">
                                <strong>{{ $errors->first('field_login') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('field_hidden') ? ' has-error' : '' }}">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="field_hidden" value="1">
                                Скрытый
                            </label>
                        </div>

                        @if ($errors->has('field_hidden'))
                            <span class="help-block">
                                <strong>{{ $errors->first('field_hidden') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">
                            Добавить
                        </button>
                    </div>
                </form>
                @endcan
            </div>
        </div>
    </div>

@endsection