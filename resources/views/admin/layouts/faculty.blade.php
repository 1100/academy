@extends('admin.layouts.default')

@section('content')
    <div class="container">
        <div class="row panel-group">
            <div class="col-md-12 row">

                <ol class="breadcrumb">
                    <li><a href="{{ route('admin.faculties.show') }}">Факультеты</a></li>
                    <li><a href="{{ route('admin.faculties.faculty.show', ['faculty' => $faculty->slug]) }}">{{ $faculty->name }}</a>
                    @if(request()->route()->getName() == 'admin.faculties.faculty.show_user'
                        || request()->route()->getName() == 'admin.faculties.faculty.add_user_form')
                    <li><a href="{{ route('admin.faculties.faculty.users', ['faculty' => $faculty->slug]) }}"> Участники </a></li>
                    @endif
                    <li class="active">
                    @if (request()->route()->getName() == 'admin.faculties.faculty.users')
                        Участники
                    @elseif (request()->route()->getName() == 'admin.faculties.faculty.rating.all')
                        Рейтинг
                    @elseif (request()->route()->getName() == 'admin.faculties.faculty.user_information')
                        Информация участников
                    @elseif (request()->route()->getName() == 'admin.faculties.faculty.test.all')
                        Тесты
                    @elseif (request()->route()->getName() == 'admin.faculties.faculty.events.all')
                        Мероприятия
                    @elseif (request()->route()->getName() == 'admin.faculties.faculty.curators')
                        Кураторы
                    @elseif (request()->route()->getName() == 'admin.faculties.faculty.settings.show')
                        Настройки
                    @elseif (request()->route()->getName() == 'admin.faculties.faculty.show_user')
                        Редактирование участника
                    @elseif (request()->route()->getName() == 'admin.faculties.faculty.add_user_form')
                        Новый участник
                    @endif
                    </li>
                </ol>

                <div class="row panel panel-body">
                    <div class="col-md-3">
                        <h4>{{ $faculty->name }}</h4>
                        <ul class="nav nav-pills nav-stacked">
                            @can('permission', 'users')
                            <li @if (request()->route()->getName() == 'admin.faculties.faculty.users'
                            || request()->route()->getName() == 'admin.faculties.faculty.add_user_form'
                            || request()->route()->getName() == 'admin.faculties.faculty.show_user') class="active" @endif>
                                <a href="{{ route('admin.faculties.faculty.users', ['faculty' => $faculty->slug]) }}">
                                    <i class="fa fa-fw fa-users"></i>
                                    Участники
                                </a>
                            </li>
                            @endcan
                            @can('permission', 'rating')
                            <li @if (request()->route()->getName() == 'admin.faculties.faculty.rating.all') class="active" @endif>
                                <a href="{{ route('admin.faculties.faculty.rating.all', ['faculty' => $faculty->slug]) }}">
                                    <i class="fa fa-fw fa-star"></i>
                                    Рейтинг
                                </a>
                            </li>
                            @endcan
                            @can('permission', 'faculty-user-information')
                            <li @if (request()->route()->getName() == 'admin.faculties.faculty.user_information') class="active" @endif>
                                <a href="{{ route('admin.faculties.faculty.user_information', ['faculty' => $faculty->slug]) }}">
                                    <i class="fa fa-fw fa-id-card-o"></i>
                                    Информация участников
                                </a>
                            </li>
                            @endcan
                            @can('permission', 'tasks')
                            <li @if (request()->route()->getName() == 'admin.faculties.faculty.test.all'
                            || request()->route()->getName() == 'admin.faculties.faculty.test.add'
                            || request()->route()->getName() == 'admin.faculties.faculty.test.edit'
                            || request()->route()->getName() == 'admin.faculties.faculty.test.question.all'
                            || request()->route()->getName() == 'admin.faculties.faculty.test.question.edit'
                            || request()->route()->getName() == 'admin.faculties.faculty.test.question.add'
                            || request()->route()->getName() == 'admin.faculties.faculty.test.add') class="active" @endif>
                                <a href="{{ route('admin.faculties.faculty.test.all', ['faculty' => $faculty->slug]) }}">
                                    <i class="fa fa-fw fa-check-square-o"></i>
                                    Тесты
                                </a>
                            </li>
                            @endcan
                            @can('permission', 'events')
                            <li @if (request()->route()->getName() == 'admin.faculties.faculty.events.show_all'
                            || request()->route()->getName() == 'admin.faculties.faculty.events.show_add_event_form'
                            || request()->route()->getName() == 'admin.faculties.faculty.events.add_event'
                            || request()->route()->getName() == 'admin.faculties.faculty.events.show_event'
                            || request()->route()->getName() == 'admin.faculties.faculty.events.edit_event'
                            || request()->route()->getName() == 'admin.faculties.faculty.events.delete_event') class="active" @endif>
                                <a href="{{ route('admin.faculties.faculty.events.show_all', ['faculty' => $faculty->slug]) }}">
                                    <i class="fa fa-fw fa-calendar"></i>
                                    Мероприятия
                                </a>
                            </li>
                            @endcan
                            @can('permission', 'admins')
                            <li @if (request()->route()->getName() == 'admin.faculties.faculty.curators') class="active" @endif>
                                <a href="{{ route('admin.faculties.faculty.curators', ['faculty' => $faculty->slug]) }}">
                                    <i class="fa fa-fw fa-user-secret"></i>
                                    Кураторы
                                </a>
                            </li>
                            @endcan
                            @if( Gate::allows('permission', 'faculty-settings') || Gate::allows('permission', 'faculty-display-settings') )
                            <li @if (request()->route()->getName() == 'admin.faculties.faculty.settings.show') class="active" @endif>
                                <a href="{{ route('admin.faculties.faculty.settings.show', ['faculty' => $faculty->slug]) }}">
                                    <i class="fa fa-fw fa-cogs"></i>
                                    Настройки
                                </a>
                            </li>
                            @endif
                        </ul>
                    </div>
                    <div class="col-md-9 faculty-content">
                        @yield('faculty_content')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection