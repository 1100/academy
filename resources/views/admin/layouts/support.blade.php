@extends('admin.layouts.default')

@section('content')
    <div class="container">
        <div class="row panel-group">
            <div class="col-md-12 row">

                <ol class="breadcrumb">
                    <li><a href="{{ route('admin.support.show_all') }}">Сообщения</a></li>
                    <li class="active">
                    @if (request()->route()->getName() == 'admin.support.show_all')
                        Все
                    @elseif (request()->route()->getName() == 'admin.support.unread')
                        Непрочитанные
                    @elseif (request()->route()->getName() == 'admin.support.read')
                        Прочитанные
                    @endif
                    </li>
                </ol>

                <div class="row panel panel-body">
                    <div class="col-md-3">
                        <h4>Сообщения</h4>
                        <ul class="nav nav-pills nav-stacked">
                            <li class="{!! classActivePath('admin.support') !!}">
                                <a href="{{ route('admin.support.show_all') }}">
                                    <i class="fa fa-fw fa-inbox"></i>
                                    <span class="badge pull-right all-messages-badge">{{ Auth::guard('admin')->user()->getAllSupportMessagesCount() }}</span>
                                    Все
                                </a>
                            </li>
                            <li class="{!! classActivePath('admin.support.unread') !!}">
                                <a href="{{ route('admin.support.unread') }}">
                                    <i class="fa fa-fw fa-envelope"></i>
                                    <span class="badge pull-right unread-messages-badge">{{ Auth::guard('admin')->user()->getUnreadSupportMessagesCount() }}</span>
                                    Непрочитанные
                                </a>
                            </li>
                            <li class="{!! classActivePath('admin.support.read') !!}">
                                <a href="{{ route('admin.support.read') }}">
                                    <i class="fa fa-fw fa-envelope-open"></i>
                                    <span class="badge pull-right read-messages-badge">{{ Auth::guard('admin')->user()->getReadSupportMessagesCount() }}</span>
                                    Прочитанные
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9 faculty-content">
                        @yield('support_content')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection