<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">


    <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/colors.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.css" rel="stylesheet">
    <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.js" defer></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/lang/summernote-ru-RU.js" defer></script>
</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Показать меню</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    Академия
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    @if (Auth::guard('admin')->user())
                        <li class="{!! classActivePath('admin.faculties', true) !!}">
                            <a href="{{ route('admin.faculties.show') }}">Факультеты</a>
                        </li>
                        @can('permission', 'user-information')
                        <li class="{!! classActivePath('admin.user_information_settings') !!}">
                            <a href="{{ route('admin.information_settings') }}">Дополнителльные поля</a>
                        </li>
                        @endcan
                        @can('permission', 'admins')
                        <li class="{!! classActivePath('admin.all_admins') !!}">
                            <a href="{{ route('admin.all_admins') }}">Администраторы</a>
                        </li>
                        @endcan
                        @can('permission', 'news')
                        <li class="{!! classActivePath('admin.news', true) !!}">
                            <a href="{{ route('admin.news.show_all') }}">Новости</a>
                        </li>
                        @endcan
                        @can('permission', 'support-messages')
                        <li class="{!! classActivePath('admin.support', true) !!}">
                            <a href="{{ route('admin.support.show_all') }}">Поддержка <span class="badge {{ Auth::guard('admin')->user()->getUnreadSupportMessagesCount() > 0 ? 'badge-danger' : '' }} unread-messages-badge">{{ Auth::guard('admin')->user()->getUnreadSupportMessagesCount() }}</span></a>
                        </li>
                        @endcan
                        @can('role', 'admin')
                        <li class="{!! classActivePath('admin.subscribers', true) !!}">
                            <a href="{{ route('admin.subscribers.show') }}">Подписчики</a>
                        </li>
                        @endcan
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guard('admin')->user())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                            {{ Auth::guard('admin')->user()->login }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ url('/admin/logout') }}"
                                   onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();
                                ">
                                    Выйти
                                </a>

                                <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/moment-with-locales.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
<script>
    function bindTooltips() {
        $('[data-toggle="tooltip"]').tooltip();
    }
    $(document).ready(function() {
        $('[data-href]').click(function (e) {
            window.location.href = this.dataset.href;
        });
        bindTooltips();
        $('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            sideBySide: true,
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
        });

        $('.wysiwyg').summernote({
            callbacks: {
                // remove image upload
                onImageUpload: function (data) { data.pop(); }
            },
            lang: 'ru-RU',
            keyMap: {
                pc: { 'ENTER': '' },
                mac: { 'ENTER': '' }
            },
            isableDragAndDrop: true,
            toolbar: [
                ['undo/redo', ['undo', 'redo']],
                ['paragraph', ['style']],
                ['style', ['bold', 'italic', 'clear']],
                ['para', ['paragraph', 'ul', 'ol']],
                ['media', ['link', 'picture', 'video', 'table']],
                ['misc', ['codeview', 'fullscreen']]
            ]
        });

    });
</script>
<style>
    /* remove image upload from summernote */
    .note-group-select-from-files {
        display: none;
    }
</style>
</body>
</html>
