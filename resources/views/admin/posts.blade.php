@extends('admin.layouts.default')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="admin_table">
                    <caption>
                        <h4 class="pull-left">Новости</h4>
                        <a href="{{ route('admin.news.add_post') }}" class="btn btn-success pull-right">
                            <i class="fa fa-plus"></i>
                            Добавить
                        </a>
                    </caption>
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Заголовок</th>
                        <th>Факультет</th>
                        <th>Контент</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                        <tr data-href="{{ route('admin.news.show_post', ['post_id' => $post->id]) }}">
                            <td> {{ $post->id }} </td>
                            <td> {{ $post->title }} </td>
                            <td> {{ $post->faculty ? $post->faculty()->name : 'Все' }} </td>
                            <td>
                                {{ str_limit($post->content, 150) }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="col-md-12 text-center">
                    {{ $posts->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection