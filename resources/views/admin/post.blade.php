@extends('admin.layouts.default')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('admin.news.edit_post', ['post_id' => $post->id]) }}">
                    {{ csrf_field() }}
                    <h4>Редактирование новости</h4>
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title">Заголовок</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Заголовок" value="{{ $post->title }}">
                    </div>
                    <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                        <label for="image">Изображение</label>
                        @if($post->image)
                            <img class="img-thumbnail img-responsive" src="{{ Storage::url($post->image) }}">
                        @endif
                        <input type="file" class="form-control" id="image" name="image" placeholder="Изображение" value="{{ old('image') }}">
                    </div>
                    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                        <label for="content">Контент</label>
                        <textarea class="form-control wysiwyg" id="content" name="content" placeholder="Контент" rows="15">
                            {!! nl2br(e($post->content)) !!}
                        </textarea>
                    </div>
                    <div class="form-group{{ $errors->has('faculty') ? ' has-error' : '' }}">
                        <label for="faculty">Факультет</label>
                        <select id="faculty" class="form-control" name="faculty">
                            @foreach ($faculties as $faculty)
                                <option @if($post->faculty == $faculty->slug) selected @endif value="{{ $faculty->slug }}">{{ $faculty->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <a class="btn btn-primary" href="{{ route('admin.news.show_all') }}">Отмена</a>
                    <button type="submit" class="btn btn-success">Сохранить</button>
                    <button onclick="event.preventDefault(); document.getElementById('delete-form').submit();" class="btn btn-danger">Удалить</button>
                </form>
                <form id="delete-form" action="{{ route('admin.news.delete_post', ['post_id' => $post->id]) }}" method="POST" style="display: none;">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                </form>
                <br>
            </div>
        </div>
    </div>
@endsection