@extends('admin.layouts.default')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form role="form" method="POST" action="{{ url('admin/create_user') }}">
                    {{ csrf_field() }}
                <table class="admin_table">
                    <caption>Список всех пользователей</caption>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Facebook_id</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach( $users as $user)
                        <tr>
                            <td> <a href="{{ url('admin/user/'.$user->id) }}">{{ $user->name }}</a> </td>
                            <td> {{ $user->email }} </td>
                            <td> {{ $user->phone }} </td>
                            <td> <a href="https://fb.com/app_scoped_user_id/{{ $user->facebook_id }}" target="_blank">{{  $user->facebook_id }}</a> </td>
                            <td> {{ $user->moderation_status()->first()->name }} </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    @can('permission', 'add-user')
                    <tr>
                        <td>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Name</label>
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" autofocus>
                            </div>
                        </td>
                        <td>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Email</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('name') }}" autofocus>
                            </div>
                        </td>
                        <td>
                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="phone" class="col-md-4 control-label">Phone</label>
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" autofocus>
                            </div>
                        </td>
                        <td></td>
                        <td>
                            <button type="submit" class="btn btn-primary">
                                Добавить
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="{{ $errors->has('name') ? ' has-error' : '' }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                            </div>
                        </td>
                        <td>
                            <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                            </div>
                        </td>
                        <td>
                            <div class="{{ $errors->has('phone') ? ' has-error' : '' }}">
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                            </div>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endcan
                    </tfoot>
                </table>
                </form>
            </div>
        </div>
    </div>

@endsection