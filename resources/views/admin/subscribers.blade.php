@extends('admin.layouts.default')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="admin_table">
                    <caption><h4>Подписчики</h4></caption>
                    <thead>
                    <tr>
                        <th>Email адресс</th>
                        <th>Дата подписки</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($subscribers as $subscriber)
                        <tr>
                            <td> {{ $subscriber->email }} </td>
                            <td> {{ $subscriber->created_at->format('d.m.y H:i') }} </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
                <div class="col-md-12 text-center">
                    {{ $subscribers->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection