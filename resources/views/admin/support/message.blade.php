<tr id="message-{{ $message->id }}">
    <td class="message{{ ($message->read_status) ? ' read' : ' unread' }}">
        <div class="modal-header row">
            <div class="col-md-8">
                <span data-toggle="tooltip" data-placement="top" title="{{ (is_null($message->user)) ? 'Незарегистрированный пользователь' : 'Факультет &laquo;'.$message->user->faculty()->first()->name.'&raquo;' }}">

                    @if($message->user)

                        <i class="fa fa-user faculty-text-{{ $message->user->faculty()->first()->color }}" aria-hidden="true"></i>

                    @endif

                    <b>{{ $message->last_name }} {{ $message->first_name }}</b>
                </span>
                <div class="small">

                    @if ($message->phone)
                        <span data-toggle="tooltip" data-placement="bottom" title="Телефон">
                            <i class="fa fa-phone text-primary" aria-hidden="true"></i>
                            {{ $message->phone }}
                        </span>
                    @endif
                    @if ($message->email)
                        <span data-toggle="tooltip" data-placement="bottom" title="E-mail">
                            <i class="fa fa-at text-primary" aria-hidden="true"></i>
                            {{ $message->email }}
                        </span>
                    @endif

                </div>
            </div>
            <div class="col-md-3 text-right">
                <span data-toggle="tooltip" data-placement="top" title="Дата отправки сообщения">
                    <i class="fa fa-clock-o text-primary" aria-hidden="true"></i>
                    {{ $message->created_at->format('d.m.y H:i') }}
                </span>
                <span class="text-{{ ($message->read_status) ? 'success' : 'danger' }}" data-toggle="tooltip" data-placement="bottom" title="Статус сообщения">
                    @if ($message->read_status)
                        прочитано <b>{{ $message->admin->login }}</b>
                    @else
                        непрочитано
                    @endif
                </span>
            </div>
            <div class="col-md-1">

                @if($message->read_status)
                    <button class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Отметить непрочитанным" onclick="changeReadStatus({{ $message->id }}, 0)">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                    </button>
                @else
                    <button class="btn btn-success" data-toggle="tooltip" data-placement="bottom" title="Отметить прочитанным" onclick="changeReadStatus({{ $message->id }}, 1)">
                        <i class="fa fa-envelope-open-o" aria-hidden="true"></i>
                    </button>
                @endif

            </div>
        </div>
        <div class="modal-body col-md-12">
            {{ $message->message }}
        </div>
    </td>
</tr>