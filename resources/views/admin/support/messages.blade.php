@extends('admin.layouts.support')
@section('support_content')
    <table class="admin_table">

        @foreach($messages as $message)
            @include('admin.support.message')
        @endforeach

    </table>
    <script src="{{ asset('js/support.js') }}"></script>
@endsection