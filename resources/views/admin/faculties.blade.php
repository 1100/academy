@extends('admin.layouts.default')

@section('content')
<div class="container">
    <div class="row panel-group">
        <div class="col-md-12 row">
            <ol class="breadcrumb">
                <li><a href="#">Факультеты</a></li>
            </ol>
        @foreach ($faculties as $faculty)
            <div class="col-md-6 card-wrapper">
                <a href="{{ route('admin.faculties.faculty.show', ['faculty' => $faculty->slug]) }}" class="card">
                    <div class="panel panel-default panel-body">
                        <h2>{{ $faculty->name }}</h2>
                        <h4>{{ $faculty->description }}</h4>
                    </div>
                </a>
            </div>
        @endforeach
        </div>
    </div>
</div>
@endsection