@extends('admin.layouts.faculty')
@section('faculty_content')
<div class="row">

    <div class="col-md-12">
        <h4 class="pull-left">Дополнительная информация</h4>
        <a href="{{ route('admin.information_settings') }}" class="btn btn-warning pull-right">
            <i class="fa fa-cog"></i>
            Управление дополнительными полями
        </a>
    </div>



    <form class="{{ $errors->has('fields') ? ' has-error' : '' }} col-md-12" role="form" method="POST" action="{{ route('admin.faculties.faculty.update_required_fields', ['faculty' => $faculty->slug]) }}">
        {{ csrf_field() }}
        @if($faculty->additionalInformationFields()->isNotEmpty())
        <div class="form-group">
            <table class="admin_table">
                <thead>
                <tr>
                    <th>Название</th>
                    <th>Описание</th>
                    <th>Тип</th>
                    <th>Обязательное</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($faculty->additionalInformationFields() as $field)
                    <tr>
                        <td>{{ $field->name }}</td>
                        <td>{{ $field->description }}</td>
                        <td>{{ $field->information_type->name }}</td>
                        <td>
                            <div class="checkbox">
                                <label for="additional-field-{{ $field->slug }}">
                                    <input id="additional-field-{{ $field->slug }}" type="checkbox" name="fields[{{ $field->slug }}]" @if($faculty->informationFieldRequired($field)) checked @endif value="{{ $field->slug }}">
                                </label>
                            </div>
                        </td>
                        <td>
                            <a href="{{ route('admin.faculties.faculty.delete_field', ['faculty' => $faculty->slug, 'field' => $field->slug]) }}" class="text-danger btn btn-default">
                                <i class="fa fa-times"></i> удалить
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary col-md-3">Сохранить</button>
        </div>
        @else
            <div class="alert alert-warning" role="alert">
                Пока не добавлено ни одного дополнительного поля
            </div>
        @endif
    </form>


    <form class="{{ $errors->has('new_field') ? ' has-error' : '' }} form-inline col-md-12" role="form" method="POST" action="{{ route('admin.faculties.faculty.add_field', ['faculty' => $faculty->slug]) }}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <h4>Новое поле информации</h4>
        @if($faculty->availableAdditionalInformationFields()->isNotEmpty())
        <div class="form-group">
            <select id="new_field" class="form-control" name="new_field" value="{{ old('new_field') }}" required>
                @foreach ($faculty->availableAdditionalInformationFields() as $field)
                    <option value="{{ $field->slug }}">{{ $field->name }}</option>
                @endforeach
            </select>
            @if ($errors->has('new_field'))
                <span class="help-block">
                    <strong>{{ $errors->first('new_field') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Добавить
            </button>
        </div>
        @else
            <div class="alert alert-warning" role="alert">
                Нет дополнительных полей<br>
                <a href="{{ route('admin.information_settings') }}">
                    Управление дополнительными полями
                </a>
            </div>
        @endif
    </form>


    <form class="{{ $errors->has('login_fields') ? ' has-error' : '' }} col-md-12" role="form" method="POST" action="{{ route('admin.faculties.faculty.update_login_fields', ['faculty' => $faculty->slug]) }}">
        {{ csrf_field() }}
        <h4>Информация используемая для входа</h4>

        @foreach ($login_information_fields as $field)

            <label for="login-field-{{ $field->slug }}" class="col-md-4">
                <input id="login-field-{{ $field->slug }}" type="checkbox" name="login_fields[]" @if($faculty->information->contains($field)) checked @endif value="{{ $field->slug }}">
                {{ $field->name }}
            </label>

        @endforeach

        @if ($errors->has('login_fields') && old('faculty') == $faculty->slug)
            <span class="help-block">
                                            <strong>{{ $errors->first('login_fields') }}</strong>
                                        </span>
        @endif
        <button type="submit" class="btn btn-primary col-md-3">Сохранить</button>
    </form>
</div>
@endsection