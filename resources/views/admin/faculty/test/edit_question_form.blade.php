@extends('admin.layouts.faculty')
@section('faculty_content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('admin.faculties.faculty.test.question.all', ['faculty' => $faculty->slug, 'test_id' => $test->id]) }}" class="btn btn-warning pull-right">
                <i class="fa fa-arrow-left"></i>
                Все вопросы
            </a>
            <h4 class="pull-left">{{ (isset($question)) ? 'Редактировать вопрос' : 'Новый вопрос' }}</h4>
        </div>
        <form role="form" method="POST" action="{{
            (isset($question)) ? route('admin.faculties.faculty.test.question.edit', ['faculty' => $faculty->slug, 'test_id' => $test->id, 'question_id' => $question->id]) : route('admin.faculties.faculty.test.question.add', ['faculty' => $faculty->slug, 'test_id' => $test->id]) }}" class="col-md-12">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="name">Название</label>
                <div class="block{{ $errors->has('name') ? ' has-error' : '' }}">

                    <textarea id="name" class="wysiwyg form-control" name="name" type="text">{!! (old('name') ? old('name') : ((isset($question)) ? $question->name : '' )) !!}</textarea>
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <script>
                var answer_index = 0;
                function addAnswer(event) {
                    event.preventDefault();
                    $('#answers_table').append('<tr>' +
                        '<td><input class="form-control" name="new_answers[' + answer_index + '][name]" type="text" value=""/></td>' +
                        '<td><input type="checkbox" name="new_answers[' + answer_index + '][is_correct]" /></td>'+
                        '</tr>');
                    answer_index++;
                }
            </script>
            <div class="form-group">
            <table class="admin_table">
                <thead>
                <tr>
                    <th>
                        Ответ
                    </th>
                    <th>
                        Верный
                    </th>
                </tr>
                </thead>
                <tbody id="answers_table">
                @isset($question)
                    @foreach($question->answers as $answer)
                        <tr>
                            <td>
                                <input class="form-control" name="answers[{{ $answer->id }}][name]" type="text" value="{{ (old('answers['.$answer->id.'][name]')) ? old('answer['.$answer->id.'][name]') : $answer->name }}"/>
                            </td>
                            <td>
                                <input type="checkbox" name="answers[{{ $answer->id }}][is_correct]" {{ ($answer->is_correct) ? 'checked' : '' }}/>
                            </td>
                        </tr>
                    @endforeach
                @endisset
                </tbody>
            </table>
            </div>
            <div class="form-group">
                <button class="btn btn-primary" onclick="addAnswer(event);">Добавить ответ</button>
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>
    </div>
@endsection