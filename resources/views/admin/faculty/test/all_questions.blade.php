@extends('admin.layouts.faculty')
@section('faculty_content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('admin.faculties.faculty.test.all', ['faculty' => $faculty->slug]) }}" class="btn btn-warning pull-right">
                <i class="fa fa-arrow-left"></i>
                Все тесты
            </a>
            <h4 class="pull-left">Тема: {{ $test->name }}</h4>
        </div>
        <ul class="nav nav-tabs">
            <li @if (request()->route()->getName() == 'admin.faculties.faculty.test.edit') class="active" @endif>
                <a href="{{ route('admin.faculties.faculty.test.edit', ['faculty' => $faculty->slug, 'test_id' => $test->id]) }}">
                    Редактирование
                </a>
            </li>
            <li @if (request()->route()->getName() == 'admin.faculties.faculty.test.question.all') class="active" @endif>
                <a href="{{ route('admin.faculties.faculty.test.question.all', ['faculty' => $faculty->slug, 'test_id' => $test->id]) }}">
                    Вопросы
                </a>
            </li>
        </ul>

        <table class="admin_table">
            <thead>
            <tr>
                <th>
                    <a href="{{ route('admin.faculties.faculty.test.question.add', ['faculty' => $faculty->slug, 'test_id' => $test->id]) }}" class="btn btn-success pull-right">
                        <i class="fa fa-plus"></i>
                        Добавить
                    </a>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($questions as $question)
                <tr data-href="{{ route('admin.faculties.faculty.test.question.edit', ['faculty' => $faculty->slug, 'test_id' => $test->id, 'question_id' => $question->id]) }}">
                    <td>{!! $question->name !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="col-md-12 text-center">
            {{ $questions->links() }}
        </div>
    </div>
@endsection