@extends('admin.layouts.faculty')
@section('faculty_content')
    <div class="row">
        <div class="col-md-12">
            @if(!$faculty->tests_enabled)
                <div class="alert alert-warning" role="alert">
                    Тесты выключены для данного факультета. Для отображения на сайте их нужно включить<br>
                    <a href="{{ route('admin.faculties.faculty.settings.show', ['faculty' => $faculty->slug]) }}">
                        Настройки факультета
                    </a>
                </div>
            @endif
            <h4 class="pull-left">Тесты</h4>
            <a href="{{ route('admin.faculties.faculty.test.export', ['faculty' => $faculty->slug]) }}" class="btn btn-warning pull-right">
                Выгрузить результаты
            </a>
            <a href="{{ route('admin.faculties.faculty.test.add', ['faculty' => $faculty->slug]) }}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i>
                Создать новый
            </a>
        </div>
        @if (count($errors) > 0)
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4>Ошибка</h4>
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
            </div>
        @endif
        @foreach ($tests as $test)
            <div class="col-md-6 card-wrapper">
                <div class="card" data-href="{{ route('admin.faculties.faculty.test.edit', ['faculty' => $faculty->slug, 'test_id' => $test->id]) }}">
                    <div class="panel panel-default panel-body">
                        <span class="pull-right card-number">ID: {{ $test->id }}</span>
                        <span class="pull-left card-number">
                            <span class="level-badge level-badge-{{ $test->level }}">{{ $test->level()->name }}</span>
                        </span>
                        <h2>{{ $test->name }}</h2>
                        <h4>{{ $test->description }}</h4>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection