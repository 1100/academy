<div class="form-group">
<label for="name">Название</label>
<div class="block{{ $errors->has('name') ? ' has-error' : '' }}">
    <input id="name" class="form-control" name="name" type="text" value="{{ (old('name') ? old('name') : ((isset($test)) ? $test->name : '' )) }}"/>
    @if ($errors->has('name'))
        <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
    @endif
</div>
</div>

<div class="form-group">
<label for="name">Описание</label>
<div class="block{{ $errors->has('description') ? ' has-error' : '' }}">
    <input id="description" class="form-control" name="description" type="text" value="{{ (old('description') ? old('description') : ((isset($test)) ? $test->description : '' )) }}"/>
    @if ($errors->has('description'))
        <span class="help-block">
        <strong>{{ $errors->first('description') }}</strong>
    </span>
    @endif
</div>
</div>

<div class="form-group">
    <label for="name">Идентификатор видео YouTube</label>
    <div class="block{{ $errors->has('video_identifier') ? ' has-error' : '' }}">
        <input id="video_identifier" class="form-control" name="video_identifier" type="text" value="{{ (old('video_identifier') ? old('video_identifier') : ((isset($test)) ? $test->video_identifier : '' )) }}"/>
        @if ($errors->has('video_identifier'))
            <span class="help-block">
            <strong>{{ $errors->first('video_identifier') }}</strong>
        </span>
        @endif
    </div>
</div>

<h4>Параметры теста</h4>

<div class="row">
    <div class="form-group col-sm-6">
        <label for="level">Уровень квалификации</label>
        <div class="block{{ $errors->has('level') ? ' has-error' : '' }}">
            <select id="level" class="form-control" name="level" required>
                @foreach ($levels as $level)
                    @if ($level->number !== 1)
                        <option @if(old('level') == $level->slug || (isset($test) && $test->level == $level->slug )) selected @endif value="{{ $level->slug }}">{{ $level->name }}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group col-sm-6">
        <label for="minimum_correct_answers">К-во правильных ответов для квалификации</label>
        <div class="block{{ $errors->has('minimum_correct_answers') ? ' has-error' : '' }}">
            <input id="minimum_correct_answers" class="form-control" name="minimum_correct_answers" type="number" value="{{ (old('minimum_correct_answers') ? old('minimum_correct_answers') : ((isset($test)) ? $test->minimum_correct_answers : '0' )) }}"/>
        </div>
    </div>

    <div class="form-group col-sm-4">
        <label for="points">Баллы за полный тест</label>
        <div class="block{{ $errors->has('points') ? ' has-error' : '' }} fixed-bottom">
            <input id="points" class="form-control" name="points" type="number" value="{{ (old('points') ? old('points') : ((isset($test)) ? $test->points : '0' )) }}"/>
            @if ($errors->has('points'))
                <span class="help-block">
                    <strong>{{ $errors->first('points') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group col-sm-4">
        <label for="duration">Продолжительность, мин.</label>
        <div class="block{{ $errors->has('duration') ? ' has-error' : '' }} fixed-bottom">
            <input id="points" class="form-control" name="duration" type="number" value="{{ (old('duration') ? old('duration') : ((isset($test)) ? $test->duration : '1' )) }}"/>
            @if ($errors->has('duration'))
                <span class="help-block">
                    <strong>{{ $errors->first('duration') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group col-sm-4">
        <label for="question_count">Количество вопросов</label>
        <div class="block{{ $errors->has('question_count') ? ' has-error' : '' }} fixed-bottom">
            <input id="question_count" class="form-control" name="question_count" type="number" value="{{ (old('question_count') ? old('question_count') : ((isset($test)) ? $test->questions_count() : '1' )) }}"/>
            @if ($errors->has('question_count'))
                <span class="help-block">
                    <strong>{{ $errors->first('question_count') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

<h4>Период активности теста</h4>

<div class="row">
    <div class="form-group col-sm-6">
        <label for="start_date">Открыть доступ к тесту</label>
        <div class="block{{ $errors->has('start_date') ? ' has-error' : '' }}">
            <input id="start_date" class="datetimepicker form-control" name="start_date" type="text" value="{{ (old('start_date') ? old('start_date') : ((isset($test)) ? $test->start_date : '' )) }}"/>
            @if ($errors->has('start_date'))
                <span class="help-block">
            <strong>{{ $errors->first('start_date') }}</strong>
        </span>
            @endif
        </div>
    </div>

    <div class="form-group col-sm-6">
        <label for="end_date">Закрыть доступ к тесту</label>
        <div class="block{{ $errors->has('end_date') ? ' has-error' : '' }}">
            <input id="end_date" class="datetimepicker form-control" name="end_date" type="text" value="{{ (old('end_date') ? old('end_date') : ((isset($test)) ? $test->end_date : '' )) }}"/>
            @if ($errors->has('end_date'))
                <span class="help-block">
            <strong>{{ $errors->first('end_date') }}</strong>
        </span>
            @endif
        </div>
    </div>
</div>

<h4>Бонус за первое прохождение</h4>

<div class="form-group">
    <label for="bonus">
        <input id="bonus" type="checkbox" name="bonus" {{ ((isset($test) && isset($test->bonus)) ? 'checked' : '' ) }}>
        активировать бонус за первое прохождение
    </label>
</div>

<div class="row">
    <div class="form-group col-sm-6">
        <label for="bonus_points">Баллы за первое прохождение</label>
        <div class="block{{ $errors->has('bonus_points') ? ' has-error' : '' }}">
            <input id="bonus_points" class="form-control" name="bonus_points" type="number" value="{{ (old('bonus_points') ? old('bonus_points') : ((isset($test) && isset($test->bonus)) ? $test->bonus->points : '0' )) }}"/>
            @if ($errors->has('bonus_points'))
                <span class="help-block">
            <strong>{{ $errors->first('bonus_points') }}</strong>
        </span>
            @endif
        </div>
    </div>

    <div class="form-group col-sm-6">
        <label for="bonus_count">Количество награждаемых участников</label>
        <div class="block{{ $errors->has('bonus_count') ? ' has-error' : '' }}">
            <input id="bonus_count" class="form-control" name="bonus_count" type="number" value="{{ (old('bonus_count') ? old('bonus_count') : ((isset($test) && isset($test->bonus)) ? $test->bonus->bonus_count : '0' )) }}"/>
            @if ($errors->has('bonus_count'))
                <span class="help-block">
            <strong>{{ $errors->first('bonus_count') }}</strong>
        </span>
            @endif
        </div>
    </div>
</div>