@extends('admin.layouts.faculty')
@section('faculty_content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('admin.faculties.faculty.test.all', ['faculty' => $faculty->slug]) }}" class="btn btn-warning pull-right">
                <i class="fa fa-arrow-left"></i>
                Все тесты
            </a>
            <h4 class="pull-left">Тема: {{ $test->name }}</h4>
        </div>
        <ul class="nav nav-tabs">
            <li @if (request()->route()->getName() == 'admin.faculties.faculty.test.edit') class="active" @endif>
                <a href="{{ route('admin.faculties.faculty.test.edit', ['faculty' => $faculty->slug, 'test_id' => $test->id]) }}">
                    Редактирование
                </a>
            </li>
            <li @if (request()->route()->getName() == 'admin.faculties.faculty.test.question.all') class="active" @endif>
                <a href="{{ route('admin.faculties.faculty.test.question.all', ['faculty' => $faculty->slug, 'test_id' => $test->id]) }}">
                    Вопросы
                </a>
            </li>
        </ul>

        <form role="form" method="POST" action="{{ route('admin.faculties.faculty.test.edit', ['faculty' => $faculty->slug, 'test_id' => $test->id]) }}" class="col-md-12">
            {{ csrf_field() }}

            @include('admin.faculty.test.edit_test_inputs')

            <button type="submit" class="btn btn-primary">Сохранить</button>
        </form>

    </div>
@endsection
