@extends('admin.layouts.faculty')
@section('faculty_content')
<div class="row">
    <div class="col-md-12">
        <h4 class="pull-left">Кураторы</h4>
        <a href="{{ route('admin.all_admins') }}" class="btn btn-warning pull-right">
            <i class="fa fa-cog"></i>
            Управление администраторами
        </a>
    </div>
    @if (count($errors) > 0)
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4>Ошибка</h4>
            @foreach ($errors->all() as $error)
                {{ $error }}<br>
            @endforeach
        </div>
    </div>
    @endif
    @foreach ($admins as $admin)
    <div class="col-md-6 card-wrapper">
        <div class="card">
            <div class="panel panel-default panel-body">
                <span class="pull-right card-number">ID: {{ $admin->id }}</span>
                <h2>{{ $admin->login }}</h2>
                <h4>
                    @foreach($admin->roles as $role)
                        {{ $role->name . ($loop->last ? '' : ', ') }}
                    @endforeach
                </h4>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection