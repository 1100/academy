@extends('admin.layouts.faculty')
@section('faculty_content')
    <div class="row">
        <div class="col-md-12">
            <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('admin.faculties.faculty.events.edit_event', ['faculty' => $faculty->slug, 'event_id' => $event->id]) }}">
                {{ csrf_field() }}
                <h4>Редактирование мероприятия</h4>

                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title">Заголовок</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Заголовок" value="{{ $event->title }}">
                </div>
                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                    <label for="image">Изображение</label>
                    @if($event->image)
                        <img class="img-thumbnail img-responsive" src="{{ Storage::url($event->image) }}">
                    @endif
                    <input type="file" class="form-control" id="image" name="image" placeholder="Изображение" value="{{ old('image') }}">
                </div>
                <div class="form-group{{ $errors->has('short_description') ? ' has-error' : '' }}">
                    <label for="short_description">Краткое описание</label>
                    <textarea class="form-control wysiwyg" id="short_description" name="short_description" placeholder="Краткое описание" rows="15">
                        {!! nl2br(e($event->short_description)) !!}
                    </textarea>
                </div>
                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label for="description">Описание</label>
                    <textarea class="form-control wysiwyg" id="description" name="description" placeholder="Описание" rows="15">
                        {!! nl2br(e($event->description)) !!}
                    </textarea>
                </div>
                <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                    <label for="location">Место проведения</label>
                    <input type="text" class="form-control" id="location" name="location" placeholder="Место проведения" value="{{ $event->location }}">
                </div>
                <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                    <label for="date">Дата проведения</label>
                    <input id="date" class="datetimepicker form-control" name="date" type="text" value="{{ $event->date }}"/>
                </div>
                <div class="form-group{{ $errors->has('registration_start_date') ? ' has-error' : '' }}">
                    <label for="registration_start_date">Дата начала регистрации</label>
                    <input id="registration_start_date" class="datetimepicker form-control" name="registration_start_date" type="text" value="{{ $event->registration_start_date }}"/>
                </div>
                <div class="form-group{{ $errors->has('registration_end_date') ? ' has-error' : '' }}">
                    <label for="registration_end_date">Дата окончания регистрации</label>
                    <input id="registration_end_date" class="datetimepicker form-control" name="registration_end_date" type="text" value="{{ $event->registration_end_date }}"/>
                </div>

                <a class="btn btn-primary" href="{{ route('admin.faculties.faculty.events.show_all', ['faculty' => $faculty->slug]) }}">Отмена</a>
                <button type="submit" class="btn btn-success">Сохранить</button>
                <button onclick="event.preventDefault(); document.getElementById('delete-form').submit();" class="btn btn-danger">Удалить</button>
            </form>
            <form id="delete-form" action="{{ route('admin.faculties.faculty.events.delete_event', ['faculty' => $faculty->slug, 'event_id' => $event->id]) }}" method="POST" style="display: none;">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
            </form>
            <br>
        </div>
    </div>
@endsection