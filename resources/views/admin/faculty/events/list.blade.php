@extends('admin.layouts.faculty')
@section('faculty_content')
    <div class="row">
        <div class="col-md-12">
            @if(!$faculty->events_enabled)
            <div class="alert alert-warning" role="alert">
                Мероприятия выключены для данного факультета. Для отображения на сайте их нужно включить<br>
                <a href="{{ route('admin.faculties.faculty.settings.show', ['faculty' => $faculty->slug]) }}">
                    Настройки факультета
                </a>
            </div>
            @endif
            <table class="admin_table">
                <caption>
                    <h4 class="pull-left">Мероприятия</h4>
                    <a href="{{ route('admin.faculties.faculty.events.add_event', ['faculty' => $faculty->slug]) }}" class="btn btn-success pull-right">
                        <i class="fa fa-plus"></i>
                        Добавить
                    </a>
                </caption>
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Заголовок</th>
                    <th>Дата проведения</th>
                    <th>Место проведения</th>
                    <th>Заявки</th>
                </tr>
                </thead>
                <tbody>
                @foreach($events as $event)
                    <tr data-href="{{ route('admin.faculties.faculty.events.show_event', ['faculty' => $faculty->slug, 'event_id' => $event->id]) }}">
                        <td> {{ $event->id }} </td>
                        <td> {{ $event->title }} </td>
                        <td> {{ $event->date }} </td>
                        <td> {{ $event->location }} </td>
                        <td>
                            {{ $event->users->count() }}
                            <a href="{{ route('admin.faculties.faculty.events.export_event', ['faculty' => $faculty->slug, 'event_id' => $event->id]) }}" class="btn btn-warning">
                                <i class="fa fa-file-excel-o"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="col-md-12 text-center">
                {{ $events->links() }}
            </div>
        </div>
    </div>
@endsection