@extends('admin.layouts.faculty')
@section('faculty_content')
    <div class="row">
        <div class="col-md-12">
            <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('admin.faculties.faculty.events.add_event', ['faculty' => $faculty->slug]) }}">
                {{ csrf_field() }}
                <h4>Новое мероприятие</h4>
                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title">Заголовок</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Заголовок" value="{{ old('title') }}">
                </div>
                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                    <label for="image">Изображение</label>
                    <input type="file" class="form-control" id="image" name="image" placeholder="Изображение" value="{{ old('image') }}">
                </div>
                <div class="form-group{{ $errors->has('short_description') ? ' has-error' : '' }}">
                    <label for="short_description">Краткое описание</label>
                    <textarea class="form-control wysiwyg" id="short_description" name="short_description" placeholder="Краткое описание" rows="15">
                        {!! nl2br(e(old('short_description'))) !!}
                    </textarea>
                </div>
                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label for="description">Описание</label>
                    <textarea class="form-control wysiwyg" id="description" name="description" placeholder="Описание" rows="15">
                        {!! nl2br(e(old('description'))) !!}
                    </textarea>
                </div>
                <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                    <label for="location">Место проведения</label>
                    <input type="text" class="form-control" id="location" name="location" placeholder="Место проведения" value="{{ old('location') }}">
                </div>
                <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                    <label for="date">Дата проведения</label>
                    <input id="date" class="datetimepicker form-control" name="date" type="text" value="{{ old('date') }}"/>
                </div>
                <div class="form-group{{ $errors->has('registration_start_date') ? ' has-error' : '' }}">
                    <label for="registration_start_date">Дата начала регистрации</label>
                    <input id="registration_start_date" class="datetimepicker form-control" name="registration_start_date" type="text" value="{{ old('registration_start_date') }}"/>
                </div>
                <div class="form-group{{ $errors->has('registration_end_date') ? ' has-error' : '' }}">
                    <label for="registration_end_date">Дата окончания регистрации</label>
                    <input id="registration_end_date" class="datetimepicker form-control" name="registration_end_date" type="text" value="{{ old('registration_end_date') }}"/>
                </div>

                <a class="btn btn-primary" href="{{ route('admin.faculties.faculty.events.show_all', ['faculty' => $faculty->slug]) }}">Отмена</a>
                <button type="submit" class="btn btn-success">Добавить</button>
            </form>
            <br>
        </div>
    </div>
@endsection