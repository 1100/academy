@extends('admin.layouts.faculty')
@section('faculty_content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('admin.faculties.faculty.users', ['faculty' => $faculty->slug]) }}" class="btn btn-warning pull-right">
                <i class="fa fa-arrow-left"></i>
                Все участники
            </a>
            <h4 class="pull-left">Редактирование участника</h4>
        </div>
        <form role="form" method="POST" action="{{ route('admin.faculties.faculty.edit_user', ['faculty' => $faculty->slug, 'user_id' => $user->id]) }}" class="col-md-12">
            {{ csrf_field() }}

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <input type="hidden" name="faculty" value="{{ $faculty->slug }}">
            <input type="hidden" name="id" value="{{ $user->id }}">

            @foreach($faculty->information()->where('slug', '!=', 'facebook')->get() as $field)
            @php ($field->value = $user->getInformation($field->slug))
            @include('user_information.input.'.$field->type)
            @endforeach

            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                <label for="status">Статус</label>
                <select id="status" class="form-control" name="status" required>
                    @foreach ($statuses as $status)
                        <option @if($user->status == $status->slug) selected @endif value="{{ $status->slug }}">{{ $status->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group{{ $errors->has('level') ? ' has-error' : '' }}">
                <label for="level">Уровень</label>
                <select id="level" class="form-control" name="level" required>
                    @foreach ($levels as $level)
                        <option @if($user->level == $level->slug) selected @endif value="{{ $level->slug }}">{{ $level->name }}</option>
                    @endforeach
                </select>
            </div>

            @can('role', 'admin')
            <div class="form-group{{ $errors->has('user_faculty') ? ' has-error' : '' }}">
                <label for="user_faculty">Факультет</label>
                <select id="user_faculty" class="form-control" name="user_faculty" required>
                    @foreach ($faculties as $f)
                        <option @if($f->slug == $faculty->slug) selected @endif value="{{ $f->slug }}">{{ $f->name }}</option>
                    @endforeach
                </select>
            </div>
            @endcan

            @can('permission', 'rating')
            <div class="form-group{{ $errors->has('user_faculty') ? ' has-error' : '' }}">
                <label>Дополнительные баллы</label>
                @foreach ($faculty->customPointables as $pointable)
                    <div class="form-group">
                        <label for="pointable_{{ $pointable->id }}">
                        <input id="pointable_{{ $pointable->id }}" type="checkbox" name="custom_pointables[{{ $pointable->id }}]" @if($pointable->getIsCompleted($user)) checked @endif>
                            {{ $pointable->name }}
                        </label>
                    </div>
                @endforeach
            </div>
            @endcan

            <button type="submit" class="btn btn-primary">Сохранить</button>
        </form>
    </div>
@endsection