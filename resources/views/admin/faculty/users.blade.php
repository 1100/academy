@extends('admin.layouts.faculty')
@section('faculty_content')
<div class="row">
    <div class="col-md-12">
        <h4 class="pull-left">Участники</h4>
        <a href="{{ route('admin.faculties.faculty.export_users', ['faculty' => $faculty->slug]) }}" class="btn btn-warning pull-right">
            Выгрузить
        </a>
        <a href="{{ route('admin.faculties.faculty.add_user_form', ['faculty' => $faculty->slug]) }}" class="btn btn-success pull-right">
            <i class="fa fa-plus"></i>
            Новый участник
        </a>
    </div>
    <form class="col-md-12 form-inline search-form">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Поиск..." name="q" value="{{ app('request')->input('q') }}">
        </div>
        <button type="submit" class="btn btn-default">Найти</button>
    </form>
    <div class="col-md-12">
        <a href="{{ route('admin.faculties.faculty.settings.show', ['faculty' => $faculty->slug]) . '#display_user_fields' }}" class="btn btn-xs pull-left">
            <i class="fa fa-cog"></i>
            Настройка отображения таблицы
        </a>
        <table class="admin_table">
            <thead>
            <tr>
                <th></th>
                @foreach($faculty->displayInformationFields() as $information)
                    <th>{{ $information->name }}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr data-href="{{ route('admin.faculties.faculty.show_user', ['faculty' => $faculty->slug, 'user_id' => $user->id]) }}">
                    <td>
                    @if($user->status == 'approved')
                        <i class="fa fa-lg fa-user-plus text-success" data-toggle="tooltip" data-placement="left" title="{{ $user->moderation_status()->first()->name }}"></i>
                    @elseif($user->status == 'rejected')
                        <i class="fa fa-lg fa-user-times text-danger" data-toggle="tooltip" data-placement="left" title="{{ $user->moderation_status()->first()->name }}"></i>
                    @else
                        <i class="fa fa-lg fa-user text-muted" data-toggle="tooltip" data-placement="left" title="{{ $user->moderation_status()->first()->name }}"></i>
                    @endif
                    </td>
                    @foreach($faculty->displayInformationFields() as $information)
                        @if ($information->slug == 'facebook')
                            <td>
                                <a href="https://fb.com/app_scoped_user_id/{{ $user->getInformation($information->slug) }}" target="_blank">{{ $user->getInformation($information->slug) }}</a></td>
                        @else
                            <td>{{ $user->getDisplayInformation($information->slug) }}</td>
                        @endif
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="col-md-12 text-center">
            {{ $users->appends(request()->except('page'))->links() }}
        </div>
        <script>
            $(document).ready(function() {
                $('[data-href]').click(function (e) {
                    window.location.href = this.dataset.href;
                });
            });
        </script>
    </div>
</div>
@endsection