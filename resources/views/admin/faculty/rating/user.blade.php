@extends('admin.layouts.faculty')
@section('faculty_content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('admin.faculties.faculty.rating.all', ['faculty' => $faculty->slug]) }}" class="btn btn-warning pull-right">
                <i class="fa fa-arrow-left"></i>
                Все участники
            </a>
            <div  class="pull-left">
                <h4>Рейтинг пользователя <b>{{ $user->getDisplayName() }}</b></h4>
                <h4>Всего: <b>{{ $user->rating }}</b></h4>
            </div>
        </div>
        <div class="col-md-12">
            <table class="admin_table">
                <thead>
                <tr>
                    <th>Тест</th>
                    <th>Баллы</th>
                @can('permission', 'user-information')
                    <th>Обнулить</th>
                @endcan
                </tr>
                </thead>
                <tbody>
                @foreach($user_ratings as $point)
                    <tr>
                        <td>{{ $point['name'] }}</td>
                        <td>{{ $point['points'] }}</td>
                    @can('permission', 'reset_test')
                        <td>
                            @if($point['type'] == 'App\Test')
                                <form method="post" class="form-inline" action="{{ route('admin.faculties.faculty.rating.reset_test_session', ['faculty' => $faculty->slug, 'user_id' => $user->id]) }}">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <input type="hidden" name="test_id" value="{{ $point['pointable_id'] }}"/>
                                    <button type="submit" class="btn btn-danger">Обнулить</button>
                                </form>
                            @endif
                        </td>
                    @endcan
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection