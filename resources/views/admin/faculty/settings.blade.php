@extends('admin.layouts.faculty')
@section('faculty_content')
<div class="row">
    <form class="{{ $errors->has('fields') ? ' has-error' : '' }} col-md-12" role="form" method="POST" action="{{ route('admin.faculties.faculty.settings.update_display_fields', ['faculty' => $faculty->slug]) }}">
        {{ csrf_field() }}
        <a name="display_user_fields"></a>
        <h4>Отображаемая информация пользователей</h4>
        <div class="form-group">
            <table class="admin_table">
                <thead>
                    <tr>
                        <th>Поле</th>
                        <th>в общей</th>
                        <th>в рейтинге</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($faculty->information as $field)
                    <tr>
                        <td>{{ $field->name }}</td>
                        <td>
                            <input type="checkbox" name="display_fields[{{ $field->slug }}]" @if($faculty->informationFieldDisplay($field)) checked @endif value="{{ $field->slug }}">
                        </td>
                        <td>
                            <input type="checkbox" name="rating_display_fields[{{ $field->slug }}]" @if($faculty->informationFieldRatingDisplay($field)) checked @endif value="{{ $field->slug }}">
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="form-group">
            <label for="open-rating">
                <input id="open-rating" type="checkbox" name="open_rating" @if($faculty->open_rating == true) checked @endif>
                Отображать таблицу рейтинга для пользователей
            </label>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </form>

    <h4>Позиция факультета</h4>

    <form class="{{ $errors->has('fields') ? ' has-error' : '' }} col-md-12" role="form" method="POST" action="{{ route('admin.faculties.faculty.settings.update_index', ['faculty' => $faculty->slug]) }}">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="index">Индекс</label>
            <div class="block{{ $errors->has('index') ? ' has-error' : '' }}">
                <input id="index" class="form-control" name="index" type="number" value="{{ old('index') ? old('index') : $faculty->index }}"/>
                @if ($errors->has('index'))
                    <span class="help-block">
                <strong>{{ $errors->first('index') }}</strong>
            </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </form>

    <h4>Пригласить друга</h4>

@if ( $can_invite )
    @if ( $faculty->isEnabledInvite() )

        <div class="alert alert-warning" role="alert">
            Внимание! Выключение приведет к обнулению всех инвайтов<br>
            <a href="#" class="btn btn-danger"
               onclick="event.preventDefault();
               document.getElementById('disable-invite-form').submit();">Выключить</a>
            <form id="disable-invite-form" style="display: none;" role="form" method="POST" action="{{ route('admin.faculties.faculty.settings.disable_invite', ['faculty' => $faculty->slug]) }}">
                {{ csrf_field() }}
            </form>
        </div>

        <form class="{{ $errors->has('invite') ? ' has-error' : '' }} col-md-12" role="form" method="POST" action="{{ route('admin.faculties.faculty.settings.update_invite', ['faculty' => $faculty->slug]) }}">
            {{ csrf_field() }}

            <div class="form-group">
            <label for="name">Название</label>
            <div class="block{{ $errors->has('invite_name') ? ' has-error' : '' }}">
                <input id="invite_name" class="form-control" name="invite_name" type="text" value="{{ (old('invite_name') ? old('invite_name') : $faculty->invite->name) }}"/>
            @if ($errors->has('invite_name'))
                <span class="help-block">
                  <strong>{{ $errors->first('invite_name') }}</strong>
                </span>
            @endif
            </div>
            </div>

            <div class="form-group">
            <label for="name">Максимум баллов (за все приглашения)</label>
            <div class="block{{ $errors->has('invite_points') ? ' has-error' : '' }}">
                <input id="invite_points" class="form-control" name="invite_points" type="number" value="{{ (old('invite_points') ? old('invite_points') : $faculty->invite->points) }}"/>
                @if ($errors->has('invite_points'))
                    <span class="help-block">
                  <strong>{{ $errors->first('invite_points') }}</strong>
                </span>
                @endif
            </div>
            </div>

            <div class="form-group">
            <label for="name">Количество вознаграждаемых приглашений</label>
            <div class="block{{ $errors->has('invites_count') ? ' has-error' : '' }}">
                <input id="invites_count" class="form-control" name="invites_count" type="number" value="{{ (old('invites_count') ? old('invites_count') : $faculty->invite->invites_count) }}"/>
                @if ($errors->has('invites_count'))
                    <span class="help-block">
                  <strong>{{ $errors->first('invites_count') }}</strong>
                </span>
                @endif
            </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>

    @else

        <form class="form-inline col-md-12" role="form" method="POST" action="{{ route('admin.faculties.faculty.settings.enable_invite', ['faculty' => $faculty->slug]) }}">
            {{ csrf_field() }}
            <div class="form-group">
                <button type="submit" class="btn btn-success">Включить</button>
            </div>
        </form>

    @endif
@else
    <div class="alert alert-warning" role="alert">
        Нужно логин-поле email, чтобы можно было включить<br>
        <a href="{{ route('admin.faculties.faculty.user_information', ['faculty' => $faculty->slug]) }}">
            Управление полями
        </a>
    </div>
@endif
    <h4>Мероприятия</h4>
    <form class="col-md-12" role="form" method="POST" action="{{ route('admin.faculties.faculty.settings.update_events_enabled', ['faculty' => $faculty->slug]) }}">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="events-enabled">
                <input id="events-enabled" type="checkbox" name="events_enabled" @if($faculty->events_enabled == true) checked @endif>
                Включить мероприятия
            </label>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </form>
    <h4>Тесты</h4>
    <form class="col-md-12" role="form" method="POST" action="{{ route('admin.faculties.faculty.settings.update_tests_enabled', ['faculty' => $faculty->slug]) }}">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="tests-enabled">
                <input id="tests-enabled" type="checkbox" name="tests_enabled" @if($faculty->tests_enabled == true) checked @endif>
                Включить тесты
            </label>
        </div>

        <div class="row">
            <div class="form-check col-sm-6">
                <label for="countdown-enabled-label">Таймер обратного отсчёта</label>
                <label for="countdown-enabled" id="countdown-enabled-label" style="width: 100%; font-weight: normal">
                    <input id="countdown-enabled" type="checkbox" name="countdown_enabled" @if($faculty->countdown_enabled == true) checked @endif>
                    Включить
                </label>
            </div>

            <div class="form-group col-sm-6">
                <label for="countdown-date">Отсчёт до</label>
                <div class="block{{ $errors->has('countdown_date') ? ' has-error' : '' }}">
                    <input id="countdown-date" class="datetimepicker form-control" name="countdown_date" type="text" value="{{ \Carbon\Carbon::parse($faculty->countdown_date) }}"/>
                </div>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </form>
    <h4>Регистрация</h4>
    <form class="col-md-12" role="form" method="POST" action="{{ route('admin.faculties.faculty.settings.update_open_registration', ['faculty' => $faculty->slug]) }}">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="open-registration">
                <input id="open-registration" type="checkbox" name="open_registration" @if($faculty->open_registration == true) checked @endif>
                Регистрация открыта
            </label>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </form>
</div>
@endsection