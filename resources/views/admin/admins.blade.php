@extends('admin.layouts.default')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="admin_table">
                    <caption><h4>Все администраторы</h4></caption>
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Логин</th>
                        <th>Факультеты</th>
                        <th>Роли</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($admins as $admin)
                        <tr>
                            <td> {{ $admin->id }} </td>
                            <td> {{ $admin->login }} </td>
                            <td>
                                @foreach($admin->faculties as $faculty)
                                    {{ $loop->first ? '' : ', ' }}
                                    {{ $faculty->name }}
                                @endforeach
                            </td>
                            <td>
                                @foreach($admin->roles as $role)
                                    {{ $loop->first ? '' : ', ' }}
                                    {{ $role->name }}
                                @endforeach
                            </td>
                            <td>
                                @can('permission', 'edit-admin')
                                <a class="text-info" href="{{ route('admin.show_admin', ['admin_id' => $admin->id]) }}">
                                    <button>
                                    <i class="fa fa-pencil"></i> редактировать
                                    </button>
                                </a>
                                @endcan
                                @can('permission', 'delete-admin')
                                <form method="post" class="form-inline" action="{{ route('admin.delete_admin', ['admin_id' => $admin->id]) }}">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                <button type="submit" class="text-danger">
                                    <i class="fa fa-times"></i> удалить
                                </button>
                                </form>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <br>
                @can('permission', 'add-admin')
                <form role="form" method="POST" action="{{ url('admin/create_admin') }}">
                    {{ csrf_field() }}
                    <h4>Добавление администратора</h4>
                    <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
                        <label class="sr-only" for="login">Логин</label>
                        <input type="text" class="form-control" id="login" name="login" placeholder="Логин">
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label class="sr-only" for="password">Пароль</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Пароль">
                    </div>
                    <div class="form-group">
                        <b>Факультеты:</b>
                        @foreach($faculties as $faculty)
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="faculties[]" value="{{ $faculty->slug }}">
                                    {{ $faculty->name }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <div class="form-group">
                        <b>Роли:</b>
                    @foreach($roles as $role)
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="roles[]" value="{{ $role->slug }}">
                                {{ $role->name }}
                            </label>
                        </div>
                    @endforeach
                    </div>
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </form>
                @endcan
            </div>
        </div>
    </div>

@endsection