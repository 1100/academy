@extends('layouts.default')
@section('content')
        <div class="container py-5">
            <div class="row mx-0">
                <div class="col col-12 col-md-8" id="postContent">
                    <h1 class="content-title underlined-title">{{ $post->title }}</h1>
                    @if($post->image)
                        <div>
                            <img class="img-fluid pb-5" src="{{ Storage::url($post->image) }}" alt="{{ $post->title }}" />
                        </div>
                    @endif
                    {!! $post->content !!}
                </div>
                <div class="col col-12 col-md-4" style="flex: 0 0 230px;">
                    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FGeberit.UKR&tabs=timeline&width=230&height=600&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1211366195628815" width="230" height="600" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                </div>
            </div>
        </div>

        <style>
            {{-- TODO: remove when proper WYSIWYG will be found --}}
            br + br { display: none; }
        </style>
@endsection