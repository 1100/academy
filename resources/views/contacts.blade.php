@extends('layouts.default')
@section('content')
    <div class="container py-5">
        <h1 class="content-title underlined-title">Контакты</h1>

        <div class="contact_map page_margin_top html" id="map" data-address="ул. Леси Украинки 23, г. Киев" data-zoom="17" data-latitude="50.4898184" data-longitude="30.4729307" data-marker="" data-style="greyscale"></div>

        <div class="row py-5">
            <div class="col col-12 col-md-9">
                <p>
                    Возникли вопросы? Мы всегда рады ответить. Давайте оставаться на связи. Отправьте Ваше сообщение через контактную форму — и мы обязательно ответим!
                </p>
                <form method="POST">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="form-group col col-12 col-md-6">
                            <label>
                                <span class="text-danger">*</span>
                                Имя
                                <input name="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" value="{{ old('first_name') }}">
                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback">
                                        {{ $errors->first('first_name') }}
                                    </span>
                                @endif
                            </label>
                        </div>
                        <div class="form-group col col-12 col-md-6">
                            <label>
                                Фамилия
                                <input name="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" value="{{ old('last_name') }}">
                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback">
                                        {{ $errors->first('last_name') }}
                                    </span>
                                @endif
                            </label>
                        </div>
                    </div>

                    <span class="text-muted">Заполните хотя бы одно поле контактных данных, чтобы мы могли с Вами связаться:</span>

                    <div class="row">
                        <div class="form-group col col-12 col-md-6">
                            <label>
                                <span class="text-danger">*</span>
                                Телефон
                                <input id="phone" name="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" value="{{ old('phone') }}">
                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback">
                                        {{ $errors->first('phone') }}
                                    </span>
                                @endif
                            </label>
                        </div>
                        <div class="form-group col col-12 col-md-6">
                            <label>
                                <span class="text-danger">*</span>
                                E-mail
                                <input name="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        {{ $errors->first('email') }}
                                    </span>
                                @endif
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>
                            <span class="text-danger">*</span>
                            Ваше сообщение
                            <textarea name="message" class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" rows="3">{{ old('message') }}</textarea>
                            @if ($errors->has('message'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('message') }}
                                </span>
                            @endif
                        </label>
                    </div>

                    <p>
                        <span class="text-danger">*</span> — обязательные для заполнения поля
                    </p>

                    <button type="submit" class="btn btn-primary">Отправить</button>

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                </form>
            </div>
            <div class="col col-12 col-md-3">
                <p>
                    ТОВ "Геберит Трейдинг"<br>
                    04073, Украина, Киев,<br>
                    просп. Степана Бандеры, 9
                </p>
                <p>
                    Представительство<br>
                    в Республике Беларусь<br>
                    220062, Республика Беларусь, Минск,<br>
                    просп. Победителей, 108, оф. 206
                </p>
                <p>
                    info@geberit-academy.com.ua
                </p>
            </div>
        </div>
    </div>
@endsection