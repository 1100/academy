<div class="container">

    <a name="registration_form"></a>
    <h1 class="content-big-title">Регистрация</h1>
    @if($faculty->slug === 'commercial')
        <p>
            Участниками факультета ПРОДАВЦЫ могут быть: продавцы розничных магазинов и менеджеры компаний-дилеров, реализующие продукцию брендов Geberit и KOLO на  территории Украины и Республики Беларусь.
        </p>
    @endif
    @if($faculty->slug === 'technical')
        <p>
            Участниками факультета МОНТАЖНИКИ могут быть: монтажники, сантехники, инсталляторы.
        </p>
    @endif
    @if($faculty->open_registration)
    <p>
        Для регистрации пожалуйста заполните форму:
    </p>
    <div class="row">
        <form class="pb-5 col col-12 col-md-8" method="POST" action="{{ route('register', ['faculty' => $faculty->slug]) }}">
            {{ csrf_field() }}
            <input type="hidden" name="faculty" value="{{ $faculty->slug }}">
            @isset( $invited_key )
                <input type="hidden" name="invited_key" value="{{ $invited_key }}">
            @endisset

            @if($errors->any())
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Ошибка</strong>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
                <button type="button" class="close" data-dismiss="alert" aria-label="Закрыть">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            @foreach ($fields as $field)
                @if ($field->slug != 'facebook')
                    @include ('user_information.input.'.$field->type)
                @endif
            @endforeach

            @include ('user_information.input.password')

            <div class="form-group">
                <div class="form-check">
                    <input id="agreement" name="agreement" type="checkbox" class="form-check-input{{ $errors->has('agreement') ? ' is-invalid' : '' }}" required>
                    <label class="form-check-label" for="agreement">
                        Осуществляя регистрацию участника проекта Geberit Academy на данном сайте я даю согласие ООО «Геберит Трейдинг» на обработку, хранение и использование моих персональных данных в соответствии с Законом Украины «Про захист персональних даних».
                    </label>
                </div>
                @if ($errors->has('agreement'))
                    <span class="invalid-feedback">
                        {{ $errors->first('agreement') }}
                    </span>
                @endif
            </div>

            <button type="submit" class="btn btn-primary">Зарегистрироваться</button>

        </form>
    </div>
    <p>
        Уже есть аккаунт?
    </p>
    <p class="pb-5">
        <a href="{{ route('login') }}" class="btn btn-secondary">
            Вход в кабинет
        </a>
    </p>
    @else
    <div class="row">
        <p class="text-muted px-3 py-5">
            Регистрация закрыта
        </p>
    </div>
    @endif
</div>