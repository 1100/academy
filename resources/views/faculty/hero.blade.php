<div class="faculty-header row no-gutters pb-5">
    <div class="faculty-header-image faculty-header-image {{'faculty-header-image_' . $faculty->slug}} col-md-6" style="background-image: url('/images/faculties/{{ $faculty->slug }}.jpg')">
    </div>
    <div class="col-md-6 faculty-header-content">
        <div class="faculty-header-title-wrapper">
            <h1 class="faculty-header-title">Факультет &laquo;{{ $faculty->name }}&raquo;</h1>
            <h2 class="faculty-header-subtitle">{{ $faculty->description }}</h2>
        </div>
    </div>
</div>