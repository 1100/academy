@extends('layouts.default')
@section('content')
    <div class="container py-5">
        <div class="row mx-0">
            <div class="col col-12 col-md-8" id="postContent">
                <h1 class="content-title underlined-title">{{ $event->title }}</h1>
                @if($event->image)
                    <div>
                        <img class="img-fluid pb-5" src="{{ Storage::url($event->image) }}" alt="{{ $event->title }}" />
                    </div>
                @endif
                {!! $event->description !!}
                <p>
                    Место: {{ $event->location }}<br>
                    Дата: {{ $event->date->format('d.m.Y H:i') }}
                </p>
                @include('faculty.events.register')
            </div>
            <div class="col col-12 col-md-4" style="flex: 0 0 230px;">
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FGeberit.UKR&tabs=timeline&width=230&height=600&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1211366195628815" width="230" height="600" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
            </div>
        </div>
    </div>

    <style>
        {{-- TODO: remove when proper WYSIWYG will be found --}}
            br + br { display: none; }
    </style>
@endsection