<div class="row py-3">
    <div class="col col-12 col-sm-5 post-image" style="background-image: url('{{ Storage::url($event->image) }}')">
    </div>
    <div class="col col-12 col-sm-7 post-content pt-3 pt-sm-0">
        <h1 class="post-title">{{ $event->title }}</h1>
        <p>
            Место: {{ $event->location }}<br>
            Дата: {{ $event->date->format('d.m.Y H:i') }}
        </p>
        <p>
            {!! $event->short_description !!}
        </p>
        @include('faculty.events.register')
        <p class="mb-0">
            <a href="{{ route('event', ['faculty' => $faculty->slug, 'event_id' => $event->id]) }}">
                <i class="icon icon-arrow"></i>
                Подробнее
            </a>
        </p>
    </div>
</div>