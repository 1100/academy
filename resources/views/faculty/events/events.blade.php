@if(count($events['actual'])>0 || count($events['past'])>0 )
<ul class="row nav nav-tabs" role="tablist">
    <li class="nav-item">
    <span class="nav-label">
        Показать мероприятия:
    </span>
    </li>
    <li class="nav-item">
        <a class="nav-link active" id="actual-tab" data-toggle="tab" href="#actual" role="tab"
           aria-controls="actual" aria-selected="true">Актуальные</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="past-tab" data-toggle="tab" href="#past" role="tab"
           aria-controls="past" aria-selected="false">Прошедшие</a>
    </li>
</ul>
<div class="tab-content pt-3">
    <div class="tab-pane fade show active" id="actual" role="tabpanel" aria-labelledby="actual-tab">
        @foreach($events['actual'] as $event)
            @include('faculty.events.event')
        @endforeach
    </div>
    <div class="tab-pane fade" id="past" role="tabpanel" aria-labelledby="past-tab">
        @foreach($events['past'] as $event)
            @include('faculty.events.event')
        @endforeach
    </div>
</div>
@else
    <div class="alert alert-light">
        У факультета пока нет доступных мероприятий
    </div>
@endif
