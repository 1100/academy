@if(!$user || $user->level()->number < 2 || $user->faculty !== $event->faculty)
    <p class="text-muted">
        Подача заявок на оффлайн мероприятие от Geberit Academy доступна для участников факультета "{{$event->faculty()->name}}" с квалификационным уровнем <span class="level-badge level-badge-basic">Basic</span> или выше.
    </p>
@else
    @if($event->users->contains($user->id))
        <p class="text-success">
            Заявка на регистрацию подана
        </p>
    @else
        @if($event->is_registration_available)
            <p>Регистрация обязательна.</p>
            <p>
                <a href="#success-{{$event->id}}" onclick="registerToEvent({{$event->id}})" id="button-{{$event->id}}">
                    <i class="icon icon-arrow"></i>
                    Подать заявку
                </a>
            </p>
            <p class="text-success" style="display: none" id="success-{{$event->id}}">
                Спасибо!<br>
                Ваша заявка на участие в мероприятии принята.<br>
                Детали и приглашение будут высланы на вашу електронную почту.
            </p>
        @else
            <p class="text-muted">
                Подача заявок недоступна
            </p>
        @endif
    @endif
@endif

<script>
    function registerToEvent(eventId) {
        var button = document.getElementById('button-' + eventId);
        var success = document.getElementById('success-' + eventId);
        $.ajax({
            type: 'GET',
            url: '/register/{{$faculty->slug}}/events/' + eventId + '/register/',
            success: function (data) {
                if (data.success) {
                    button.style.display = 'none';
                    success.style.display = 'block';
                }
            }
        });
    }

</script>