<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('news', 'NewsController@index')->name('news');
Route::get('news/{post_id}', 'NewsController@showPost')->name('post');
Route::post('/subscribe', 'SubscribeController@subscribe')->name('subscribe');
Route::group(['prefix' => 'contacts', 'as' => 'contacts'], function() {
    Route::get('/', 'ContactsController@index');
    Route::post('/', 'ContactsController@send');
});

// Auth Routes
Route::get('/register/{faculty}', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register/{faculty}', 'Auth\RegisterController@register');
Route::get('/register/{faculty}/events/{event_id}', 'EventsController@showEvent')->name('event');
Route::get('/register/{faculty}/events/{event_id}/register', 'EventsController@register')->name('event_register');
Route::get('/register/{faculty}/invite/{invited_key}', 'Auth\RegisterController@showRegistrationForm')->name('invite');
Route::group(['prefix' => 'login', 'as' => 'login'], function() {
    Route::get('/', 'Auth\LoginController@showLoginForm');
    Route::post('/', 'Auth\LoginController@login');

    // OAuth Routes
    Route::get('/facebook/callback', 'Auth\SocialAuthController@handleProviderCallback')->name('.facebook_callback');
    Route::get('/facebook/{faculty?}', 'Auth\SocialAuthController@redirectToProvider')->name('.facebook');
});

// Password reset routes
Route::group(['prefix' => '/password', 'as' => 'password'], function() {
    Route::post('/send', 'Auth\ForgotPasswordController@sendReset')->name('.send');
    Route::get('/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('.request');
    Route::post('/reset', 'Auth\ResetPasswordController@reset');
    Route::get('/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('.reset');
    Route::get('/resetbysms/{token}/{phone}', 'Auth\ResetPasswordController@showResetBySmsForm')->name('.reset_by_sms.form');
    Route::post('/resetbysms', 'Auth\ResetPasswordController@resetBySmsCode')->name('.reset_by_sms');
    Route::get('/reset/from_email/{email}', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('.reset.from_email');
});
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

// User cabinet routes
Route::group(['prefix' => '/cabinet', 'as' => 'cabinet'], function() {
    Route::get('/', 'AccountController@showDashboard')->name('.dashboard');
    Route::get('/materials', 'AccountController@showMaterials')->name('.materials');
    // Test routes
    Route::get('/tests', 'TestController@index')->name('.tests');
    Route::group(['prefix' => 'test/{test_id}', 'as' => '.test'], function() {
        Route::get('/', 'TestController@show')->name('.show');
        Route::get('start', 'TestController@start')->name('.start');
        Route::group(['prefix' => 'question', 'as' => '.question'], function() {
            Route::get('/', 'TestController@nextQuestion')->name('.next');
            Route::get('{question_id}/answer/{answer_id}', 'TestController@setAnswer')->name('.answer');
        });
    });

    Route::group(['prefix' => '/events', 'as' => '.events'], function() {
        Route::get('/', 'EventsController@showCabinetEvents')->name('.show');
    });

    Route::group(['prefix' => '/information', 'as' => '.information'], function() {
        Route::get('/', 'AccountController@showInformation')->name('.show');
        Route::post('/change_avatar', 'AccountController@changeAvatar')->name('.change_avatar');
        Route::post('/change_additional', 'AccountController@changeAdditional')->name('.change_additional');
        Route::post('/change_password', 'AccountController@changePassword')->name('.change_password');
    });
    // TODO: uncomment
//    Route::get('/terms', 'AccountController@showTerms')->name('.terms');
    Route::get('/conditions', 'AccountController@showConditions')->name('.conditions');
    Route::group(['prefix' => 'help', 'as' => '.help'], function() {
        Route::get('/', 'AccountController@showHelp');
        Route::post('/', 'AccountController@sendHelp');
    });

    Route::post('/update', 'AccountController@updateUser');
    Route::get('/delete_facebook', 'AccountController@deleteFacebook');
    Route::get('/additional', 'AccountController@additional')->name('.additional');
    Route::post('/additional', 'AccountController@updateAdditional');

    Route::post('/invite', 'AccountController@sendInvite')->name('.send_invite');
});

// Admin Routes
Route::group(['prefix' => 'admin', 'as' => 'admin'], function() {

    // Authorization
    Route::get('/login', 'Admin\Auth\LoginController@showLoginForm')->name('.login');
    Route::post('/login', 'Admin\Auth\LoginController@login');
    Route::post('/logout', 'Admin\Auth\LoginController@logout');

    Route::get('/', 'Admin\AdminController@index');

    Route::group(['prefix' => 'faculties', 'as' => '.faculties'], function() {
        Route::get('/', 'Admin\FacultyController@showAll')->name('.show');
        Route::group(['prefix' => '{faculty}', 'as' => '.faculty'], function () {

            Route::get('/users/export', 'Admin\FacultyController@exportUsers')->name('.export_users');

            Route::get('/', 'Admin\FacultyController@index')->name('.show');
            Route::get('/users', 'Admin\FacultyController@showUsers')->name('.users');
            Route::get('/users/add', 'Admin\FacultyController@addUserForm')->name('.add_user_form');
            Route::post('/users/add', 'Admin\FacultyController@addUser')->name('.add_user');
            Route::get('/users/{user_id}', 'Admin\FacultyController@showUser')->name('.show_user');
            Route::post('/users/{user_id}', 'Admin\FacultyController@editUser')->name('.edit_user');

            Route::get('/user_information', 'Admin\FacultyController@showUserInformationSettings')->name('.user_information');
            Route::post('/update_login_fields', 'Admin\FacultyController@updateLoginInformationFields')->name('.update_login_fields');
            Route::put('/field', 'Admin\FacultyController@addInformationField')->name('.add_field');
            Route::get('/field/delete/{field}', 'Admin\FacultyController@deleteInformationField')->name('.delete_field');
            Route::post('/update_required_fields', 'Admin\FacultyController@updateRequiredInformationFields')->name('.update_required_fields');

            // Test routes
            Route::group(['prefix' => 'test', 'as' => '.test'], function() {
                Route::get('/', 'Admin\TestController@showAll')->name('.all');
                Route::get('/export', 'Admin\TestController@export')->name('.export');
                Route::group(['prefix' => 'add'], function() {
                    Route::get('/', 'Admin\TestController@showAddTestForm')->name('.add');
                    Route::post('/', 'Admin\TestController@addTest');
                });
                Route::group(['prefix' => '{test_id}'], function() {
                    Route::group(['prefix' => 'edit'], function() {
                        Route::get('/', 'Admin\TestController@showEditForm')->name('.edit');
                        Route::post('/', 'Admin\TestController@edit');
                    });
                    Route::group(['prefix' => 'question', 'as' => '.question'], function() {
                        Route::get('/', 'Admin\TestController@showAllQuestions')->name('.all');
                        Route::group(['prefix' => 'add'], function() {
                            Route::get('/', 'Admin\TestController@showAddQuestionForm') ->name('.add');
                            Route::post('/', 'Admin\TestController@addQuestion');
                        });
                        Route::group(['prefix' => '{question_id}'], function() {
                            Route::get('/', 'Admin\TestController@showEditQuestionForm')->name('.edit');
                            Route::post('/', 'Admin\TestController@editQuestion');
                        });
                    });
                });
            });

            // Events routes
            Route::group(['prefix' => 'events', 'as' => '.events'], function () {
                Route::get('/', 'Admin\EventsController@index')->name('.show_all');
                Route::get('add', 'Admin\EventsController@showAddEventForm')->name('.show_add_event_form');
                Route::post('add', 'Admin\EventsController@addEvent')->name('.add_event');
                Route::get('{event_id}', 'Admin\EventsController@showEvent')->name('.show_event');
                Route::get('{event_id}/export', 'Admin\EventsController@exportEvent')->name('.export_event');
                Route::post('{event_id}', 'Admin\EventsController@editEvent')->name('.edit_event');
                Route::delete('{event_id}', 'Admin\EventsController@deleteEvent')->name('.delete_event');
            });

            Route::group(['prefix' => 'rating', 'as' => '.rating'], function() {
                Route::get('/', 'Admin\RatingController@show')->name('.all');
                Route::get('/{user_id}', 'Admin\RatingController@showUser')->name('.user');
                Route::delete('/{user_id}', 'Admin\RatingController@resetUserTestSession')->name('.reset_test_session');
            });

            Route::get('/curators', 'Admin\FacultyController@showCurators')->name('.curators');

            Route::group(['prefix' => 'settings', 'as' => '.settings'], function() {
                Route::get('/', 'Admin\FacultyController@showSettings')->name('.show');
                Route::post('/update_display_fields', 'Admin\FacultyController@updateDisplayInformationFields')->name('.update_display_fields');
                Route::post('/update_invite', 'Admin\FacultyController@updateInvite')->name('.update_invite');
                Route::post('/enable_invite', 'Admin\FacultyController@enableInvite')->name('.enable_invite');
                Route::post('/disable_invite', 'Admin\FacultyController@disableInvite')->name('.disable_invite');
                Route::post('/update_open_registration', 'Admin\FacultyController@updateOpenRegistration')->name('.update_open_registration');
                Route::post('/update_index', 'Admin\FacultyController@updateIndex')->name('.update_index');
                Route::post('/update_events_enabled', 'Admin\FacultyController@updateEventsEnabled')->name('.update_events_enabled');
                Route::post('/update_tests_enabled', 'Admin\FacultyController@updateTestsEnabled')->name('.update_tests_enabled');
            });
        });
    });

    Route::get('/all_users', 'Admin\AdminController@showAllUsers');
    Route::post('/create_user', 'Admin\AdminController@createUser');

    Route::get('/user/{user_id}', 'Admin\AdminController@showUser');

    Route::get('/user_information_settings', 'Admin\AdminController@userInformationSettings')->name('.information_settings');
    Route::post('/add_user_information', 'Admin\AdminController@addUserInformation');

    Route::get('/user_information/{user_information_slug}', 'Admin\AdminController@showUserInformation')->name('.show_user_information');
    Route::post('/user_information/{user_information_slug}', 'Admin\AdminController@editUserInformation')->name('.edit_user_information');
    Route::delete('/user_information/{user_information_slug}', 'Admin\AdminController@deleteUserInformation')->name('.delete_user_information');

    Route::get('/all_admins', 'Admin\AdminController@showAllAdmins')->name('.all_admins');
    Route::post('/create_admin', 'Admin\AdminController@createAdmin');

    Route::get('/admin/{admin_id}', 'Admin\AdminController@showAdmin')->name('.show_admin');
    Route::post('/admin/{admin_id}', 'Admin\AdminController@editAdmin')->name('.edit_admin');
    Route::delete('/admin/{admin_id}', 'Admin\AdminController@deleteAdmin')->name('.delete_admin');

    Route::group(['prefix' => 'news', 'as' => '.news'], function () {
        Route::get('/', 'Admin\NewsController@index')->name('.show_all');
        Route::get('add', 'Admin\NewsController@showAddPostForm')->name('.show_add_post_form');
        Route::post('add', 'Admin\NewsController@addPost')->name('.add_post');
        Route::get('{post_id}', 'Admin\NewsController@showPost')->name('.show_post');
        Route::post('{post_id}', 'Admin\NewsController@editPost')->name('.edit_post');
        Route::delete('{post_id}', 'Admin\NewsController@deletePost')->name('.delete_post');
    });

    Route::group(['prefix' => 'support', 'as' => '.support'], function () {
        Route::get('/', 'Admin\SupportController@index')->name('.show_all');
        Route::get('unread', 'Admin\SupportController@showUnread')->name('.unread');
        Route::get('read', 'Admin\SupportController@showRead')->name('.read');
        Route::post('change_read_status', 'Admin\SupportController@changeReadStatus')->name('.change_read_status');
    });

    Route::group(['prefix' => 'subscribers', 'as' => '.subscribers'], function() {
        Route::get('/', 'Admin\SubscribersController@show')->name('.show');
    });

});