<?php

use Illuminate\Foundation\Inspiring;

use App\Admin;
use Illuminate\Support\Facades\DB;
use App\Faculty;
/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('create-admin {login} {password}', function ($login, $password) {
    $admin = Admin::create([
        'login' => $login,
        'password' => bcrypt($password),
    ]);
    DB::table('admin_admin_role')->insert([
        [
            'admin_id' => $admin->id,
            'admin_role' => 'admin',
        ]
    ]);
});

Artisan::command('update-ratings', function () {
    $faculties = Faculty::all();
    foreach ($faculties as $faculty) {
        $faculty->updateUsersRating();
        $this->info($faculty->name . ' ratings updated');
    }
})->describe('Update all users ratings');