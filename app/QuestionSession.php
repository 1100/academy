<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionSession extends Model
{
    public $timestamps = false;

    public $table = 'questions_sessions';

    protected $fillable = [
        'question_id', 'test_session_id',
    ];

    public function testSession()
    {
        return $this->belongsTo('App\TestSession', 'test_session_id', 'id');
    }

    public function question()
    {
        return $this->belongsTo('App\Question');
    }

    public function answers()
    {
        return $this->belongsToMany('App\Answer', 'answers_sessions', 'question_session_id', 'answer_id');
    }
}
