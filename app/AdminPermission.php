<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\AdminRole;

class AdminPermission extends Model
{
    protected $primaryKey = 'slug';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = ['slug'];

    public static function createAndAttachFromArray($roles_permissions)
    {
        foreach ($roles_permissions as $role => $permissions) {
            $role = AdminRole::find($role);
            if($role) {
                foreach ($permissions as $permission_name) {
                    $permission = AdminPermission::firstOrCreate([
                        'slug' => $permission_name
                    ]);
                    if(!$role->permissions->contains($permission->slug)) {
                        $role->permissions()->attach($permission->slug);
                    }
                }
            }
        }
    }
}
