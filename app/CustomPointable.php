<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomPointable extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'points', 'faculty',
    ];

    public function points()
    {
        return $this->morphMany('App\Point', 'pointable');
    }

    public function faculty()
    {
        return $this->belongsTo('App\Faculty', 'faculty', 'slug');
    }

    public function getIsCompleted($user)
    {
        $user_point = $this->points()->where('user_id', $user->id)->first();
        return is_null($user_point) ? false : ($user_point->percent == 100);
    }

    public function setIsCompleted($user, $is_completed)
    {
        $user_point = $this->points()->where('user_id', $user->id)->first();
        if (is_null($user_point)) {
            $user_point = Point::create([
                'user_id' => $user->id,
                'percent' => 0,
                'pointable_id' => $this->id,
                'pointable_type' => get_class($this),
            ]);
        }
        $user_point->setPercent($is_completed ? 100 : 0);
        $user_point->save();
    }
}
