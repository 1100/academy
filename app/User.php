<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResetPassword;
use App\Mail\SetPassword;
use App\Mail\SendResetCode;
use App\Mail\Invite;
use App\Mail\Approved;
use App\Custom\Phone;
use App\Custom\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class User extends Authenticatable implements CanResetPasswordContract
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'password',
        'status',
        'activation_status',
        'level',
        'faculty',
        'rating',
        'invite_key',
        'invited_key',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function moderation_status()
    {
        return $this->belongsTo('App\UserStatus', 'status', 'slug');
    }

    public function activation_status()
    {
        return $this->belongsTo('App\UserActivationStatus', 'activation_status', 'id')->first();
    }

    public function level()
    {
        return $this->belongsTo('App\Level', 'level', 'slug')->first();
    }

    public function faculty()
    {
        return $this->belongsTo('App\Faculty', 'faculty', 'slug');
    }

    public function information()
    {
        return $this->belongsToMany('App\InformationField', 'user_information', 'user_id', 'information_field')->withPivot('value');
    }

    public function informationValues()
    {
        return $this->hasMany('App\UserInformation');
    }

    public function events()
    {
        return $this->belongsToMany('App\Event', 'users_events');
    }

    public function avatarUrl()
    {
        if ($this->avatar) {
            if (starts_with($this->avatar, ['http://', 'https://'])) {
                return $this->avatar;
            }
            return Storage::url($this->avatar);
        } else {
            return '/images/noavatar.gif';
        }
    }

    public function supportMessages()
    {
        return $this->hasMany('App\SupportMessage', 'user_id', 'id');
    }

    public function testSessions()
    {
        return $this->hasMany('App\TestSession');
    }

    public function getDisplayName()
    {
        $first_name = $this->information()->find('first_name');
        $last_name = $this->information()->find('last_name');
        if (isset($first_name) && isset($last_name)) {
            return $first_name->pivot->value . ' ' . $last_name->pivot->value;
        } else {
            return $this->information()->where('login', true)->first()->pivot->value;
        }
    }

    public function getInformation($information_field)
    {
        $user_information = $this->information()->where('information_field', $information_field)->first();
        if ($user_information) {
            return $user_information->pivot->value;
        } else {
            return null;
        }
    }

    public function getDisplayInformation($information_field)
    {
        $user_information = $this->information()->where('information_field', $information_field)->first();
        if ($user_information) {
            if ($user_information->slug === 'region_select') {
                $region = Region::find($user_information->pivot->value);
                return $region ? $region->name : '';
            } else if ($user_information->slug === 'experience_select') {
                $experience = Experience::find($user_information->pivot->value);
                return $experience ? $experience->name : '';
            } else if ($user_information->slug === 'employment_type_select') {
                $employment_type = EmploymentType::find($user_information->pivot->value);
                return $employment_type ? $employment_type->name : '';
            } else {
                return $user_information->pivot->value;
            }
        } else {
            return null;
        }
    }

    public static function getBySearchQuery($faculty, $query)
    {
        $information = UserInformation::query();
        $search_field = '`user_information`.`value`';

        $is_probably_phone = preg_match('/^[0-9\(\)\+\s\-]+$/', $query);
        if ($is_probably_phone) {
            $raw_phone = preg_replace('/[\(\)\+\s\-]+/', '', $query);
            $information = $information->where(function ($information) use ($raw_phone, $search_field) {
                $search_field = '
                REPLACE(
                    REPLACE(
                        REPLACE(
                            REPLACE(' . $search_field . ', "(", "")
                        , ")", "")
                    , "-", "")
                , " ", "")
            ';
                return $information->orWhereRaw($search_field . ' LIKE ?', '%' . $raw_phone . '%');
            });
        } else {
            $query = preg_replace('/\s+/', ' ', $query);
            $words = explode(' ', $query);

            foreach ($words as $word) {
                $information = $information->where(function ($information) use ($word, $search_field) {
                    $information = $information->orWhereRaw($search_field . ' LIKE ?', '%'.$word.'%');
                    return $information;
                });
            }
        }
        $user_ids = $information->get()->pluck('user_id')->unique();

        $users = self::query()
            ->where('faculty', $faculty->slug)
            ->whereIn('id', $user_ids);

        return $users;
    }

    public function sendApprovedEmailNotification()
    {
        $email = $this->getInformation('email');
        if ($email) {
            Mail::to($email)->send(new Approved());
        }
    }

    public function sendPasswordSetEmailNotification()
    {
        $email = $this->getInformation('email');
        Mail::to($email)->send(new SetPassword($email));
    }

    public function sendPasswordResetEmailNotification($token)
    {
        Mail::to($this->getInformation('email'))->send(new ResetPassword($token));
    }

    public function sendPasswordResetSMSNotification($code) {
        $phone = Phone::getPhoneIfValid($this->getInformation('phone'))->rawNumber();
        $message = View::make('emails.sms-password-reset', ['code' => $code])->render();

        $query_data = [
            'charset' => 'utf-8',
            'login'   => env('SMSC_LOGIN'),
            'psw'     => env('SMSC_PASSWORD'),
            'phones'  => $phone,
            'mes'     => $message,
        ];
        $query = http_build_query($query_data);

        file_get_contents('http://smsc.com.ua/sys/send.php?'.$query);
    }

    public function getLoginForPasswordReset($field_name)
    {
        $field =  $this->information()->find($field_name);
        if ($field) {
            return $field->pivot->value;
        } else {
            return null;
        }
    }

    public function points()
    {
        return $this->hasMany('App\Point', 'user_id', 'id');
    }

    public function getPoints()
    {
        $ratings = [];
        foreach ($this->points as $point) {
            if ($point->pointable) {
                $ratings[] = [
                    'id' => $point->id,
                    'name' => $point->pointable->name,
                    'points' => $point->number(),
                    'type' => $point->pointable_type,
                    'pointable_id' => $point->pointable->id
                ];
            }
        }
        return $ratings;
    }

    public function generateInvite()
    {
        if (is_null($this->invite_key)) {
            $this->invite_key = md5(env('APP_KEY') . $this->id . time());
            $this->save();
        }
        return $this->invite_key;
    }

    public function sendInviteForEmail($email)
    {
        $this->generateInvite();
        Mail::to($email)->send(new Invite($this->faculty, $this->invite_key));
    }

    public static function createAndAttachFromArray($users_array)
    {
        $faculty = Faculty::find($users_array['faculty']);
        foreach ($users_array['users'] as $old_user) {
            $user = $faculty->addUser($old_user);
            $user->status = 'approved';
            $user->save();
        }
    }

    public function invitedByUser()
    {
        if ($this->invited_key !== null) {
            return User::where('invite_tey', $this->invited_key)->first();
        } else {
            return null;
        }
    }

    public function allInvitedUsers()
    {
        if ($this->invite_key !== null) {
            return User::where('invited_key', $this->invite_key);
        } else {
            return null;
        }
    }

    public function approvedInvitedUsers()
    {
        if ($this->invite_key !== null) {
            return User::where([
                ['invited_key', '=', $this->invite_key],
                ['status', '=', 'approved']
            ]);
        } else {
            return null;
        }
    }

    public function updateInvitePoints()
    {
        $faculty = $this->faculty()->first();
        if ($faculty->invite) {
            $faculty->invite->updatePoints($this);
        }
        $this->updateRating();
    }

    public function updateRating()
    {
        $rating = 0;
        foreach ($this->points as $point) {
            $rating += $point->number();
        }

        $this->proceedToNextLevelIfCan();

        $this->rating = $rating;
        $this->save();
    }

    public function canProceedToNextLevel()
    {
        if (!$this->level()->higherLevel()) {
            return false;
        }

        $tests = $this->faculty()->first()->tests()->where('level', $this->level()->higherLevel()->slug)->get();
        foreach ($tests as $test) {
            if (!$test->userPassed($this->id)) {
                return false;
            }
        }

        return true;
    }

    public function proceedToNextLevelIfCan()
    {
        if ($this->canProceedToNextLevel()) {
            $this->level = $this->level()->higherLevel()->slug;
            $this->save();
        }
    }
}
