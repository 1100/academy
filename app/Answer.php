<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'question_id', 'is_correct',
    ];

    public function question()
    {
        return $this->belongsTo('App\Question');
    }
}
