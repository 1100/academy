<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Auth\AuthServiceProvider as ServiceProvider;
use App\Custom\Auth\AuthManager;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register the application's policies.
     *
     * @return void
     */
    public function registerPolicies()
    {
        foreach ($this->policies as $key => $value) {
            Gate::policy($key, $value);
        }
    }

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies();

        $gate->define('role', function ($user, $role) {
            return $user->roles->contains($role);
        });

        $gate->define('permission', function ($user, $permission) {
            foreach ($user->roles as $role) {
                if($role->permissions->contains($permission)) {
                    return true;
                }
            }
        });

        $gate->define('faculty', function ($user, $faculty) {
            return $user->faculties->contains($faculty);
        });

        $gate->before(function ($user, $ability) {
            if($user->roles->contains('admin')) {
                return true;
            }
        });
    }

    /**
     * Register the authenticator services.
     *
     * @return void
     */
    protected function registerAuthenticator()
    {
        $this->app->singleton('auth', function ($app) {
            // Once the authentication service has actually been requested by the developer
            // we will set a variable in the application indicating such. This helps us
            // know that we need to set any queued cookies in the after event later.
            $app['auth.loaded'] = true;

            return new AuthManager($app);
        });

        $this->app->singleton('auth.driver', function ($app) {
            return $app['auth']->guard();
        });
    }
}
