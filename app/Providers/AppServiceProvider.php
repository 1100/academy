<?php

namespace App\Providers;

use App\Custom\Phone;
use App\InformationField;
use App\Faculty;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        /*
         * validate attribute for unique login except values in parameters
         */
        Validator::extend('uniqueLogin', '\App\Custom\Validation\Rule@uniqueLogin');
        Validator::extend('uniqueLoginReset', '\App\Custom\Validation\Rule@uniqueLogin');

        Validator::extend('phone', function($attribute, $value, $parameters) {
            return Phone::check($value);
        });

        $faculties = Faculty::orderBy('index')->get();
        View::share('faculties', $faculties);

        \Illuminate\Pagination\AbstractPaginator::defaultView("pagination::bootstrap-4");
        \Illuminate\Pagination\AbstractPaginator::defaultSimpleView("pagination::simple-bootstrap-4");
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
