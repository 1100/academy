<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Test extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'faculty',
        'level',
        'start_date',
        'end_date',
        'question_count',
        'minimum_correct_answers',
        'points',
        'duration',
        'video_identifier',
    ];

    protected $appends = ['video_identifiers'];

    public function faculty()
    {
        return $this->belongsTo('App\Faculty', 'faculty', 'slug');
    }

    public function level()
    {
        return $this->belongsTo('App\Level', 'level', 'slug')->first();
    }

    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    public function sessions()
    {
        return $this->hasMany('App\TestSession');
    }

    public function points()
    {
        return $this->morphMany('App\Point', 'pointable');
    }

    public function userSession($user_id)
    {
        $user = User::findOrFail($user_id);
        if ($user) {
            $user_session = $this->sessions()->where('user_id', $user->id);
            if ($user_session) {
                return $user_session->get()->first();
            }
        }
        return null;
    }

    public function createUserSession($user_id)
    {
        $user = User::findOrFail($user_id);
        $user_session = $this->userSession($user_id);
        if ($user_session) {
            return $user_session;
        } else {
            $points = Point::create([
                'user_id' => $user->id,
                'percent' => 0,
                'pointable_id' => $this->id,
                'pointable_type' => get_class($this),
            ]);
            $test_session = new TestSession([
                'test_id' => $this->id,
                'user_id' => $user->id,
                'start_date' => Carbon::now(),
                'end_date' => Carbon::now()->addMinutes($this->duration),
            ]);
            $test_session->points()->associate($points);
            $test_session->save();
            if ($this->bonus) {
                $this->bonus->addBonusPointsIfCan($user_id);
            }
            return $test_session;
        }
    }

    public function getStatus()
    {
        $now = Carbon::now();
        $start = Carbon::parse($this->start_date);
        $end = Carbon::parse($this->end_date);
        if ($now->lt($start)) {
            return 'not_open';
        } else if ($now->gt($end)) {
            return 'closed';
        } else {
            return 'open';
        }
    }

    public function getPassingStatus($user_id = null)
    {
        if (is_null($user_id)) {
            $user_id = Auth::id();
        }
        $user_session = $this->userSession($user_id);
        if ($user_session) {
            $now = Carbon::now();
            $end = Carbon::parse($user_session->end_date);
            if ($now->lte($end) && $user_session->getQuestionsLeft() > 0) {
                return 'passing';
            } else {
                return 'completed';
            }
        } else {
            return 'not_passing';
        }
    }

    public function userHasEnoughLevel($user_id = null)
    {
        if (is_null($user_id)) {
            $user_id = Auth::id();
        }

        $user = User::findOrFail($user_id);
        return $this->level()->number - $user->level()->number <= 1;
    }

    public function userPassed($user_id = null)
    {
        if (is_null($user_id)) {
            $user_id = Auth::id();
        }

        if ($this->minimum_correct_answers === 0) {
            return true;
        }

        $user_session = $this->userSession($user_id);

        if ($user_session && $user_session->correctAnswersCount() >= $this->minimum_correct_answers) {
            return true;
        }

        return false;
    }

    public static function createFromArray($test_array)
    {
        $test = Test::create([
            'name' => $test_array['name'],
            'description' => $test_array['description'],
            'faculty' => $test_array['faculty'],
            'points' => $test_array['points'],
            'question_count' => $test_array['question_count'],
            'duration' => $test_array['duration'],
            'video_identifier' => $test_array['video_identifier'],
            'start_date' => Carbon::createFromFormat('Y-m-d H:i:s', $test_array['start_date']),
            'end_date' => Carbon::createFromFormat('Y-m-d H:i:s', $test_array['end_date']),
        ]);
        foreach ($test_array['questions'] as $question_array) {
            $question = new Question([
                'name' => $question_array['name'],
            ]);
            $test->questions()->save($question);
            foreach ($question_array['answers'] as $single_answer) {
                $answer = new Answer([
                    'name' => $single_answer['name'],
                    'is_correct' => $single_answer['is_correct']
                ]);
                $question->answers()->save($answer);
            }
        }
    }

    public function bonus()
    {
        return $this->hasOne('App\TestBonus', 'test_id', 'id');
    }

    public function updateBonus($points, $count)
    {
        $bonus = $this->bonus;
        if ($bonus) {
            $bonus->points = $points;
            $bonus->bonus_count = $count;
            $bonus->save();
        } else {
            $bonus = new TestBonus([
                'name' => 'Первое прохождение теста: "'.$this->name.'"',
                'points' => $points,
                'bonus_count' => $count,
            ]);
            $this->bonus()->save($bonus);
            $this->bonus = $bonus;
        }
    }

    public function removeBonus()
    {
        if ($this->bonus) {
            $this->bonus->points()->delete();
            $this->bonus->delete();
            $this->bonus = null;
        }
    }

    public function updateUsersRatings()
    {
        $points = $this->points()->get();
        foreach ($points as $point) {
            $point->updateUsersRatings();
        }
        if ($this->bonus) {
            $this->bonus->updateUsersRating();
        }
    }

    public function questions_count()
    {
        if ($this->question_count > $this->questions()->count()) {
            return $this->questions()->count();
        } else {
            return $this->question_count;
        }
    }

    public function resetUserSession($user_id)
    {
        $user = User::findOrFail($user_id);
        $this->points()->where('user_id', $user->id)->delete();
        $this->sessions()->where('user_id', $user->id)->delete();
        $bonus = $this->bonus;
        if ($bonus) {
            $bonus_points = $bonus->points()->where('user_id', $user->id);
            if ($bonus_points) {
                $bonus_points->delete();
            }
        }
        $user = User::findOrFail($user_id);
        foreach($user->points as $point) {
            $point->updateUsersRatings();
        }
    }

    public function getVideoIdentifiersAttribute()
    {
        $identifiers = explode(',', $this->video_identifier);
        $clean_identifiers = [];
        foreach ($identifiers as $identifier) {
            $identifier = trim($identifier);
            if ($identifier) {
                $clean_identifiers[] = $identifier;
            }
        }
        return $clean_identifiers;
    }
}
