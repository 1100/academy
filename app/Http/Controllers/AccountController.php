<?php

namespace App\Http\Controllers;

use App\Faculty;
use App\SupportMessage;
use App\UserInformation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showDashboard(Request $request)
    {
        $user = $request->user();
        $faculty = Faculty::find($user->faculty);
        $tests = $user->faculty()->first()->tests()->get();
        $user_ratings = $user->getPoints();
        return view('cabinet.dashboard', [
            'user' => $user,
            'faculty' => $faculty,
            'tests' => $tests,
            'user_ratings' => $user_ratings,
        ]);
    }

    public function showMaterials(Request $request)
    {
        $user = $request->user();
        return view('cabinet.materials-'.$user->faculty)->with('user', $user);
    }

    public function showInformation(Request $request)
    {
        $user = $request->user();
        $login_fields = $user->faculty()->first()->loginInformationFields()->get();
        $additional_fields = $user->faculty()->first()->additionalVisibleInformationFields();
        $user_information = $user->information()->get();

        return view('cabinet.information')->with([
            'user' => $user,
            'login_fields' => $login_fields,
            'additional_fields' => $additional_fields,
            'user_information' => $user_information
        ]);
    }

    public function showRatings(Request $request)
    {
        $user = $request->user();
        $faculty = Faculty::find($user->faculty);
        $rating_users = $faculty->open_rating
            ? $faculty->users()
                ->where('status', 'approved')
                ->orderBy('rating', 'desc')
                ->paginate(6)
            : null;
        return view('cabinet.ratings', [
            'user' => $user,
            'faculty' => $faculty,
            'rating_users' => $rating_users,
        ]);
    }

    public function showTerms(Request $request)
    {
        return view('cabinet.terms', [
            'user' => $request->user(),
            'faculty_slug' => $request->user()->faculty()->first()->slug
        ]);
    }

    public function showConditions(Request $request)
    {
        $user = $request->user();
        $faculty = Faculty::find($user->faculty);
        return view('cabinet.conditions', [
            'user' => $user,
            'faculty' => $faculty,
        ]);
    }

    public function showHelp(Request $request)
    {
        return view('cabinet.help')->with('user', $request->user());
    }

    public function sendHelp(Request $request)
    {
        $this->validate($request, [
            'message' => 'string|required'
        ]);
        $user = $request->user();
        SupportMessage::create([
            'message' => $request->input('message'),
            'user_id' => $user->id,
            'faculty' => $user->faculty,
            'email' => $user->getInformation('email'),
            'phone' => $user->getInformation('phone'),
            'first_name' => $user->getInformation('first_name'),
            'last_name' => $user->getInformation('last_name'),
        ]);
        return $this->helpSuccessResponse(__('contact.send.success'));
    }

    protected function helpSuccessResponse($response)
    {
        return back()->with('status', $response);
    }

    public function updateUser(Request $request) {
        $user = $request->user();
        $email_unic_validate = '';
        if ($request->has('email') && $request->input('email') != $user->email) {
            $email_unic_validate = '|unique:users';
        }
        $phone_unic_validate = '';
        if ($request->has('phone') && $request->input('phone') != $user->phone) {
            $phone_unic_validate = '|unique:users';
        }
        // TODO: refactor validator to use 'validator' field from 'information_type' table
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required_without_all:phone|email|max:255|nullable'.$email_unic_validate,
            'phone' => 'required_without_all:email|phone|max:255|nullable'.$phone_unic_validate,
            'password' => 'min:6|confirmed|nullable',
        ]);
        $user->name = $request->input('name');
        if ($request->has('email')) {
            $user->email = $request->input('email');
        }
        if ($request->has('phone')) {
            $user->phone = $request->input('phone');
        }
        if ($request->has('password')) {
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();
        return redirect()->action('AccountController@showInformation');
    }

    public function deleteFacebook()
    {
        $user = Auth::user();
        UserInformation::where('user_id', $user->id)->where('information_field', 'facebook')->delete();
        return redirect()->action('AccountController@showInformation');
    }

    public function changeAvatar(Request $request)
    {
        $user = Auth::user();
        $this->validate($request, [
            'avatar' => 'image',
        ]);
        if ($request->file('avatar')) {
            if ($user->avatar) {
                Storage::delete($user->avatar);
            }
            $path = $request->file('avatar')->store('avatars');
            Storage::setVisibility($path, 'public');
            $user->avatar = $path;
            $user->save();
        }

        return redirect()->action('AccountController@showInformation');
    }

    public function changeAdditional(Request $request)
    {
        $user = Auth::user();
        $fields = $user->faculty()->first()->information()->where('login', false)->get();
        $user_information = [];
        foreach ($fields as $field) {
            $user_information[$field->slug] = ['value' => $request->input($field->slug)];
        }
        $user->information()->sync($user_information, false);
        $user->save();

        return redirect()->action('AccountController@showInformation');
    }

    public function changePassword(Request $request)
    {
        $user = $request->user();
        $this->validate($request, [
            'password' => 'required|min:6|confirmed',
        ]);
        $user->password = bcrypt($request->input('password'));
        $user->save();
        return redirect()->action('AccountController@showInformation');
    }

    public function sendInvite(Request $request)
    {
        $user = Auth::user();
        $this->validate($request, [
            'email' => 'required|email|uniqueLogin'
        ]);
        $user->sendInviteForEmail($request->input('email'));
        return back()->with('invite-status', 'Приглашение успешно отправлено');
    }
}
