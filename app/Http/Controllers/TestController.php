<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use \App\Test;
use \App\TestSession;
use Illuminate\Support\Facades\Auth;

class TestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        $user = $request->user();
        $tests = $user->faculty()->first()->tests()->get();
        return view('cabinet.test.index', [
            'user' => $user,
            'tests' => $tests
        ]);
    }

    public function show(Request $request, $test_id)
    {
        $user = $request->user();
        $test = $user->faculty()->first()->tests()->findOrFail($test_id);
        $test_session = $test->userSession($user->id);
        if (!$test->userHasEnoughLevel()) {
            return view('cabinet.test.level', [
                'user' => $user,
                'test' => $test
            ]);
        } else if ($test->getPassingStatus($user->id) == 'completed') {
            return view('cabinet.test.end', [
                'user' => $user,
                'test' => $test,
                'test_session' => $test_session
            ]);
        } else if ($test->getStatus() == 'open') {
            return view('cabinet.test.start', [
                'user' => $user,
                'test' => $test
            ]);
        } else {
            return view('cabinet.test.closed', [
                'user' => $user,
                'test' => $test
            ]);
        }
    }

    public function start(Request $request, $test_id)
    {
        $user = $request->user();
        if ($user->status == 'approved') {
            $test = Test::findOrFail($test_id);
            if ($test->userHasEnoughLevel()) {
                $test_session = $test->createUserSession($user->id);
                return response()->json([
                    'question_form' => view('cabinet.test.question', [
                        'question' => $test_session->getNextQuestion()
                    ])->render(),
                    'questions_left' => $test_session->getQuestionsLeft(),
                    'time_left' => $test_session->getSecondsLeft()
                ]);
            } else {
                return abort(403);
            }
        } else {
            return abort(404);
        }
    }

    public function nextQuestion(Request $request, $test_id)
    {
        $user = $request->user();
        $test = Test::findOrFail($test_id);
        $test_session = $test->userSession($user->id);

        if ($test_session->getQuestionsLeft() == 0 || $test_session->getSecondsLeft() <=0) {
            return response()->json([
                'end_test' => true,
            ]);
        } else {
            return response()->json([
                'question_form' => view('cabinet.test.question', [
                                        'question' => $test_session->getNextQuestion()
                                    ])->render(),
                'questions_left' => $test_session->getQuestionsLeft(),
                'time_left' => $test_session->getSecondsLeft()
            ]);
        }
    }

    public function setAnswer(Request $request, $test_id, $question_id, $answer_id)
    {
        $user = $request->user();
        $test = Test::findOrFail($test_id);
        $test_session = $test->userSession($user->id);
        $correct_answer_id = $test_session->saveAndGetCorrectAnswer($question_id, $answer_id);
        return response()->json([
            'correct_answer_id' => $correct_answer_id,
            'user_points' => User::find($user->id)->rating,
            'questions_left' => $test_session->getQuestionsLeft(),
        ]);
    }
}
