<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class SubscribeController
{

    public function subscribe(Request $request)
    {
        Validator::make([
            'email' => $request->input('email'),
        ],
        [
            'email' => 'required|email|unique:subscribers,email',
        ])->validate();
        Subscriber::create([
            'email' => $request->input('email'),
        ]);
        return response()->json([
            'success' => true,
            'message' => __('subscribe.success'),
        ], 200);
    }

}