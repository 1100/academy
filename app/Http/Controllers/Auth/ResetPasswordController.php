<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Custom\Facades\Password;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */

    protected function redirectPath()
    {
        return route('cabinet.dashboard');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetBySmsForm(Request $request, $token, $phone)
    {
        return view('auth.passwords.sms')->with([
            'token' => $token,
            'phone' => $phone,
        ]);
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function resetBySmsCode(Request $request)
    {
        $this->validate($request, $this->rulesForCode(), $this->validationErrorMessages());

        $response = $this->broker()->reset(
            $this->credentialsCode($request), function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );

        return $response == Password::PASSWORD_RESET
                    ? redirect($this->redirectPath())
                    : $this->sendResetFailedResponse($request, $response);
    }

    protected function rulesForCode()
    {
        return [
            'token' => 'required',
            'phone' => 'phone|required',
            'code' => 'required|numeric|min:0|max:999999',
            'password' => 'required|confirmed|min:6',
        ];
    }

    protected function credentialsCode(Request $request)
    {
        return $request->only(
            'token', 'phone', 'code', 'password', 'password_confirmation'
        );
    }

    /**
     * Get the response for a failed password reset.
     *
     * @param  \Illuminate\Http\Request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendResetFailedResponse(Request $request, $response)
    {
        return redirect()->back()
            ->withInput($request->only('email', 'code'))
            ->withErrors([
                'email' => trans($response),
                'code' => 'Incorrect code',
            ]);
    }

}
