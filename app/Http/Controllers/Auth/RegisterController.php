<?php

namespace App\Http\Controllers\Auth;

use App\Faculty;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\AddUserRequest;
use Illuminate\Support\Facades\Cookie;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    protected $faculty;
    protected $rules;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['showRegistrationForm']);
    }

    protected function redirectTo()
    {
        return route('cabinet.dashboard');
    }

    protected function validator(array $data)
    {
        $data['faculty'] = $this->faculty->slug;
        return Validator::make($data, $this->rules);
    }

    protected function create(array $data, $invited_key = null)
    {
        return $this->faculty->addUser($data, $invited_key);
    }


    public function showRegistrationForm(Request $request, $faculty_slug, $invited_key = null)
    {
        $this->faculty = Faculty::findOrFail($faculty_slug);
        $events = $this->faculty->events()->orderBy('date');
        $response = response()->view('auth.register', [
            'faculty' => $this->faculty,
            'fields' => $this->faculty->requiredInformationFields()->get(),
            'events' => [
                'actual' => $events->actual()->get(),
                'past' => $events->past()->get(),
            ],
            'user' => $request->user(),
        ]);
        if ($invited_key) {
            return $response->withCookie(cookie('invited_key', $invited_key, 4320)); // 4320 minutes == 3 days
        } else {
            return $response;
        }
    }

    public function register(AddUserRequest $request, $faculty_slug)
    {
        $this->rules = $request->rules();
        $this->faculty = Faculty::findOrFail($faculty_slug);

        if (!$this->faculty->open_registration) {
            return abort(403);
        }

        $this->validator($request->all())->validate();

        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
            'agreement' => 'accepted',
        ]);

        $user = $this->create($request->all(), $request->cookie('invited_key'));
        $this->guard()->login($user);

        return redirect($this->redirectPath())->withCookie(Cookie::forget('invited_key'));
    }
}
