<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\InformationField;
use App\Custom\InvalidPhoneException;
use App\Custom\Phone;
use App\UserInformation;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use Socialite;
use App\User;
use App\Faculty;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    protected $faculty;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    protected function redirectTo()
    {
        return route('cabinet.dashboard');
    }

    protected function username()
    {
        return 'login';
    }

    public function showLoginForm()
    {
        return view('auth.login', [
            'fields' => InformationField::loginFields(),
        ]);
    }



    public function login(Request $request)
    {
        // TODO Custom error message
        $this->validateLogin($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        $request = $this->formatPhoneIfExists($request);
        $user_field = UserInformation::where('value', $request->input('login'))->with('loginField')->first();
        if ($user_field && $user_field->loginField) {
            $user = User::find($user_field->user_id);
            if (Hash::check($request->input('password'), $user->password)) {
                $request->session()->regenerate();
                $this->guard()->login($user);
                $this->clearLoginAttempts($request);
                return redirect($this->redirectTo());
            }
        }

        $this->incrementLoginAttempts($request);
        return $this->sendFailedLoginResponse($request);
    }

    private function formatPhoneIfExists(Request $request)
    {
//        if ($this->faculty->informationFieldRequired(InformationField::find('phone'))) {
            if ($phone = Phone::getPhoneIfValid($request->input('login'))) {
                $request->merge(['login' => $phone->formattedNumber()]);
            }
//        }
        return $request;
    }

}
