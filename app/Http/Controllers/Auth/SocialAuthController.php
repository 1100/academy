<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\UserInformation;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Socialite;
use App\User;
use App\Faculty;
use Illuminate\Support\Facades\Cookie;

class SocialAuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $faculty;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectToRoute = 'cabinet.dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Redirect the user to the OAuth Provider.
     *
     * @return Response
     */
    public function redirectToProvider($faculty_slug = null)
    {
        return Socialite::driver('facebook')->with(['state' => $faculty_slug])->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback(Request $request)
    {
        $faculty_slug = request()->input('state');
        if($faculty_slug) {
            $this->faculty = Faculty::findOrFail($faculty_slug);
        }
        $user = Socialite::driver('facebook')->stateless()->user();
        $authUser = $this->findOrCreateUser($user, $request->cookie('invited_key'));
        if ($authUser) {
            Auth::login($authUser, true);
        }
        return redirect()->route($this->redirectToRoute)->withCookie(Cookie::forget('invited_key'));
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $facebookUser Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($facebookUser, $invited_key = null)
    {
        $authUser = false;
        preg_match('/([^\s]*)\s(.*)/', $facebookUser->getName(), $name);
        // if facebook user exists
        $authUserInformation = UserInformation::where([
            'information_field' => 'facebook',
            'value' => $facebookUser->getId(),
        ])->first();
        if($authUserInformation) {
            $authUser = $authUserInformation->user()->get()->first();
        }
        if ($authUser) {
            return $authUser;
        }
        // if user logged in or same email user exists
        $authUser = Auth::user();
        if (!$authUser) {
            $authUserInformation = UserInformation::where([
                'information_field' => 'email',
                'value' => $facebookUser->getEmail(),
            ])->first();
            if($authUserInformation) {
                $authUser = $authUserInformation->user()->get()->first();
            }
        } else {
            $this->redirectTo = '/cabinet';
        }
        if ($authUser) {
            $user_information = [
                'facebook' => [
                    'value' => $facebookUser->getId(),
                ],
            ];
            if($authUser->information()->where(['information_field' => 'first_name'])->orWhere(['information_field' => 'last_name'])->get()->isEmpty()) {
                $user_information['first_name']['value'] = $name[1];
                $user_information['last_name']['value'] = $name[2];
            }
            $authUser->information()->attach($user_information);
            return $authUser;
        }
        // otherwise register new user
        if ($this->faculty) {
            $authUser = User::create([
                'faculty' => $this->faculty->slug,
                'invited_key' => $invited_key,
            ]);
            $authUser->information()->attach([
                'facebook' => [
                    'value' => $facebookUser->getId(),
                ],
                'email' => [
                    'value' => $facebookUser->getEmail(),
                ],
                'first_name' => [
                    'value' => $name[1],
                ],
                'last_name' => [
                    'value' => $name[2],
                ],
            ]);

            $authUser->avatar = $facebookUser->getAvatar();
            $authUser->save();
        }
        return $authUser;
    }

}
