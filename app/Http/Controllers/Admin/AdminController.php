<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\AdminRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\User;
use App\InformationType;
use App\InformationField;
use App\Faculty;
use Gate;
use function PHPSTORM_META\elementType;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
        Auth::shouldUse('admin');
    }

    public function index()
    {
        return redirect()->route('admin.faculties.show');
    }

    public function showAllUsers()
    {
        return view('admin.users')->with('users', User::all());
    }

    public function userInformationSettings()
    {
        if (Gate::allows('permission', 'user-information')) {
            $information_types = InformationType::all();
            $information_fields = InformationField::all();
            return view('admin.user_information_settings', [
                'information_types' => $information_types,
                'information_fields' => $information_fields
            ]);
        } else {
            return abort(404);
        }
    }

    public function addUserInformation(Request $request) {
        if (Gate::allows('permission', 'user-information')) {
            $this->validate($request, [
                'field_name' => 'required|string',
                'field_description' => 'string|nullable',
                'field_default_value' => 'string|nullable',
                'type' => 'required|exists:information_types,slug',
                'field_login' => 'boolean|nullable',
                'field_hidden' => 'boolean|nullable'
            ]);
            InformationField::create([
                'slug' => str_slug($request->input('field_name')),
                'name' => $request->input('field_name'),
                'description' => $request->input('field_description'),
                'default_value' => $request->input('field_default_value'),
                'type' => $request->input('type'),
                'login' => (bool)$request->input('field_login'),
                'hidden' => (bool)$request->input('field_hidden'),
            ]);
        }

        return redirect()->action('Admin\AdminController@userInformationSettings');
    }

    public function showUserInformation($user_information_slug) {
        if (Gate::allows('permission', 'user-information')) {
            $information_types = InformationType::all();
            $field = InformationField::findOrFail($user_information_slug);

            return view('admin.user_information', [
                'information_types' => $information_types,
                'field' => $field
            ]);
        } else {
            return abort(404);
        }
    }

    public function editUserInformation(Request $request, $user_information_slug) {
        if (Gate::allows('permission', 'user-information')) {
            $field = InformationField::findOrFail($user_information_slug);
            $this->validate($request, [
                'field_name' => 'required|string',
                'field_description' => 'string|nullable',
                'type' => 'required|exists:information_types,slug',
                'field_login' => 'boolean|nullable'
            ]);

            $field->name = $request->input('field_name');
            $field->description = $request->input('field_description');
            $field->type = $request->input('type');
            $field->login = (bool)$request->input('field_login');

            $field->save();
        }

        return redirect()->action('Admin\AdminController@userInformationSettings');
    }

    public function deleteUserInformation($user_information_slug) {
        if (Gate::allows('permission', 'user-information')) {
            $field = InformationField::findOrFail($user_information_slug);
            $field->delete();
        }

        return redirect()->action('Admin\AdminController@userInformationSettings');
    }

    public function showAllAdmins()
    {
        if (Gate::allows('permission', 'admins')) {
            return view('admin.admins', [
                'admins' => Admin::all(),
                'roles' => AdminRole::all(),
                'faculties' => Faculty::all(),
            ]);
        } else {
            return abort(404);
        }
    }

    public function createAdmin(Request $request)
    {
        if (Gate::allows('permission', 'admins')) {
            $this->validate($request, [
                'login' => 'required|max:255|unique:admins',
                'password' => 'required|max:255',
                'roles' => 'required',
                'roles.*' => 'required|exists:admin_roles,slug',
                'faculties.*' => 'nullable|exists:faculties,slug',
            ]);

            $admin = Admin::create([
                'login' => $request->input('login'),
                'password' => bcrypt($request->input('password')),
            ]);

            $admin->roles()->attach($request->input('roles'));
            $admin->faculties()->attach($request->input('faculties'));
        }

        return redirect()->action('Admin\AdminController@showAllAdmins');
    }

    public function showAdmin($admin_id) {
        if (Gate::allows('permission', 'admins')) {
            $admin = Admin::findOrFail($admin_id);
            return view('admin.admin', [
                'admin' => $admin,
                'roles' => AdminRole::all(),
                'faculties' => Faculty::all(),
            ]);
        } else {
            return abort(404);
        }
    }

    public function editAdmin(Request $request, $admin_id)
    {
        if (Gate::allows('permission', 'admins')) {
            $this->validate($request, [
                'login' => [
                    'required',
                    'max:255',
                    Rule::unique('admins')->ignore($admin_id),
                ],
                'roles.*' => 'required|exists:admin_roles,slug',
                'faculties.*' => 'nullable|exists:faculties,slug',
            ]);

            $admin = Admin::find($admin_id);

            $admin->login = $request->input('login');
            $admin->roles()->sync($request->input('roles'));
            $admin->faculties()->sync($request->input('faculties'));

            $admin->save();
        }
        return redirect()->route('admin.all_admins');
    }

    public function deleteAdmin(Request $request, $admin_id)
    {
        if (Gate::allows('permission', 'admins')) {
            $admin = Admin::find($admin_id);
            $admin->delete();
        }
        return redirect()->route('admin.all_admins');
    }

}
