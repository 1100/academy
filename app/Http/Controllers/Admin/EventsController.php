<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use App\Faculty;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Gate;
use Illuminate\Support\Facades\Storage;
use Excel;
use URL;

class EventsController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
        Auth::shouldUse('admin');
    }

    public function index($faculty)
    {
        if (Gate::allows('permission', 'events') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            return view('admin.faculty.events.list', [
                'events' => $faculty->events()->paginate(15),
                'faculty' => $faculty,
            ]);
        }
        return abort(403);
    }

    public function showEvent($faculty, $event_id)
    {
        if (Gate::allows('permission', 'events') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $event = $faculty->events()->findOrFail($event_id);
            return view('admin.faculty.events.event', [
                'event' => $event,
                'faculty' => $faculty,
            ]);
        }
        return abort(403);
    }

    public function exportEvent($faculty, $event_id)
    {
        if (Gate::allows('permission', 'events') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $event = $faculty->events()->findOrFail($event_id);
            Excel::create('Заявки_'.$event->title.'_'.Carbon::now()->format('d.m.y_H:i'), function($excel) use($event) {
                $excel->setTitle('Заявки на участие в мероприятии '.$event->title)
                    ->setCreator(URL::to('/'))
                    ->setCompany('Geberit Academy')
                    ->setDescription('Заявки на участие в мероприятии "'.$event->title.'" с сайта '.URL::to('/'))
                    ->sheet('Заявки', function($sheet) use($event) {
                        $sheet->fromArray($event->getUsersData());
                        $sheet->setAutoSize(true);
                    });
            })->export('xlsx');
        } else {
            return abort(404);
        }
    }

    public function showAddEventForm($faculty)
    {
        if (Gate::allows('permission', 'events') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            return view('admin.faculty.events.add_event', [
                'faculty' => $faculty,
            ]);
        }
        return abort(403);
    }

    public function addEvent($faculty, Request $request)
    {
        if (Gate::allows('permission', 'events') && Gate::allows('faculty', $faculty)) {
            $this->validate($request, [
                'title' => 'required',
                'image' => 'image',
                'short_description' => 'required',
                'description' => 'required',
                'location' => 'required',
                'date' => 'date|required',
                'registration_start_date' => 'date|required',
                'registration_end_date' => 'date|required|after:registration_start_date',
            ]);
            $path = null;
            if ($request->file('image')) {
                $path = $request->file('image')->store('events_images');
                Storage::setVisibility($path, 'public');
            }
            $event = Event::create([
                'title' => $request->input('title'),
                'image' => $path,
                'short_description' => $request->input('short_description'),
                'description' => $request->input('description'),
                'location' => $request->input('location'),
                'date' => $request->input('date'),
                'registration_start_date' => $request->input('registration_start_date'),
                'registration_end_date' => $request->input('registration_end_date'),
                'faculty' => $faculty,
            ]);
            return redirect()->action('Admin\EventsController@editEvent', [
                'faculty' => $faculty,
                'event_id' => $event->id,
            ]);
        }
        return abort(404);
    }

    public function editEvent($faculty, Request $request, $event_id)
    {
        if (Gate::allows('permission', 'events') && Gate::allows('faculty', $faculty)) {
            $this->validate($request, [
                'title' => 'required',
                'image' => 'image',
                'short_description' => 'required',
                'description' => 'required',
                'location' => 'required',
                'date' => 'date|required',
                'registration_start_date' => 'date|required',
                'registration_end_date' => 'date|required|after:registration_start_date',
            ]);

            $faculty = Faculty::findOrFail($faculty);
            $event = $faculty->events()->findOrFail($event_id);

            $path = null;
            if ($request->file('image')) {
                if ($event->image) {
                    Storage::delete($event->image);
                }
                $path = $request->file('image')->store('events_images');
                Storage::setVisibility($path, 'public');
                $event->image = $path;
            }

            $event->title = $request->input('title');
            $event->short_description = $request->input('short_description');
            $event->description = $request->input('description');
            $event->location = $request->input('location');
            $event->date = $request->input('date');
            $event->registration_start_date = $request->input('registration_start_date');
            $event->registration_end_date = $request->input('registration_end_date');

            $event->save();

            return view('admin.faculty.events.event', [
                'faculty' => $faculty,
                'event' => $event,
            ]);
        }
        return abort(404);
    }

    public function deleteEvent($faculty, $event_id)
    {
        $faculty = Faculty::findOrFail($faculty);
        if (Gate::allows('permission', 'events') && Gate::allows('faculty', $faculty->slug)) {
            $event = $faculty->events()->findOrFail($event_id);
            if ($event->image) {
                Storage::delete($event->image);
            }
            $event->delete();
        }
        return redirect()->action('Admin\EventsController@index', [
            'faculty' => $faculty->slug,
        ]);
    }
}
