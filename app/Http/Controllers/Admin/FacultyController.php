<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AddUserRequest;
use App\Invite;
use App\Level;
use App\User;
use App\UserStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Faculty;
use App\InformationField;
use App\Admin;
use App\AdminRole;
use Gate;
use Validator;
use Excel;
use URL;

class FacultyController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
        Auth::shouldUse('admin');
    }

    public function index($faculty)
    {
        if (Gate::allows('permission', 'users') && Gate::allows('faculty', $faculty)) {
            return redirect()->route('admin.faculties.faculty.users', [
                'faculty' => $faculty
            ]);
        } else if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            return redirect()->route('admin.faculties.faculty.test.all', [
                'faculty' => $faculty
            ]);
        } else {
            return abort(404);
        }
    }

    public function showAll()
    {
        if (Gate::allows('role', 'admin')) {
            $faculties = Faculty::all();
        } else {
            $faculties = Auth::guard('admin')->user()->faculties()->get();
        }
        $login_information_fields = InformationField::loginFields();
        $information_fields = InformationField::additionalFields();

        return view('admin.faculties', [
            'faculties' => $faculties,
            'login_information_fields' => $login_information_fields,
            'information_fields' => $information_fields,
        ]);
    }

    public function showUsers(Request $request, $faculty)
    {
        if (Gate::allows('permission', 'users') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            if ($request->has('q')) {
                $users = User::getBySearchQuery($faculty, $request->input('q'));
            } else {
                $users = $faculty->users();
            }
            return view('admin.faculty.users', [
                'faculty' => $faculty,
                'users' => $users->paginate(25),
            ]);
        } else {
            return abort(404);
        }
    }

    public function showUser($faculty, $user_id)
    {
        if (Gate::allows('permission', 'users') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            return view('admin.faculty.user', [
                'faculties' => Faculty::all(),
                'faculty' => $faculty,
                'user' => $faculty->users()->find($user_id),
                'statuses' => UserStatus::all(),
                'levels' => Level::orderBy('number')->get(),
            ]);
        } else {
            return abort(404);
        }
    }

    public function editUser(AddUserRequest $request, $faculty, $user_id)
    {
        if (Gate::allows('permission', 'users') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $user = User::find($user_id);
            $fields = $faculty->information()->where('slug', '!=', 'facebook')->get();
            $user_information = [];
            foreach ($fields as $field) {
                $user_information[$field->slug] = ['value' => $request->input($field->slug)];
            }
            $user->information()->sync($user_information, false);

            $user->level = $request->input('level');
            $user->save();

            if ($request->has('status')) {
                if ($user->status != 'approved' && $request->input('status') == 'approved') {
                    $user->sendApprovedEmailNotification();
                }
                $user->status = $request->input('status');
                $user->save();
                if ($user->invited_key) {
                    User::where('invite_key', $user->invited_key)->first()->updateInvitePoints();
                }
            }

            if ($request->has('user_faculty') && Gate::allows('role', 'admin')) {
                $faculty = Faculty::findOrFail($request->input('user_faculty'));
                $user->faculty = $request->input('user_faculty');
                $user->save();
            }
            if (Gate::allows('permission', 'rating')) {
                foreach ($faculty->customPointables as $pointable) {
                    $is_completed = $request->has('custom_pointables')
                        && is_array($request->input('custom_pointables'))
                        && array_key_exists($pointable->id, $request->input('custom_pointables'))
                        && (bool)$request->input('custom_pointables')[$pointable->id];
                    $pointable->setIsCompleted($user, $is_completed);
                }
                $user->updateRating();
            }
        }

        return redirect()->route('admin.faculties.faculty.show_user', ['faculty' => $faculty->slug, 'user_id' => $user_id]);
    }

    public function addUserForm($faculty)
    {
        if (Gate::allows('permission', 'users') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            return view('admin.faculty.add_user', [
                'faculty' => $faculty,
            ]);
        } else {
            return abort(404);
        }
    }

    public function addUser(AddUserRequest $request, $faculty)
    {
        if (Gate::allows('permission', 'users') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $user = $faculty->addUser($request->all());
            $user->status = 'approved';
            $user->save();
            if ($request->has('email')) {
                $user->sendPasswordSetEmailNotification();
            }
        }
        return redirect()->route('admin.faculties.faculty.users', ['faculty' => $faculty->slug]);
    }

    public function showUserInformationSettings($faculty)
    {
        if (Gate::allows('permission', 'faculty-user-information') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $login_information_fields = InformationField::loginFields();
            $information_fields = InformationField::additionalFields();
            return view('admin.faculty.user_information', [
                'faculty' => $faculty,
                'login_information_fields' => $login_information_fields,
                'information_fields' => $information_fields,
            ]);
        } else {
            return abort(404);
        }
    }

    public function showTasks($faculty)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            return view('admin.faculty.tasks', [
                'faculty' => $faculty,
            ]);
        } else {
            return abort(404);
        }
    }

    public function showCurators($faculty)
    {
        if (Gate::allows('permission', 'admins')) {
            $faculty = Faculty::findOrFail($faculty);
            return view('admin.faculty.curators', [
                'faculty' => $faculty,
                'admins' => $faculty->admin()->get(),
                'roles' => AdminRole::where('slug', '!=', 'admin')->get(),
                'faculties' => Faculty::all(),
            ]);
        } else {
            return abort(404);
        }
    }

    public function showSettings($faculty)
    {
        if (
            (Gate::allows('permission', 'faculty-settings')
             || Gate::allows('permission', 'faculty-display-settings'))
            && Gate::allows('faculty', $faculty)
        ) {
            $faculty = Faculty::with('invite')->with('information')->findOrFail($faculty);
            $can_invite = $faculty->loginInformationFields()->where('type', 'email')->get()->isNotEmpty();
            return view('admin.faculty.settings', [
                'faculty' => $faculty,
                'can_invite' => $can_invite,
            ]);
        } else {
            return abort(404);
        }
    }

    public function updateLoginInformationFields(Request $request, $faculty)
    {
        if (Gate::allows('permission', 'faculty-user-information') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $this->validate($request,
                [
                    'login_fields' => 'required|array',
                    'login_fields.*' => 'exists:information_fields,slug',
                ],
                [
                    'login_fields.required' => 'Нужно разрешить хотя бы один способ регистрации'
                ]
            );

            $login_fields_slugs = [];
            foreach (InformationField::loginFields() as $field) {
                $login_fields_slugs[] = $field->slug;
            }
            $faculty->information()->detach($login_fields_slugs);
            $faculty->information()->attach($request->input('login_fields'), [
                'required' => true,
                'display' => true,
                'rating_display' => false,
                'position' => 0,
            ]);
            return redirect()->route('admin.faculties.faculty.user_information', ['faculty' => $faculty->slug]);
        } else {
            return abort(404);
        }
    }

    public function updateRequiredInformationFields(Request $request, $faculty)
    {
        if (Gate::allows('permission', 'faculty-user-information') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $this->validate($request, [
                    'fields' => 'array',
                    'fields.*' => 'max:255|exists:information_fields,slug'
                ]
            );
            $required_fields = $request->input('fields');
            foreach ($faculty->additionalInformationFields() as $field) {
                $faculty->information()->updateExistingPivot($field->slug, ['required' => isset($required_fields[$field->slug])]);
            }
            return redirect()->route('admin.faculties.faculty.user_information', ['faculty' => $faculty->slug]);
        } else {
            return abort(404);
        }
    }

    public function addInformationField(Request $request, $faculty)
    {
        if (Gate::allows('permission', 'faculty-user-information') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $this->validate($request, [
                'new_field' => 'required|max:255|exists:information_fields,slug'
            ]);
            $max_position = $faculty->getMaxInformationFieldPosition();
            $faculty->information()->attach($request->input('new_field'), ['required' => false, 'position' => $max_position + 1]);
            return redirect()->route('admin.faculties.faculty.user_information', ['faculty' => $faculty->slug]);
        } else {
            return abort(404);
        }
    }

    public function deleteInformationField(Request $request, $faculty, $field)
    {
        if (Gate::allows('permission', 'faculty-user-information') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            Validator::make(
                ['field' => $field],
                ['field' => 'required|max:255|exists:information_fields,slug',]
            )->validate();
            $faculty->information()->detach($field);
            return redirect()->route('admin.faculties.faculty.user_information', ['faculty' => $faculty->slug]);
        } else {
            return abort(404);
        }
    }

    public function updateDisplayInformationFields(Request $request, $faculty)
    {
        if (Gate::allows('permission', 'faculty-display-settings') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);

            $this->validate($request, [
                'display_fields.*' => 'max:255|exists:information_fields,slug',
                'rating_display_fields.*' => 'max:255|exists:information_fields,slug',
            ]);
            $display_fields = $request->input('display_fields');
            foreach ($faculty->information()->get() as $field) {
                $faculty->information()->updateExistingPivot($field->slug, ['display' => isset($display_fields[$field->slug])]);
            }
            $rating_display_fields = $request->input('rating_display_fields');
            foreach ($faculty->information()->get() as $field) {
                $faculty->information()->updateExistingPivot($field->slug, ['rating_display' => isset($rating_display_fields[$field->slug])]);
            }
            if ($request->has('open_rating') && $request->input('open_rating') == 'on') {
                $faculty->open_rating = true;
            } else {
                $faculty->open_rating = false;
            }
            $faculty->save();

            return redirect()->route('admin.faculties.faculty.settings.show', ['faculty' => $faculty->slug]);
        } else {
            return abort(404);
        }
    }

    public function updateInvite(Request $request, $faculty)
    {
        if (Gate::allows('permission', 'faculty-settings') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::with('invite')->findOrFail($faculty);

            $this->validate($request, [
                'invite_name' => 'required|string|max:512',
                'invite_points' => 'integer|min:0',
                'invites_count' => 'integer|min:0',
            ]);

            $faculty->invite->name = $request->input('invite_name');
            $faculty->invite->points = $request->input('invite_points');
            $faculty->invite->invites_count = $request->input('invites_count');
            $faculty->invite->save();
            $faculty->updateAllUsersInvitePoints();
            return redirect()->route('admin.faculties.faculty.settings.show', ['faculty' => $faculty->slug]);
        } else {
            return abort(404);
        }
    }

    public function enableInvite(Request $request, $faculty)
    {
        if (Gate::allows('permission', 'faculty-settings') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            Invite::create([
                'faculty' => $faculty->slug,
            ]);
            $users = User::where('faculty', $faculty->slug)->get();
            foreach($users as $user) {
                $user->generateInvite();
                $user->invited_key = null;
                $user->save();
            }
            return redirect()->route('admin.faculties.faculty.settings.show', ['faculty' => $faculty->slug]);
        } else {
            return abort(404);
        }
    }

    public function disableInvite(Request $request, $faculty)
    {
        if (Gate::allows('permission', 'faculty-settings') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $faculty->invite->points()->delete();
            $faculty->invite->delete();
            $users = User::where('faculty', $faculty->slug)->get();
            foreach($users as $user) {
                $user->invite_key = null;
                $user->invited_key = null;
                $user->save();
            }
            return redirect()->route('admin.faculties.faculty.settings.show', ['faculty' => $faculty->slug]);
        } else {
            return abort(404);
        }
    }

    public function updateOpenRegistration(Request $request, $faculty)
    {
        if (Gate::denies('permission', 'faculty-settings') || Gate::denies('faculty', $faculty)) {
            return abort(404);
        }
        $faculty = Faculty::findOrFail($faculty);
        $faculty->open_registration = (bool)$request->input('open_registration');
        $faculty->save();
        return redirect()->route('admin.faculties.faculty.settings.show', ['faculty' => $faculty->slug]);
    }

    public function exportUsers(Request $request, $faculty)
    {
        if (Gate::allows('permission', 'faculty-settings') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            Excel::create('Пользователи_'.$faculty->name, function($excel) use($faculty) {
                $excel->setTitle('Пользователи с факультета '.$faculty->name)
                      ->setCreator(URL::to('/'))
                      ->setCompany('Geberit Academy')
                      ->setDescription('Личные данные пользователей факультета "'.$faculty->name.'" с сайта '.URL::to('/'))
                      ->sheet('Пользователи', function($sheet) use($faculty) {
                          $sheet->fromArray($faculty->getUsersData());
                          $sheet->setAutoSize(true);
                      });
            })->export('xlsx');
        } else {
            return abort(404);
        }
    }

    public function updateIndex(Request $request, $faculty)
    {
        if (Gate::denies('permission', 'faculty-settings') || Gate::denies('faculty', $faculty)) {
            return abort(404);
        }
        $faculty = Faculty::findOrFail($faculty);
        $faculty->index = (int)$request->input('index');
        $faculty->save();
        return redirect()->route('admin.faculties.faculty.settings.show', ['faculty' => $faculty->slug]);
    }

    public function updateEventsEnabled(Request $request, $faculty)
    {
        if (Gate::denies('permission', 'faculty-settings') || Gate::denies('faculty', $faculty)) {
            return abort(404);
        }
        $faculty = Faculty::findOrFail($faculty);
        $faculty->events_enabled = (bool)$request->input('events_enabled');
        $faculty->save();
        return redirect()->route('admin.faculties.faculty.settings.show', ['faculty' => $faculty->slug]);
    }

    public function updateTestsEnabled(Request $request, $faculty)
    {
        if (Gate::denies('permission', 'faculty-settings') || Gate::denies('faculty', $faculty)) {
            return abort(404);
        }
        $faculty = Faculty::findOrFail($faculty);
        $faculty->tests_enabled = (bool)$request->input('tests_enabled');
        $faculty->countdown_enabled = (bool)$request->input('countdown_enabled');
        $faculty->countdown_date = Carbon::createFromFormat('Y-m-d H:i:s', $request->input('countdown_date'));
        $faculty->save();
        return redirect()->route('admin.faculties.faculty.settings.show', ['faculty' => $faculty->slug]);
    }
}
