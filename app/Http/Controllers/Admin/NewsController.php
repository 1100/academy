<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\AdminRole;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Faculty;
use Gate;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
        Auth::shouldUse('admin');
    }

    public function index()
    {
        if (Gate::allows('permission', 'news')) {
            if (Gate::allows('role', 'admin')) {
                $faculties = Faculty::all();
                $posts = Post::query();
            } else {
                $faculties = Auth::guard('admin')->user()->faculties()->get();
                $posts = Post::whereIn('faculty', $faculties->pluck('slug')->toArray());
            }
            $posts = $posts->latest()->paginate(15);
            return view('admin.posts', [
                'posts' => $posts,
                'faculties' => $faculties,
            ]);
        } else {
            return abort(404);
        }
    }

    public function showPost($post_id)
    {
        if (Gate::allows('permission', 'news')) {
            $post = Post::findOrFail($post_id);
            if (Gate::allows('faculty', $post->faculty)) {
                if (Gate::allows('role', 'admin')) {
                    $faculties = Faculty::all();
                } else {
                    $faculties = Auth::guard('admin')->user()->faculties()->get();
                }
                return view('admin.post', [
                    'post' => $post,
                    'faculties' => $faculties
                ]);
            }
        }
        return abort(404);
    }

    public function showAddPostForm()
    {
        if (Gate::allows('permission', 'news')) {
            if (Gate::allows('role', 'admin')) {
                $faculties = Faculty::all();
            } else {
                $faculties = Auth::guard('admin')->user()->faculties()->get();
            }
            return view('admin.add_post', [
                'faculties' => $faculties,
            ]);
        } else {
            return abort(404);
        }
    }

    public function addPost(Request $request)
    {
        if (Gate::allows('permission', 'news')) {
            $this->validate($request, [
                'title' => 'required',
                'image' => 'image',
                'faculty' => 'exists:faculties,slug',
            ]);
            if (Gate::allows('faculty', $request->input('faculty'))) {
                $path = null;
                if ($request->file('image')) {
                    $path = $request->file('image')->store('news_images');
                    Storage::setVisibility($path, 'public');
                }
                $post = Post::create([
                    'title' => $request->input('title'),
                    'image' => $path,
                    'content' => $request->input('content'),
                    'faculty' => $request->input('faculty'),
                ]);
                return redirect()->action('Admin\NewsController@editPost', ['post_id' => $post->id]);
            }
        }
        return abort(404);
    }

    public function editPost(Request $request, $post_id)
    {
        if (Gate::allows('permission', 'news')) {
            $this->validate($request, [
                'title' => 'required',
                'image' => 'nullable|image',
                'content' => 'required',
                'faculty' => 'exists:faculties,slug',
            ]);

            if (Gate::allows('faculty', $request->input('faculty'))) {

                $post = Post::findOrFail($post_id);

                $path = null;
                if ($request->file('image')) {
                    if ($post->image) {
                        Storage::delete($post->image);
                    }
                    $path = $request->file('image')->store('news_images');
                    Storage::setVisibility($path, 'public');
                }

                $post->title = $request->input('title');
                if ($path) {
                    $post->image = $path;
                }
                $post->content = $request->input('content');
                $post->faculty = $request->input('faculty');

                $post->save();

                if (Gate::allows('role', 'admin')) {
                    $faculties = Faculty::all();
                } else {
                    $faculties = Auth::guard('admin')->user()->faculties();
                }

                return view('admin.post', [
                    'post' => $post,
                    'faculties' => $faculties,
                ]);
            }
        }
        return abort(404);
    }

    public function deletePost($post_id)
    {
        if (Gate::allows('permission', 'news')) {
            $post = Post::findOrFail($post_id);
            if (Gate::allows('faculty', $post->faculty)) {
                if ($post->image) {
                    Storage::delete($post->image);
                }
                $post->delete();
            }
        }
        return redirect()->action('Admin\NewsController@index');
    }
}
