<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\SupportMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Gate;

class SupportController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
        Auth::shouldUse('admin');
    }
    public function index(Request $request)
    {
        $messages = $this->getMessages($request->user());
        if(is_null($messages)) {
            return abort('404');
        } else {
            return view('admin.support.messages', ['messages' => $messages]);
        }
    }

    public function showUnread(Request $request)
    {
        $messages = $this->getMessages($request->user())->where('read_status', false);
        if(is_null($messages)) {
            return abort('404');
        } else {
            return view('admin.support.messages', ['messages' => $messages]);
        }
    }

    public function showRead(Request $request)
    {
        $messages = $this->getMessages($request->user())->where('read_status', true);
        if(is_null($messages)) {
            return abort('404');
        } else {
            return view('admin.support.messages', ['messages' => $messages]);
        }
    }

    protected function getMessages($admin)
    {
        $messages = SupportMessage::with('user', 'user.faculty', 'admin');
        if (Gate::allows('role', 'admin')) {
            return $messages->get();
        } elseif (Gate::allows('role', 'curator')) {
            return $messages->whereIn(
                    'faculty',
                    $admin->faculties()
                        ->pluck('slug')
                        ->toArray())
                ->get();
        } else {
            return null;
        }
    }

    public function changeReadStatus(Request $request)
    {
        $message = SupportMessage::findOrFail($request->input('message_id'));
        $message->read_status = (bool)$request->input('read_status');
        if ($message->read_status) {
            $message->admin_id = $request->user()->id;
        }
        $message->save();
//        return response()->json(['status' => 'success'], 200);
        return view('admin.support.message', [
            'message' => $message
        ]);
    }
}
