<?php

namespace App\Http\Controllers\Admin;

use App\Faculty;
use App\Level;
use App\Test;
use App\Question;
use App\Answer;
use App\TestBonus;
use Carbon\Carbon;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Maatwebsite\Excel\Facades\Excel;

class TestController extends Controller
{

    public $excel_storage_path = 'tests_excel';

    public function __construct()
    {
        $this->middleware('admin');
        Auth::shouldUse('admin');
    }

    public function showAll($faculty)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $tests = Test::where('faculty', $faculty->slug)->get();
            return view('admin.faculty.test.all', [
                'faculty' => $faculty,
                'tests' => $tests,
            ]);
        } else {
            return abort(404);
        }
    }

    public function export(Request $request, $faculty)
    {
        if (Gate::allows('permission', 'faculty-settings') && Gate::allows('faculty', $faculty)) {

            $faculty = Faculty::findOrFail($faculty);

            $last_question_session = DB::table('questions_sessions')
                ->orderBy('id', 'desc')
                ->select('id')
                ->limit(1)
                ->first();

            $file_extension = 'xlsx';
            $storage_path = storage_path('app/' . $this->excel_storage_path . '/' . $faculty->slug);
            $file_name = $last_question_session->id;
            $file_path = $storage_path . '/' . $file_name . '.' . $file_extension;

            $clean_file_name = 'Тестирование_' . $faculty->name . '.' . $file_extension;

            $fs = new Filesystem();

            if (!$fs->exists($file_path)) {
                $fs->cleanDirectory($storage_path);

                Excel::create($file_name, function($excel) use($faculty) {

                    $tests = $faculty->getTestsData();
                    $excel->setTitle('Тесты с факультета '.$faculty->name)
                        ->setCreator(URL::to('/'))
                        ->setCompany('Geberit Academy')
                        ->setDescription('Прохождение тестов пользователями факультета "'.$faculty->name.'" с сайта '.URL::to('/'));

                    foreach ($tests as &$test) {
                        $sheet_name = preg_replace('/[\/\:\\\?\*\[\]]/', '_', $test['name']);
                        $sheet_name = mb_substr($sheet_name, 0, 31);
                        $excel->sheet($sheet_name, function($sheet) use(&$test) {
                            $sheet->fromArray($test['answers']);
                            $sheet->setAutoSize(true);
                        });
                    }

                })->store($file_extension, $storage_path);
            }

            return response()->download($file_path, $clean_file_name);

        } else {
            return abort(404);
        }
    }

    public function showAddTestForm($faculty)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            return view('admin.faculty.test.add', [
                'faculty' => $faculty,
                'levels' => Level::orderBy('number')->get(),
            ]);
        } else {
            return abort(404);
        }
    }

    public function addTest(Request $request, $faculty)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $this->validate($request, [
                'name' => 'string|required',
                'description' => 'string|nullable',
                'points' => 'numeric|min:0|required',
                'question_count' => 'numeric|min:1|required',
                'minimum_correct_answers' => 'numeric|min:0||required',
                'duration' => 'numeric|min:1|required',
                'video_identifier' => 'string|required',
                'start_date' => 'date|required|after_or_equal:today',
                'end_date' => 'date|required|after:start_date',
                'bonus_points' => 'integer|min:0',
                'bonus_count' => 'integer|min:0'
            ]);
            $test = Test::create([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'faculty' => $faculty->slug,
                'level' => $request->input('level'),
                'points' => $request->input('points'),
                'question_count' => $request->input('question_count'),
                'minimum_correct_answers' => $request->input('minimum_correct_answers'),
                'duration' => $request->input('duration'),
                'video_identifier' => $request->input('video_identifier'),
                'start_date' => Carbon::createFromFormat('Y-m-d H:i:s', $request->input('start_date')),
                'end_date' => Carbon::createFromFormat('Y-m-d H:i:s', $request->input('end_date')),
            ]);
            if ($request->has('bonus') && $request->input('bonus') == 'on') {
                $test->updateBonus($request->input('bonus_points'), $request->input('bonus_count'));
            }
            return redirect()->route('admin.faculties.faculty.test.all', ['faculty' => $faculty->slug]);
        } else {
            return abort(404);
        }
    }

    public function showEditForm($faculty, $test_id)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $test = Test::findOrFail($test_id);
            return view('admin.faculty.test.edit', [
                'faculty' => $faculty,
                'test' => $test,
                'levels' => Level::orderBy('number')->get(),
            ]);
        } else {
            return abort(404);
        }
    }

    public function edit(Request $request, $faculty, $test_id)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $test = Test::findOrFail($test_id);
            $this->validate($request, [
                'name' => 'string',
                'description' => 'string|nullable',
                'points' => 'numeric|min:0|required',
                'question_count' => 'numeric|min:1|max:'.$test->questions->count().'|required',
                'minimum_correct_answers' => 'numeric|min:0|max:'.$test->questions->count().'|required',
                'duration' => 'numeric|min:1|required',
                'video_identifier' => 'string|nullable',
                'start_date' => 'date|required',
                'end_date' => 'date|required|after:start_date',
                'bonus_points' => 'integer|min:0',
                'bonus_count' => 'integer|min:0'
            ]);
            $test->fill([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'faculty' => $faculty->slug,
                'level' => $request->input('level'),
                'points' => $request->input('points'),
                'question_count' => $request->input('question_count'),
                'minimum_correct_answers' => $request->input('minimum_correct_answers'),
                'duration' => $request->input('duration'),
                'video_identifier' => $request->input('video_identifier'),
                'start_date' => Carbon::createFromFormat('Y-m-d H:i:s', $request->input('start_date')),
                'end_date' => Carbon::createFromFormat('Y-m-d H:i:s', $request->input('end_date')),
            ]);
            $test->save();
            if ($request->has('bonus') && $request->input('bonus') == 'on') {
                $test->updateBonus($request->input('bonus_points'), $request->input('bonus_count'));
            } else {
                $test->removeBonus();
            }
            $test->updateUsersRatings();
            return redirect()->route('admin.faculties.faculty.test.all', ['faculty' => $faculty->slug]);
        } else {
            return abort(404);
        }
    }

    public function showAllQuestions($faculty, $test_id)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $test = Test::findOrFail($test_id);
            $questions = Question::where('test_id', $test_id)->paginate(10);
            if ($test) {
                return view('admin.faculty.test.all_questions', [
                    'faculty' => $faculty,
                    'test' => $test,
                    'questions' => $questions,
                ]);
            }
        }
        return abort(404);
    }

    public function showAddQuestionForm($faculty, $test_id)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $test = Test::findOrFail($test_id);
            return view('admin.faculty.test.edit_question_form', [
                'faculty' => $faculty,
                'test' => $test,
            ]);
        }
        return abort(404);
    }

    public function addQuestion(Request $request, $faculty, $test_id)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $test = Test::findOrFail($test_id);
            $this->validate($request, [
                'name' => 'string|required',
            ]);
            $question = Question::create([
                'name' => $request->input('name'),
                'test_id' => $test->id,
            ]);
            foreach ($request->input('new_answers') as $new_answer) {
                if (isset($new_answer['name']) && !empty($new_answer['name'])) {
                    Answer::create([
                        'name' => $new_answer['name'],
                        'is_correct' => (isset($new_answer['is_correct']) && $new_answer['is_correct'] == 'on') ? true : false,
                        'question_id' => $question->id,
                    ]);
                }
            }
            return redirect()->route('admin.faculties.faculty.test.question.all', ['faculty' => $faculty->slug, 'test_id' => $test->id]);
        }
        return abort(404);
    }

    public function showEditQuestionForm($faculty, $test_id, $question_id)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $test = Test::findOrFail($test_id);
            $question = Question::where('id', $question_id)->with('answers')->first();
            if ($question) {
                return view('admin.faculty.test.edit_question_form', [
                    'faculty' => $faculty,
                    'test' => $test,
                    'question' => $question,
                ]);
            }
        }
        return abort(404);
    }

    public function editQuestion(Request $request, $faculty, $test_id, $question_id)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $test = Test::findOrFail($test_id);
            $question = Question::findOrFail($question_id);
            $this->validate($request, [
                'name' => 'string|required',
            ]);
            $question->name = $request->input('name');
            $question->save();
            if ($request->has('answers')) {
                foreach ($request->input('answers') as $key =>  $answer) {
                    if (!empty($answer['name'])) {
                        $current_answer = Answer::find($key);
                        $current_answer->name = $answer['name'];
                        $current_answer->is_correct = (isset($answer['is_correct']) && $answer['is_correct'] == 'on') ? true : false;
                        $current_answer->save();
                    }
                }
            }
            if ($request->has('new_answers')) {
                foreach ($request->input('new_answers') as $new_answer) {
                    if (isset($new_answer['name']) && !empty($new_answer['name'])) {
                        Answer::create([
                            'name' => $new_answer['name'],
                            'is_correct' => (isset($new_answer['is_correct']) && $new_answer['is_correct'] == 'on') ? true : false,
                            'question_id' => $question->id,
                        ]);
                    }
                }
            }
            return redirect()->route('admin.faculties.faculty.test.question.all', ['faculty' => $faculty->slug, 'test_id' => $test->id]);
        }
        return abort(404);
    }
}
