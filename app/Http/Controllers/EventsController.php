<?php

namespace App\Http\Controllers;

use App\Faculty;
use App\Event;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    public function showCabinetEvents(Request $request)
    {
        $user = $request->user();
        $faculty = $user->faculty()->first();
        $events = $faculty->events()->orderBy('date');

        return response()->view('cabinet.events.list', [
            'faculty' => $faculty,
            'events' => [
                'actual' => $events->actual()->get(),
                'past' => $events->past()->get(),
            ],
            'user' => $request->user(),
        ]);
    }

    public function showEvent(Request $request, $faculty, $event_id)
    {
        $faculty = Faculty::findOrFail($faculty);

        $event = $faculty->events()->findOrFail($event_id);
        return view('faculty.events.single', [
            'faculty' => $faculty,
            'event' => $event,
            'user' => $request->user(),
        ]);
    }

    public function register(Request $request, $faculty, $event_id)
    {
        $faculty = Faculty::findOrFail($faculty);

        $event = $faculty->events()->findOrFail($event_id);

        if (!$request->user()
            || $request->user()->level()->number < 2
            || !$event->is_registration_available
            || $request->user()->faculty !== $faculty->slug
        ) {
            return abort(403);
        }

        $event->users()->attach($request->user()->id);

        return response()->json([
            'success' => true,
        ], 200);
    }
}
