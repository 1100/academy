<?php

if (!function_exists('classActivePath')) {
    function classActivePath($path, $allow_subpaths = false, $class = ' active')
    {
        $segments = request()->segments();
        if($path == '/' && empty($segments)) {
            return $class;
        }
        $path = explode('.', $path);
        if ($allow_subpaths) {
            $segments =  array_slice($segments,0, count($path));
        }
        if($path === $segments) {
            return $class;
        }
        return '';
    }
}