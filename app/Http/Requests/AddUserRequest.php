<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Faculty;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $faculty = Faculty::findOrFail($this->input('faculty'));
        $fields = $faculty->requiredInformationFields()->where('slug', '!=', 'facebook')->get();
        $rules = [];
        $user = User::find($this->input('id'));
        foreach ($fields as $field) {
            $rules[$field->slug] = [
                'required',
                $field->information_type->validator,
            ];
            if ($field->login) {
                if ($user) {
                    $rules[$field->slug][] = 'uniqueLogin:' . $user->getInformation($field->slug);
                } else {
                    $rules[$field->slug][] = 'uniqueLogin';
                    $rules[$field->slug][] = 'uniqueLoginReset';
                }
            }
        }
        return $rules;
    }
}
