<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Faculty extends Model
{
    protected $primaryKey = 'slug';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'slug',
        'name',
        'description',
        'color',
        'open_rating',
        'index',
        'events_enabled',
        'tests_enabled',
        'countdown_enabled',
        'countdown_date',
    ];

    public function users()
    {
        return $this->hasMany('App\User', 'faculty');
    }

    public function approvedUsers()
    {
        return $this->hasMany('App\User', 'faculty')->where('status', 'approved');
    }

    public function admin()
    {
        return $this->belongsToMany('App\Admin', 'admin_faculty', 'faculty', 'admin_id');
    }

    public function information()
    {
        return $this->belongsToMany('App\InformationField', 'faculty_information_field', 'faculty', 'information_field')->withPivot(['required', 'display', 'rating_display'])->orderBy('position');
    }

    public function tests()
    {
        return $this->hasMany('App\Test', 'faculty', 'slug');
    }

    public function events()
    {
        return $this->hasMany('App\Event', 'faculty', 'slug');
    }

    public function additionalInformationFields()
    {
        return $this->information->where('login', false);
    }

    public function additionalVisibleInformationFields()
    {
        return $this->information()->where([
            ['login', false],
            ['hidden', false]
        ])->get();
    }

    public function informationFieldRequired($field)
    {
        $faculty_field = $this->information->find($field->slug);
        if ($faculty_field) {
            return $faculty_field->pivot->required;
        } else {
            return false;
        }
    }

    public function informationFieldDisplay($field)
    {
        $faculty_field = $this->information->find($field->slug);
        if ($faculty_field) {
            return $faculty_field->pivot->display;
        } else {
            return false;
        }
    }

    public function informationFieldRatingDisplay($field)
    {
        $faculty_field = $this->information->find($field->slug);
        if ($faculty_field) {
            return $faculty_field->pivot->rating_display;
        } else {
            return false;
        }
    }

    public function availableAdditionalInformationFields()
    {
        $all_additional_fields = InformationField::additionalFields();
        $additional_fields = $this->additionalInformationFields();
        return $all_additional_fields->diff($additional_fields);
    }

    public function requiredInformationFields()
    {
        return $this->belongsToMany('App\InformationField', 'faculty_information_field', 'faculty', 'information_field')->wherePivot('required', true)->orderBy('position');
    }

    public function loginInformationFields()
    {
        return $this->information()->where('login', true);
    }

    public function displayInformationFields()
    {
        return $this->belongsToMany('App\InformationField', 'faculty_information_field', 'faculty', 'information_field')->wherePivot('display', true)->orderBy('position')->get();
    }

    public function displayRatingInformationFields()
    {
        return $this->belongsToMany('App\InformationField', 'faculty_information_field', 'faculty', 'information_field')->wherePivot('rating_display', true)->orderBy('position')->get();
    }

    public function addUser(array $data, $invited_key = null)
    {
        $fields = $this->information()->where('slug', '!=', 'facebook')->get();
        $user = User::create([
            'password' => (isset($data['password'])) ? bcrypt($data['password']) : null,
            'faculty' => $this->slug,
        ]);
        if ( $this->isEnabledInvite() && $invited_key) {
            $user->invited_key = $invited_key;
            $user->save();
        }
        $user_information = [];
        foreach ($fields as $field) {
            if (array_has($data, $field->slug)) {
                $user_information[$field->slug] = ['value' => $data[$field->slug]];
            }
        }
        $user->information()->attach($user_information);
        return $user;
    }

    public function invite()
    {
        return $this->hasOne('App\Invite', 'faculty', 'slug');
    }

    public function isEnabledInvite()
    {
        return !empty($this->invite);
    }

    public function customPointables()
    {
        return $this->hasMany('App\CustomPointable', 'faculty', 'slug');
    }

    public function updateAllUsersInvitePoints()
    {
        foreach ($this->users as $user) {
            $user->updateInvitePoints();
        }
    }

    public function updateUsersRating()
    {
        foreach($this->users as $user) {
            $user->updateRating();
        }
    }

    public function getMaxInformationFieldPosition()
    {
        return $this->belongsToMany('App\InformationField', 'faculty_information_field', 'faculty', 'information_field')->withPivot(['required', 'display', 'rating_display'])->max('position');
    }

    public function getUsersData()
    {
        $information_fields = $this->information()->get();
        $users = $this->users()->with('moderation_status')->get();
        $users_data = [];
        $i = 0;
        foreach ($users as $user) {
            $users_data[$i]['Дата регистрации'] = $user->created_at->format('d.m.y H:i:s');
            $users_data[$i]['Статус'] = $user->moderation_status()->first()->name;
            foreach ($information_fields as $field) {
                $user_facebook_id = $user->getInformation($field->slug);
                if ($field->slug == 'facebook' && $user_facebook_id) {
                    $users_data[$i][$field->name] = 'https://fb.com/app_scoped_user_id/'.$user_facebook_id;
                } else {
                    $users_data[$i][$field->name] = $user->getDisplayInformation($field->slug);
                }
            }
            foreach ($this->tests()->get() as $test) {
                $test_point = $test->points()->where('user_id', $user->id)->first();
                $test_session = $test->sessions()->where('user_id', $user->id)->first();
                if ($test_point) {
                    $users_data[$i][$test->name] = $test_point->number();
                    $users_data[$i]['Время начала "' . $test->name .'"'] = $test_session->start_date;
                } else {
                    $users_data[$i][$test->name] = null;
                    $users_data[$i]['Время начала "' . $test->name .'"'] = null;
                }
                if ($test->bonus) {
                     $bonus_point = $test->bonus->points()->where('user_id', $user->id)->first();
                    if ($bonus_point) {
                        $users_data[$i][$test->bonus->name] = $bonus_point->number();
                    } else {
                        $users_data[$i][$test->bonus->name] = null;
                    }
                }
            }
            if ($this->isEnabledInvite()) {
                $invite_points = $this->invite->points()->where('user_id', $user->id)->first();
                if ($invite_points) {
                    $users_data[$i][$this->invite->name] = $invite_points->number();
                } else {
                    $users_data[$i][$this->invite->name] = null;
                }
            }
            foreach ($this->customPointables as $custom_pointable) {
                $points = $custom_pointable->getIsCompleted($user) ? $custom_pointable->points : 0;
                $users_data[$i][$custom_pointable->name] = $points;
            }
            $i++;
        }
        return $users_data;
    }

    public function getTestsData()
    {
        $users_data = [];
        $users = $this->approvedUsers;
        foreach ($users as &$user) {
            $test_sessions = $user->testSessions()->with('questionSessions')->get();
            $answers = [];
            if ($test_sessions->isNotEmpty()) {
                $test_sessions = $test_sessions->toArray();
                foreach ($test_sessions as &$test_session) {
                    foreach ($test_session['question_sessions'] as $question_session) {
                        $answers_sessions = DB::table('answers_sessions')
                            ->where('question_session_id', $question_session['id'])
                            ->get();
                        if ($answers_sessions->isNotEmpty()) {
                            $answers[$answers_sessions->first()->answer_id] = 1;
                        }
                    }
                }
            }
            $user_name = $user->getDisplayName() . ', ' . $user->getInformation('email');
            $users_data[$user_name] = $answers;
        }


        $answers_data = DB::table('answers')
            ->leftJoin('questions', 'questions.id', '=', 'answers.question_id')
            ->leftJoin('tests', 'tests.id', '=', 'questions.test_id')
            ->select([
                'tests.id as test_id',
                'tests.name as test_name',
                'questions.id as question_id',
                'questions.name as question_name',
                'answers.id as answer_id',
                'answers.name as answer_name',
                'answers.is_correct',
            ])
            ->where('tests.faculty', $this->slug)
            ->get()
            ->toArray();
        $tests = [];
        foreach ($answers_data as &$answer_data) {
            if (!array_key_exists($answer_data->test_id, $tests)) {
                $tests[$answer_data->test_id] = [
                    'name' => $answer_data->test_name,
                    'answers' => [],
                ];
            }
            if (!array_key_exists($answer_data->answer_id, $tests[$answer_data->test_id]['answers'])) {
                $answer = [
                    'Питання' => $answer_data->question_name,
                    'Відповідь' => $answer_data->answer_name,
                    'Вірна' => $answer_data->is_correct ? '1' : '',
                    'Всього відповідей' => '',
                ];

                foreach ($users_data as $user_name => &$user_answers) {
                    if (!array_key_exists($user_name, $answer)) {
                        $answer[$user_name] = '';
                        if (array_key_exists($answer_data->answer_id, $user_answers)) {
                            $answer[$user_name] = $user_answers[$answer_data->answer_id];
                        }
                    }
                }

                $tests[$answer_data->test_id]['answers'][] = $answer;
            }

        }

        return $tests;
    }
}