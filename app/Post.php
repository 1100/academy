<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title', 'content', 'image', 'faculty'
    ];

    public function faculty()
    {
        return $this->belongsTo('App\Faculty', 'faculty', 'slug')->first();
    }
}
