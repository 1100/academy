<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InformationField extends Model
{
    protected $primaryKey = 'slug';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'slug', 'name', 'description', 'type', 'login', 'hidden', 'default_value',
    ];

    public function information_type()
    {
        return $this->belongsTo('App\InformationType', 'type', 'slug');
    }

    public function faculties()
    {
        return $this->belongsToMany('App\Faculty', 'faculty_information_field', 'information_field', 'faculty');
    }

    public function userLoginFields()
    {
        return $this->hasMany('App\UserInformation', 'information_field', 'slug');
    }

    public function scopeLoginFields($query)
    {
        return $query->where('login', '=', true)->get();
    }

    public function scopeAdditionalFields($query)
    {
        return $query->where('login', '!=', true)->get();
    }
}
