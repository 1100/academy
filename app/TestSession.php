<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TestSession extends Model
{
    public $timestamps = false;

    public $table = 'tests_sessions';

    protected $fillable = [
        'test_id', 'user_id', 'start_date', 'end_date', 'points_id',
    ];

    public function test()
    {
        return $this->belongsTo('App\Test');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function points()
    {
        return $this->belongsTo('App\Point', 'points_id', 'id');
    }

    public function questionSessions()
    {
        return $this->hasMany('App\QuestionSession', 'test_session_id', 'id');
    }

    public function generateQuestionSessions()
    {
        $question_count = $this->test()->first()->questions_count();
        $question_ids = $this->test()->first()->questions()->get()->random($question_count)->pluck('id')->all();
        foreach ($question_ids as $question_id) {
            $this->questionSessions()->create([
                'question_id' => $question_id
            ]);
        }
    }

    public function saveAndGetCorrectAnswer($question_id, $answer_id)
    {
        $question_session = $this->questionSessions()->where('question_id', $question_id)->first();
        $answer = Answer::find($answer_id);
        if ($answer->is_correct) {
            $this->points->addPercent((int)round((100 / $this->test->questions_count())));
            $this->user()->first()->proceedToNextLevelIfCan();
        }
        $question_session->answers()->attach($answer);
        return $question_session->question()->first()->answers()->where('is_correct', true)->first()->id;
    }

    public function getQuestionsLeft()
    {
        $question_count = $this->test()->first()->questions_count();
        $answers_count = 0;
        foreach ($this->questionSessions()->get() as $question_session) {
            if($question_session->answers()->get()->isNotEmpty()) {
                $answers_count++;
            }
        }
        return $question_count - $answers_count;
    }

    public function getNextQuestion()
    {
        if ($this->questionSessions()->get()->isEmpty()) {
            $this->generateQuestionSessions();
        }
        foreach ($this->questionSessions()->get() as $question_session) {
            if($question_session->answers()->get()->isEmpty()) {
                return $question_session->question()->first();
            }
        }
        return null;
    }

    public function getSecondsLeft()
    {
        $now = Carbon::now();
        $end = Carbon::parse($this->end_date);
        return $now->diffInSeconds($end, false);
    }

    public function correctAnswersCount()
    {
        $correct_answers_count = 0;
        $question_sessions = $this->questionSessions()->get();
        foreach ($question_sessions as $question_session) {
            if ($question_session->answers()->first() && $question_session->answers()->first()->is_correct) {
                $correct_answers_count++;
            }
        }
        return $correct_answers_count;
    }
}
