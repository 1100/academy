<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'points', 'invites_count', 'faculty',
    ];

    public function points()
    {
        return $this->morphMany('App\Point', 'pointable');
    }

    public function faculty()
    {
        return $this->belongsTo('App\Faculty', 'faculty', 'slug');
    }

    public function getInvitePointPercent($invites_count = null)
    {
        if ($this->invites_count != 0) {
            $percent = 100 / $this->invites_count;
            return ($invites_count !== null ) ? $percent * $invites_count : $percent;
        } else {
            return 0;
        }
    }

    public function updatePoints($user)
    {
        $invited_users = $user->approvedInvitedUsers();
        $invites_count = ($invited_users) ? $invited_users->count() : 0;
        $invites_count = ($invites_count > $this->invites_count) ? $this->invites_count : $invites_count;
        $user_point = $this->points()->where('user_id', $user->id)->first();
        if ($invites_count === 0) {
            if ($user_point) {
                $user_point->delete();
                $user->updateRating();
            }
        } else {
            if (is_null($user_point)) {
                $user_point = Point::create([
                    'user_id' => $user->id,
                    'percent' => 0,
                    'pointable_id' => $this->id,
                    'pointable_type' => get_class($this),
                ]);
            }
            $user_point->setPercent($this->getInvitePointPercent($invites_count));
            $user_point->save();
        }
    }
}
