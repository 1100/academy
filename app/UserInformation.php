<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInformation extends Model
{
    public $timestamps = false;
    protected $table = 'user_information';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function loginField()
    {
        return $this->belongsTo('App\InformationField', 'information_field', 'slug')->where('login', true);
    }
}
