<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'user_id', 'pointable_id', 'pointable_type', 'percent',
    ];

    public function pointable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function number()
    {
        $number = 0;
        if ($this->pointable) {
            $number = (int)round(($this->pointable->points * $this->percent) / 100);
            if ($this->pointable_type === 'App\Test') {
                if ($this->pointable->points > 0) {
                    $points_per_one_answer = $this->pointable->points / $this->pointable->question_count;
                    if ($this->pointable && $this->user_id && $this->pointable->userSession($this->user_id)) {
                        $number = $this->pointable->userSession($this->user_id)->correctAnswersCount() * $points_per_one_answer;
                    } else {
                        $percents_per_one_answer = (int)round((100 / $this->pointable->question_count));
                        $number = ($this->percent / $percents_per_one_answer) * $points_per_one_answer;
                    }
                }
            }
        }
        return $number;
    }

    public function addPercent($percent)
    {
        $this->setPercent($this->percent + $percent);

    }

    public function setPercent($percent)
    {
        $this->percent = $percent;
        if($this->percent > 100) {
            $this->percent = 100;
        }
        $this->save();
        $rating = 0;
        foreach (Point::where('user_id', $this->user->id)->get() as $point) {
            $rating += $point->number();
        }
        $this->user->rating = $rating;
        $this->user->save();
    }

    public function updateUsersRatings()
    {
        $this->user->updateRating();
    }
}
