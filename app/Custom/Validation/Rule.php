<?php

namespace App\Custom\Validation;

use App\InformationField;

class Rule
{
    public function uniqueLogin($attribute, $value, $parameters)
    {
        if(!empty($parameters) && in_array($value, $parameters)) {
            return true;
        }
        $field = InformationField::find($attribute);
        return $field->userLoginFields()->where('value', $value)->get()->isEmpty();
    }
}