<?php

namespace App\Custom;

class Phone
{
    private $raw_number;
    private $formatted_number;

    /**
     * Return Phone if phone number is valid
     *
     * @param  string  $input
     * @return Phone if number is valid and null if not
     */
    public static function getPhoneIfValid($input) {
        if (self::check($input)) {
            $phone = preg_replace('/\D/', '', $input);
            return new Phone($phone);
        } else {
            return null;
        }
    }

    public static function check($input) {
        if (preg_match('/^[0-9\(\)\+\s\-]+$/', $input)) {
            $phone = preg_replace('/\D/', '', $input);
            if (strlen($phone) <= 12 && strlen($phone) >= 9) {
                return true;
            }
        }
        return false;
    }

    public function __construct($phone)
    {
        if (preg_match('/^[0-9]{9,12}$/', $phone)) {
            $prefixes = ['380', '375'];
            foreach ($prefixes as $prefix) {
                $phone = substr($prefix, 0, 12-strlen($phone)) . $phone;
                $this->raw_number = $phone;
            }
            if (in_array(substr($phone, 0, 3), $prefixes)) {
                $phone = preg_replace(
                    '/^(\d{2})(\d{3})(\d{3})(\d{2})(\d{2})$/',
                    '+\1 (\2) \3-\4-\5',
                    $phone
                );
                $this->formatted_number = $phone;
            }
        } else {
            throw new InvalidPhoneException();
        }
    }

    public function rawNumber() //  380971234567
    {
        return $this->raw_number;
    }

    public function formattedNumber() // +38 (097) 123-45-67
    {
        return $this->formatted_number;
    }

}