<?php

namespace App\Custom\Auth\Passwords;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use App\Custom\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class TokenRepository implements TokenRepositoryInterface
{

    /**
     * The database connection instance.
     *
     * @var \Illuminate\Database\ConnectionInterface
     */
    protected $connection;

    /**
     * The Hasher implementation.
     *
     * @var \Illuminate\Contracts\Hashing\Hasher
     */
    protected $hasher;

    /**
     * The token database table.
     *
     * @var string
     */
    protected $table;

    /**
     * The hashing key.
     *
     * @var string
     */
    protected $hashKey;

    /**
     * The number of seconds a token should last.
     *
     * @var int
     */
    protected $expires;

    /**
     * Create a new token repository instance.
     *
     * @param  \Illuminate\Database\ConnectionInterface  $connection
     * @param  \Illuminate\Contracts\Hashing\Hasher  $hasher
     * @param  string  $table
     * @param  string  $hashKey
     * @param  int  $expires
     * @return void
     */
    public function __construct(ConnectionInterface $connection, HasherContract $hasher,
                                $table, $hashKey, $expires = 60)
    {
        $this->table = $table;
        $this->hasher = $hasher;
        $this->hashKey = $hashKey;
        $this->expires = $expires * 60;
        $this->connection = $connection;
    }

    /**
     * Create a new token and code.
     *
     * @param  \App\Custom\Contracts\Auth\CanResetPassword  $user
     * @return array
     */
    public function create(CanResetPasswordContract $user, $login_field)
    {
        $phone = $user->getLoginForPasswordReset($login_field);
        $this->delete($user);
        $secret = $this->generateNewTokenAndCode();
        $this->getTable()->insert($this->getPayload($phone, $secret['token'], $secret['code']));
        return $secret;
    }

    protected function generateNewTokenAndCode()
    {
        return [
            'token' => hash_hmac('sha256', Str::random(40), $this->hashKey),
            'code' => str_pad(rand(0,999999), 6, '0', STR_PAD_LEFT),
        ];
    }

    /**
     * Build the record payload for the table.
     *
     * @param  string  $login
     * @param  string  $token
     * @param  string  $code
     * @return array
     */
    protected function getPayload($login, $token, $code)
    {
        return [
            'login' => $login,
            'token' => $this->hasher->make($token),
            'code' => $code,
            'created_at' => new Carbon(),
        ];
    }

    /**
     * Determine if a token record exists and is valid.
     *
     * @param  \App\Custom\Contracts\Auth\CanResetPassword  $user
     * @param  array  $secrets
     * @return bool
     */
    public function exists(CanResetPasswordContract $user, $secrets)
    {
        $record = (array) $this->getTable()
            ->where(
            'login', $user->getLoginForPasswordReset('email'))
            ->orWhere('login', $user->getLoginForPasswordReset('phone')
            )->first();
        return $record &&
            ! $this->tokenExpired($record['created_at']) &&
            $this->hasher->check($secrets['token'], $record['token']) &&
            (isset($secrets['code']) && !empty($secrets['code']))
                ? $secrets['code'] == $record['code']
                : true;
    }

    /**
     * Determine if the token has expired.
     *
     * @param  string  $createdAt
     * @return bool
     */
    protected function tokenExpired($createdAt)
    {
        return Carbon::parse($createdAt)->addSeconds($this->expires)->isPast();
    }

    /**
     * Delete a token record by user.
     *
     * @param  \App\Custom\Contracts\Auth\CanResetPassword  $user
     * @return void
     */
    public function delete(CanResetPasswordContract $user)
    {
        $email = $user->getLoginForPasswordReset('email');
        $phone = $user->getLoginForPasswordReset('phone');
        $query = $this->getTable();
        if (!is_null($email)) {
            $query->where('login', $email);
            if (!is_null($phone)) {
                $query->orWhere('login', $phone);
            }
        } elseif (!is_null($phone)) {
            $query->where('login', $phone);
        } else {
            return;
        }
        $query->delete();
    }

    /**
     * Delete expired tokens.
     *
     * @return void
     */
    public function deleteExpired()
    {
        $expiredAt = Carbon::now()->subSeconds($this->expires);

        $this->getTable()->where('created_at', '<', $expiredAt)->delete();
    }

    /**
     * Get the database connection instance.
     *
     * @return \Illuminate\Database\ConnectionInterface
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Begin a new database query against the table.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function getTable()
    {
        return $this->connection->table($this->table);
    }

    /**
     * Get the hasher instance.
     *
     * @return \Illuminate\Contracts\Hashing\Hasher
     */
    public function getHasher()
    {
        return $this->hasher;
    }
}
