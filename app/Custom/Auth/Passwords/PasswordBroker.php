<?php

namespace App\Custom\Auth\Passwords;

use App\Custom\Contracts\Auth\CanResetPassword;
use Closure;
use Illuminate\Support\Arr;
use UnexpectedValueException;
use App\Custom\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Custom\Contracts\Auth\PasswordBroker as PasswordBrokerContract;
use App\Custom\Auth\UserProvider;

class PasswordBroker implements PasswordBrokerContract
{

    /**
     * The password token repository.
     *
     * @var \App\Custom\Auth\Passwords\TokenRepositoryInterface
     */
    protected $tokens;

    /**
     * The user provider implementation.
     *
     * @var \App\Custom\Auth\UserProvider
     */
    protected $users;

    /**
     * The custom password validator callback.
     *
     * @var \Closure
     */
    protected $passwordValidator;

    protected $currentToken;
    protected $currentCode;
    protected $currentUser;

    /**
     * Create a new password broker instance.
     *
     * @param  \App\Custom\Auth\Passwords\TokenRepositoryInterface  $tokens
     * @param  \Illuminate\Contracts\Auth\UserProvider  $users
     * @return void
     */
    public function __construct(TokenRepositoryInterface $tokens,
                                UserProvider $users)
    {
        $this->users = $users;
        $this->tokens = $tokens;
    }

    /**
     * Send a password reset link to a user.
     *
     * @param  array  $credentials
     * @return string
     */
    public function sendResetLink(array $credentials)
    {
        $this->currentUser = $this->getUser($credentials);
        if (is_null($this->currentUser)) {
            return static::INVALID_USER;
        }
        $this->currentUser->sendPasswordResetEmailNotification(
            $this->tokens->create($this->currentUser, 'email')['token']
        );
        return static::RESET_LINK_SENT;
    }

    /**
     * Send a password reset link to a user.
     *
     * @param  array  $credentials
     * @return string
     */
    public function sendResetSMS(array $credentials)
    {
        $this->currentUser = $this->getUser($credentials);
        if (is_null($this->currentUser)) {
            return static::INVALID_USER;
        }
        $secret = $this->tokens->create($this->currentUser, 'phone');
        $this->currentToken = $secret['token'];
        $this->currentCode = $secret['code'];
        $this->currentUser->sendPasswordResetSMSNotification($secret['code']);

        return static::RESET_SMS_SENT;
    }

    /**
     * Reset the password for the given token.
     *
     * @param  array  $credentials
     * @param  \Closure  $callback
     * @param  string  $login_field
     * @return mixed
     */
    public function reset(array $credentials, Closure $callback)
    {
        $user = $this->validateReset($credentials);
        if (! $user instanceof CanResetPasswordContract) {
            return $user;
        }
        $callback($user, $credentials['password']);
        $this->tokens->delete($user);
        return static::PASSWORD_RESET;
    }

    /**
     * Validate a password reset for the given credentials.
     *
     * @param  array  $credentials
     * @return \App\Custom\Contracts\Auth\CanResetPassword|string
     */
    protected function validateReset(array $credentials)
    {
        if (is_null($user = $this->getUser($credentials))) {
            return static::INVALID_USER;
        }
        if (! $this->validateNewPassword($credentials)) {
            return static::INVALID_PASSWORD;
        }
        $rules = [];
        $rules['token'] = $credentials['token'];
        if (array_has($credentials, 'phone')) {
            $rules['code'] = $credentials['code'];
        }
        if (! $this->tokens->exists($user, $rules)
        ) {
            return static::INVALID_TOKEN;
        }
        return $user;
    }

    /**
     * Set a custom password validator.
     *
     * @param  \Closure  $callback
     * @return void
     */
    public function validator(Closure $callback)
    {
        $this->passwordValidator = $callback;
    }

    /**
     * Determine if the passwords match for the request.
     *
     * @param  array  $credentials
     * @return bool
     */
    public function validateNewPassword(array $credentials)
    {
        if (isset($this->passwordValidator)) {
            list($password, $confirm) = [
                $credentials['password'],
                $credentials['password_confirmation'],
            ];

            return call_user_func(
                    $this->passwordValidator, $credentials
                ) && $password === $confirm;
        }

        return $this->validatePasswordWithDefaults($credentials);
    }

    /**
     * Determine if the passwords are valid for the request.
     *
     * @param  array  $credentials
     * @return bool
     */
    protected function validatePasswordWithDefaults(array $credentials)
    {
        list($password, $confirm) = [
            $credentials['password'],
            $credentials['password_confirmation'],
        ];

        return $password === $confirm && mb_strlen($password) >= 6;
    }

    /**
     * Get the user for the given credentials.
     *
     * @param  array  $credentials
     * @return \App\Custom\Contracts\Auth\CanResetPassword
     *
     * @throws \UnexpectedValueException
     */
    public function getUser(array $credentials)
    {
        $credentials = Arr::except($credentials, ['token', 'code']);
        $user = $this->users->retrieveByCredentials($credentials);
        if ($user && ! $user instanceof CanResetPasswordContract) {
            throw new UnexpectedValueException('User must implement CanResetPassword interface.');
        }
        return $user;
    }

    /**
     * Create a new password reset token for the given user.
     *
     * @param  \App\Custom\Contracts\Auth\CanResetPassword $user
     * @param string $login_field
     * @return string
     */
    public function createToken(CanResetPasswordContract $user, $login_field)
    {
        return $this->tokens->create($user, $login_field);
    }

    /**
     * Delete password reset tokens of the given user.
     *
     * @param  \App\Custom\Contracts\Auth\CanResetPassword $user
     * @return void
     */
    public function deleteToken(CanResetPasswordContract $user)
    {
        $this->tokens->delete($user);
    }

    /**
     * Validate the given password reset token.
     *
     * @param  \App\Custom\Contracts\Auth\CanResetPassword $user
     * @param  array $secret
     * @return bool
     */
    public function tokenExists(CanResetPasswordContract $user, $secret)
    {
        return $this->tokens->exists($user, $secret);
    }

    /**
     * Get the password reset token repository implementation.
     *
     * @return \App\Custom\Auth\Passwords\TokenRepositoryInterface
     */
    public function getRepository()
    {
        return $this->tokens;
    }

    public function getCurrentToken()
    {
        return $this->currentToken;
    }

    public function getCurrentCode()
    {
        return $this->currentCode;
    }

    public function getCurrentUser()
    {
        return $this->currentUser;
    }

}
