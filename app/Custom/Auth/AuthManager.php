<?php

namespace App\Custom\Auth;

use Illuminate\Auth\AuthManager as IlluminateAuthManager;
use InvalidArgumentException;

class AuthManager extends IlluminateAuthManager
{

    /**
     * Create the user provider implementation for the driver.
     *
     * @param  string  $provider
     * @return \Illuminate\Contracts\Auth\UserProvider
     *
     * @throws \InvalidArgumentException
     */
    public function createUserProvider($provider)
    {
        $config = $this->app['config']['auth.providers.'.$provider];

        if (isset($this->customProviderCreators[$config['driver']])) {
            return call_user_func(
                $this->customProviderCreators[$config['driver']], $this->app, $config
            );
        }

        switch ($config['driver']) {
            case 'database':
                return $this->createDatabaseProvider($config);
            case 'eloquent':
                return $this->createEloquentProvider($config);
            case 'academy_eloquent':
                return $this->createAcademyProvider($config);
            default:
                throw new InvalidArgumentException("Authentication user provider [{$config['driver']}] is not defined.");
        }
    }

    /**
     * Create an instance of the Academy user provider.
     *
     * @param  array  $config
     * @return UserProvider
     */
    protected function createAcademyProvider($config)
    {
        return new UserProvider($this->app['hash'], $config['model']);
    }
}