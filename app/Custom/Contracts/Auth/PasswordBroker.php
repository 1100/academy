<?php

namespace App\Custom\Contracts\Auth;

use Closure;
use Illuminate\Contracts\Auth\PasswordBroker as IlluminatePasswordBroker;

interface PasswordBroker extends IlluminatePasswordBroker
{
    /**
     * Constant representing a successfully sent sms with code.
     *
     * @var string
     */
    const RESET_SMS_SENT = 'passwords.sentSMS';

    /**
     * Send a password reset SMS to a user.
     *
     * @param  array  $credentials
     * @return string
     */
    public function sendResetSMS(array $credentials);

    /*
     * Return current token
     *
     * @return string
     */
    public function getCurrentToken();

    /*
     * Return current sms code
     *
     * @return string
     */
    public function getCurrentCode();

    /*
     * Return current user
     *
     * @return User
     */
    public function getCurrentUser();

}
