<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestBonus extends Model
{
    public $timestamps = false;
    protected $table = 'tests_bonus';

    protected $fillable = [
        'name', 'test_id', 'points', 'bonus_count'
    ];

    public function points()
    {
        return $this->morphMany('App\Point', 'pointable');
    }

    public function test()
    {
        return $this->belongsTo('App\Test', 'test_id', 'id');
    }

    public function addBonusPointsIfCan($user_id)
    {
        if($this->points()->count() < $this->bonus_count) {
            $point = new Point([
                'user_id' => $user_id,
                'pointable_id' => $this->id,
                'pointable_type' => get_class($this),
            ]);
            $point->addPercent(100);
        }
    }

    public function updateUsersRating()
    {
        if ($this->points()->count() === 0) {
            $this->createPoints();
        } else {
            $this->updateExistsPoints();
        }
    }

    private function updateExistsPoints() {
        $bonus_points = $this->points()->get();
        foreach ($bonus_points as $bonus_point) {
            $bonus_point->updateUsersRatings();
        }
    }

    private function createPoints() {
        $sessions = $this->test->sessions()
            ->orderBy('start_date', 'asc')
            ->take($this->bonus_count)
            ->get();
        foreach ($sessions as $session) {
            $this->addBonusPointsIfCan($session->user->id);
        }
    }
}
