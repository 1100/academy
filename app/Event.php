<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Event extends Model
{
    protected $fillable = [
        'faculty',
        'title',
        'image',
        'short_description',
        'description',
        'location',
        'date',
        'registration_start_date',
        'registration_end_date',
    ];
    protected $appends = ['is_registration_available'];
    protected $dates = [
        'created_at',
        'updated_at',
        'date',
        'registration_start_date',
        'registration_end_date',
    ];

    public function faculty()
    {
        return $this->belongsTo('App\Faculty', 'faculty', 'slug')->first();
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'users_events');
    }

    public function getIsRegistrationAvailableAttribute()
    {
        $now = Carbon::now();
        $start = Carbon::parse($this->registration_start_date);
        $end = Carbon::parse($this->registration_end_date);
        return $now->gte($start) && $now->lte($end);
    }

    public function scopeActual($query)
    {
        return $query->where('date', '>=', Carbon::now());
    }

    public function scopePast($query)
    {
        return $query->where('date', '<', Carbon::now());
    }

    public function getUsersData()
    {
        $information_fields = $this->faculty()->information()->get();
        $users = $this->users()->get();
        $users_data = [];
        $i = 0;
        foreach ($users as $user) {
            foreach ($information_fields as $field) {
                $user_facebook_id = $user->getInformation($field->slug);
                if ($field->slug === 'facebook' && $user_facebook_id) {
                    $users_data[$i][$field->name] = 'https://fb.com/app_scoped_user_id/'.$user_facebook_id;
                } else {
                    $users_data[$i][$field->name] = $user->getDisplayInformation($field->slug);
                }
            }
            $i++;
        }
        return $users_data;
    }

}
