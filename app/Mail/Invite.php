<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Invite extends Mailable
{
    use Queueable, SerializesModels;

    public $faculty;
    public $invite_key;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($faculty, $invite_key)
    {
        $this->faculty = $faculty;
        $this->invite_key = $invite_key;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'), 'Geberit Academy')
                    ->subject('Приглашаем Вас в Академию сантехнических наук Geberit!')
                    ->view('emails.invite');
    }
}
