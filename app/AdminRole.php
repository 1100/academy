<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminRole extends Model
{
    protected $primaryKey = 'slug';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'slug',
        'name',
    ];

    protected $hidden = [];

    public function permissions()
    {
        return $this->belongsToMany('App\AdminPermission', 'admin_role_admin_permission', 'admin_role', 'admin_permission');
    }
}
