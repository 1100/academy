<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $primaryKey = 'slug';
    public $timestamps = false;
    public $incrementing = false;

    protected $fillable = [
        'slug',
        'name',
        'number',
    ];

    public function user()
    {
        return $this->hasMany('App\User', 'level', 'slug');
    }

    public function tests()
    {
        return $this->hasMany('App\Test', 'level', 'slug');
    }

    public function lowerLevel()
    {
        return self::where('number', '<', $this->number)->orderBy('number', 'desc')->limit(1)->get()->first();
    }

    public function higherLevel()
    {
        return self::where('number', '>', $this->number)->orderBy('number', 'asc')->limit(1)->get()->first();
    }
}
